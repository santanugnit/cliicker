<?php
class Photographer extends CI_Controller
{
    var $data;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        //populate viewfor header / footer elements
        $this->load->library('common_functions');
        $this->load->library('public_init_elements');
        $this->public_init_elements->init_elements();

        $this->is_login     = $this->public_init_elements->is_photographer_logged_in();
        $this->curr_ctrl    = $this->public_init_elements->get_current_controller_name();
        $this->curr_func    = $this->public_init_elements->get_current_function_name();

        $this->apikey       = $this->config->item('encryption_key');
        $this->DS           = DIRECTORY_SEPARATOR;
    }

    //##################################################//
    //########### Calling default function #############//

    function photographer_serach()
    {
        $data = array();
        //echo "Santanu -- ". $this->curr_ctrl; 

        $data["ctrl_name"]  = $this->curr_ctrl . $this->DS;
        $data["func_name"]  = $this->curr_func . $this->DS;

        
        $postData           = array();
        $postData["apitoken"]           = $this->apikey;

        $rs_arr     = call_restapi('photographer/get_photographer_lists', "post", $postData);

        //echo "<pre>"; print_r($rs_arr); echo "</pre>"; echo "Hii"; exit;

        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data['pg_lists']    = (array)$rs_arr["rs_row"];
           
        }
        //echo "<pre>"; print_r($data['pg_lists'] ); echo "</pre>"; exit;


        $this->data['cdnJS']    = array('https://unpkg.com/sweetalert/dist/sweetalert.min.js',);
        $this->data['cdnCSS']   = array();

        $this->data['pageJS']   = array('js/jquery-ui.js', 'js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js', 'js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js');
        $this->data['pageCSS']  = array('css/jquery-ui.css', 'js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css');

        $this->data['header']   = $this->load->view('elements/header-inner', $data, true);
        $this->data['banner']   = $this->load->view('elements/banner/banner-pg-serach', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/photographer_serach', $data, true);
    	$this->load->view('layout_home', $this->data);
    }



    function ajax_photographer_filter()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $pg_name            = $this->input->get('pg_name');
        $pg_budget          = $this->input->get('pg_budget');
        $ratings            = $this->input->get('ratings');
        $pg_loc['address']  = $this->input->get('pg_location');
        
        if( isset($pg_loc['address']) && trim($pg_loc['address']) != '' ) {
            $get_src_latlong    = get_latlong_from_address( $pg_loc );
        }  

        //$searchStr = $this->input->get(NULL, TRUE);
        //echo "<pre>"; print_r($get_src_latlong); echo "</pre>"; exit;
               
        $postData                   = array();
        $postData["pg_name"]        = $pg_name;
        $postData["pg_budget"]      = $pg_budget;
        $postData["ratings"]        = $ratings;
        $postData["pg_location"]    = $pg_loc['address'];
        $postData["pg_lat"]         = ( isset($get_src_latlong['lat']))? $get_src_latlong['lat'] :"";
        $postData["pg_long"]        = ( isset($get_src_latlong['long']) )? $get_src_latlong['long'] :"";
        $postData["apitoken"]       = $this->apikey;

        $rs_arr     = call_restapi('photographer/api_photographer_filter', "post", $postData);
        //echo "<pre>"; print_r($rs_arr); echo "</pre>"; exit;
        
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {            
            $data["pg_lists"]   = (array)$rs_arr["rs_row"];
            $return["result"]   = $this->load->view('/maincontents/photographer_serach_ajax', $data, true);           
            $return['status']   = true;
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $return             = (array) $rs_arr["rs_row"];
            $return['result']   = '';
            echo json_encode($return);
            exit(0);
        }        
    }


    function profile()
    {
        $data = array();
        $data["ctrl_name"]  = $this->curr_ctrl . $this->DS;
        $data["func_name"]  = $this->curr_func . $this->DS;

        $sess_arr           = $this->session->userdata();
        
        $postData               = array();
        $postData["apitoken"]   = $this->apikey;
        $postData["pgID"]           = ( isset($sess_arr["pgID"]) && (int)$sess_arr["pgID"] > 0 )? (int)$sess_arr["pgID"] : NULL;
        $postData["search_pgID"]    = $this->uri->segment(3);

        if( (int)$postData["pgID"] < 1 && (int)$postData["search_pgID"] < 1 ) {
            redirect('home');
        }

        $data["is_pg_login"]    = $postData["is_pg_login"]    = $this->is_login;

        // Call to rest server 
        $rs_arr     = call_restapi('photographer/get_pg_details', "post", $postData);

        /*$src  = $rs_arr["rs_row"]->img_url . $rs_arr["rs_row"]->featured_image;
        $con    = file_get_contents($src); $en     = base64_encode($con); $mime   = $size['mime'];
        $binary_data    = 'data:' . $mime . ';base64,' . $en ;*/

        if ( $rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            if( empty($rs_arr["rs_row"]) && empty($rs_arr["search_pg"]) ) {
                redirect("/photographer/photographer_serach");
            }
            $data['pg_lists']       = (array)$rs_arr["rs_row"];
            $data['pg_nofify']      = (array)$rs_arr["pg_notify"]; 
            if( !empty($rs_arr["search_pg"]) ) {
                $data['search_pg']      = (array)$rs_arr["search_pg"];           
            } else {
                $data['search_pg']      = (array)$rs_arr["rs_row"];
            }
        }
        echo "<pre>"; print_r($data);   exit();
       
        /* If and only if a photographer is login the left menu appear */

        $this->data['cdnCSS']   = array();
        $this->data['cdnJS']    = array('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

        if( $this->is_login ) {
            $data['left_menu']          = $this->load->view('elements/sidebar/pg-left-menu', $data, true);
        }
        $data['my_pg_photos']           = $this->load->view('elements/sidebar/pg-photos', $data, true);
        $data['pg_follow_following']    = $this->load->view('elements/sidebar/pg-follower-following', $data, true);
        $data['client_reviews']         = $this->load->view('elements/sidebar/client-reviews', $data, true);

        $this->data['leftsidebar']  = $this->load->view('elements/left-sidebar-temp', $data, true);
        $this->data['header']       = $this->load->view('elements/header-inner', $data, true);
        $this->data['banner']       = $this->load->view('elements/banner/banner-pg-profile', $data, true);
        $this->data['footer']       = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/photographer_home_view', $data, true);
        $this->load->view('layout_inner_page', $this->data);
    }

    function LoadIMG($img_name = NULL)
    {
        header('Content-Type: image/*');

        $img = LoadJpeg("https://upload.wikimedia.org/wikipedia/commons/2/23/".base64_decode($img_name));
        
        imagejpeg($img);
        imagedestroy($img);
    }


    function ajax_pg_follow_status_changed()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $sess_arr           = $this->session->userdata();
        if ( !isset($sess_arr["pgID"]) || (int)$sess_arr["pgID"] < 1 )
        {
            $return['result']   = 'User sesion expire. Please login again.';
            echo json_encode($return);
            exit(0);
        }
        $postData                   = array();
        $postData["pgID"]           = ( isset($sess_arr["pgID"]) && (int)$sess_arr["pgID"] > 0 )? (int)$sess_arr["pgID"] : NULL;
        $postData["curr_status"]    = $this->input->post('curr_status');
        $postData["followed_id"]    = $this->input->post('followed_id');
        $postData["apitoken"]       = $this->apikey;

        if ( (int)$postData["pgID"] == (int)$postData["followed_id"] ) {
            $return['result']       = 'User sesion id and followed_id same.';
            echo json_encode($return);
            exit(0);
        }

        $rs_arr     = call_restapi('photographer/api_ajax_pg_follow_status_changed', "post", $postData);

        /*echo "<pre>"; print_r($rs_arr); exit();*/
        if ( $rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {          
            $return_arr         = (array)$rs_arr["rs_row"];
            $return["result"]   = $return_arr["result"];     
            $return['status']   = true;        
            $return["curr_follow_status"]   = $return_arr["curr_follow_status"];
            echo json_encode($return);
            exit(0);
        }
        else if ( $rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'failed' )
        {
            $return_arr         = (array) $rs_arr["rs_row"];
            $return["result"]   = $return_arr["result"];        
            $return['status']   = false;
            $return["curr_follow_status"]   = $return_arr["curr_follow_status"];
            echo json_encode($return);
            exit(0);
        } else {

            $return["result"]   = "There some error occure.";        
            $return['status']   = false;
            $return["curr_follow_status"]   = $postData["curr_status"];
            echo json_encode($return);
            exit(0);
        }
    }

    



}


?>