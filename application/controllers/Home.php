<?php
class Home extends CI_Controller
{
    var $data;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        //populate viewfor header / footer elements
        $this->load->library('common_functions');
        $this->load->library('public_init_elements');
        $this->public_init_elements->init_elements();

        $this->curr_ctrl    = $this->public_init_elements->get_current_controller_name();
        $this->curr_func    = $this->public_init_elements->get_current_function_name();

        $this->apikey       = $this->config->item('encryption_key');
        $this->DS          = DIRECTORY_SEPARATOR;
    }

    //##################################################//
    //########### Calling default function #############//

    function index()
    {
        $data = array();
        //echo "Santanu -- ". $this->curr_ctrl; 

        $data["ctrl_name"] = $this->curr_ctrl . $this->DS;
        $data["func_name"] = $this->curr_func . $this->DS;

        

        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/home_content', $data, true);
    	$this->load->view('layout_home', $this->data);
    }


    function ajax_photographer_send_invitation()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $visitor            = $this->input->post('visitor');
        $pg_email           = $this->input->post('pg_email');

        $this->form_validation->set_error_delimiters('', '');

        $this->form_validation->set_rules('visitor', 'email/name/phone no.', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('pg_email', 'photographer email', 'trim|required|valid_email');
       

        if ( $this->form_validation->run() === FALSE )
        {
            $return['result']   = validation_errors();
            echo json_encode($return);
            exit(0);
        }


        $postData   = array();
        $postData["visitor"]            = $visitor;
        $postData["pg_email"]           = $pg_email;
        $postData["apitoken"]           = $this->apikey;

        $rs_arr     = call_restapi('photographer/ajax_photographer_send_invitation', "post", $postData);

        /*echo json_encode($rs_arr);
        exit(0);*/
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $return_arr     = (array)$rs_arr["rs_row"];
            $return['result'] = $return_arr["result"];
            $return['status'] = true;
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $return_arr         = (array) $rs_arr["rs_row"];
            $return['result']   = $return_arr["result"];
            echo json_encode($return);
            exit(0);
        }
    }

}


?>