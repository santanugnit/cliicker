<?php
class Auth extends CI_Controller
{
    var $data;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        //populate viewfor header / footer elements
        $this->load->library('common_functions');
        $this->load->library('public_init_elements');

        $already_not_login_escape_func  = array('logout');
        $already_login_escape_func      = array('signup', 'login');
        $res = $this->public_init_elements->is_photographer_logged_in();
        //var_dump($res); exit;
        $curr_ctrl = $this->public_init_elements->get_current_controller_name();
        $curr_func = $this->public_init_elements->get_current_function_name();

        if ( $res && in_array($curr_func, $already_login_escape_func) )
        {
            $sess_arr       = $this->session->userdata();
            redirect('photographer/profile/'.$sess_arr['pgID']);
        } elseif ( !$res && in_array($curr_func, $already_not_login_escape_func) ) {
            redirect('auth/login');
        }
        $this->public_init_elements->init_elements();
        $this->apikey = $this->config->item('encryption_key');
    }

    function index(){
        redirect(strtolower(__CLASS__). DIRECTORY_SEPARATOR . 'login');
    }


    //##################################################//
    //########### Calling default function #############//
    function login()
    {
        $data = array();

        if ( $this->input->post('pto_login') )
        {
            $postData   = array();
            $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email');
            $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
            if ( $this->form_validation->run() == TRUE )
            {
                $postData["email"]      = $this->input->post('email');
                $postData["password"]   = $this->input->post('password');
                $postData["apitoken"]   = $this->apikey;

                $rs_arr     = call_restapi('photographer/photographer_login/', "post", $postData);
                //echo "<pre>"; print_r($rs_arr); exit;
                if($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
                {
                    $return_arr     = (array) $rs_arr["rs_row"];

                    $sessData = array(
                        'pgID'              => $return_arr["id"],
                        'pg_clicker_code'   => $return_arr["pg_clicker_code"],
                        'first_name'        => $return_arr["first_name"],
                        'full_name'         => $return_arr["full_name"],
                        'last_login_datetime'  => $return_arr["last_login_datetime"],
                        'email'             => $return_arr["email"],
                        'reg_number'        => $return_arr["reg_number"],
                        'logged_in'         => TRUE
                    );

                    $this->session->set_userdata($sessData);
                    redirect('photographer/profile/'.$return_arr["id"]);
                } else {
                    $data['message']   = $rs_arr['rs_row'];
                }

            } else {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['message'] = $error_msg;
            }
            //print_r($data); exit;
        }

        $data["ctrl_name"] = $this->uri->segment(1);
        $data["func_name"] = $this->uri->segment(2);

        $this->data['maincontent'] = $this->load->view('/maincontents/login_content_view', $data, true);
    	$this->load->view('layout_login', $this->data);
    }


    /**
     *
     */
    public function signup()
    {
        $data = array();
        if ( $this->input->post("sign_up_btn") )
        {
            $postData   = array();
            $this->form_validation->set_rules('name', 'name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email');
            $this->form_validation->set_rules('contact_number', 'contact number', 'required|trim|xss_clean');
            $this->form_validation->set_rules('gender', 'gender', 'required|trim|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
            $this->form_validation->set_rules('conf_pass', 'confirm password', 'required|trim|xss_clean|matches[password]');

            if ( $this->form_validation->run() == TRUE )
            {
                $postData["apitoken"]   = $this->apikey;
                $postData["name"]       = $this->input->post('name');
                $postData["email"]      = $this->input->post('email');
                $postData["contact_number"] = $this->input->post('contact_number');
                $postData["gender"]     = $this->input->post('gender');
                $postData["password"]   = $this->input->post('password');
                $postData["conf_pass"]  = $this->input->post('conf_pass');
                //echo "<pre>"; print_r($_FILES);
                $postData['featured_image'] = json_encode($_FILES['featured_image']);

                $rs_arr     = call_restapi('photographer/photographer_signup/', "post", $postData);
                //print_r($rs_arr); exit(0);
                if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
                {
                    $data['message_signup']   = $rs_arr['rs_row'];
                } else {
                    $data['message_signup']   = $rs_arr['rs_row'];
                }
            }
            else
            {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['message_signup'] = $error_msg;
            }
        }

        $data["ctrl_name"] = $this->uri->segment(1);
        $data["func_name"] = $this->uri->segment(2);

        $this->data['maincontent'] = $this->load->view('/maincontents/login_content_view', $data, true);
        $this->load->view('layout_login', $this->data);
    }

    /**
     *
     */
    public function logout()
    {

        $is_pg_logged_in = $this->session->userdata('logged_in');

        if( isset($is_pg_logged_in) && $is_pg_logged_in == TRUE )
        {
            $unst_data = array('pgID', 'pg_clicker_code', 'first_name', 'full_name', 'last_login_datetime', 'email', 'reg_number', 'logged_in');
            $this->session->unset_userdata($unst_data);
            redirect('auth');
        } else {
            redirect('home');
        }
    }

}


?>