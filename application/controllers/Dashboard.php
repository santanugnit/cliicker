<?php
/*
 *  @property Common_functions       $common_functions                        Library Common_functions
 */
class Dashboard extends CI_Controller
{
    var $data;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        //populate viewfor header / footer elements
        $this->load->library('common_functions');
        $this->load->library('public_init_elements');
        $res = $this->public_init_elements->is_photographer_logged_in();
        $this->curr_ctrl = $this->public_init_elements->get_current_controller_name();
        $this->curr_func = $this->public_init_elements->get_current_function_name();
        if ( !$res ) {
            redirect('auth/login');
        }
        $this->load->model('Album_model');
        $this->mdl_obj  = $this->Album_model;
        $this->public_init_elements->init_elements();
        $this->apikey   = $this->config->item('encryption_key');
        $this->DS       = DIRECTORY_SEPARATOR;
    }

    function index() {

        redirect(strtolower(__CLASS__). $this->DS . 'home');
    }

    /**
     *
     */
    function home()
    {
        $data = array();
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. $this->DS . "create_album";
        $data["ctrl_name"] = $this->curr_ctrl;
        $data["func_name"] = $this->curr_func;


        $pgSessData     = $this->session->userdata();
        echo "<pre>"; print_r($pgSessData); echo "</pre>"; exit;

        $data['my_pg_photos']     = $this->load->view('elements/sidebar/pg-photos', $data, true);
        $data['pg_follow_following']   = $this->load->view('elements/sidebar/pg-follower-following', $data, true);
        $data['client_reviews']   = $this->load->view('elements/sidebar/client-reviews', $data, true);
        $this->data['leftsidebar']   = $this->load->view('elements/left-sidebar-temp', $data, true);


        $this->data['header']   = $this->load->view('elements/header-inner', $data, true);
        $this->data['banner']   = $this->load->view('elements/banner/banner-pg-profile', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/photographer_home_view', $data, true);
        $this->load->view('layout_inner_page', $this->data);
    }

    /**
     *
     */
    function myaccount()
    {
        $data = array();
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. $this->DS . "create_album";
        $data["ctrl_name"] = $this->curr_ctrl;
        $data["func_name"] = $this->curr_func;


        $pgSessData     = $this->session->userdata();
        //echo "<pre>"; print_r($pgSessData); echo "</pre>";


        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/myaccount_content_view', $data, true);
        $this->load->view('layout_home', $this->data);
    }

    /**
     *
     */
    public function create_album()
    {
        $data = array();
        $data["ctrl_name"]  = $this->curr_ctrl;
        $data["func_name"]  = $this->curr_func;
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. DIRECTORY_SEPARATOR . "create_album";

        $pgSessData     = $this->session->userdata();

        if ( $this->input->post('SaveNext') == 'Next' )
        {
            $postData   = array();
            $this->form_validation->set_rules('album_name', 'album name', 'trim|required');
            $this->form_validation->set_rules('preferred_subdomain_name', ' subdomain name', 'trim|required|is_unique[]');

            $postData["apitoken"]           = $this->apikey;
            $postData["album_name"]         = $this->input->post("album_name");
            $postData["short_description"]  = $this->input->post("short_description");
            $postData["intro_text"]         = $this->input->post("intro_text");
            $postData["preferred_subdomain_name"]   = $this->input->post("preferred_subdomain_name");
            $postData["google_drive_link"]  = $this->input->post("google_drive_link");
            $postData["dropbox_link"]       = $this->input->post("dropbox_link");
            $postData["photographer_id"]    = $pgSessData["pgID"];
            $postData["occasion_id"]        = 1;
            $postData["album_price"]        = 15000;
            $postData["creation_datetime"]  = date("Y-m-d H:i:s");

            $rs_arr     = call_restapi('album/api_album_create', "post", $postData);
            if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
            {
                $result_arr     = (array)$rs_arr["rs_row"];
                redirect('dashboard/album_file_upload/'.$result_arr["album_id"]);
            } else {
                if( $rs_arr['api_action_message'] == 'Invalid access token'){
                    $data['message']   = $rs_arr['api_action_message'];
                } else {
                    $data['message']   = $rs_arr['rs_row'];
                }
            }
        }

        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);
        $this->data['maincontent'] = $this->load->view('/maincontents/album_create_view', $data, true);
        $this->load->view('layout_home', $this->data);
    }

    /**
     * @return null|string
     */
    public function album_file_upload()
    {
        $data = array();
        $pgSessData         = $this->session->userdata();
        $data["album_id"]   = (int)$this->uri->segment(3);
        $data["curr_ctrl"]  = $this->curr_ctrl;
        $data["payu_URL"]   = base_url(). $this->curr_ctrl. DIRECTORY_SEPARATOR . 'payment_payu';

        if ( $data["album_id"] < 1) {
            redirect('dashboard/myaccount');
        }

        echo "<pre>"; print_r($pgSessData); echo "</pre>";
        echo $targetDir = FCPATH . "restserver/assets". $this->DS . "uploaded_files" . $this->DS . "album_files";



        $postData               = array();
        $data["album_detls"]    = array();

        $data["photographer_id"]    = $pgSessData["pgID"];
        $data["album_id"]           = $data["album_id"];


        $postData["apitoken"]           = $this->apikey;
        $postData["album_id"]           = $data["album_id"];
        $postData["photographer_id"]    = $data["photographer_id"];
        # Is there any album exists for current login photographer id.
        $rs_arr     = call_restapi('album/api_album_authenticate_check', "post", $postData);

        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data["album_detls"]    = (array)$rs_arr["rs_row"];
        }

        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/album_files_upload_view', $data, true);
        $this->load->view('layout_home', $this->data);

    }


    /**
     *
     */
    public function ajax_client_check_unique_subdomain_name()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;
        $preferred_subdomain_name   = $this->input->post('preferred_subdomain_name');
        $album_id           = $this->input->post('alb_id');
        $operation_mode     = $this->input->post('operation_mode');

        $this->form_validation->set_error_delimiters('', '');
        if ( $operation_mode == "edit" && (int)$album_id > 0 )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash');
        } else {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash');
        }

        if ( $this->form_validation->run() === FALSE )
        {
            $return['result']   = validation_errors();
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $postData   = array();
            $postData["preferred_subdomain_name"]   = $preferred_subdomain_name;
            $postData["album_id"]           = $album_id;
            $postData["operation_mode"]     = $operation_mode;
            $postData["apitoken"]           = $this->apikey;

            $rs_arr     = call_restapi('album/ajax_check_unique_subdomain_name', "post", $postData);

            if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
            {
                $return_arr     = (array)$rs_arr["rs_row"];
                $return['result'] = $return_arr["result"];
                $return['status'] = true;
                echo json_encode($return);
                exit(0);
            }
            else
            {
                $return_arr         = (array) $rs_arr["rs_row"];
                $return['result']   = $return_arr["result"];
                echo json_encode($return);
                exit(0);
            }
        }
    }


    /**
     *
     */
    function front_ajax_plupload()
    {
        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);
        // Current photographer session data
        $pgSessData     = $this->session->userdata();
        // Settings
        //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
        $targetDir = FCPATH . "restserver/assets". $this->DS . "uploaded_files" . $this->DS . "album_files" . $this->DS . $pgSessData["pg_clicker_code"];

        //$targetDir = 'uploads';
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        $postData   = array();

        $this->form_validation->set_rules('album_id', 'album id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('pgID', 'photographer id', 'trim|required|is_natural_no_zero');
        if ( $this->form_validation->run() == FALSE )
        {
            $error_arr = array(1, validation_errors());
            $error_msg = $this->common_functions->view_message($error_arr);
            $result["status"]   = false;
            $result["message"]  = $error_msg;
            echo json_encode($result);
            exit;
        }
        $postData["pgID"]       = $this->input->post("pgID");
        $postData["album_id"]   = $this->input->post("album_id");

        // Get a file name
        if (isset($_REQUEST["name"])) {
            $fileName = time() .'_'.rand(999,9999999).'_'. $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = time() .'_'.rand(999,9999999).'_'. $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . $this->DS . $fileName;

        // Chunking might be enabled
        $chunk  = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $uploadedFileName   = array();

        // Remove old temp files
        if ($cleanupTargetDir)
        {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false)
            {
                $tmpfilePath = $targetDir . $this->DS . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if ( !empty($_FILES) )
        {
            if ( $_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"]) ) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 123456, "message": "Force exit if."}, "id" : "123"}');

            }
            else
            {
                $postdata  = array(
                    'album_id' => $postData["album_id"],
                    'upload_file_name' => $fileName,
                    'upload_file_size' => $_FILES["file"]['size'],
                    'uploaded_datetime' => date('Y-m-d H:i:s')
                );
                $album_files_id = $this->mdl_obj->album_file_upload($postdata);

                if ($album_files_id)
                {
                    $this->mdl_obj->update_album_space_usages( array('space_usage' => $_FILES["file"]['size']), $postData["album_id"]);
                    $uploadedFileName[$album_files_id] = $fileName;
                }
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);


        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }

        $result["status"]   = true;
        $result["message"]  = 'Success';
        $result["file_name"]    = $uploadedFileName;

        echo json_encode($result);
        exit;

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "filename" : '.$uploadedFileName.'}');
    }


    /**
     * @return mixed
     */
    public function payment_payu()
    {

    }

    /**
     *
     */
    public function show_profile()
    {

    }

    /**
     *
     */
    public function edit_profile()
    {
        $data = array();
        $data["ctrl_name"]  = $this->curr_ctrl;
        $data["func_name"]  = $this->curr_func;
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. $this->DS . "edit_profile";

        $pgSessData     = $this->session->userdata();
        $postData["apitoken"]       = $this->apikey;
        $postData["pgID"]           = $pgSessData["pgID"];

        if( strtolower($_SERVER["REQUEST_METHOD"]) === "post" && $this->input->post('profile_edit_btn') == 'Submit' )
        {
            $postData   = array();
            $this->form_validation->set_rules('first_name', 'first name', 'trim|required');
            $this->form_validation->set_rules('last_name', ' last name', 'trim|required');
            $this->form_validation->set_rules('contact_number', 'contact number', 'trim|required');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required|in_list[M,F]');

            $postData["apitoken"]           = $this->apikey;
            $postData["pgID"]               = $pgSessData["pgID"];
            $postData["first_name"]         = $this->input->post("first_name");
            $postData["last_name"]          = $this->input->post("last_name");
            $postData["address"]            = $this->input->post("address");
            $postData["gender"]             = $this->input->post("gender");
            $postData["contact_number"]     = $this->input->post("contact_number");

            if ($_FILES['featured_image']['name'] != '' )
                $postData['featured_image']     = json_encode($_FILES['featured_image']);

            if ( $this->form_validation->run() == true )
            {
                $rs_arr = call_restapi('photographer/api_pg_edit_profile', "post", $postData);
                if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success')
                {
                    $data['message'] = $rs_arr["rs_row"];
                    //print_r($data['message']);
                }
                else
                {
                    if ($rs_arr['api_action_message'] == 'Invalid access token') {
                        $data['message'] = $rs_arr['api_action_message'];
                    } else {
                        $data['message'] = $rs_arr['rs_row'];
                    }
                }
            }
            else
            {
                $error_arr      = array(1, validation_errors());
                $error_msg      = $this->common_functions->view_message($error_arr);
                $data['message']    = $error_msg;
            }
        }

        $rs_arr     = call_restapi('photographer/get_pg_details', "post", $postData);
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data["pg_detls"]   = (array)$rs_arr["rs_row"];
        } else {
            $data["message"]    = $rs_arr["rs_row"];
        }

        //echo "<pre>"; print_r($data["pg_detls"]); echo "</pre>";
        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);
        $this->data['maincontent'] = $this->load->view('/maincontents/edit_profile_view', $data, true);
        $this->load->view('layout_home', $this->data);
    }

    /**
     *
     */
    public function change_password()
    {
        $pgSessData             = $this->session->userdata();
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. $this->DS . "edit_profile";

        if( strtolower($_SERVER["REQUEST_METHOD"]) === "post" && $this->input->post('chg_pwd_btn') == 'Submit' )
        {
            $postData = array();
            $this->form_validation->set_rules('old_pwd', 'current password', 'trim|required'); //|callback_current_password_check
            $this->form_validation->set_rules('new_pwd', 'new password', 'trim|required');
            $this->form_validation->set_rules('new_conf_pwd', 'confirm password', 'trim|required|matches[new_pwd]');

            if ( $this->form_validation->run() == FALSE )
            {
                $error_arr          = array(1, validation_errors());
                $error_msg          = $this->common_functions->view_message($error_arr);
                $data['message']    = $error_msg;
            }
            else
            {
                $postData["pgID"]           = $pgSessData["pgID"];
                $postData["apitoken"]       = $this->apikey;
                $postData["old_pwd"]        = $this->input->post('old_pwd');
                $postData["new_pwd"]        = $this->input->post('new_pwd');
                $postData["new_conf_pwd"]   = $this->input->post('new_conf_pwd');

                $rs_arr = call_restapi('photographer/api_pg_change_password', "post", $postData);
                if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success') {

                    $data['message'] = $rs_arr["rs_row"];
                } else {

                    $data['message'] = $rs_arr["rs_row"];
                }
            }

        }

        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);
        $this->data['maincontent'] = $this->load->view('/maincontents/change_password_view', $data, true);
        $this->load->view('layout_home', $this->data);

    }

    /**
     *
     */
    public function album_lists()
    {
        $data = array();
        $data["ctrl_name"]  = $this->curr_ctrl;
        $data["func_name"]  = $this->curr_func;
        $data["album_create_url"]   = base_url(). $this->curr_ctrl. $this->DS . "edit_profile";

        $pgSessData     = $this->session->userdata();

        $postData       = array();
        $postData["apitoken"]   = $this->apikey;
        $postData["pgID"]       = $pgSessData["pgID"];

        $rs_arr     = call_restapi('album/api_get_pg_album_lists', "post", $postData);
        //echo "<pre>"; print_r((array)$rs_arr); echo "</pre>";
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data["alb_detls"]   = (array)$rs_arr["rs_row"];
        }


        $this->data['header']   = $this->load->view('elements/header', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);
        $this->data['maincontent'] = $this->load->view('/maincontents/album_lists_view', $data, true);
        $this->load->view('layout_home', $this->data);
    }






}


?>