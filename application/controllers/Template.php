<?php
class Template extends CI_Controller
{
    var $data;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        //populate viewfor header / footer elements
        $this->load->library('common_functions');
        $this->load->library('public_init_elements');
        $this->public_init_elements->init_elements();

        $this->curr_ctrl    = $this->public_init_elements->get_current_controller_name();
        $this->curr_func    = $this->public_init_elements->get_current_function_name();

        $this->apikey       = $this->config->item('encryption_key');
        $this->DS          = DIRECTORY_SEPARATOR;
    }

    //##################################################//
    //########### Calling default function #############//

    function index(){
        redirect(strtolower(__CLASS__). DIRECTORY_SEPARATOR . 'serach');
    }

    function serach()
    {
        $data = array();
        $data["ctrl_name"]  = $this->curr_ctrl . $this->DS;
        $data["func_name"]  = $this->curr_func . $this->DS;

        $data["temp_suggest"]   = $data["ctrl_name"] . 'ajax_send_photographer_template_suggest' . $this->DS;
        $data["temp_filter"]    = $data["ctrl_name"] . 'ajax_template_filter' . $this->DS;

        
        $postData               = array();
        $postData["apitoken"]   = $this->apikey;

       
        $rs_arr     = call_restapi('template/get_templates_lists', "post", $postData);

        //echo "<pre>"; print_r($rs_arr); echo "</pre>"; echo "Hii"; exit;
        $data['theme_cat_arr']  = array();
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data['theme_lists']        = (array)$rs_arr["rs_row"];
            $data['theme_cat_lists']    = (array)$rs_arr["cat_row"];
            $data['pg_lists']           = (array)$rs_arr["pg_lists"];
        }
        
        if( isset($data['theme_cat_lists']) && !empty($data['theme_cat_lists']) )
        {
            foreach ( $data['theme_cat_lists'] as $catArr ) 
            {
                $catArr = (array)$catArr;                
                $data['theme_cat_arr'][$catArr['cat_parent']][$catArr['id']] =  $catArr;
            }
        } 
        
        /*echo "<pre>";  
        //print_r($data['theme_lists'] ); 
        //print_r($data['theme_cat_lists']); 
        //print_r($data['theme_cat_arr']); 
        print_r($data['pg_lists']);
        echo "</pre>"; */
        
        $this->data['cdnCSS']   = array('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css');
        $this->data['cdnJS']    = array('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', 'https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        
        $this->data['pageJS']   = array('js/tooltipster-js/js/tooltipster.bundle.min.js');
        $this->data['pageCSS']  = array('js/tooltipster-js/css/tooltipster.bundle.min.css');

        $this->data['header']   = $this->load->view('elements/header-inner', $data, true);
        $this->data['banner']   = $this->load->view('elements/banner/banner-template-serach', $data, true);
        $this->data['footer']   = $this->load->view('elements/footer', $data, true);

        $this->data['maincontent'] = $this->load->view('/maincontents/template_serach', $data, true);
    	$this->load->view('layout_inner_page', $this->data);
    }



    function ajax_template_filter()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $postData               = array();
        $postData["apitoken"]   = $this->apikey;
        $postData["s_name"]     = $this->input->get('s_name');
        $community              = $this->input->get('community');
        
        $postData['community']  = '';
        if( is_array($community) && !empty($community) )
        {
            $postData['community'] = implode(",", $community );
        }
        
        $rs_arr     = call_restapi('template/api_templates_filter', "post", $postData);
        //echo "<pre>"; print_r($rs_arr); echo "</pre>"; exit;
        
        if ($rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $data['theme_lists']    = (array)$rs_arr["rs_row"];
            $return["result"]       = $this->load->view('/maincontents/template_serach_ajax', $data, true);           
            $return['status']       = true;
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $return['result']   = '<li class="no_found_wrapper">
                <div class="not_found_content">
                    <span class="not_found_msg">Some error on template search :(</span>
                    <span class="suggestion_txt"><i>Please clear your browser cache or check internet access or try some time later.</i></span>
                </div>
            </li>';
            $return['status']   = false;
            echo json_encode($return);
            exit(0);
        }        
    }


    function ajax_send_photographer_template_suggest()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $postData               = array();
        $postData["apitoken"]   = $this->apikey;

        $postData['temptale_id']    = $this->input->post('temptale_id');
        $postData['c_name']         = $this->input->post('c_name');
        $postData['c_email']        = $this->input->post('c_email');
        $postData['c_phone_no']     = $this->input->post('c_phone_no');
        $postData['c_massage']      = $this->input->post('c_massage');
        $reg_photographer           = $this->input->post('reg_photographer');

        $postData['reg_photographer']   = '';
        if( is_array($reg_photographer) && !empty($reg_photographer) )
        {
            $postData['reg_photographer'] = implode(",", $reg_photographer );
        }
        //$containsDigit   = preg_match('/\d/', $postData['reg_photographer']);
        //echo "<pre>" . $containsDigit;  print_r($postData); exit();

        $rs_arr     = call_restapi('template/api_send_photographer_template_suggest', "post", $postData);

        
        //echo "<pre>"; print_r($rs_arr); 

        if ( $rs_arr['api_syntax_success'] == 1 && $rs_arr['api_action_success'] == 1 && $rs_arr['api_action_message'] == 'success' )
        {
            $return["result"]   = 'Your suggestion send successfully.';           
            $return['status']   = true;
            echo json_encode($return);
            exit(0);
        }
        else
        {
            if( isset($rs_arr['message']) )
                $return['result']   = $rs_arr['message'];
            else
                $return['result']   = 'There is some problem, please contact with admin or try again after minites.';
            $return['status']   = false;
            echo json_encode($return);
            exit(0);
        }
        
    }

    



}


?>