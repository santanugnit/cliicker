<?php

class Album_model extends CI_Model
{


    /**
     * Album_model constructor.
     */
    function __construct()
    {
        parent::__construct();

    }

    /**
     * @param array $data
     * @return mixed
     */
    function album_file_upload( $data = array())
    {
        $this->db->insert('album_uploaded_files', $data);
        return $this->db->insert_id();
    }

    /**
     * @param array $data
     * @param int $album_id
     * @return bool
     */
    function update_album_space_usages( $data = array(), $album_id = 0 )
    {
        if ( empty($data) || (int)$album_id < 1 )
            return false;

        $this->db->set('space_usage', 'space_usage+'. $data["space_usage"], false);
        $this->db->where('id', $album_id);
        $this->db->update('albums');

        if ($this->db->affected_rows())
            return true;
        else
            return false;
    }

    /**
     * @param int $id
     * @return int
     */
    function get_album_uploaded_file_details_by_id( $id = 0 )
    {
        if( $id == null || (int)$id < 1 ) {
            return 0;
        }
        $query = $this->db->get_where('album_uploaded_files', array('id' => $id));
        foreach( $query->result_array() as $row )
        {
            return $row["upload_file_size"];
        }
        return 0;
    }

    /**
     * @param array $data
     * @param int $album_id
     * @return bool
     */
    function update_album_data( $data = array(), $album_id = 0 )
    {
        if( (int)$album_id < 1 || empty($data) || !isset($data["album_code"]) || trim($data["album_code"]) == "" ) {
            return false;
        }
        $fileSize = 0;
        if ( isset($data["upload_ids"]) && trim($data["upload_ids"]) != "" )
        {
            $upload_id_arr  = explode(",", $data["upload_ids"]);
            if( is_array($upload_id_arr) && !empty($upload_id_arr) )
            {
                foreach ( $upload_id_arr as $ids )
                {
                    $this->db->where('id', $ids);
                    $this->db->update('album_uploaded_files', array("album_id" => $album_id));
                    $fileSize += $this->get_album_uploaded_file_details_by_id($ids);
                }
            }
        }
        $updateData = array("album_code" => $data["album_code"], "space_usage" => $fileSize);
        /*$updateData["album_code"]   = $data["album_code"];
        $updateData["space_usage"]   = $fileSize;*/

        $this->db->where('id', $album_id);
        $this->db->update('albums', $updateData);

        if ($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }



}
