<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_functions
{

	var $CI;
	var $category_tree_dropdown = "";
	var $tree_ul_li = "";

    function  __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }
	
	/**
	 * Dynamic function return a meassge div identifying its type
	 * @param array $val array field
	 * return a div with message identifying its type
	 */
	function view_message($val)
	{   
		$message_val = $val;
		$output_message="";
		$msg_type="";
		$msg_heading="";
				
		if(is_array($message_val))
		{
			//******** GET MESSAGE TYPE ********//
			if($message_val[0]==1)
			{    
				$msg_type = "danger";
				$msg_heading="Error!";
			}
			elseif($message_val[0]==2)
			{    
				$msg_type = "success";
				$msg_heading="Success!";
			}
			elseif($message_val[0]==3)
			{
				$msg_type = "warning";
				$msg_heading="Warning!";
			}
			elseif($message_val[0]==4)
			{
				$msg_type = "info";
				$msg_heading="Info!";
			}
			else
			{    
				$msg_type="";
				$msg_heading="";
			}
			
			$output_message .='<div class="col-md-12 header_notification_message">';
			$output_message .='<div class="alert alert-'.$msg_type.'">';
			$output_message .='<a href="#" data-dismiss="alert" onclick="close_alert();" class="close alert-close">x</a>';
			$output_message .='<h4 class="alert-heading">'.$msg_heading.'</h4>';
			$output_message .='<p>';
			
			$output_message .="<ul>";
			$output_message .= $message_val[1];
			$output_message .="</ul>";
			
			$output_message .='</p>';
			$output_message .='</div>';
			$output_message .='</div>';
			
		}
		
		return $output_message;
		
	}
	
	
	/**
     * Dynamic function to get a value respect to a particular filed of a 
     * table with passing where clause as parameter 
     * @param string $field Field name
     * @param string $tablename The table name
     * @param string $where Where clause condition
     * @return mixed Return a value. Also return NULL if record is not exists. 
     */
    function get_value($field,$tablename,$where='')
    {        
        $fieldval="";
		
        $sql="SELECT ".$field." FROM ".$tablename;
		if($where!="")
		{
			$sql = $sql." WHERE ".$where;
		}
		
		$result = mysql_query($sql);
		
		if(mysql_num_rows($result))
		{	
			$rec = mysql_fetch_object($result);
			$fieldval= $rec->$field;
		}
		
		return $fieldval;
		
    }
	
	
	/**
     * Dynamic function to get tree view structure of categories within select box
     * @param array $data_array category array id=>name structure
     * @param array $selected_ids passing ids displaying as a selected status within select box
     * @return string with html format 
     */
	function get_category_tree($data_array, $selected_ids=array(), $ignore_ids=array(), $params=array())
	{	
		global $category_tree_dropdown;
        
        $selectbox_name = "";
        $selectbox_class = "";
        $is_multiple = 0; //0 means single selected
        $default_selected = "";
        
        if(count($params))
        {  
            $selectbox_name = $params['selectbox_name'];
            $selectbox_class = $params['class'];
            $is_multiple =  ($params['multiple']==1) ? 'multiple="multiple"' : '';
            $selectbox_class = $params['class'];
            $default_select_value = $params['default_select_value'];
            $default_select_text = ($params['default_select_text']!="") ? $params['default_select_text'] : "Select";
        }
         
        if(count($selected_ids)<=0)
        {    
            $default_selected = 'selected="selected"'; 
        }
        
		$category_tree_dropdown .= '<select name="'.$selectbox_name.'" class="'.$selectbox_class.'" '.$is_multiple.'>';
		$category_tree_dropdown .= '<option value="'.$default_select_value.'" '.$default_selected.'>********* '.$default_select_text.' *********</option>';
		
		if(count($data_array))
		{
			$category_tree = $this->build_category_tree($data_array);
			$category_tree_dropdown .= $this->category_tree_dropdown($category_tree,$selected_ids,$ignore_ids);
		}
		
		$category_tree_dropdown .= '</select>'; 
		
		return $category_tree_dropdown;
		
		
	}
	
	function build_category_tree($data , $parent=0)
	{
		$tree = array();
		foreach($data as $d) 
		{
			if($d['parent_id'] == $parent) 
			{
				$children = $this->build_category_tree($data, $d['id']);
				
				// set a trivial key
				if (!empty($children))
				{
					$d['_children'] = $children;
				}
				$tree[] = $d;
			}
		}
		
		return $tree;
	}
	
	function category_tree_dropdown($tree, $selected_ids=array(),$ignore_ids=array(), $r = 0, $p = null)
	{
		global $category_tree_dropdown;
		
		foreach($tree as $i => $t) 
		{
			$dash = ($t['parent_id'] == 0) ? '' : str_repeat('--- ---', $r).' ';
			
            if(count($ignore_ids)!=0 && !in_array($t['id'],$ignore_ids))
            {
                $category_tree_dropdown .= '<option value="'.$t['id'].'" ';
                if(count($selected_ids) && in_array($t['id'],$selected_ids))
                {  
                    $category_tree_dropdown .='selected="selected"';
                }    
                $category_tree_dropdown .= ' >'.$dash.$t["name"].'</option>';
            }
            else
            {    
                $category_tree_dropdown .= '<option value="'.$t['id'].'" ';
                if(count($selected_ids) && in_array($t['id'],$selected_ids))
                {  
                    $category_tree_dropdown .='selected="selected"';
                }    
                $category_tree_dropdown .= ' >'.$dash.$t["name"].'</option>';
            }
			
			if(isset($t['_children']))
			{
				$this->category_tree_dropdown($t['_children'], $selected_ids, $ignore_ids, $r+1, $t['parent_id']);
			}
		}
	}
	
	#---------------------------------------------------------------------------------##
    #---------------- Build n-level tree structure into dropdown menu ----------------##
    ##--------------------------------------------------------------------------------##
	
	
	##-------------------------------------------------------------------------------------##
    ##------------ Get geometric location (latitude, longitude) of an address -------------##
    ##-------------------------------------------------------------------------------------##
    
    function get_long_lat($params)
    {   
        
		$city = str_replace(" ", "+", $params['city']);
		$state = str_replace(" ", "+", $params['state']);
		$country = str_replace(" ", "+", $params['country']);
		$address = str_replace(" ", "+", $params['address']);
		$zip = str_replace(" ", "+", $params['zipcode']);
		
        
        /*$street_name = "82 Fensome Dr";
        $address = "Houghton Regis";
        $city = "Dunstable";
        $country = "";
        $postcode = "LU5 5SH"; */
        
		if($state!="" && $city!="" && $address!="" && $zip!="")
			$main_address = $address.",".$state.",".$city.",".$zip;
		else if($address!="" && $state!="" && $city=="" && $zip=="")
			$main_address = $address.",".$state;
		else if($address!="" && $state!="" && $city!="" && $zip=="")
			$main_address = $address.",".$state.",".$city;
		else
			$main_address ="";
        
        $url = "http://maps.google.com/maps/api/geocode/json?address=".$main_address."&sensor=false&region=".$country; 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        
        $lat="";
        $long="";
        
        if(count($response_a->results))
        {  
            $lat = $response_a->results[0]->geometry->location->lat;
            $long = $response_a->results[0]->geometry->location->lng;
        }
        
        $return=array('latitude'=>$lat,'longitude'=>$long);
        //echo "<pre>"; print_r($return); die();
        return $return;
    }
	
	
	##----------------------------------------------------------------------##
    ##------------ Get Unique slug value respect to post title -------------##
    ##----------------------------------------------------------------------##
	//@ $post_title   : Slug Value that you want to create
	//@ $field_name : Comparing with the uniqueness of the slug value 
	//@ $table_name  : Tablename that contents the slug value
	//@ $ignore_fields  : 
	
	function get_unique_slug($post_title, $field_name, $table_name, $ignore_fields=NULL)
	{
		
		$sql = "SELECT count(*) row_count FROM ".$table_name." WHERE ".$field_name." = '".$post_title."'";
		if(count($ignore_fields))
		{
			$str = "";
			foreach($ignore_fields as $text=>$val)
			{
				$str .= " AND ".$text."!= '".$val."'";
			}
			$sql .= $str;
		}
		//echo $sql; die();
		$query = $this->CI->db->query($sql);
		$result = $query->row_array();
		
		$slug_name = url_title($post_title);
		##-------- MAX LENGTH SHOULD BE 254 ---------##
		if(strlen($slug_name) >= '254'){
			$slug_name=substr($slug_name,0,252);
		}
		##-------- MAX LENGTH SHOULD BE 254 ---------##
		
		if($result['row_count'] >0)  //Duplicate value exists
		{
			$counter = ($result['row_count'] + 1);
			$slug_name = $slug_name."-".$counter; 
		}
		
		
		return strtolower($slug_name);
		
	}


	
	
}

?>