<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_init_elements
{
    var $CI;
	
	//#####################################################//
	//################# LOAD CONSTRUCTOR ##################//
	//#####################################################//
    
	function  __construct()
    {
        $this->CI =& get_instance();
    }
	
	//#####################################################//
	//################# LOAD CONSTRUCTOR ##################//
	//#####################################################//
	
	
	//#####################################################//
	//#################### LOAD HEAD ######################//
	//#####################################################//

    function init_head()
    {
        //$this->CI->load->library('common_functions');
        $this->CI->data = array();

		//$this->CI->load->model('restserver/Static_pages_model');
        //$data['states'] = $this->CI->state_model->get_state_list_by_country(3);
        //$page_id                = $this->CI->static_pages_model->get_id( $this->CI->uri->segment(2) == '' ? 'home' : $this->CI->uri->segment(2) );
		//$data['static_page']    = $this->CI->static_pages_model->get_row_data($page_id);

		//populate default meta values for any controller
		if( isset($data['static_page']) && !empty($data['static_page']) ) {
			$data['title']              = $data['static_page']['meta_title'];
			$data['meta_keyword']       = $data['static_page']['meta_keywords'];
			$data['meta_description']   = $data['static_page']['meta_description'];
		} else {
			$data['title']              = "CLIICKER";
			$data['meta_keyword']       = "CLIICKER";
			$data['meta_description']   = "CLIICKER";
		}

        $this->CI->data['head'] = $this->CI->load->view('elements/head', $data, true);
        //$this->CI->data['header'] = $this->CI->load->view('elements/header', $data, true);
        //$this->CI->data['footer'] = $this->CI->load->view('elements/footer', $data, true);
    }

	//#####################################################//
	//#################### LOAD HEAD ######################//
	//#####################################################//
	
	
	//##############################################################//
	//#################### LOAD INIT ELEMENTS ######################//
	//##############################################################//
    
    function init_elements()
    {
        //index call to populate all the header / footer / side bar elements
        $this->init_head();
    }
	
	//##############################################################//
	//#################### LOAD INIT ELEMENTS ######################//
	//##############################################################//
    
	
    //##############################################################//
	//########## CHECK PATIENT LOGIN SESSION EXISTS ################//
	//##############################################################//
	function is_photographer_logged_in( $pg = NULL )
    {
        $sess_arr       = $this->CI->session->userdata();

        if(!$sess_arr)
            return false;

        $is_logged_in   = ( isset($sess_arr['logged_in']) )? $sess_arr['logged_in']:'';
		$pgID           = ( isset($sess_arr['pgID']) )? $sess_arr['pgID']:'';

        if( !isset($is_logged_in) || (int)$pgID < 1 || $is_logged_in != TRUE )
            return false;
        else
            return true;
    }

    function get_current_controller_name()
    {
        //return $this->CI->uri->segment(1);
        return $this->CI->router->class;
    }

    function get_current_function_name()
    {
        //return $this->CI->uri->segment(2);
        return $this->CI->router->method;
    }
    


	
		
	
	
}

?>