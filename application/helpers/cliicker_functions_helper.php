<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if ( !function_exists('getRomanNumerals') )
	{
		function getRomanNumerals($decimalInteger) 
		{
			$n = intval($decimalInteger);
			$res = '';
			$roman_numerals = array('M'  => 1000, 'CM' => 900, 'D'  => 500, 'CD' => 400, 'C'  => 100, 'XC' => 90, 'L'  => 50, 'XL' => 40, 'X'  => 10, 'IX' => 9, 'V'  => 5, 'IV' => 4, 'I'  => 1);

			foreach ($roman_numerals as $roman => $numeral) 
			{
				$matches = intval($n / $numeral);
				$res .= str_repeat($roman, $matches);
				$n = $n % $numeral;
			}
			return $res;
		}
	}

	if ( !function_exists('getOrdinalNumerals') )
	{
		function getOrdinalNumerals($decimalInteger) 
		{
			$ends = array('th','st','nd','rd','th','th','th','th','th','th');
			if ((($decimalInteger % 100) >= 11) && (($decimalInteger%100) <= 13))
				return $decimalInteger. 'th';
			else
				return $decimalInteger. $ends[$decimalInteger % 10];
		}
	}

	if( !function_exists('CliickerfileExists') ) 
	{
		function CliickerfileExists($path){
		    return (@fopen($path,"r") == true);
		}
	}


	if( ! function_exists('get_latlong_from_address')) 
	{		
		function get_latlong_from_address( $data = array() )
		{
			$resultArr = array();
			if( !is_array($data) || empty($data) ) {
				return $resultArr; 
			}
			$addressStr = '';
			if( isset($data["address"]) && trim($data["address"]) != '' ) {
				$addressStr .= $data["address"];
			} else {
				return $resultArr;
			}
			if( isset($data["country"]) && trim($data["country"]) != '' ) {
				$addressStr .= "+". $data["country"];
			}
			if( isset($data["city"]) && trim($data["city"]) != '' ) {
				$addressStr .= "+". $data["city"];
			}
			if( isset($data["zip_code"]) && trim($data["zip_code"]) != '' ) {
				$addressStr .= "+". $data["zip_code"];
			}
			
			//$address = "DL+205+Bidhannagar+sector+5+kolkata+700128";
			//$address 	= trim($addArr["emp_addr"]) ."+". trim($addArr["emp_city"]) ."+". trim($addArr["emp_pincode"]);
			$addressStr 	= str_replace(array(" ", "-", ",", ";"), "-", $addressStr);
			$addressStr 	= preg_replace('/--+/', '+', $addressStr);
			
			//$url = "http://maps.google.com/maps/api/geocode/json?address=$addressStr&sensor=false&region=IN";
			$url = "http://maps.google.com/maps/api/geocode/json?address=$addressStr&sensor=true";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			$response_a = json_decode($response);
			//echo "<pre>"; print_r($response_a) ; echo "</pre>";
			$resultArr["lat"]	= (isset($response_a->results[0]->geometry->location))? $response_a->results[0]->geometry->location->lat :"";
			$resultArr["long"]	= (isset($response_a->results[0]->geometry->location))? $response_a->results[0]->geometry->location->lng :"";
			
			/*echo "Latitude: ". $lat = $response_a->results[0]->geometry->location->lat;
			echo "<br />";
			echo "Longitude: ". $long = $response_a->results[0]->geometry->location->lng;*/
			
			return $resultArr;
		}
	}

	
	
	function LoadJpeg($imgname)
	{
	    /* Attempt to open */
	    $im = @imagecreatefromjpeg($imgname);
	    
	    /* See if it failed */
	    if(!$im)
	    {
	        /* Create a black image */
	        $im  = imagecreatetruecolor(150, 30);
	        $bgc = imagecolorallocate($im, 255, 255, 255);
	        $tc  = imagecolorallocate($im, 0, 0, 0);

	        imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

	        /* Output an error message */
	        imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
	    }

	    return $im;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

