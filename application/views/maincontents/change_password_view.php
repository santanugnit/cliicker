<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Bind your memory once</h2>
        </div>


        <div class="row">
            <div class="col-md-3">
                <ul class="tab_url">
                    <li class="tab_url_li"><a href="<?php echo $album_create_url; ?>">Create album</a></li>
                    <li class="tab_url_li"><a href="javascript:void(0);">List album</a></li>
                    <li class="tab_url_li"><a href="javascript:void(0);">Edit Profile</a></li>
                    <li class="tab_url_li"><a href="javascript:void(0);">Change Password</a></li>
                    <li class="tab_url_li"><a href="javascript:void(0);">Logout</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="row">

                    <?php echo ( isset($message))? $message :""; ?>

                    <form id="chgPwdFrm" name="chgPwdFrm" method="post" action="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="old_pwd">Current password</label>
                                <input type="password" class="form-control" id="old_pwd" name="old_pwd" value="" placeholder="Enter current password" />
                            </div>
                            <div class="form-group">
                                <label for="old_pwd">New password</label>
                                <input type="password" class="form-control" id="new_pwd" name="new_pwd" value="" placeholder="Enter new password" />
                            </div>
                            <div class="form-group">
                                <label for="old_pwd">New confirm password</label>
                                <input type="password" class="form-control" id="new_conf_pwd" name="new_conf_pwd" value="" placeholder="Enter new confirm password" />
                            </div>
                        </div>

                        <div class="col-md-8">
                            <input type="submit" name="chg_pwd_btn" id="chg_pwd_btn" class="btn btn-primary" value="Submit" />
                        </div>
                    </form>

                </div>
            </div>

        </div>



    </div>
</section>

<script>
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>