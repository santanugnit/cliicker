<section class="content-area bg-ground">
    <div class="container">
        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Bind your memory once</h2>
        </div>


        <div class="row">
            <ul class="accordion" id="accordion">

                <li class="bg1">
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/1.jpg"></a>
                    <div class="description">
                        <h2><span>just</span> married</h2>
                    </div>
                </li>

                <li class="bg2">
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/2.jpg"></a>
                    <div class="description">
                        <h2><span>main</span> ceremony</h2>
                    </div>
                </li>

                <li class="bg3">
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/3.jpg"></a>
                    <div class="description">
                        <h2><span>sweet</span> couples</h2>
                    </div>
                </li>

                <li class="bg4">
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/4.jpg"></a>
                    <div class="description">
                        <h2><span>the </span>bride</h2>
                    </div>
                </li>

            </ul>
        </div>


    </div>
</section>


<section class="content-area">
    <div class="container">
        <div class="mono-type-text-content">

            <div class="col-md-5 pull-right">
                <div class="image-area">
                    <img src="<?php echo base_url(); ?>assets/images/img1.jpg">
                </div>
            </div>

            <div class="col-md-7">
                <div class="txt-contener">
                    <div class="bottom_txt">
                        <h3><span>how clicker</span> save your memories</h3>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem IpsumThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem IpsumThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem IpsumThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="content-area">
    <div class="container">
        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Choose your event</h2>
        </div>

        <div class="row">

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon1.png">
                            <span>wedding</span>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon2.png">
                            <span>pre wedding</span>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon3.png">
                            <span>birthday</span>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon4.png">
                            <span>maternity</span>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon5.png">
                            <span>Fashion</span>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-2">
                <div class="boxes-area">
                    <a href="#">
                        <div class="img_txt_area">
                            <img src="<?php echo base_url(); ?>assets/images/icon6.png">
                            <span>anniversary</span>
                        </div>
                    </a>
                </div>
            </div>


        </div>

    </div>
</section>

<section class="content-area package">
    <div class="container">
        <div class="mono-type-text-content">

            <div class="col-md-5">
                <div class="image-area">
                    <img src="<?php echo base_url(); ?>assets/images/img2.jpg">
                    <div class="overLays">
                        <h2><span>main</span> ceremony</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="txt-contener">
                    <div class="bottom_txt">
                        <h3><span>lorem ipsum dolor</span>select your package</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="small-img-area">
                                    <img src="<?php echo base_url(); ?>assets/images/img3.jpg">
                                    <div class="overLays">
                                        <h2><span>pre</span> wedding</h2>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="small-img-area">
                                    <img src="<?php echo base_url(); ?>assets/images/img4.jpg">
                                    <div class="overLays">
                                        <h2><span>the</span> anniversary</h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>