<?php
    $max_storage_allocate   = ( isset($album_detls["max_storage_allocate"]) )? $album_detls["max_storage_allocate"] : 1073741824;
    $space_usage            = ( isset($album_detls["space_usage"]) )? $album_detls["space_usage"] : 0;
    $remaining_space        = (($max_storage_allocate - $space_usage) / (1024*1024));
?>
<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Create your new album</h2>
        </div>
        <?php echo ( isset($message) )? $message : ""; ?>
        <form name="formCreateAlbum" id="formCreateAlbum" action="<?php echo $payu_URL; ?>" method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-lg-8 col-lg-offset-2">
                    <div class="box">
                        <div class="box-body">
                            <div class="body-head">
                                <!--<p><span>*</span> fields are mandatory</p>-->
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Please upload .zip file <span class="required_class"> </span><small></small></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="panel-body">
                                        <div id="img_prev"></div>
                                        <div id="container">
                                            <a id="pickfiles" href="javascript:;">[Select files]</a>
                                            <a id="uploadfiles" href="javascript:;">[Upload files]</a>
                                        </div>
                                        <div id="log"> </div>
                                        <input type="hidden" id="upload_files_ids" name="upload_files_ids" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="clearfix"></div>

                            <div class="form-footer">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <input type="submit" style="" id="fileUpload_btn" class="btn save_btn" value="Next" name="SaveNext" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>


            </div>
        </form>
    </div>
</section>



<script src="<?php echo base_url(); ?>assets/js/plupload/plupload.full.min.js"></script>
<script>
    var maxFileUploadSize = <?php echo (float)$remaining_space; ?>;
    var fileUploadProgressFlag = true;

    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('container'), // ... or DOM Element itself
        url : '<?php echo base_url() . $curr_ctrl . "/front_ajax_plupload"; ?>',
        flash_swf_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.swf',
        silverlight_xap_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.xap',

        filters : {
            max_file_size : maxFileUploadSize + 'mb',
            mime_types: [
                {title : "Zip files", extensions : "zip"}
            ]
        },

        multipart_params : { "album_id" : <?php echo $album_id; ?>, "pgID" : "<?php echo $photographer_id; ?>" },

        init: {
            PostInit: function() {
                document.getElementById('img_prev').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },

            FilesAdded: function(up, files) {
                var total_file_size = 0;

                plupload.each(files, function(file) {
                    document.getElementById('img_prev').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b> <a style="display: block;" href="javascript:void(0)" class="upload_progress_cancel" id="cancel_'+ file.id +'"> cancel </a><i></i></div>';

                    total_file_size += parseFloat(file.size);

                });
                maxFileUploadSize   = parseInt((parseFloat(maxFileUploadSize * 1024 * 1024) - total_file_size) / parseInt(1024 * 1024)) ;
                // Reset
                uploader.settings.filters.max_file_size = maxFileUploadSize + 'mb';
                document.getElementById('log').innerHTML = '';
            },

            UploadProgress: function(up, file) {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";

                if( fileUploadProgressFlag )
                {
                    (function($) {
                        var ind = file.id;
                        $("#cancel_" + ind).on("click", function() {
                            var _thatElem = $(this);
                            if ( file.status == plupload.UPLOADING )
                            {
                                uploader.stop();
                                uploader.removeFile(file);
                                $(_thatElem).unbind().remove();

                                //$("#pickfiles").hide();
                                //$("#fileUpload_btn").val("Proceede to pay").attr("disabled", false);
                            } else {
                                console.log("Else ... " + file);
                            }
                        });
                    })(jQuery);
                    fileUploadProgressFlag = false;
                }
            },
            BeforeUpload: function (up, file) {
                //console.log("BeforeUpload ... " + up+" -- "+ file.id);
                /*(function($) {
                    var ind = file.id;
                    $("#cancel_" + ind).on("click", function() {
                        var _thatElem = $(this);
                        if ( file.status == plupload.UPLOADING )
                        {
                            uploader.stop();
                            uploader.removeFile(file);
                            $(_thatElem).unbind().remove();

                            $("#pickfiles").hide();
                            $("#fileUpload_btn").val("Proceede to pay").attr("disabled", false);
                        } else {
                            console.log("Else ... " + file);
                        }
                    });
                })(jQuery);*/

                $("#fileUpload_btn").val("Progress....").attr("disabled", true);
                $("#pickfiles").hide();

                var ind = file.id;
                $("#cancel_" + ind).show();

                fileUploadProgressFlag = false;

            },
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                var upload_files_ids    = [];
                var upload_old_ids      = $('#upload_files_ids').val();
                if ( $('#upload_files_ids').val().trim() != '' ) {
                    upload_files_ids.push(upload_old_ids);
                }
                console.log(info);
                console.log(info.response);
                var data = $.parseJSON(info.response);

                if (data.status)
                {
                    $.each(data.file_name, function (index, value) {

                        upload_files_ids.push(index);
                    });
                    $("#upload_files_ids").val(upload_files_ids.join(","));

                    var ind = file.id;
                    $("#cancel_" + ind).hide();
                    $("#fileUpload_btn").val("Click to proceeds & pay").attr("disabled", false);
                }
                else
                {

                }
            },
            Error: function(up, err) {
                //document.getElementById('log').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
                document.getElementById('log').innerHTML = "Error #" + err.code + ": " + err.message;
                console.log(err);
            }
        }
    });


    uploader.init();

    /*(function($) {
        //var ind = file.id;
        $(document).on("click", ".upload_progress_cancel", function(e) {
            var _thatElem   = $(this);
            //var _thatElemID = $(this).attr("id");
            //var file = plupload.getFile(_thatElemID);
            //console.log(file);
            if ( file.status == plupload.UPLOADING )
            {
                uploader.stop();
                uploader.removeFile(file);
                $(_thatElem).unbind().remove();

                $("#pickfiles").hide();
                $("#fileUpload_btn").val("Click to upload & pay").attr("disabled", false);
            } else {
                console.log("Else ... " + file);
            }
        });
    })(jQuery);*/

</script>