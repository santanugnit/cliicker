<div class="col-md-6">
    <div class="photographer-feeds-area">
            
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams"> John smith<span>added a photo</span></dd>
            </div>
            <div class="photograph-image-area singleImg">
                <ul>
                    <li><a href="javascript:void(0);"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                </ul>
            </div>
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
        </div>
        
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams">John smith<span>added a photo</span></dd>
            </div>
            <div class="photograph-image-area doubleImg">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img2.jpg"></a></li>
                </ul>
            </div>
            
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
            
        </div>
        
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams">John smith<span>added a photo</span></dd>
            </div>
            <div class="photograph-image-area threeImg">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img2.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/1.jpg"></a></li>
                </ul>
            </div>
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
        </div>
        
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams">John smith <span>added a photo</span></dd>
            </div>
            <div class="photograph-image-area forthImg">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                </ul>
            </div>
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
        </div>
        
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams">John smith <span> added a photo </span></dd>
            </div>
            <div class="photograph-image-area fihthImg">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li>
                    <span class="counter">+1</span>
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a>
                    </li>
                </ul>
            </div>
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
        </div>
        
        <div class="feeds-block-area">
            <div class="photographer-pics-area">
                <span class="pics"><img src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></span>
                <dd class="nams">John smith <span> added a photo </span></dd>
            </div>
            <div class="photograph-image-area sixthImg">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a></li>
                    <li>
                    <span class="counter">+4</span>
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/img1.jpg"></a>
                    </li>
                </ul>
            </div>
            <div class="photographer-comemt-area">
                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Like</a>
                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>Comment</a>
            </div>
        </div>
        
    </div>
</div>
