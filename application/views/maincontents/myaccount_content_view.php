<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Bind your memory once</h2>
        </div>


        <div class="row">
            <div class="col-md-3">
                <ul class="tab_url">
                    <li class="tab_url_li"><a href="<?php echo $album_create_url; ?>">Create album</a></li>
                    <li class="tab_url_li"><a href="javascript:void(0);">List album</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/edit_profile'; ?>">Edit Profile</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/change_password'; ?>">Change Password</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'auth/logout'; ?>">Logout</a></li>
                </ul>
            </div>
            <div class="col-md-9">

            </div>

        </div>



    </div>
</section>