<div class="content-area">
    <div class="container">
    <div class="row">
        <div class="login-area">
            <div class="col-md-8 col-xs-12">
                <h3>Trusted by photographers worldwide</h3>
                <p>Photographers can create their album online and add photos from any source & customize.</p>
            </div>
            
            <div class="col-md-4 col-xs-12">
                <a href="#" class="logsBtn">REGISTER</a>
                <a href="#" class="logsBtn">Login</a>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="photographer-area">
    <div class="container">
        <div class="col-md-6 col-xs-12">
            <div class="photoSlider">
                <div>
                    <div class="potoslide"><img src="<?php echo base_url(); ?>assets/images/2.jpg" alt=""></div>
                </div>
            </div>
        </div>
        
        <div class="col-md-6 col-xs-12">
            <div class="poto-txt-contener">
                <span>wELCOME TO CLICKER</span>
                <h3>WE CREATE THE BEST EXPERIENCE <br>JUST FOR YOU</h3>
                <p>Our award-winning templates are the most beautiful way to present store your memories online. Stand out with a professional website, portfolio, or online store.we denounce with righteous indignation and dislike men who are so beguiled and demoralized
                </p>
                
                <a href="javascript:void(0);" class="logsBtn" data-toggle="modal" data-target="#invite_your_photographer">invite your photographer</a>
                <a href="javascript:void(0);" class="logsBtn">choose your photographer</a>
                
            </div>
        </div>
        
    </div>
</div>

<div class="templete-design-area">
    <div class="left-contener">
        <div class="col-md-10 col-xs-12 pull-right">
            <div class="templete-txt-contener">
                <span>wELCOME TO CLICKER</span>
                <h3>Choose form various unique <br>templete design</h3>
                <p>Our award-winning templates are the most beautiful way to present store your memories online. Stand out with a professional website, portfolio, or online store.we denounce with righteous indignation and dislike men who are so beguiled and demoralized
                </p>
                
                <a href="#" class="logsBtn">select your templete</a>
                
            </div>
        </div>
    </div>
    
    <div class="right-contener">
        <div class="templete-slider">
        <a href="#" class="left_arrow"><img src="<?php echo base_url(); ?>assets/images/arw2.png" alt=""></a>
        <a href="#" class="right_arrow"><img src="<?php echo base_url(); ?>assets/images/arw1.png" alt=""></a>
            <div class="temple-slide-area">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider2.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider2.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/slider2.jpg" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="our-client-feedback-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="client-txt-contener">
            <span>wELCOME TO CLICKER</span>
            <h3>Our Client Stream</h3>
        </div>
    </div>
    </div>
    </div>

    <div class="feedbakslider">
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image1.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image2.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image1.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image2.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image1.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
        <div>
            <div class="client-area">
                <div class="img-area">
                    <div class="img-overlay"><a href="#" class="viewMoreBtn">view story</a></div>
                    <img src="<?php echo base_url(); ?>assets/images/image2.jpg" alt="">
                </div>
                <div class="client-txt-area">
                    <h3>Rijul smita</h3>
                    <p>Lorem Ipsum industry</p>
                </div>
            </div>
        </div>
        
    </div>
</div>


<div class="modal fade" id="invite_your_photographer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="pg_invite_form">
                    <form id="frmSendInvitation" name="frmSendInvitation" action="#" enctype="multipart/form-data" role="form">
                        <div class="form-group">
                            <input type="text" class="form-control validate[required]" id="visitor" name="visitor" placeholder="Enter your name/email/phone no." />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control validate[required,custom[email]]" id="pg_email" name="pg_email" placeholder="Enter your photographer email." />
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <span class="text_msg" style="display: none;"></span>
                    </form>
                </div>
                <div class="ajax_loader" style="display: none;"> 
                    <img src="<?php echo base_url(); ?>assets/images/ajax_loader.gif" alt="">
                </div>

            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link type="text/css" rel="stylesheet" href="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css" />
<!-- LOAD JQUERY VALIDATION ENGINE SCRIPT -->
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js"></script>
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js"></script>
<script>
    jQuery("#frmSendInvitation").validationEngine('attach', {
        /*onValidationComplete: function(form, status){
            alert("The form status is: " +status+", it will never submit");
        },*/
        promptPosition : "bottomLeft", autoPositionUpdate : true
    });
    $(document).ready( function() {

        $("#frmSendInvitation").on("submit", function(e){
            e.preventDefault();
            
            var _currElem   = $(this);
            var visitor     = $('#visitor').val();
            var pg_email    = $('#pg_email').val();
            if( visitor.trim() == '' ||  pg_email.trim() == '' ) {
                /*$('.pg_invite_form').fadeIn(400, function(){
                    $('.ajax_loader').fadeOut(400);
                });*/
                return false;
            }
            $('.pg_invite_form').fadeOut(400, function(){
                $('.ajax_loader').fadeIn(400);
            });

            $.ajax({
                url     : '<?php echo base_url() . $ctrl_name . "ajax_photographer_send_invitation" ?>',
                type    : 'POST',
                data    : { 'visitor' : visitor,  'pg_email' : pg_email},
                dataType: 'html',
                complete: function(){

                },
                success : function(data) {
                    var result = $.parseJSON(data);
                    //$('div.ajax_loader').css({"display":"none"});
                    if(result.status)
                    {
                        _currElem.find(".text_msg").html(result.result).removeClass("ajax-error-msg").addClass("ajax-success-msg").fadeIn(600);

                        //$('.ajax_loader').hide(400, function(){
                            $('.pg_invite_form').fadeIn(400);
                            $('#invite_your_photographer').modal('toggle');
                            swal("Invitation send", '', "success");
                        //});
                    } 
                    else 
                    {
                        
                        $('.pg_invite_form').fadeIn(400, function(){
                            $('.ajax_loader').fadeOut(400);
                            _currElem.find(".text_msg").html(result.result).removeClass("ajax-success-msg").addClass("ajax-error-msg").fadeIn(600);
                        });

                        //$('#invite_your_photographer').modal('toggle');
                       /* swal({
                            title: "Sweet!",
                            text: result.result,
                            icon: "error",
                            timer: 3000,
                        }).then((value) => {
                            switch (value) {

                                case "defeat":
                                swal("Pikachu fainted! You gained 500 XP!");
                                break;

                                case "catch":
                                swal("Gotcha!", "Pikachu was caught!", "success");
                                break;

                                default:
                                swal("Got away safely!");
                            }
                        });*/
                    }
                },
                error: function(error) {
                    alert(error);
                }
            });
        });



        $('#invite_your_photographer').on('show.bs.modal', function (e) {
            
            var modal = $(this)
            
            modal.find('form').trigger('reset');
            modal.find('.modal-body > .ajax_loader').css({"display":"none"});
            modal.find(".modal-body .text_msg").html('').removeClass("ajax-success-msg ajax-error-msg");

        });


    });
</script>