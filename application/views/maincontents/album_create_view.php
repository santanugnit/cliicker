<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Create your new album</h2>
        </div>
        <?php echo ( isset($message) )? $message : ""; ?>
        <form name="formCreateAlbum" id="formCreateAlbum" action="<?php echo $album_create_url; ?>" method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-lg-8 col-lg-offset-2">
                    <div class="box">
                        <div class="box-body">
                            <div class="body-head">
                                <p><span>*</span> fields are mandatory</p>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Album Name <span class="required_class"> *</span><small>Enter a album name</small></label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="album_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('album_name'); ?>" placeholder="Enter a album name" />
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Album Description <small>Enter a short description</small></label>
                                </div>
                                <div class="col-md-9">
                                    <textarea name="short_description" class="form-control control-width-large" placeholder="Enter a short description" rows="5"><?php echo $this->input->post('short_description'); ?></textarea>
                                    <br>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Introduction text <small>Album short introduction text.</small></label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="intro_text" class="form-control control-width-large" value="<?php echo $this->input->post('intro_text'); ?>" placeholder="Album short introduction text..">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Subdomain name <span class="required_class">*</span> <small>Preferred subdomain name</small></label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" id="preferred_subdomain_name" name="preferred_subdomain_name" class="form-control control-width-medium validate[required]" value="<?php echo $this->input->post('preferred_subdomain_name'); ?>" placeholder="" data-previous_value="" style="display: inline-block;"/> <span class="form-control-static" style="display: inline-block;	color: gray; letter-spacing: 1px;	padding-left: 5px; ">.cliicker.com</span> <span class="text_msg" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Album price<span class="required_class"></span><small>Enter price for album creation</small></label>
                                </div>
                                <div class="col-md-9">
                                    <label for="" class="text_lbl">Rs. 15,000/- </label>
                                    <input type="hidden" name="album_price" class="form-control control-width-small" value="15000" />
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Max storage<small>Max storage are in settings section.</small></label>
                                </div>
                                <div class="col-md-9">
                                    <label for="" class="text_lbl">1073741824 bytes (1 GB)</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Google drive link<small>Google drive link</small></label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="google_drive_link" class="form-control control-width-large" value="<?php echo $this->input->post('google_drive_link'); ?>" placeholder="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Dropbox link<small>Dropbox link of a files.</small></label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="dropbox_link" class="form-control control-width-large" value="<?php echo $this->input->post('dropbox_link'); ?>" placeholder="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-footer">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <input type="submit" class="btn save_btn" value="Next" name="SaveNext">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>


            </div>
        </form>
    </div>
</section>

<link type="text/css" rel="stylesheet" href="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css" />
<!-- LOAD JQUERY VALIDATION ENGINE SCRIPT -->
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js"></script>
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js"></script>
<script>
    jQuery("#formCreateAlbum").validationEngine('attach', {
        /*onValidationComplete: function(form, status){
            alert("The form status is: " +status+", it will never submit");
        },*/
        promptPosition : "bottomLeft", autoPositionUpdate : true
    });

    $(document).ready( function() {

        $("#preferred_subdomain_name").on("focusout", function(e){
            var _currElem = $(this);
            var previous_value  = _currElem.attr('data-previous_value');
            if ( previous_value.trim() != '' && previous_value == _currElem.val() )
            {
                return false;
            }
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-error-msg");
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-success-msg");
            var preferred_subdomain_name = $(this).val();

            if( preferred_subdomain_name.trim() == '' ) {
                _currElem.attr('data-previous_value', "");
                return false;
            }

            $.ajax({
                url		: '<?php echo base_url() . $ctrl_name . "/ajax_client_check_unique_subdomain_name" ?>',
                type	: 'POST',
                data	: { 'preferred_subdomain_name' : preferred_subdomain_name,  },
                dataType: 'html',
                complete: function(){

                },
                success	: function(data) {
                    var result = $.parseJSON(data);
                    if(result.status){
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-error-msg").addClass("ajax-success-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    } else {
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-success-msg").addClass("ajax-error-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    }
                },
                error: function(error) {
                    alert(error);
                }
            });
        });


    });

</script>