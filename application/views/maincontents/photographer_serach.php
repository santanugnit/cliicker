
<div class="main-container nomargin">
    <div class="container">
        <div class="row">
        
            <div class="col-md-3">
                <div class="left-side-bar nomargin">
                    
                    <div class="search-area">
                        <div class="form-group">
                            <input type="text" class="form-control" id="pg_name" placeholder="Search By Name" name="pg_name" value="" /><button class="form-butn" id="pg_name_srch"> </button>
                        </div>
                    </div>
                    
                    <div class="buget-area">
                        <h2 class="subHead-sidebar">search by budget</h2>                        
                        <div class="range-slider">
                            <p>Max :<span id="amount">Rs.5000 </span>
                                <span id="amount2">Rs.120000 </span>
                                <input type="hidden" id="min_budget" name="min_budget" value="5000" />
                                <input type="hidden" id="max_budget" name="max_budget" value="120000" />
                            </p>
                            <div id="slider-range"></div>
                        </div>
                    </div>
                    
                    <div class="sidebar-rating-area">
                        <h2 class="subHead-sidebar">search by Ratings</h2>                        
                        <div class="show-rating">
                        <?php 
                            for( $j = 1; $j <= 5; $j++ )
                            {  
                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        ?>
                            <label for="<?php echo $f->format($j); ?>_star">
                                <input type="checkbox" class="pg_ratings" name="pg_rating[]" value="<?php echo $j; ?>" id="<?php echo $f->format($j); ?>_star" style="visibility: visible;" />
                            <?php for( $i = 1; $i <= 5; $i++ ){ ?>                            
                                <span class="stars <?php echo ( $i <= $j )? "reviewstart" : ""; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                            <?php } ?>
                            </label>
                        <?php } ?>                         

                        </div>
                    </div>
                    
                    <div class="buget-area">
                        <h2 class="subHead-sidebar">search by reviews</h2>                        
                        <div class="range-slider">
                            <p>Reviews<span id="review">Rs.0</span>
                              <span id="review2">Rs.300</span>
                            </p>
                            <div id="slider-review-range"></div>
                        </div>
                    </div>
                    
                    <div class="buget-area">
                        <h2 class="subHead-sidebar">search by Locatrion</h2>                        
                        <div class="search-area">
                            <div class="form-group">
                                <input type="text" class="form-control" id="pg_location" name="pg_location" placeholder="Search By Address" value="" />
                                <button class="form-butn" id="pg_loc_srch"> </button>
                            </div>
                        </div>                        
                        <div class="map_area">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3683.9519499445987!2d88.42523121449854!3d22.580900385178406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a0275bbbdf15555%3A0x7e4724d1fc0a0d69!2sCodopoliz!5e0!3m2!1sen!2sin!4v1528193638384" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="col-md-9">
                <div class="photographer-listing-area">
                    <ul id="pg_list_content">
        <?php /*echo "<pre>"; print_r($pg_lists); echo "</pre>";*/
            if( isset($pg_lists) && !empty($pg_lists) )
            {
                foreach ($pg_lists as $pg_arr) 
                {
                    $pg_arr = (array)$pg_arr;
        ?>
                        <li>
                            <div class="ph-list-area">
                                <div class="ph-list-area-img"><img src="<?php echo $pg_arr['img_url'] . $pg_arr['featured_image']; ?>" alt="<?php echo $pg_arr['first_name']; ?> <?php echo $pg_arr['last_name']; ?>"></div>
                                <h2 class="ph-name"><?php echo $pg_arr['first_name']; ?> <?php echo $pg_arr['last_name']; ?></h2>
                                <div class="star-rating">
                                    <div class="show-rating">
                                <?php for( $i = 1; $i <= 5; $i++ ){ ?>
                                        <span class="stars <?php echo ( $i <= (int)$pg_arr['avg_rating'] )? "reviewstart" : ""; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                                <?php } ?>
                                    </div>
                                    
                                    <div class="album-counter">
                                        <p><span><?php echo $pg_arr['no_albums']; ?> </span> Albums</p>
                                    </div>
                                </div>
                                <div class="button-area">
                                    <a href="javascript:void(0);" class="myBtn">view profile</a>
                                    <a href="javascript:void(0);" class="myBtn" data-toggle="modal" data-target="#contact_your_photographer">Contact</a>
                                </div>
                            </div>
                        </li>
        <?php 
                }
            } 
            else 
            {
        ?>
                        <li><div class="ph-list-area"><h3>No Photographer found.</h3></div></li>
        <?php   
            }

        ?>

                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>



<div class="modal fade" id="contact_your_photographer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="pg_invite_form">
                    <form id="frmCntPg" name="frmCntPg" action="#" enctype="multipart/form-data" role="form">
                        <div class="form-group">
                            <input type="text" class="form-control validate[required]" id="v_name" name="v_name" placeholder="Enter your name." />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control validate[required,custom[email]]" id="v_email" name="v_email" placeholder="Enter your email-id." />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control validate[required]" id="v_contact" name="v_contact" placeholder="Enter your phone no." />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control validate[required]" id="v_message" name="v_message" placeholder="Enter some text" />
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <span class="text_msg" style="display: none;"></span>
                    </form>
                </div>
                <div class="ajax_loader" style="display: none;"> 
                    <img src="<?php echo base_url(); ?>assets/images/ajax_loader.gif" alt="">
                </div>

            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>


<script> 
    $(document).ready(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 5000,
            max: 120000,
            values: [ 5000, 120000 ],
            slide: function( event, ui ) {
                // $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $( "#amount" ).text("Rs." + ui.values[ 0 ]);
                $( "#amount2" ).text("Rs." + ui.values[ 1 ]);
                $( "#min_budget" ).val(ui.values[ 0 ]);
                $( "#max_budget" ).val(ui.values[ 1 ]);
            }
        });
        
        /* When click on serach by name */
        $("#pg_name_srch").on("click", function(e){
            photographer_serach();
        });

        $( "#slider-range" ).on( "slidestop", function( event, ui ) {
            photographer_serach();
        } );

        /* When click on serach by name */
        $(".pg_ratings").on("click", function(e){
            photographer_serach();
        });

        /* When click on serach by location */
        $("#pg_loc_srch").on("click", function(e){
            photographer_serach();
        });


    });


    function photographer_serach()
    {
        var dataString  = '';
        var pg_name     = $("#pg_name").val();
        if( $.trim(pg_name).length > 0 )
        {
            dataString      += '&pg_name=' + pg_name;
        }

        var pg_budget   = [];
        pg_budget[0]    = $("#min_budget").val();
        pg_budget[1]    = $("#max_budget").val();
        dataString      += '&pg_budget=' + pg_budget;

        var count_checked = $("input[name='pg_rating[]']:checked").length;
        if( count_checked ) 
        {
            var pg_rating = [];
            $.each($("input[name='pg_rating[]']:checked"), function(){
                pg_rating.push($(this).val());
            });
            dataString  += '&ratings=' + pg_rating;
        }

        var pg_location     = $("#pg_location").val();
        if( $.trim(pg_location).length > 0 )
        {
            dataString      += '&pg_location=' + pg_location;
        }
        /*
        console.log("pg budget : "+ pg_budget);
        console.log("pg rating : "+ count_checked);
        console.log("pg string : "+ dataString);
        */
        $.ajax({
                url     : '<?php echo base_url() . $ctrl_name . "ajax_photographer_filter" ?>',
                type    : 'GET',
                data    : dataString,
                dataType: 'html',
                complete: function(){

                },
                success : function(result) {
                    console.log(result);
                    var result = $.parseJSON(result);
                    /*//$('div.ajax_loader').css({"display":"none"});*/
                    if(result.status)
                    {
                        //_currElem.find(".text_msg").html(result.result).removeClass("ajax-error-msg").addClass("ajax-success-msg").fadeIn(600);
                        $("#pg_list_content").html(result.result);
                    }
                },
                error: function(error) {
                    alert(error);
                }
            });
        
    }

</script>