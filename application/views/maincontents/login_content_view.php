<div class="login-logo-area"><img src="<?php echo base_url(); ?>assets/images/logo-login.png"></div>
<div class="login-foter-area"><p>Copyright 2018. All rights reserved</p></div>
<div id="clientsSlider"></div>
<div class="ovarLay-login-bg"></div>

<div class="login-back-area">

    <div class="loginBtn-area">

        <ul>
            <li>
                <a class="<?php echo ( !isset($func_name) || in_array(trim($func_name), array(NULL, '', 'index', 'login')) )? "activeLi" : ""; ?>" data-name="login" href="javascript:void(0);">
                    <span><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    Login
                </a>
            </li>
            <li>
                <a class="sgn <?php echo ( isset($func_name) && in_array($func_name, array('signup')) )? "activeLi2" : ""; ?>" href="javascript:void(0);" data-name="signup">
                    <span><i class="fa fa-plus" aria-hidden="true"></i></span>
                    Signup
                </a>
            </li>
        </ul>
    </div>

    <div class="login-img-area">
        <img src="<?php echo base_url(); ?>assets/images/login-img.jpg">
    </div>

    <div class="all-login-area">
        <!-- login content area -->
        <div class="login-bg" style="display: <?php echo ( !isset($func_name) || in_array(trim($func_name), array(NULL, '', 'index', 'login')) )? "block" : "none"; ?>">

            <h2>Welcome to Clicker </h2>
            <span>Login here to start</span>

            <div class="login-forms">
                <form name="loginFrm" id="loginFrm" action="<?php echo base_url(); ?>auth/login" method="post">
                    <div class="form-group">
                        <span class="logIcon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input class="form-control" id="login_email" placeholder="Email id" name="email" type="email" value="<?php echo $this->input->post('email'); ?>" />
                    </div>

                    <div class="form-group">
                        <span class="logIcon"><i class="fa fa-key" aria-hidden="true"></i></span>
                        <input class="form-control" id="login_password" placeholder="Password" name="password" type="password" value="" />
                    </div>

                    <div class="btn-area">
                        <label>
                            <span class="subIcon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                            <input class="btn btn-default submitBtn" id="pto_login" name="pto_login" value="Login" type="submit">
                        </label>
                    </div>
                </form>
                <?php echo ( isset($message) )? $message : ""; ?>
            </div>

            <a class="forgotpass" href="javascript:void(0);">Forgot Password</a>
            <p class="singin">Don't have an account? <a href="javascript: void(0);" id="signInLink">Sign In</a></p>
        </div>

        <!-- sign up content area -->
        <div class="signUp" style="display: <?php echo ( isset($func_name) && in_array($func_name, array('signup')) )? "block" : "none"; ?>">
            <p>Signup for create account</p>
            <div class="login-forms">
                <form name="signupFrm" id="signupFrm" action="<?php echo base_url(); ?>auth/signup" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <input class="form-control validate[required]" id="name" placeholder="Name" name="name" type="text" value=""  />
                    </div>

                    <div class="form-group">
                        <input class="form-control validate[required]" id="email" placeholder="Email" name="email" type="email" value="" />
                    </div>

                    <div class="form-group">
                        <input class="form-control validate[required]" id="contact_number" placeholder="Contact No" name="contact_number" type="text" value="" />
                    </div>

                    <div class="col-md-5 pad-left-0">
                        <div class="form-group">
                            <select class="form-control validate[required]" name="gender">
                                <option value="">Gender</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-7 pad-right-0">
                        <div class="form-group">
                            <div class="upload-btn-wrapper">
                                <button class="uploadbtn">Upload 3 Samples</button>
                                <input type="file" id="featured_image" name="featured_image[]" multiple />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input class="form-control validate[required]" id="password" placeholder="Password" name="password" type="password" value="" />
                    </div>

                    <div class="form-group">
                        <input class="form-control validate[required,equals[password]]" id="conf_pass" placeholder="Confirm Password" name="conf_pass" type="password" value="" />
                    </div>

                    <div class="btn-area">
                        <label>
                            <span class="subIcon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                            <input class="btn btn-default submitBtn" id="sign_up_btn" name="sign_up_btn" value="Sign up" type="submit" />
                        </label>
                    </div>
                </form>
                <?php echo ( isset($message_signup) )? $message_signup : ""; ?>
            </div>
        </div>
        <!-- sign up content area end -->

    </div>

</div>
<!-- LOAD JQUERY VALIDATION ENGINE STYLESHEET -->
<link type="text/css" rel="stylesheet" href="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css" />
<!-- LOAD JQUERY VALIDATION ENGINE SCRIPT -->
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js"></script>
<script src="http://localhost/cliicker/assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js"></script>
<script>
    jQuery("#signupFrm").validationEngine('attach', {
        /*onValidationComplete: function(form, status){
            alert("The form status is: " +status+", it will never submit");
        },*/
        promptPosition : "bottomLeft", autoPositionUpdate : true
    });

    $('#featured_image').change(function(){
        if(this.files.length > 3){
            this.value = '';
            alert('Too many files. Maximum 3 image file you can upload.');
        }

    });

</script>