<?php
function buildMenu( $parentId = 0, $ctgLists = array() )
{
    $html = '';
    if( isset($ctgLists[$parentId]) ) 
    {
        $html .= '<ul class="lavel_'.$parentId.'">';

        foreach ( $ctgLists[$parentId] as $key => $childId )
        {
            $html .= "<li>"; 
            $html .= '<input type="checkbox" class="srch-cat srch_chield_cat_'.$parentId.'" id="community_'.$parentId.'_'.$childId['id'].'" name="community[]" value="'.$childId['id'].'" />
                    <label id="label_id'.$childId['id'].'" for="community_'.$parentId.'_'.$childId['id'].'" class="check-container"> '. $childId['cat_name'] .'
                        
                        <span class="checkmark"></span>
                    </label>';            
            $html .= buildMenu($childId['id'], $ctgLists ); 
            $html .=  "</li>";
        }
        $html .= "</ul>";
    }
    return $html;  
}

?>
    <div class="col-md-3">
        <div class="left-side-bar nomargin">
            <form id="filterFrm" name="filterFrm" action="#" onsubmit="return false;">
                <div class="search-area">
                    <div class="form-group">
                        <input type="text" class="form-control" id="s_name" name="s_name" placeholder="Search By Name" />
                    </div>
                </div>            
                <div class="buget-area">
                    <h2 class="subHead-sidebar">search by commounity</h2>

                    <div class="tempalate-catagory">
                    <?php 
                        /*echo "<pre>"; print_r($theme_cat_arr); echo "</pre>";*/
                        if( isset($theme_cat_arr) && !empty($theme_cat_arr) )
                        {
                            echo buildMenu(0, $theme_cat_arr); 
                        }
                    ?>
                    </div>                
                </div>
            </form> 
        </div>
    </div>


    <div class="col-md-9">
        <div class="templates-listing-area">
            <ul id="templates-listing-wrapper">
<?php
    if( isset($theme_lists) && !empty($theme_lists) )
    {
        foreach ( $theme_lists as $themeArr )
        {
            $themeArr = (array)$themeArr;
            $imageURL = base_url().'restserver/assets/uploaded_files/templates_image/medium/'
?>

                <li>
                    <div class="single-template">
                        <div class="temp-image">
                            <img src="<?php echo ( file_exists($themeArr['img_url'].$themeArr['theme_thumbnail_image']) ) ? $imageURL.$themeArr['theme_thumbnail_image'] : base_url()."assets/images/no-theme-image.jpeg" ; ?>" alt="" />
                            <div class="template-butns">
                                <a href="javascript:void(0);">VIEW</a>
                                <a class="pops-snd" type="button" data-theme-id="<?php echo $themeArr['id']; ?>" data-toggle="modal" data-target="#send-modal">SEND</a>
                            </div>
                        </div>
                        <div class="temp-caps">
                            <h4><?php echo $themeArr['theme_name']; ?></h4>
                        </div>
                    </div>
                </li>
<?php
        }
    }
?>               
            </ul>
        </div>
    </div>

    
<!-- ************ Select photographer *************** -->
<div class="modal fade my-send-modal" id="send-modal" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">SEND</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body my-send-modal-body">
                <div class="contact-popup-form">
                    <form name="tempSendFrm" id="tempSendFrm" action="#">
                        <input type="hidden" name="temptale_id" value="" />
                        <div class="form-group">
                            <input type="text" class="form-control tooltip-appear" id="c_name" name="c_name" placeholder="Name*" value="" required/>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control tooltip-appear" id="c_email" name="c_email" placeholder="Email*" value="" />
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control tooltip-appear" id="c_phone_no" name="c_phone_no" placeholder="Contact no." value="" />
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" id="c_massage" name="c_massage" placeholder="Description"></textarea>
                        </div>

                        <div class="form-group">
                            <select class="form-control custom_select2 tooltip-appear" style="width: 100%;" id="choose_reg_pg" name="reg_photographer[]" multiple="multiple">
                        <?php 
                            if( isset($pg_lists) && !empty($pg_lists) ) {
                                foreach ( $pg_lists as $pgArr ) {
                                    $pgArr = (array)$pgArr;
                                    $imageURL = base_url().'assets/images/';
                                    $profileImg     = trim($pgArr['featured_image']) != '' ? $pgArr['featured_image'] : "no_profile_img.jpeg";
                        ?>
                                <option data-img="<?php echo ( CliickerfileExists($pgArr['img_url'] .$profileImg))? $pgArr['img_url'] .$profileImg : $imageURL."no_profile_img.jpeg"; ?>" data-rating="<?php echo (int)$pgArr['avg_rating']; ?>" data-album_cnt="<?php echo (int)$pgArr['no_albums']; ?>" value="<?php echo $pgArr['id']; ?>"><?php echo $pgArr['first_name']; ?></option>
                        <?php 
                                }
                            }
                        ?>
                            </select>
                        </div> 


                        <div class="form-group">
                            <input type="submit" id="sendBtn" class="cardBtn" value="send" />
                        </div>

                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ************ Select photographer *************** -->
<script>
(function( $ ) {
    /* On - page document ready  start */
    $(document).ready(function() {

        $(document).on("change", ".srch-cat", function(e) {
            
            var checked     = $(this).prop("checked"),
            container       = $(this).parent(),
            siblings        = container.siblings();
            
            container.find('input[type="checkbox"]').prop({
                indeterminate: false,
                checked: checked
            });
            
            function checkSiblings(el) 
            {
                var parent  = el.parent().parent(),
                    all     = true;
                    
                el.siblings().each(function() {
                    return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
                });
                
                if (all && checked) 
                {
                    //alert(all+'----all && checked');
                    parent.children('input[type="checkbox"]').prop({
                        indeterminate: false,
                        checked: checked
                    });
                    checkSiblings(parent);
                } else if (all && !checked) {
                    //alert(all+'----all && !checked');
                    parent.children('input[type="checkbox"]').prop("checked", checked);
                    parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
                    checkSiblings(parent);
                } else {
                    
                    el.parents("li").children('input[type="checkbox"]').prop({
                        indeterminate: true,
                        checked: true
                    });
                }
            }
            checkSiblings(container);

            //clone the form, we don't want this to impact the ui
            var $form = $('#filterFrm').clone();
            // remove (actually disabled) unchecked checkbox from form clone. 
            removeParam( $form.find("input[type=checkbox]:not(:checked)") );
            // serialize the clone form and store in varible.
            var data = $form.serialize();

            //var arr_checked_values = $('input[type=checkbox]:checked').map(function(){return this.value}).get();
            //var arr_indeterminate_values = $('input[type=checkbox]:indeterminate').map(function(){return this.value}).get();

            $.ajax({
                url     : '<?php echo base_url() . $temp_filter; ?>',
                type    : 'GET',
                data    : data,
                dataType: 'html',
                beforeSend: function(xhr){
                    
                },
                complete: function(){

                },
                success : function(result) {
                    /*console.log(result);*/
                    var result = $.parseJSON(result);
                    if( result.status ) {

                        /*$("#templates-listing-wrapper").html(result.result);*/
                        $("#templates-listing-wrapper").hide().html(result.result).fadeIn(400);
                    } else {
                        $("#templates-listing-wrapper").hide().html(result.result).fadeIn(400);
                    }        
                    
                },
                error: function(error) {
                    alert(error);
                }
            });

        });

        
        var FormValidetion;

        /* Photographer select2 dropdown start */
        $('#choose_reg_pg').select2({

            templateResult: formatState,
            
        }).on("change", function (e) {
            $(this).valid();
        });            


        $('#send-modal').on('hide.bs.modal', function (event) {
            $(this).removeData('bs.modal');
            $('#tempSendFrm').trigger('reset');
            $("#choose_reg_pg").val('').trigger('change');
            //$('#tempSendFrm').find('span.error').remove();
            //$('#tempSendFrm').find('.form-control').removeClass('error');
            $('.tooltip-appear').tooltipster('close');
            FormValidetion.resetForm();        
        });


        $('#send-modal').on('shown.bs.modal', function (event) {
            //FormValidetion.resetForm();            
            //$('.tooltip-appear').tooltipster('open');
            var button      = $(event.relatedTarget) // Button that triggered the modal
            var themeID     = button.data('theme-id') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('input:hidden[name="temptale_id"]').val(themeID);
        });


        $('.tooltip-appear').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'right'
        });


        /* Form validation start */
        FormValidetion = $('#tempSendFrm').validate({

            errorPlacement: function (error, element) { 
                var lastError   = $(element).data('lastError'),
                    newError    = $(error).text();
                
                //$(element).data('lastError', newError);
                         
                if ( newError !== '' && newError !== lastError )
                {
                    if( element.hasClass('select2-hidden-accessible') )
                    {
                        $(element).siblings('.select2-container').addClass('tooltip-appear').tooltipster({
                            trigger: 'custom',
                            onlyOne: false,
                            position: 'right'
                        });
                        
                        $(element).siblings('.select2-container').tooltipster('content', newError);
                        $(element).siblings('.select2-container').tooltipster('show');
                        
                    } else {
                        
                        $(element).tooltipster('content', newError);
                        $(element).tooltipster('show');  
                    }                    
                }
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
                //$(element).siblings('.select2-container').tooltipster('hide');
            },
            //errorElement: 'span',
            errorClass: 'error',
            rules: {
                // simple rule, converted to {required:true}
                c_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                // compound rule
                c_email: {
                    required: true,
                    email: true
                },
                /*c_phone_no: "required",*/
                'reg_photographer[]' : { required: true },
            },
            highlight: function (element, errorClass, validClass) { 
                
            },
            unhighlight: function (element, errorClass, validClass) {
                
            },
            submitHandler: function (form) { // for demo
                $.ajax({
                    url     : '<?php echo base_url() . $temp_suggest ?>',
                    type    : 'POST',
                    data    : $("#tempSendFrm").serialize(),
                    dataType: 'html',
                    complete: function(){

                    },
                    success : function(result) {
                        console.log(result);
                        var result = $.parseJSON(result);
                        $('#send-modal').modal('hide');                        
                        if(result.status)
                        {
                            swal(result.result, {
                                icon: "success",
                                button: {
                                    text: "ok",
                                },
                            });
                        } 
                        else 
                        {
                            swal(result.result, {
                                icon: "error",
                                button: {
                                    text: "ok",
                                },
                            });
                        }
                    },
                    error: function(error) {
                        alert(error);
                    }
                });
                return false;
            }
        });/* Modal form validation end */




    }); /* Document.ready end */

    function removeParam(p) {
        $(p).attr("disabled",true);
    }

    function formatState (state) {
        
        if (!state.id) { return state.text; }
        //console.log("Hi.."+state.id+' -- '+ $(state.element).data('img')+' -- '); 
        var txtRateing = '';
        for ( var i = 1; i <= 5; i++ ) {
            txtRateing += '<span class="stars '+ ( i <= $(state.element).data('rating') ? "reviewstart" : "" ) +'"><i class="fa fa-star" aria-hidden="true"></i></span>';
        }
        var $state = $(
        '<div class="custom-select2-dropdown"> <div class="pf-photo"><img src="'+ $(state.element).data('img') +'" alt="'+ state.text +'"/></div> <div class="pf-desc"> <h4>'+ state.text +'</h4> <p>'+ txtRateing +'</p> <dd>'+ $(state.element).data('album_cnt') +' Album(s)</dd></div> </div>'
        );
        return $state;
    }

})( jQuery );
</script>

<style>
    .error { color: #c80000; border: 1px solid #ccc; }
    span.error { position: absolute; }

    #templates-listing-wrapper li.no_found_wrapper{ width: 100%; position: relative; }
    #templates-listing-wrapper li.no_found_wrapper .not_found_content {
        display: block;
        background: #fff1e7;
        border: 1px solid #f59855;
        padding: 10px;
        text-align: center;
    }
    #templates-listing-wrapper li.no_found_wrapper .not_found_msg {
        font-size: 22px;
        text-align: center;
        display: block;
    }
    #templates-listing-wrapper li.no_found_wrapper .suggestion_txt {
        display: inline-block;
        text-align: center;
        font-size: 11px;
        font-family: sans-serif;
        font-weight: 500;
    }

</style>


