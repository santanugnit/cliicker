<?php
    $occationArr = array("1" => "Marriage Ceremony" );
?>

<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Bind your memory once</h2>
        </div>


        <div class="row">
            <div class="col-md-3">
                <ul class="tab_url">
                    <li class="tab_url_li"><a href="<?php echo $album_create_url; ?>">Create album</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/album_lists'; ?>">List album</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/edit_profile'; ?>">Edit Profile</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/change_password'; ?>">Change Password</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'auth/logout'; ?>">Logout</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Your album lists</div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">SL. No</th>
                            <th scope="col">Album Code</th>
                            <th scope="col">Album Info</th>
                            <th scope="col">Storage</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                <?php
                    if ( isset($alb_detls) && !empty($alb_detls) ) {
                        $i = 1;
                        foreach ( (array)$alb_detls as $albID => $albArr ) {
                           //echo "<pre>"; print_r((array)$albArr); echo "</pre>";
                            $albArrNew = (array)$albArr;
                ?>
                            <tr>
                                <th scope="row"><?php echo $i;?>.</th>
                                <td>
                                    <label for="">Album Code: <small><?php echo ($albArrNew["album_code"])? $albArrNew["album_code"] : "***"; ?></small></label><hr>
                                    <label for="">Album Name: <small><?php echo ($albArrNew["album_name"])? $albArrNew["album_name"] : "***"; ?></small></label>
                                    <hr>
                                    <label for="">Intro text: <small><?php echo ($albArrNew["intro_text"])? $albArrNew["intro_text"] : "***"; ?></small></label>

                                </td>
                                <td>
                                    <label for="">Occation Name: </label><small><?php echo ( array_key_exists( $albArrNew["occasion_id"], $occationArr) )? $occationArr[$albArrNew["occasion_id"]] : "***"; ?></small><hr>
                                    <label for="">Description: <small><?php echo ($albArrNew["short_description"])? $albArrNew["short_description"] : "***"; ?></small></label><hr>
                                    <label for="">Created On: <small><?php echo ($albArrNew["creation_datetime"])? date("d/m/Y", strtotime($albArrNew["creation_datetime"])) : "***"; ?></small></label>
                                </td>
                                <td>
                                    <label for="">Max storage allow: </label><small><?php echo ( $albArrNew["max_storage_allocate"] )? ($albArrNew["max_storage_allocate"]/(1024*1024*1024)) : "***"; ?>GB</small><hr>
                                    <label for="">Use Space: <small><?php echo ($albArrNew["space_usage"])? (int)($albArrNew["space_usage"]/(1024*1024)) : "***"; ?>Mb</small></label>
                                    <hr>
                                    <label for="">Remaining space: <small><?php echo (int)(($albArrNew["max_storage_allocate"] - $albArrNew["space_usage"])/(1024*1024)); ?>Mb</small></label>

                                </td>

                                <td>
                            <?php
                            if ( isset($albArrNew["album_file_detls"]) && !empty($albArrNew["album_file_detls"]))
                            {
                            ?>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Size</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                <?php
                                foreach ( $albArrNew["album_file_detls"] as $albFileArr ) {
                                    $albFileArrNew  = (array)$albFileArr;
                                ?>
                                        <tr>
                                            <td><span class="badge badge-primary"><?php echo $albFileArrNew["upload_file_name"]; ?></span></td>
                                            <td><span class="badge badge-warning"><?php echo round($albFileArrNew["upload_file_size"]/(1024*1024), 2); ?> Mb</span></td>
                                        </tr>
                                <?php
                                }
                                ?>
                                        </tbody>
                                    </table>
                            <?php
                            }
                            ?>

                                </td>
                            </tr>
                <?php
                            $i++;
                        }
                    }
                ?>
                        </tbody>
                    </table>
                </div>




            </div>

        </div>



    </div>
</section>