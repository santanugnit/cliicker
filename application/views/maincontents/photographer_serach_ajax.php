
        <?php /*echo "<pre>"; print_r($pg_lists); echo "</pre>";*/
            if( isset($pg_lists) && !empty($pg_lists) )
            {
                foreach ($pg_lists as $pg_arr) 
                {
                    $pg_arr = (array)$pg_arr;
        ?>
                        <li>
                            <div class="ph-list-area">
                                <div class="ph-list-area-img"><img src="<?php echo $pg_arr['img_url'] . $pg_arr['featured_image']; ?>" alt="<?php echo $pg_arr['first_name']; ?> <?php echo $pg_arr['last_name']; ?>"></div>
                                <h2 class="ph-name"><?php echo $pg_arr['first_name']; ?> <?php echo $pg_arr['last_name']; ?></h2>
                                <div class="star-rating">
                                    <div class="show-rating">
                                <?php for( $i = 1; $i <= 5; $i++ ){ ?>
                                        <span class="stars <?php echo ( $i <= (int)$pg_arr['avg_rating'] )? "reviewstart" : ""; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                                <?php } ?>
                                    </div>
                                    
                                    <div class="album-counter">
                                        <p><span><?php echo $pg_arr['no_albums']; ?> </span> Albums</p>
                                    </div>
                                </div>
                                <div class="button-area">
                                    <a href="javascript:void(0);" class="myBtn">view profile</a>
                                    <a href="javascript:void(0);" class="myBtn" data-toggle="modal" data-target="#contact_your_photographer">Contact</a>
                                </div>
                            </div>
                        </li>
        <?php 
                }
            } 
            else 
            {
        ?>
                        <li><div class="ph-list-area"><h3>No Photographer found.</h3></div></li>
        <?php   
            }

        ?>

                    