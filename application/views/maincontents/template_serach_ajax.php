    
<?php
    if( isset($theme_lists) && !empty($theme_lists) )
    {
        foreach ( $theme_lists as $themeArr )
        {
            $themeArr = (array)$themeArr;
            $imageURL = base_url().'restserver/assets/uploaded_files/templates_image/medium/'
?>

                <li>
                    <div class="single-template">
                        <div class="temp-image">
                            <img src="<?php echo ( file_exists($themeArr['img_url'].$themeArr['theme_thumbnail_image']) ) ? $imageURL.$themeArr['theme_thumbnail_image'] : base_url()."assets/images/no-theme-image.jpeg" ; ?>" alt="" />
                            <div class="template-butns">
                                <a href="javascript:void(0);">VIEW</a>
                                <a class="pops-snd" type="button" data-theme-id="<?php echo $themeArr['id']; ?>" data-toggle="modal" data-target="#send-modal">SEND</a>
                            </div>
                        </div>
                        <div class="temp-caps">
                            <h4><?php echo $themeArr['theme_name']; ?></h4>
                        </div>
                    </div>
                </li>
<?php
        }
    } else {
?>
            <li class="no_found_wrapper">
                <div class="not_found_content">
                    <span class="not_found_msg">No template found ..!</span>
                    <span class="suggestion_txt">Please select another category for batter serach.</span>
                </div>
            </li>
<?php 
    }
?>          
 

