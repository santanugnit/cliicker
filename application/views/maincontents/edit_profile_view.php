<section class="content-area bg-ground">
    <div class="container">

        <div class="content-heading">
            <span>lorem ipsum dolor sit</span>
            <h2>Bind your memory once</h2>
        </div>


        <div class="row">
            <div class="col-md-3">
                <ul class="tab_url">
                    <li class="tab_url_li"><a href="<?php echo $album_create_url; ?>">Create album</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/album_lists'; ?>">List album</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/edit_profile'; ?>">Edit Profile</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'dashboard/change_password'; ?>">Change Password</a></li>
                    <li class="tab_url_li"><a href="<?php echo base_url().'auth/logout'; ?>">Logout</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="row">

                    <?php echo ( isset($message))? $message :""; ?>

                    <form id="editProfileFrm" name="editProfileFrm" method="post" action="" enctype="multipart/form-data">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for=""> <strong><?php echo (isset($pg_detls["pg_clicker_code"]))? $pg_detls["pg_clicker_code"]:""; ?></strong> </label>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo (isset($pg_detls["first_name"]))? $pg_detls["first_name"]:""; ?>" placeholder="Enter first name" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo (isset($pg_detls["last_name"]))? $pg_detls["last_name"]:""; ?>" placeholder="Enter last name" />
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" disabled="disabled" value="<?php echo (isset($pg_detls["email"]))? $pg_detls["email"]:""; ?>" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Enter phone no." value="<?php echo (isset($pg_detls["contact_number"]))? $pg_detls["contact_number"]:""; ?>" />
                            </div>

                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="male" value="M" <?php echo (isset($pg_detls["gender"]) && $pg_detls["gender"] == "M")? 'checked' :""; ?> />
                                    <label class="form-check-label" for="male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="female" value="F" <?php echo (isset($pg_detls["gender"]) && $pg_detls["gender"] == "F")? 'checked' :""; ?> />
                                    <label class="form-check-label" for="female">Female</label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <textarea class="form-control" id="" rows="3" name="address"><?php echo (isset($pg_detls["address"]))? $pg_detls["address"]:""; ?></textarea>
                            </div>

                            <div class="form-group">
                                <div id="img_prev">
                            <?php
                                if ( isset($pg_detls['featured_image']) && $pg_detls['featured_image'] != "" )
                                {
                            ?>
                                    <div class="">
                                <?php
                                    echo img(array('id'=>'cat_img','src'=>'restserver/assets/uploaded_files/photographer_profile_image/medium/'.$pg_detls['featured_image'],'width' => 200));
                                ?><br>
                                        <span id="delete_profile_img">
                                        <!--<button id="delete_img" class="btn btn-rounded btn-default" type="button">Delete profile picture</button>-->
                                        </span>
                                    </div>
                            <?php
                                }
                            ?>
                                </div>
                                <input type="hidden" name="existing_featured_image" id="existing_featured_image" value="<?php echo (isset($pg_detls['featured_image']) && $pg_detls['featured_image'] != '' )? $pg_detls['featured_image']:""; ?>" />
                                <input type="file" onchange="getimage(this)" name="featured_image" id="featured_image" />
                                <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </small>

                            </div>

                        </div>

                        <div class="col-md-8 col-md-offset-4">
                            <input type="submit" name="profile_edit_btn" id="profile_edit_btn" class="btn btn-primary" value="Submit" />
                        </div>
                    </form>

                </div>
            </div>

        </div>



    </div>
</section>

<script>
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>