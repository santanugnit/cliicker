<?php
/* template name: My Account */
if ( !is_user_logged_in() ) {
    ob_clean();
    wp_redirect('login');
    exit(0);
}




get_header('inner-myaccount');
global $wpdb;
$user_id 		= get_current_user_id();
$user_info 		= get_userdata($user_id);
//echo "<pre>"; print_r($user_info);echo "</pre>";
$title 			= $user_info->user_login;
$sqldata 		= $wpdb->get_results("select * from chi_posts where post_title='".$title."' and post_status='publish' order by ID desc");
$post_id 		= $sqldata[0]->ID;
$start_date 		= get_post_meta($post_id, 'plan_start_date', true);
//$end_date 		= get_post_meta($post_id, 'plan_end_date', true);
$status_plan_type 	= get_post_meta($post_id , 'plan_status_type' ,true);
$pstart_date 		= get_post_meta($post_id , 'admin_plan_start_date' ,true);
$pend_date 		= get_post_meta($post_id , 'admin_plan_end_date' ,true);
$plan_type 		= get_post_meta($post_id, 'plan_type', true);



if($plan_type =='trial' && $status_plan_type =='active' ){
    //$end_date = get_option('plan_end_date');
    $date = $pstart_date;
    $date = strtotime($date);
    $date = strtotime("+6 day", $date);
    $end_date = date('d-m-Y', $date);
}

$current_date 	= date('d-m-Y');

/*TIME DIFFERENCE BETWEEN CURRENT TIME AND LAST CHAT TIME */
$chatCurrentdatetime = date('Y-m-d H:i:s', time());
global $wpdb;
$post_chat = $wpdb->get_results("SELECT * FROM chi_users_chat_histroy WHERE user_id ='". $user_id ."' AND `user_type` = 'U' ORDER BY chat_id DESC LIMIT 0 ,1 ");

$lastChatTime = $post_chat[0]->reply_date_time;

$to_time = strtotime($chatCurrentdatetime);
$from_time = strtotime($lastChatTime);
$chatTimeDifference =  round(abs($to_time - $from_time) / 60,3);
/*TIME DIFFERENCE BETWEEN CURRENT TIME AND LAST CHAT TIME */


//echo "<pre>";
//print_r ($chatTimeDifference).'<br />';
//print_r($post_chat[0]->reply_date_time).'<br />';
//print_r($chatCurrentdatetime).'<br />';
//print_r ("start date".$user_id);
//print_r ("current date".$current_date);
//print_r ("end date".$end_date);
//echo "</pre>";

//echo $pstart_date;
//echo $pend_date;


function dateDiff($current_date, $pend_date, $pstart_date)
{
    if( !validateDate($current_date) || !validateDate($pend_date) ){
        return 'Please contact with admin.';
    }

    $date1	= date_create($current_date);
    $date2	= date_create($pend_date);
    $diff	= date_diff($date1, $date2);

    //return $diff->format("%R%a days");
    $diffDays= $diff->format("%R%a");
    //echo " -- ". sign($diffDays);
    //var_dump(sign($diffDays));

    if( strtotime($pstart_date) > strtotime($current_date) ){
        return 'You plan is not activated yet!';
    }

    if( strtotime($current_date) > strtotime($pend_date) ){
        return "Your plan has been deactivated";
    }

    if ( sign($diffDays) < 1 ) {
        return 'Contact to admin!';
    } else { /* if ( gmp_sign($diffDays) >= 1 ) */
        return $diff->format("%a day(s) to go");
    }

    /*
    $plane_start_date 	= strtotime($pstart_date);
    $plane_end_date 	= strtotime($pend_date);
    $plane_current_date 	= strtotime($current_date);


    //$diff = $date1_ts - $date2_ts;
    $difftime = round($diff / 86400);
    if ( ($plane_current_date >= $plane_start_date ) && ( $plane_current_date<= $plane_start_date ) ) {
        //return round(($diff / 86400)+1)." day(s) to go";
    } elseif ($$plane_end_date< $plane_current_date) {
        return "Your plan has been deactivated";
    } elseif ($plane_current_date< $plane_start_date) {
        return "Your plan is not activate now!";
    }
    */
}

$dateDiff = dateDiff($current_date, $pend_date, $pstart_date);
/*
*	new added as on 15/11/2017 Start
*/
$fitness_plan = array();
$args = array(
    'post_type'  => 'fitness_plan',
    'post_status' => array('publish'),
    'posts_per_page' => -1,
    'order' =>'ASC',
    'orderby'=>'ID',
);
$query = new WP_Query( $args );
if ( $query->have_posts() ){
    while ( $query->have_posts() ) { $query->the_post();

        $fitness_plan[get_the_id()]	= get_the_title();
    }
    wp_reset_postdata();
}
/*
*	new added as on 15/11/2017 End
*/
//echo "<pre>"; print_r($fitness_plan); echo "</pre>";

?>
<!-- Dashboard area page html start -->


<section class="dashboard_section golddashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="sidebar">
                    <ul class="accordion" id="side_accordion">
                        <li data-tab="tab-1" class="current tabber"><a href="#"><span><i class="fa fa-tachometer" aria-hidden="true"></i></span>Dashborad</a></li>
                        <li>
                            <a class="toggle" href="javascript:void(0);"><span><img src="<?php bloginfo('template_url');?>/assets/images/2.png" height="15" width="15" /></span>Exercise Plan</a>
                            <ul class="inner">
                                <?php
                                if( isset($fitness_plan) && is_array($fitness_plan) && !empty($fitness_plan) ){
                                    foreach($fitness_plan  as $id => $f_plan) {
                                        $slug = get_post_field( 'post_name', $id);
                                        ?>
                                        <li class="child-li-childs tabber" data-tab="tab-6" id="<?php echo $slug;?>" class="search_planwise_data_<?php echo $id;?>" onClick="return get_search_by_plan_type('<?php echo get_the_excerpt($id); ?>', '<?php echo $id;?>', 'exercise_plan', '<?php echo $title; ?>', 'active_excercise_plan_wise_data', '<?php echo $f_plan;?>')"><a href="#"><?php echo $f_plan;?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                                <?php /* ?>
							<li class="child-li-childs tabber" data-tab="tab-6" onClick="return get_search_by_plan_type('' , '','exercise_plan', '','active_excercise_plan_wise_data','paid')"><a href="#">Paid</a></li>
							<?php */ ?>
                            </ul>
                        </li>
                        <li><a class="toggle" href="javascript:void(0);"><span><img src="<?php bloginfo('template_url');?>/assets/images/1.png" height="15" width="15"/></span>Active Nutrition Plan</a>
                            <ul class="inner">
                                <?php
                                if( isset($fitness_plan) && is_array($fitness_plan) && !empty($fitness_plan) ){
                                    foreach($fitness_plan  as $id => $f_plan) {
                                        $slug = get_post_field( 'post_name', $id);
                                        ?>
                                        <li class="child-li-childs tabber" data-tab="tab-4" id="<?php echo $slug;?>" class="search_planwise_data_<?php echo $id;?>"  onClick="return get_search_by_plan_type('<?php echo get_the_excerpt($id); ?>', '<?php echo $id;?>','active_nutrition', '<?php echo $title; ?>','active_nutrition_plan_wise_data', '<?php echo $f_plan;?>')"><a href="#"><?php echo $f_plan;?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                                <?php /* ?>
		                    <li class="child-li-childs tabber" data-tab="tab-4" onClick="return get_search_by_plan_type('', '', 'active_nutrition', '', 'active_nutrition_plan_wise_data', 'paid')"><a href="#">Paid</a></li>
							<?php */ ?>

                            </ul>
                        </li>
                        <li data-tab="tab-3" class="tabber"><a href="#"><span><i class="fa fa-calendar" aria-hidden="true"></i></span>Calendar</a></li>
                        <li data-tab="tab-9" class="tabber progress_row"><a href="#"><span><i class="fa fa-line-chart" aria-hidden="true"></i></span>Progress</a></li>
                        <li data-tab="tab-10" class="tabber"><a href="#"><span><i class="fa fa-comments" aria-hidden="true"></i></span>Message</a></li>
                        <li><a class="toggle" href="#"><span><i class="fa fa-cogs" aria-hidden="true"></i></span>Settings</a>
                            <ul class="inner">
                                <li class="child-li-childs"><a href="<?php echo get_permalink(get_page_by_path('edit-profile'));?>"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>Edit Profile</a></li>
                                <li data-tab="tab-8" class="child-li-childs tabber"><a href="#"><span><i class="fa fa-history" aria-hidden="true"></i></span>Transaction History</a></li>
                            </ul>
                        </li>
                        <li><a class="toggle" href="#"><span><i class="fa fa-user-circle" aria-hidden="true"></i></span>My Account</a>
                            <ul class="inner">
                                <li class="child-li-childs tabber" data-tab="tab-11"><a href="#"><span><i class="fa fa-user-o" aria-hidden="true"></i></span>Profile</a></li>
                                <li class="child-li-childs"><a href="<?php echo get_permalink(get_page_by_path('fitness-form'));?>"><span><i class="fa fa-file-text" aria-hidden="true"></i></span>Recall Sheet</a></li>
                                <?php $entries = get_post_meta( $post_id, 'customer_special_info_summary', true );?>
                                <li class="child-li-childs"><a href="#" data-toggle="modal" data-target="#notification_info_popup" data-count="<?php echo count($entries );?>" id="modalp"><span><i class="fa fa-bell-o" aria-hidden="true"></i></span>Notification</a></li>
                                <li class="child-li-childs tabber" data-tab="tab-12"><a href="#"><span><i class="fa fa-key" aria-hidden="true"></i></span>Change Password</a></li>
                            </ul>
                        </li>
                        <li style="display:none;"><a href="#" data-toggle="modal" data-target="#demo-modal" id="fade_in" >Popup Form</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-9 col-sm-9 col-xs-12 pull-right">
                <?php
                $fitness_frm_status = get_user_meta($user_id,'form_fields_status',true);
                if($fitness_frm_status == 0)
                {
                    ?>
                    <div class="fitness_form_feed"><span><i class="fa fa-exclamation" aria-hidden="true"></i></span><h5> Your form is incomplete.Please fill up all fields and submit.</h5> <a href="<?php echo get_permalink( get_page_by_path( 'fitness-form' ) );?>">Click Here</a>  </div>
                    <?php
                }
                ?>
                <div class="dashboard-content-area">
                    <div class="loader_files" id="loader" style="display:none;"><img src="<?php bloginfo('template_url');?>/assets/images/loaderf.gif"></div>
                    <div class="tab-content current" id="tab-1">
                        <?php


                        if($status_plan_type == 'active'|| $plan_type=='trial'){
                            ?>

                            <div class="content-section">
                                <h2 class="heading">Package information</h2>
                                <div class="plan-section">
                                    <p> <?php echo $plane_name = str_replace("-"," ",$sqldata[0]->post_content); ?>  <!--(<?php echo get_post_meta($post_id,'plan_type',true);?>)--><!--CHIRAG RANA FITNESS gold plan activated from--></p>
                                    <?php
                                    //echo $pstart_date;
                                    if(isset($pstart_date) && !empty($pstart_date)){
                                        echo '<h2>'.date("j F, Y", strtotime($pstart_date)).'-'.date("j F, Y", strtotime($pend_date )).'</h2><span>'.$dateDiff.'</span>';
                                    }else{
                                        echo '<div class="default_msg" ><p style="font-size:15px !important;">Our fitness expert will get back to you with customized plans in the next 24 hours.</p></div>';
                                    }
                                    ?>

                                    <?php /*
	<h2><?php if($pstart_date){?> <?php echo date("j F, Y", strtotime($pstart_date));}else{ echo date("j F, Y", strtotime($start_date));} ?> - <?php echo date("j F, Y", strtotime($pend_date)); ?> </h2>
                            <span><?php echo $dateDiff;?> days to go</span>
                          */  ?>
                                </div>
                            </div>
                        <?php }?>                       <div class="content-section">
                            <h2 class="heading">Profile information</h2>
                            <?php $img = get_user_meta($user_id,'user_avatar',true); ?>
                            <div class="section-area">
                                <?php if(@$img[url]){?>
                                    <div class="profile-img">
                                        <a href="javascript:void(0);"><img src="<?php echo @$img[url];?>"></a>
                                    </div>
                                    <!--<a href="javascript:void(0);" data-image="<?php echo @$img[url];?>"  data-toggle="modal" data-target="#profile_image_popup">Edit Image</a>-->
                                <?php }else{?>
                                    <div class="profile-img">
                                        <a href="javascript:void(0);"><img src="<?php bloginfo('template_url');?>/assets/images/no_profile_img.jpg"></a>
                                    </div>
                                <?php }?>
                                <ul class="information">
                                    <li><span><i class="fa fa-user-o" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'full_name',true);?> (<?php echo get_user_meta($user_id ,'gender',true);?>)</li>
                                    <li><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'date_of_birth',true);?></li>
                                    <li><span><i class="fa fa-envelope" aria-hidden="true"></i></span><?php echo $user_info->user_login;?></li>
                                    <li><span><i class="fa fa-phone" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'phone_no',true);?></li>
                                    <li><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'address1',true);?> <?php echo get_user_meta($user_id ,'address2',true);?></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="tab-content" id="tab-3">
                        <div class="content-section">
                            <h2 class="heading">Calendar</h2>
                            <div class="custom_calender">
                                <div class="default_msg"><p>Your fitness expert will update you with regular health routine from time to time.</p></div>
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-4">

                        <div class="content-section">
                            <h2 class="heading">Active Nutrition Plan
                                <div id="active_nutrition_plan_wise_data" class="palns"></div>
                            </h2>
                            <div id="active_nutrition">
                            </div>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-5">
                        <div id="" class="palns"></div>
                        <div class="content-section">
                            <h2 class="heading">Active Supplement Plan</h2>
                            <div id="active_supplement_plan">  </div>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-6">

                        <div class="content-section">
                            <h2 class="heading">Exercise Plan
                                <div id="active_excercise_plan_wise_data" class="palns"></div>
                            </h2>

                            <div id="exercise_plan">

                            </div>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-7">
                        <div class="content-section">
                            <h2 class="heading">Talk to fitness consultant</h2>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-8">
                        <div class="content-section">
                            <h2 class="heading">Transaction History</h2>
                            <?php
                            global $wpdb;
                            $sqltrans = $wpdb->get_results("select * from chi_posts where post_type = 'customer_plan' and  post_title='".$title."' order by ID DESC");
                            ?>
                            <table class="table"  style="color:#FFF">
                                <thead>
                                <tr>
                                    <th>Plan Name</th>
                                    <th>Package Amount</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Remaining Days</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                /*echo "<pre>";
                                print_r($sqltrans);
                                echo "</pre>";*/
                                foreach($sqltrans as $restran)
                                {
                                    $padminstart_date = get_post_meta($restran->ID , 'admin_plan_start_date' ,true);
                                    if($padminstart_date){
                                        $starD = $padminstart_date;
                                    }else{

                                        $starD = get_post_meta($restran->ID, 'plan_start_date' ,true );
                                    }
                                    $plan_type = get_post_meta($restran->ID, 'plan_type', true);
                                    if($plan_type =='trial'){
                                        $endD = get_option('plan_end_date');
                                    }else{
                                        $endD = get_post_meta($restran->ID, 'admin_plan_end_date', true);
                                    }

                                    //$endD  = get_post_meta($restran->ID, 'plan_end_date' ,true );
                                    $pane_name_in_transactionHistory =  str_replace("-"," ",$restran->post_content);
                                    $todays_date = date('Y-m-d');
                                    $remaining_days = dateDiff($current_date, $endD, $starD );
                                    $plan_type = get_post_meta($restran->ID, 'plan_type', true);
                                    $plan_amount = get_post_meta($restran->ID, 'payment_amount', true);
                                    ?>
                                    <tr>
                                        <td><?php echo $pane_name_in_transactionHistory ;?></td>
                                        <td><?php echo $plan_amount;?></td>
                                        <td><?php echo date("j F, Y", strtotime($starD));?></td>
                                        <td><?php echo date("j F, Y", strtotime($endD));?></td>
                                        <td><?php if($remaining_days > 0){ echo $remaining_days; }else{echo 'Plan Expired.'; } ?> </td>
                                        <td><?php if($remaining_days > 0){?><div style="color:green; font-size:15px;"> Active</div> <?php } else{ ?><div style="color:red; font-size:15px;">Expired</div><?php } ?> </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-content" id="tab-9">
                        <div class="content-section">
                            <h2 class="heading">Progress Photos</h2>
                            <div class="default_msg"><p>Upload your photo so that our fitness expert can keep a track on your progress.</p></div>
                            <div class="progress-area-bulids">
                                <div class="img-upload-head">
                                    <!--<button type="button" class="gtToolbarButton" id="btn_photoRefresh" role="button" title="Refresh"><span></span><span class="ui-button-text"><i class="fa fa-refresh" aria-hidden="true"></i></span></button>-->
                                    <select id="dp_photoFilter" class="ClientFilterDL">
                                        <option value="all" selected="selected">All Photos</option>
                                        <option value="front">Front</option>
                                        <option value="side">Side</option>
                                        <option value="back">Back</option>
                                        <!---<option value="other">Other</option>--->
                                    </select>

                                    <button type="button" class="gtToolbarButton" id="btn_photoNew" role="button" style="display: inline-block;"><span class="ui-button-text">Upload New Photo</span></button>
                                </div>
                                <form action="" method="post" id="prgressForm">
                                    <div id="showhide_slide" class="uploader-area">
                                        <div id="btn_CancelUpload" class="uploadCls"><i class="fa fa-times" aria-hidden="true"></i></div>
                                        <p style="position:relative;">
                                            <label>Date</label>
                                            <input id="uploadPhoto_datepicker" class="hasDatepicker" type="text">
                                        <div id="date_error_msg" style="color:red;"></div>
                                        </p>
                                        <p>
                                            <label>Pose</label>
                                            <select id="upload_pose" name="pose" class="ClientFilterDL">
                                                <option value="front">Front</option>
                                                <option value="side">Side</option>
                                                <option value="back">Back</option>
                                            </select>
                                        </p>
                                        <input type="file" name="files[]" id="multiFiles" class="file">
                                        <div id="date_error_file" style="color:red;"></div>

                                        <!-- Drag and Drop container-->
                                        <div class="upload-area"  id="uploadfile">
                                            <span>Drop image file here</span>
                                            <small>or click here to select a file</small>
                                            <div class="fileType">Only .jpg, .gif and .png files allowed.</div>
                                            <div id="Successprogressimage" style="display:none; color:green;"></div>
                                            <div id="errorprogressimage" style="display:none; color:red;"></div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php
                        $get_image 	= get_user_meta($user_id ,'users_progress_image_front' , true);
                        $get_image_side = get_user_meta($user_id ,'users_progress_image_side' , true);
                        $get_image_back = get_user_meta($user_id ,'users_progress_image_back' , true);
                        $get_date_back 	= get_user_meta($user_id ,'uploadPhoto_datepicker_back' , true);
                        $get_date_front = get_user_meta($user_id ,'uploadPhoto_datepicker_front' , true);
                        $get_date_side 	= get_user_meta($user_id ,'uploadPhoto_datepicker_side' , true);
                        //print_r(rsort($get_image));
                        ?>

                        <div id="showajaxresponsedata">
                            <div class="img-show-area">
                                <ul class="progresImg1">
                                    <?php if(!empty($get_image)){
                                        for($i = 0; $i < count($get_image); $i++) { ?>
                                            <li class="front"><div class="img-area">
                                                    <button type="button" name="closebtn" data-id="<?php echo $i;?>" data-field="front" class="closebtn">X</button>
                                                    <a data-fancybox="gallery" href="<?php echo $get_image[$i]['url'];?>"><img src="<?php echo $get_image[$i]['url'];?>"/></a>
                                                    <div class="upload-overlay">
                                                        <p><?php echo $get_date_front[$i] ;?></p>
                                                        <span>Front</span>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }}
                                    if(!empty($get_image_side)){
                                        for($j = 0; $j < count($get_image_side); $j++) { ?>
                                            <li class="side"><div class="img-area">
                                                    <button type="button" name="closebtn" data-id="<?php echo $j;?>" data-field="side" class="closebtn">X</button>

                                                    <a data-fancybox="gallery" href="<?php echo $get_image_side[$j]['url'];?>"><img src="<?php echo $get_image_side[$j]['url'];?>"/></a>
                                                    <div class="upload-overlay">
                                                        <p><?php echo $get_date_side[$j]; ?></p>
                                                        <span>Side</span>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }}
                                    if(!empty($get_image_back)){
                                        for($k = 0; $k < count($get_image_back); $k++) { ?>
                                            <li class="back"><div class="img-area">
                                                    <button type="button" name="closebtn" data-id="<?php echo $k;?>" data-field="back" class="closebtn">X</button>

                                                    <a data-fancybox="gallery" href="<?php echo $get_image_back[$k]['url'];?>"><img src="<?php echo $get_image_back[$k]['url'];?>"/></a>
                                                    <div class="upload-overlay">
                                                        <p><?php echo $get_date_back[$k]; ?></p>
                                                        <span>Back</span>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }}?>

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-content" id="tab-10">
                    <div class="content-section">
                        <h2 class="heading">Message</h2>
                        <input type="hidden" name="admin_last_msg_id" id="admin_last_msg_id" value="" />
                        <button type="button" name="refresk_chat" id="refresh_chat"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                        <div class="chat-window-area">
                            <div id="chatInfoData">

                            </div>
                        </div>
                        <form action="" method="" id="chatForm">
                            <textarea name="comment" id="comment" placeholder="Enter your message"></textarea>
                            <button type="button" name="users_comments" id="users_comments">Send</button>
                        </form>

                    </div>
                </div>

                <div class="tab-content" id="tab-12">
                    <div class="content-section">
                        <h2 class="heading">Change Password</h2>
                        <div class="col-md-6 col-md-offset-3 col-xs-12">
                            <div class="changePass-area">
                                <form action="" id="change_password" method="post">
                                    <div class="form-group">
                                        <input type="password" name="oldpassword" id="oldpassword" value="" placeholder="Old Password"/>
                                        <div class="error_message_old" style="color:red;"></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="newpassword" id="newpassword" value="" placeholder="New Password"/>
                                        <div class="error_message_new" style="color:red;"></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirmpassword" id="confirmpassword" value="" placeholder="Confirm Password"/>
                                        <div class="error_message" style="color:red;"></div>
                                    </div>
                                    <div class="reg-sumbmisn">
                                        <div class="form-group">
                                            <button type="button" name="change_password" class="btn btn-success" id="change_password_user">Change Password</button>
                                        </div>
                                    </div>
                                    <div style="color:green; font-size:12px; display:none;" id="show_success_password">You have successfully change password!</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-content" id="tab-11">
                    <div class="content-section">
                        <h2 class="heading">Profile information</h2>
                        <?php $img =get_user_meta($user_id,'user_avatar',true); ?>
                        <div class="section-area">
                            <?php if(@$img[url]){?>
                                <div class="profile-img">
                                    <a href="#"><img src="<?php echo @$img[url];?>"></a>
                                </div>
                                <!--<a href="#" data-image="<?php echo @$img[url];?>"  data-toggle="modal" data-target="#profile_image_popup">Edit Image</a>-->
                            <?php }else{?>
                                <div class="profile-img">
                                    <a href="#"><img src="<?php bloginfo('template_url');?>/assets/images/profile_img.png"></a>
                                </div>
                            <?php }?>
                            <ul class="information">
                                <li><span><i class="fa fa-user-o" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'full_name',true);?> (<?php echo get_user_meta($user_id ,'gender',true);?>)</li>
                                <li><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'date_of_birth',true);?></li>
                                <li><span><i class="fa fa-envelope" aria-hidden="true"></i></span><?php echo $user_info->user_login;?></li>
                                <li><span><i class="fa fa-phone" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'phone_no',true);?></li>
                                <li><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo get_user_meta($user_id ,'address1',true);?> <?php echo get_user_meta($user_id ,'address2',true);?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>


</section>

<!-- multistep Boostrap modal Code -->
<div class="custom-modal-wrapper">
    <form class="modal multi-step" id="demo-modal" action="" method="post">
        <div class="modal-dialog">
            <div class="modal-content modal-area">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title step-1 steps" data-step="1">Step 1</h4>
                    <h4 class="modal-title step-2 steps" data-step="2">Step 2</h4>
                    <h4 class="modal-title step-3 steps" data-step="3">Step 3</h4>
                    <h4 class="modal-title step-4 steps" data-step="4">Step 4</h4>
                    <h4 class="modal-title step-5 steps" data-step="5">Step 5</h4>
                    <h4 class="modal-title step-6 steps" data-step="6">Step 6</h4>
                    <h4 class="modal-title step-7 steps" data-step="7">Step 7</h4>
                    <h4 class="modal-title step-8 steps" data-step="8">Step 8</h4>
                    <h4 class="modal-title step-9 steps" data-step="9">Step 9</h4>
                    <h4 class="modal-title step-10 steps" data-step="10">Step 10</h4>
                    <h4 class="modal-title step-11 steps" data-step="11">Congratulation!</h4>

                </div>
                <div class="modal-body step-1" data-step="1">
                    <div class="form-group">
                        <label for="email">Gender:</label>
                        <select name="gender" id="gender">
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Age:</label>
                        <input type="text" name="age" id="age" class="form-control" placeholder="Age" required="required">

                    </div>

                </div>
                <div class="modal-body step-2" data-step="2">
                    <div class="form-group">
                        <label for="email">Body Measurement:</label>
                        <div class='input50'>
                            <input type="text" name="height_ft" id="height_ft" class="form-control" placeholder="Height (ft)" required="required">
                        </div>
                        <div class='input50'>
                            <input type="text" name="height_inc" id="height_inc" class="form-control" placeholder="Height (inc)" required="required">
                        </div>
                        <div class='input50'>
                            <input type="text" name="height_m" id="height_m" class="form-control" placeholder="Height (m)" required="required">
                        </div>
                        <div class='input50'>
                            <input type="text" name="height_cms" id="height_cms" class="form-control" placeholder="Height (cms)" required="required">
                        </div>
                        <div class='input50'>
                            <input type="text" class="form-control" name="current_body_weight_kgs" id="current_body_weight_kgs" placeholder="Current Body Weight(kgs)">
                        </div>
                        <div class='input50'>
                            <input type="text" class="form-control" name="current_body_weight_lbs" id="current_body_weight_lbs" placeholder="Current Body Weight(lbs)">
                        </div>
                        <div class='input50'>
                            <input type="text" class="form-control" name="desired_body_weight_kgs" id="desired_body_weight_kgs" placeholder="Desired Body Weight(kgs)">
                        </div>
                        <div class='input50'>
                            <input type="text" class="form-control" name="desired_body_weight_lbs" id="desired_body_weight_lbs" placeholder="Desired Body Weight(lbs)">
                        </div>

                        <div class="input100">
                            <input type="text" class="form-control" name="body_fat_percentage" id="body_fat_percentage" placeholder="Body Fat Percentage ">
                            <div class="tooltips">
                                <a href="#">i</a>
                                <span>Get your body fat percentage checked at any  world class fitness center like Gold’s Gym OR any physiotherapist.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body step-3" data-step="3">
                    <div class="form-group">
                        <label for="email">Muscle Mass :</label>
                        <div class="input100">
                            <input type="text" class="form-control" id="muscle_mass_kg" name="muscle_mass_kg" placeholder="Muscle Mass (Kgs) ">

                            <div class="tooltips">
                                <a href="#">i</a>
                                <span>Get your Muscle Mass checked at any  world class fitness center like Gold’s Gym OR any physiotherapist.</span>
                            </div>
                        </div>
                        <label for="email">Fat Free Mass :</label>
                        <div class="input100">
                            <input type="text" class="form-control" name="fat_free_mass" id="fat_free_mass" placeholder="Fat Free Mass ">
                            <div class="tooltips 3rd">
                                <a href="#">i</a>
                                <span>Your total body weight minus your body fat mass in kgs.</span>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-body step-4" data-step="4">
                    <div class="form-group">
                        <label for="email">Your body goal: </label>
                        <label class="radio-inline">
                            <input type="radio" name="body_goal" value="Muscle Gain">Muscle Gain
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="body_goal" value="Fat Loss"> Fat Loss
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="body_goal" value="Maintenance">Maintenance
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="body_goal" value="Extreme Fat Loss / Rapid Fat loss">Extreme Fat Loss / Rapid Fat loss
                        </label>
                        <div class="tooltips topstoool">
                            <a href="#">i</a>
                            <span><h4>Fat loss</h4>
							This program will help reduce your body's fat to a level that you desire. This will improve your overall health, increase longevity and enhance your quality of life.Here you will have the tools to find a leaner trimmer you.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-body step-5" data-step="5">
                    <div class="form-group">
                        <label for="email">Your Activity Level: </label>
                        <label class="radio-inline">
                            <input type="radio" name="activity_level" value="Sedentary">Sedentary
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="activity_level" value="Light Active" >Light Active
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="activity_level" value="Moderate Active">Moderate Active
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="activity_level" value="Very Active">Very Active
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="activity_level" value="Extremely Active">Extremely Active
                        </label>
                        <div class="tooltips topstoool">
                            <a href="#">i</a>
                            <span><h4>Moderately Active</h4>
							Moderate exercise or sports 3 - 5days per week. This includes slow, steady-state cardiovascular activities that speed up your breathing/ heart rate and cause you to be out of breadth but not to the extent of an all out sprint or exhaustion.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-body step-6" data-step="6">
                    <div class="form-group">
                        <label for="email">Occupation Details:</label>
                        <input type="text" class="form-control"  name="occupation_type" id="occupation_type" placeholder="Type of occupation">
                        <label for="email">Hours of Work:</label>
                        <input type="text" class="form-control"  name="hour_of_work"  data-format="hh:mm:ss" id="hour_of_work"  placeholder="Hours of work">
                        <span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
						</span>
                    </div>
                </div>
                <div class="modal-body step-7" data-step="7">
                    <div class="form-group">
                        <label for="email">Do you exercise regularly: </label>
                        <label class="radio-inline">
                            <input type="radio" name="exercise_regularly" value="Yes">Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="exercise_regularly" value="No" >No
                        </label>

                        <!--<div class="input100 ">
                        <input type="text" class="form-control" name="fat_free_mass" id="fat_free_mass" placeholder="days per week">
                        </div>-->

                        <!-- <label for="email">Any Bad Habits :</label>
                        <input type="text" class="form-control" name="bad_habit" id="bad_habit"> -->
                    </div>
                </div>
                <div class="modal-body step-8" data-step="8">
                    <div class="form-group">
                        <label for="email">Rate your ability to perform cardio exercises: </label>
                        <label class="radio-inline">
                            <input type="radio" name="cardio_exercises" value="Poor">Poor
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="cardio_exercises" value="Moderate" >Moderate
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="cardio_exercises" value="Good" >Good
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="cardio_exercises" value="Very Good" >Very Good
                        </label>
                    </div>
                    <!-- <div style="color:green; font-size:12px; display:none;" id="show_success_message">You have successfully submitted fitness form</div>-->
                </div>
                <div class="modal-body step-9" data-step="9">
                    <div class="form-group">
                        <div class="input100 ">
                            <label>What equipments do you have access to</label>
                            <input type="text" class="form-control" name="equipments_access" id="equipments_access" placeholder="">
                        </div>
                        <div class="input100 ">
                            <label>Medical Problems / Allergies</label>
                            <input type="text" class="form-control" name="medical_problems" id="medical_problems" placeholder="">
                        </div>
                        <div class="input100 ">
                            <label>Any medication are you currently on</label>
                            <input type="text" class="form-control" name="medication_currenty" id="medication_currenty" placeholder="">
                        </div>
                    </div>
                    <!-- <div style="color:green; font-size:12px; display:none;" id="show_success_message">You have successfully submitted fitness form</div>-->
                </div>

                <div class="modal-body step-10" data-step="10">
                    <div class="form-group">
                        <div class="input100 ">
                            <label>Do you smoke?</label>
                            <label class="radio-inline">
                                <input type="radio" name="smoke" value="Yes">Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="smoke" value="No">No
                            </label>
                        </div>
                        <div class="input100 ">
                            <label>Do you drink?</label>
                            <label class="radio-inline">
                                <input type="radio" name="drink" value="Yes">Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="drink" value="No">No
                            </label>
                        </div>
                        <div class="input100 ">
                            <label>Any additional comment we should know about?</label>
                            <input type="text" class="form-control" name="comment" id="comment" placeholder="Comment">
                        </div>
                    </div>
                    <!-- <div style="color:green; font-size:12px; display:none;" id="show_success_message">You have successfully submitted fitness form</div>-->
                </div>
                <div class="modal-body step-11" data-step="11">
                    <div class="form-group">
                        <p>Thank you for filling up the questionnaire. Our fitness expert will get back to you with customized plans in the next 24 hours.</p>
                    </div>
                    <!-- <div style="color:green; font-size:12px; display:none;" id="show_success_message">You have successfully submitted fitness form</div>-->
                </div>

                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary" id="final_submit_data">Save</button>-->
                    <button type="button" class="btn btn-primary step step-1"  data-step="1" onclick="sendEvent('#demo-modal', 2)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-2"  data-step="2" onclick="sendEvent('#demo-modal', 3)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-3"  data-step="3" onclick="sendEvent('#demo-modal', 4)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-4"  data-step="4" onclick="sendEvent('#demo-modal', 5)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-5"  data-step="5" onclick="sendEvent('#demo-modal', 6)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-6"  data-step="6" onclick="sendEvent('#demo-modal', 7)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-7"  data-step="7" onclick="sendEvent('#demo-modal',8)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-8"  data-step="8" onclick="sendEvent('#demo-modal',9)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-9"  data-step="9" onclick="sendEvent('#demo-modal',10)">Save & Continue</button>
                    <button type="button" class="btn btn-primary step step-10" data-step="10" id="final_submit_data" onclick="sendEvent('#demo-modal',11)">Save</button>


                    <button type="button" class="btn btn-primary step step-1"  data-step="2" onclick="prevEvent('#demo-modal', 1)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-2"  data-step="3" onclick="prevEvent('#demo-modal', 2)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-3"  data-step="4" onclick="prevEvent('#demo-modal', 3)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-4"  data-step="5" onclick="prevEvent('#demo-modal', 4)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-5"  data-step="6" onclick="prevEvent('#demo-modal', 5)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-6"  data-step="7" onclick="prevEvent('#demo-modal', 6)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-7"  data-step="8" onclick="prevEvent('#demo-modal', 7)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-8"  data-step="9" onclick="prevEvent('#demo-modal', 8)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-9"  data-step="10" onclick="prevEvent('#demo-modal',9)">Previous Form</button>
                    <button type="button" class="btn btn-primary step step-10" data-step="11" onclick="prevEvent('#demo-modal',10)">Previous Form</button>


                </div>
            </div>
        </div>
    </form>
</div>
<!-- End Multistep boostrap Modal-->

<!--Special info popup html -->
<div id="special_info_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title date-info"></h4>
            </div>
            <div class="modal-body">
                <div class="special_info"></div>
            </div>

        </div>

    </div>
</div>
<!--End Special info popup html -->
<!-- Notification Popup Info-->
<div id="notification_info_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Notification Information</h4>
            </div>
            <div class="modal-body">
                <div class="notification_info"></div>
            </div>
        </div>
    </div>
</div>
<!--End Notification Popup Info-->
<!--Talk to fitness consultant-->
<div id="talkto_fitness_consultant" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Talk to fitness consultant </h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="fitnessform">
                    <textarea name="message" id="message" class="form-control" placeholder="Talk to your fitness consultant"></textarea>
                    <div class="error_message" style="color:red;"></div>
                    <div class="reg-sumbmisn btn-ares">
                        <input type="button" name="talk_toconsult"  class="btn btn-default myButton" id="talkto_fitness" value="Submit" />
                    </div>
                </form>
                <div style="color:green; font-size:12px; display:none;" id="show_fitness_consultant">You have successfully submitted consultant message</div>
            </div>
        </div>
    </div>
</div>

<div id="profile_event_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Your Message</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="fitness_event_add" enctype="multipart/form-data">
                    <textarea name="add_fitness" id="add_fitness" class="form-control" placeholder="Add Your Message" /></textarea>
                    <div class="error_message_fitnessinfo" style="color:red;"></div>
                    <div class="reg-sumbmisn btn-ares">
                        <input type="button" name="add_calendar_event"  class="btn btn-default myButton" id="add_calendar_event" value="Submit" />
                    </div>
                </form>
                <div style="color:green; font-size:12px; display:none;" id="show_fitness_message">You have successfully added fitness info!</div>
            </div>
        </div>
    </div>
</div>
<?php get_footer('inner');?>

<script>

    jQuery('#uploadPhoto_datepicker').datepicker({
        inline: true,
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
        endDate: new Date()
    });
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>


<?php


function renewplan_overlay(){
    global $wpdb;
    $user_id 		= get_current_user_id();
    $user_info 		= get_userdata($user_id);
    $title 			= $user_info->user_login;
    //echo "<pre>"; print_r($user_info);echo "</pre>";
    $sqldata 		= $wpdb->get_results("select * from chi_posts where post_title='".$title."' and post_status='publish' order by ID desc");
    $post_id 		= $sqldata[0]->ID;
    $pend_date 		= get_post_meta($post_id , 'admin_plan_end_date' ,true);
    //echo "<pre>"; print_r($pend_date);echo "</pre>";
    if(strtotime($pend_date) < strtotime((date('d-m-Y')))){ ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                //alert('hiiii');
                jQuery('body').append(jQuery('<div class="expired_overlay"><?php echo'<div class="active-message expired_msg"><span><i class="fa fa-exclamation" aria-hidden="true"></i></span><h5>Your plan has been expired. To avial chirag rana fitness services Please click on the below link to renew your the paid version.</h5> <a href="'.site_url().'/renew-plan/">Click Here</a></div>'; ?></div>'));
            });
        </script>
        <?php
    }
}
echo renewplan_overlay();



?>



<script>
    jQuery(document).ready(function(e) {
        /*var $=jQuery.noconflict();*/
        var json_events = '';
        var end = '';
        var calendar = jQuery('#calendar').fullCalendar({
            events: [],
            editable: true,
            utc: true,
            droppable: true,
            header: {
                left: 'prev ,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            eventClick: function(event, jsEvent, view) {
                var  calendarDate = moment(event.start).format('DD/MM/YYYY');
                //console.log(calendarDate);
                jQuery('.date-info').html(calendarDate);
                jQuery('.special_info').html(event.title);
                jQuery('#special_info_popup').modal();
            },
            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                // var title = prompt('Add Fitness Infomation:');
                jQuery('#profile_event_popup').modal();
                var date =  moment(start).format('YYYY-MM-DD');
                //var url = prompt('Type Event url, if exits:');
                jQuery('#add_calendar_event').click(function(){
                    var title = jQuery('#add_fitness').val();
                    //alert(title);
                    if (title=='') {
                        jQuery('.error_message_fitnessinfo').html('This field is required');
                        return false;
                    } else {
                        jQuery.ajax({
                            url: '<?php echo admin_url('admin-ajax.php');?>',
                            data : {
                                action : 'add_event_user_data',
                                post_id : '<?php echo $post_id;?>',
                                user_event : title,
                                event_date : date,
                            },
                            type: "POST",
                            success: function(respone) {
                                jQuery('#fitness_event_add')[0].reset();
                                jQuery('#show_fitness_message').show();
                                setTimeout(function(){
                                    jQuery('#profile_event_popup').modal('hide')
                                }, 3000);
                                //alert(respone);
                            }
                        });
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }
                });
                calendar.fullCalendar('unselect');
            }
        });

        jQuery.ajax({
            url :'<?php echo admin_url('admin-ajax.php'); ?>',
            type : 'post',
            data : {
                action : 'get_search_plan_data_user_data',
                post_id : '<?php echo $post_id;?>'
            },
            dataType: 'JSON',
            success : function( data ) {
                //console.log(data);
                jQuery('#calendar').fullCalendar('removeEvents');
                jQuery('#calendar').fullCalendar('addEventSource', data); //var itemObj = $.parseJSON(data);
            }
        });

        <?php if(is_page('my-account')){
        //if($fitness_frm_status == 0){
        ?>
        //localStorage.removeItem('user_id');

        var userloggedid = '<?php echo $user_id; ?>';
        var isshow = localStorage.getItem('isshow');
        var uid  = localStorage.getItem('user_id');

        if (uid != userloggedid) {
            jQuery('#fade_in').trigger('click');
            localStorage.setItem('isshow', 1);
            localStorage.setItem('user_id', userloggedid);
        }
        <?php
        //}
        }
        ?>

        jQuery('.step').click(function(){
            //var formData = jQuery('#demo-modal').serialize();
            var gender = jQuery('#gender').val();
            var age = jQuery('#age').val();
            var height_ft = jQuery('#height_ft').val();
            var height_m = jQuery('#height_m').val();
            var height_inc = jQuery('#height_inc').val();
            var height_cms = jQuery('#height_cms').val();
            var current_body_weight_kgs = jQuery('#current_body_weight_kgs').val();
            var current_body_weight_lbs = jQuery('#current_body_weight_lbs').val();
            var desired_body_weight_kgs = jQuery('#desired_body_weight_kgs').val();
            var desired_body_weight_lbs = jQuery('#desired_body_weight_lbs').val();
            var body_fat_percentage = jQuery('#body_fat_percentage').val();
            var muscle_mass_kg = jQuery('#muscle_mass_kg').val();
            var fat_free_mass = jQuery('#fat_free_mass').val();
            var body_goal =  jQuery('input[name=body_goal]:checked').val();
            var activity_level =  jQuery('input[name=activity_level]:checked').val();
            var occupation_type = jQuery('#occupation_type').val();
            var hour_of_work = jQuery('#hour_of_work').val();
            var exercise_regularly =  jQuery('input[name=exercise_regularly]:checked').val();
            var cardio_exercises =  jQuery('input[name=cardio_exercises]:checked').val();
            var equipments_access = jQuery('#equipments_access').val();
            var medical_problems = jQuery('#medical_problems').val();
            var medication_currenty = jQuery('#medication_currenty').val();
            //var medication_currenty = jQuery('#medication_currenty').val();
            var smoke =  jQuery('input[name=smoke]:checked').val();
            var drink =  jQuery('input[name=drink]:checked').val();
            var comment = jQuery('#comment').val();


            var user_id = '<?php echo $user_id ;?>';
            jQuery.ajax({
                url : '<?php echo admin_url('admin-ajax.php'); ?>',
                type : 'post',
                data : {
                    action : 'get_request_enquiry_form_data',
                    gender :  gender,
                    age    : age,
                    height_ft :  height_ft,
                    height_m  : height_m,
                    height_inc : height_inc,
                    height_cms : height_cms,
                    current_body_weight_kgs : current_body_weight_kgs,
                    current_body_weight_lbs : current_body_weight_lbs,
                    desired_body_weight_kgs : desired_body_weight_kgs,
                    desired_body_weight_lbs : desired_body_weight_lbs,
                    body_fat_percentage :  body_fat_percentage,
                    muscle_mass_kg :  muscle_mass_kg,
                    fat_free_mass :  fat_free_mass,
                    body_goal :  body_goal,
                    activity_level :  activity_level,
                    occupation_type :  occupation_type,
                    hour_of_work   : hour_of_work,
                    exercise_regularly : exercise_regularly,
                    cardio_exercises  : cardio_exercises,
                    equipments_access : equipments_access,
                    medical_problems  : medical_problems,
                    medication_currenty : medication_currenty,
                    smoke                : smoke,
                    drink				: drink,
                    comment             : comment,
                    user_id         : user_id,
                },
                success : function( response ) {
                    //alert(response);
                    if (response == 1) {
                        $('#demo-modal')[0].reset();
                        $('#show_success_message').show();
                        setTimeout(function(){
                            $('#demo-modal').modal('hide')
                            location.reload();
                        }, 3000);
                    }
                }
            });
        });
////// Talk to fitness consultant ajax call ////////
        jQuery('#talkto_fitness').click(function(){
            var message = jQuery('#message').val();
            if(message==''){
                jQuery('.error_message').html('This field is required');
                return false;
            }else{
                jQuery.ajax({
                    url :'<?php echo admin_url('admin-ajax.php'); ?>',
                    type : 'post',
                    dataType: 'html',
                    data : {
                        action : 'get_message_to_fitness_consultant',
                        message : message,
                    },
                    success : function( response ) {
                        //console.log(response);
                        if(response != ''){
                            $('#show_fitness_consultant').show();
                            $('#fitnessform')[0].reset();
                            setTimeout(function(){
                                $('#talkto_fitness_consultant').modal('hide');
                                //location.reload();
                            }, 3000);
                        }
                    }
                });
            }
        });

        jQuery('#btn_photoNew').click(function(){
            //alert('dsfdf');
            jQuery('#showhide_slide').slideDown();
            jQuery('#btn_photoNew').hide();
        });
        jQuery('#btn_CancelUpload').click(function(){
            jQuery('#showhide_slide').slideUp();
            jQuery('#btn_photoNew').show();
        })

        jQuery('#profile_image').click(function(){
            var user_id = '<?php echo $user_id ;?>';

            var file_data =  $('#useravater')[0].files[0];
            var fd = new FormData($('#userimage')[0]);
            fd.append('main_image', file_data.name);
            fd.append('action', 'profile_image_upload');
            fd.append('user_id', user_id );

            if(file_data ==''){
                jQuery('.error_message').html('This field is required');
                return false;
            }else{
                jQuery.ajax({
                    url :'<?php echo admin_url('admin-ajax.php'); ?>',
                    type : 'post',
                    data : fd,
                    contentType: false,
                    processData: false,
                    success : function( response ) {
                        console.log(response);
                        $('#show_fitness_image').show();
                        $('#userimage')[0].reset();
                        setTimeout(function(){
                            $('#profile_image_popup').modal('hide')
                        }, 5000);
                    }
                });
            }
        });

        jQuery('#users_comments').click(function(){
            ajaxChatDataInsertCall();
        });

///// Boostrap modal notification ///
        var refreshTimer;
        function ajaxNotificationPopupCall()
        {
            var count_noti = document.getElementById('noti_remove').innerHTML;
            var popupcont = jQuery('#modalp').attr('data-count');
            //alert(popupcont); return false;
            var notiall = '';
            if(count_noti){
                notiall = count_noti;
            }else {

                notiall = popupcont;
            }
            jQuery.ajax({
                url :'<?php echo admin_url('admin-ajax.php'); ?>',
                type : 'post',
                data : {
                    action : 'get_search_plan_notification_data',
                    post_id : '<?php echo $post_id;?>',
                    notification_no : notiall,
                },
                dataType: 'html',
                success : function( data ) {
                    var content_length = data.length;
                    jQuery('.notification_info').html(data);
                    jQuery('#noti_remove').hide();
                },
                error: function(err) {
                    console.log('error');
                }/*,
					complete: function(data) {
						clearInterval(refreshTimer); // Note: This'd not stop the setInterval.
					}*/
            });
        }

        jQuery('#notification_info_popup').on('shown.bs.modal', function() {
            ajaxNotificationPopupCall();
        }) ;

        function ajaxCallCountNotification()
        {
            var cont = '';
            jQuery.ajax({
                url :'<?php echo admin_url('admin-ajax.php'); ?>',
                type : 'post',
                data : {
                    action : 'get_search_plan_notification_count',
                    post_id : '<?php echo $post_id;?>',
                },
                dataType: 'html',
                success : function( data ) {
                    //console.log(data.length);
                    if(data.length > 1){
                        jQuery('.notify').addClass('noti_remove');
                        jQuery('#noti_remove').show().html(data);
                    }
                },
                error: function(err) {
                    console.log('error');
                }
            });
        }
        //var intervalTime = 24 * 60 * 60 * 1000;  /// every one day
        //var intervalTime = 1 * 60 * 60 * 1000; /// every one hr
        var intervalTime = 10000;
        refreshTimer = setInterval(ajaxCallCountNotification, intervalTime); ///300000

        /*jQuery(function () {
        jQuery('#hour_of_work').datetimepicker({
                format : 'HH:mm'
            });
        });*/

        jQuery('#change_password_user').click(function(){
            //alert('ddfdf');
            var user_id = '<?php echo $user_id ;?>';
            var oldpassword = jQuery('#oldpassword').val();
            var newpassword = jQuery('#newpassword').val();
            var confirmpassword = jQuery('#confirmpassword').val();
            if(oldpassword==''){
                jQuery('.error_message_old').html('This fields is required');
                return false;
            }else if(newpassword==''){
                jQuery('.error_message_new').html('This fields is required');
                return false;

            }else if(confirmpassword==''){
                jQuery('.error_message_confirm').html('This fields is required');
                return false;
            }
            else if(newpassword!= confirmpassword){
                jQuery('.error_message').html('New password and confirm password does not match!');
                return false;
            }
            else{
                jQuery.ajax({
                    url :'<?php echo admin_url('admin-ajax.php'); ?>',
                    type : 'post',
                    data : {
                        action : 'get_change_password_users',
                        oldpassword : oldpassword,
                        newpassword : newpassword,
                        user_id     : user_id
                    },
                    beforeSend: function(){
                        jQuery('#loader').show();
                    },
                    success : function( response ) {
                        //console.log(response);
                        if(response != ''){
                            $('#show_success_password').show();
                            $('#change_password')[0].reset();
                            setTimeout(function(){
                                $('#show_success_password').hide();
                            }, 3000);
                        }
                    },
                    complete: function(){
                        jQuery('#loader').hide();
                    }

                });
            }
        });
        jQuery(function () {
            jQuery('#dateofbirth').datepicker({
                inline: true,
                format: 'dd/mm/yyyy',
            }); /// uploadPhoto_datepicker
        });
        jQuery('#sidebar').bind('tabsshow', function(event, ui) {
            //alert('ffgf');
        });
    });
    sendEvent = function(sel, step) {

        jQuery(sel).trigger('next.m.' + step);
    }
    prevEvent = function(sel, step) {

        jQuery(sel).trigger('prev.m.' - step);
    }

    function get_search_by_plan_type( plan_type , post_id , plan_id, plan_type_title, plan_div_id , plan_paid_trial)
    {
        jQuery.ajax({
            url :'<?php echo admin_url('admin-ajax.php'); ?>',
            type : 'post',
            data : {
                action : 'get_search_plan_data',
                post_id : post_id,
                plan_type : plan_type,
                plan_id   : plan_id,
                plan_paid_trial : plan_paid_trial,
                plan_title : plan_type_title,
            },
            beforeSend: function(){
                jQuery('#loader').show();
            },
            success : function( response ) {
                jQuery('#'+plan_id).html(response);
                //$('#'+plan_div_id).html(plan_type);
                jQuery('#'+plan_div_id).html(plan_paid_trial);
                jQuery(".weeklyslider").not('.slick-initialized').slick({
                    centerMode: true,
                    centerPadding: '200px',
                    slidesToShow: 1,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                centerPadding: '0px',
                                slidesToShow: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                centerPadding: '0px',
                                slidesToShow: 1
                            }
                        }
                    ]
                });

                jQuery('.scrolltxt').slimScroll({
                    height: '150px',
                    color: '#0c8397',
                    alwaysVisible: true,
                    size: '4px',
                });
            },
            complete: function(){
                jQuery('#loader').hide();
            }
        });
    }
    jQuery(function() {

        // preventing page from redirecting
        jQuery("html").on("dragover", function(e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery("h1").text("Drag here");
        });

        jQuery("html").on("drop", function(e) { e.preventDefault(); e.stopPropagation(); });

        // Drag enter
        jQuery('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drag over
        jQuery('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            jQuery("h1").text("Drop");
        });

        // Drop
        jQuery('.upload-area').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            jQuery("h1").text("Upload");

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();

            fd.append('file', file[0]);

            uploadData(fd);
        });

        // Open file selector on div click
        jQuery("#uploadfile").click(function(){
            jQuery(".file").click();
        });

        // file selected
        jQuery(".file").change(function(){
            var uploadPhoto_datepicker = jQuery('#uploadPhoto_datepicker').val();
            var upload_pose = jQuery('#upload_pose').val();
            var user_id =  '<?php echo $user_id;?>';
            /* alert(uploadPhoto_datepicker); */
            if(uploadPhoto_datepicker == ''){
                jQuery('#date_error_msg').html('Select Date !');
                return false;
            }
            var form_data = new FormData();
            var ins = document.getElementById('multiFiles').files.length;
            /* alert(ins);
            return false; */
            for (var x = 0; x < ins; x++) {
                form_data.append("files[]", document.getElementById('multiFiles').files[x]);
            }

            form_data.append('action', 'get_progress_image_data');
            form_data.append('user_id', user_id );
            form_data.append('uploadPhoto_datepicker', uploadPhoto_datepicker );
            form_data.append('upload_pose', upload_pose );
            uploadData(form_data);
        });
    });

    // Sending AJAX request and upload file
    function uploadData(formdata){
        //console.log(formdata);
        /* alert(formdata);
        return false; */
        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php');?>',
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            beforeSend: function(){
                jQuery('#loader').show();
            },
            //dataType: 'html',
            success: function(response){
                console.log(response);
                var itemObj = jQuery.parseJSON(response);
                console.log(itemObj.error_msg);
                jQuery('#prgressForm')[0].reset();
                if(itemObj.success_msg){
                    console.log('error');
                    jQuery('#showajaxresponsedata').html(itemObj.msg);
                    jQuery('#errorprogressimage').hide();
                    setTimeout(function(){
                        jQuery('#Successprogressimage').html(itemObj.success_msg).show();
                    }, 3000);
                }else{
                    console.log('ok');
                    console.log(itemObj.error_msg);
                    jQuery('#Successprogressimage').hide();
                    jQuery('#errorprogressimage').html(itemObj.error_msg).show(); //errorprogressimage1
                }
            },
            complete: function(){
                jQuery('#loader').hide();
            }
        });
    }

    function ajaxChatDataInsertCall()
    {
        var comment = document.getElementById('comment').value;
        var user_id = '<?php echo $user_id ;?>';
        if( !comment.replace(/\s/g, '').length){
            return false;
        }
        jQuery.ajax({
            url :'<?php echo admin_url('admin-ajax.php'); ?>',
            type : 'post',
            data : {
                action : 'get_chat_insert_data',
                user_id : user_id ,
                comment : comment,
            },
            beforeSend: function(){
                jQuery('#loader').show();
            },
            dataType: 'html',
            success : function( data ) {
                var itemObj = jQuery.parseJSON(data);
                if(itemObj.admin_last_msg_id > 0){

                    jQuery('#chatInfoData').append(itemObj.msg);
                    jQuery('#tab-10').find("#admin_last_msg_id").attr("value", itemObj.admin_last_msg_id);
                    var scrollTo_int = jQuery('.chat-window-area').prop('scrollHeight') + 'px';
                    jQuery('.chat-window-area').slimScroll({
                        height: '300px',
                        width: '100%',
                        color: '#d1af44',
                        alwaysVisible: true,
                        size: '4px',
                        scrollTo : scrollTo_int,
                    });
                    jQuery('#chatForm')[0].reset();
                }
            },
            complete: function(){
                jQuery('#loader').hide();
            },
            error: function(err) {
                console.log('error');
            }
        });

    }


    jQuery('.chat-window-area').slimScroll({
        height: '300px',
        width: '100%',
        color: '#d1af44',
        alwaysVisible: true,
        size: '4px',
    });
    jQuery('#refresh_chat').click(function(){
        //alert('ok');
        ajaxCallChatDisplayData();

    });
    function ajaxCallChatDisplayData()
    {
        var user_id = '<?php echo $user_id ;?>';
        var admin_last_msg_id	= jQuery('#tab-10').find("#admin_last_msg_id").val();
        jQuery.ajax({
            url :'<?php echo admin_url('admin-ajax.php'); ?>',
            type : 'post',
            data : {
                action : 'get_chat_users_data',
                user_id : user_id,
                admin_last_msg_id : admin_last_msg_id,
                /* admin_id : 1, */
            },
            /*dataType: 'html',
            beforeSend: function(){
            jQuery('#loader').show();
            },*/
            success : function( data ) {
                var itemObj = jQuery.parseJSON(data);
                console.log(data);
                console.log(itemObj.msg);
                if( itemObj.admin_last_msg_id != admin_last_msg_id ) {
                    jQuery('#tab-10').find("#admin_last_msg_id").attr("value", itemObj.admin_last_msg_id);
                    jQuery('#chatInfoData').append(itemObj.msg);
                }else {

                }
                /* jQuery('#chatInfoData').html(data);*/
                //jQuery('#chatInfoData').html(data);
            },
            /*complete: function(){
                jQuery('#loader').hide();
            },*/
            error: function(err) {
                console.log('error');
            }
        });
    }

    jQuery(document).keypress(function(e) {
        if(e.which == 13) {
            //alert('You pressed enter!');
            ajaxChatDataInsertCall();
        }

    });

    jQuery(document).on('click', '.closebtn', function(){
        var id = jQuery(this).attr('data-id');
        var fields =  jQuery(this).attr('data-field');
        var user_id = '<?php echo $user_id;?>';

        jQuery.ajax({
            url :'<?php echo admin_url('admin-ajax.php'); ?>',
            type : 'post',
            data : {
                action : 'get_progress_image_delete',
                id : id,
                fields : fields,
                user_id: user_id,
            },
            beforeSend: function(){
                jQuery('#loader').show();
            },
            dataType: 'html',
            success : function( data ) {
                console.log(data);
                jQuery('#showajaxresponsedata').html(data);
            },
            complete: function(){
                jQuery('#loader').hide();
            },
            error: function(err) {
                console.log('error');
            }
        });

    })

    ajaxCallChatDisplayData();
    //var intervalChatDataTime = 3000;
    //refreshTimerChatdata = setInterval(ajaxCallChatDisplayData, intervalChatDataTime); ///300000

</script>
<script type="text/javascript">
    jQuery( "#side_accordion" ).accordion();
</script>
<script src="<?php echo bloginfo('template_url');?>/assets/js/multi-step-modal.js"></script>




