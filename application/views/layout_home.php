<!DOCTYPE HTML>
<html>
    <head>

        <?php echo $head; ?>

	<?php 
		if( isset($pageCSS) && !empty($pageCSS) ){
			foreach ($pageCSS as $css) {
				echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/'.$css.'" />';
			}
		}
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">

	<?php 
		if( isset($cdnJS) && !empty($cdnJS) ) {
			foreach ($cdnJS as $cdn_js) {
				echo '<script type="text/javascript" src="'.$cdn_js.'"></script>';
			}
		}
	?>

	<?php 
		if( isset($pageJS) && !empty($pageJS) ) {
			foreach ($pageJS as $js) {
				echo '<script type="text/javascript" src="'.base_url().'assets/'.$js.'"></script>';
			}
		}
	?>
		
    </head>

<body>

    <?php echo $header; ?>


    <?php echo ( isset($banner) )? $banner : ''; ?>

    <?php //echo ( isset($)) ? "":""; ?>


    <?php echo $maincontent; ?>


    <?php echo $footer; ?>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/global.js"></script>


</body>
</html>