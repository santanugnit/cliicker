<style> 
	.follow-link p span {
	    display: inline-block;
	}
</style>
<div class="photographer-banner-section" style="background-image: url(<?php echo base_url(); ?>assets/images/photographer-background.jpg);">
	<div class="container">
		<div class="photographer-sort-details-wrapper wrapper2">
			<div class="dp-wrapper">
				<div class="photographer-dp new-dp2">
					<img src="<?php $search_pg_img = fopen ($search_pg['img_url'].$search_pg['featured_image'], "r");  echo ( isset($search_pg['featured_image']) && $search_pg_img ) ? $search_pg['img_url'].$search_pg['featured_image'] : base_url()."assets/images/no-theme-image.jpeg" ; ?>" alt="<?php echo base_url(); ?>assets/images/4.jpg" />
				</div>

			<?php 
				if( isset($pg_lists["id"]) && (int)$pg_lists["id"] !== (int)$search_pg["id"] ) 
				{
					$followTxt = "FOLLOW"; $follow_status 	= 0; $followSign_class = "fa-plus";
					if( isset($pg_lists["id"]["no_followers"]) && (int)$pg_lists["id"]["no_followers"] > 0 && ( isset($pg_lists["id"]["follower_ids"]) && in_array($search_pg["id"], explode(",", $pg_lists["id"]["follower_ids"])) ) ) 
					{
						$followTxt = "UNFOLLOW"; $follow_status = 1; $followSign_class = "fa-minus";
					} 
			?>
				<div class="follow-link">
					<p> <span class=""><?php echo $followTxt; ?></span> <a href="javascript:void(0);" class="follow-btn" data-follow-status="<?php echo $follow_status; ?>" data-followed_id="<?php echo $search_pg['id']; ?>"><i class="fa <?php echo $followSign_class; ?>" aria-hidden="true"></i></a></p>
				</div>
			<?php } ?>

				<div class="contact-link">
					<p><a href="tel: <?php echo $search_pg["contact_number"]; ?> "><i class="fa fa-phone" aria-hidden="true"></i></a> CONTACT</p>
				</div>
			</div>
			<div class="photographer-review profile-review2">
				<h3> <?php echo $search_pg["first_name"] . ' ' . $search_pg["last_name"]; ?> </h3>
				<div class="star-rating">
                    <div class="show-rating">
               	<?php for( $i = 1; $i <= 5; $i++ ) { ?>
	                    <span class="stars <?php echo ( $i <= (int)$search_pg['avg_rating'] )? "reviewstart" : ""; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
	            <?php } ?>
                       
                    </div>
                </div>
				<h5>Followers : <span>50</span></h5>
				<h5><?php echo (int)$search_pg["no_albums"]; ?> Album(s)</h5>
			</div>
		</div>
	</div>
</div>

<script> 
$(document).ready(function(event) {

	$(document).on("click", ".follow-btn", function(e){
		var _that		= $(this);
		var f_status 	= _that.attr("data-follow-status");
		var followed_id = _that.attr("data-followed_id");
		console.log(f_status); 
		$.ajax({
            url     : '<?php echo base_url() . $ctrl_name . "ajax_pg_follow_status_changed" ?>',
            type    : 'POST',
            data    : {"curr_status": f_status, "followed_id": followed_id},
            dataType: 'html',
            complete: function(){

            },
            success : function(result) {
                console.log(result);
                var result = $.parseJSON(result); 
                if( result.status )
                {
                	swal(result.result, { buttons: false, timer: 2000, });

                    _that.attr("data-follow-status", result.curr_follow_status);
                    _that.parent("p").find("span").text( (result.curr_follow_status == 1)? "UNFOLLOW":"FOLLOW" );
                    _that.find("i").removeClass((result.curr_follow_status == 1)? "fa-plus":"fa-minus").addClass((result.curr_follow_status == 1)? "fa-minus":"fa-plus");
                } else {
                	swal(result.result, { buttons: false, timer: 2000, });
                	_that.attr("data-follow-status", result.curr_follow_status);
                    _that.parent("p").find("span").text( (result.curr_follow_status == 1)? "UNFOLLOW":"FOLLOW" );
                    _that.find("i").removeClass((result.curr_follow_status == 1)? "fa-plus":"fa-minus").addClass((result.curr_follow_status == 1)? "fa-minus":"fa-plus");
                }
            },
            error: function(error) {
                alert(error);
            }
        });
	});



});
</script>