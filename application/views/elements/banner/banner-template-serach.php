<div class="photographer-banner">
    <div class="container">
    <div class="row">
        <h3>Templates</h3>
        <p>Our beautifully-designed templates come with hundreds of customizable features. Every<br>
template is just a starting point—you can style it to look any way you want.</p>
    </div>
    </div>
</div>