<div class="left-side-bar">
    <ul class="leftmenu">
        <li><a href="#"><dd><i class="fa fa-file-text" aria-hidden="true"></i></dd>News Feed</a></li>
        <li><a href="#"><dd><i class="fa fa-envelope" aria-hidden="true"></i></dd>My Messages</a></li>
        <li><a href="#"><dd><i class="fa fa-user" aria-hidden="true"></i></dd>My Followings</a></li>
        <li><a href="#"><dd><i class="fa fa-users" aria-hidden="true"></i></dd>My Followers</a></li>
        <li><a href="#"><dd><i class="fa fa-file-text-o" aria-hidden="true"></i></dd>My Orders</a></li>
        <li><a href="#" class="active"><dd><i class="fa fa-bell" aria-hidden="true"></i></dd>My Notifications</a></li>
        <li><a href="#"><dd><i class="fa fa-picture-o" aria-hidden="true"></i></dd>My Albums</a></li>
        <li><a href="#"><dd><i class="fa fa-folder-open" aria-hidden="true"></i></dd>My Files</a></li>
        <li><a href="#"><dd><i class="fa fa-star" aria-hidden="true"></i></dd>My Reviews &amp; Ratings</a></li>
    </ul>
</div>