<div class="list-chat-wrapper profile-album-list">
    <div class="tab-butn">
        <a href=""><i class="fa fa-users"></i> Followers</a>
        <a href=""><i class="fa fa-user-plus"></i>Followings</a>
    </div>
    <div class="chat-visible-list">
        <ul>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher1.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Sabir Ali</h4>
                            <p>3 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher2.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Subham Dey</h4>
                            <p>5 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher3.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Sovan Sain</h4>
                            <p>8 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher1.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Sabir Ali</h4>
                            <p>10 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher2.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Subham Dey</h4>
                            <p>8 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="sort-pf-wrapper">
                        <div class="pf-photo"><img src="<?php echo base_url(); ?>assets/images/photo-grapher3.png" alt="" /></div>
                        <div class="pf-desc">
                            <h4>Sovan Sain</h4>
                            <p>6 Albums</p>
                        </div>
                    </div>
                </a>
            </li>
            
        </ul>
    </div>
</div>