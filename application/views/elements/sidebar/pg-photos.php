<div class="my-picture-section">
    <h2>My Photos</h2>
    <div class="picture-wrapper">
        <ul>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict1.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict2.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict3.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict4.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict5.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict6.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict7.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict8.png" alt="" /></a></li>
            <li><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/pict9.png" alt="" /></a></li>
        </ul>
    </div>
    <div class="vew-butn">
        <a href="javascript:;" class="follow">View All</a>
    </div>
</div>