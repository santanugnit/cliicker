<header>
    <nav class="navbar navbar-default myNavBar navbar-fixed-top">
        <div class="container">
            <div class="row">

                <div class="myMenubar">
                    <div class="logo"><a href="#"><img src="<?php echo base_url(); ?>assets/images/logo.png"></a></div>
                    <div class="menuBar"><a href=""><i class="fa fa-bars" aria-hidden="true"></i></a></div>
                </div>

                </div><!-- /.navbar-collapse -->
            </div>
        </div>
    </nav>
</header>

<div class="container-body">
    <div class="slider-contaner" id="slideshow">
        <!-- ------slide1 ----------- -->
        <div class="slider" style="background:#fcebbc">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-4">
                    <div class="row">
                        <div class="banner-text-content">
                            <h3>Make your memories</h3>
                            <h2>beautiful</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="#">PHOTOGRAPHER LOGIN</a>
                        </div>
                    </div>
                    </div>
                
                    <div class="col-md-8 pull-right imgDiv">
                        <div class="img-holder">
                            <img src="<?php echo base_url(); ?>assets/images/home-img1.png" alt="">
                            <div class="thumbslider"></div>
                            <div class="imgslidethumb">
                                <div class="scrollImg"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ------slide2 ----------- -->
        <div class="slider" style="background:#d6a5ea">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-4">
                    <div class="row">
                        <div class="banner-text-content">
                            <h3>Make your memories</h3>
                            <h2>beautiful</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="#">PHOTOGRAPHER LOGIN</a>
                        </div>
                    </div>
                    </div>
                
                    <div class="col-md-8 pull-right imgDiv">
                        <div class="img-holder">
                            <img src="<?php echo base_url(); ?>assets/images/home-img1.png" alt="">
                            <div class="thumbslider"></div>
                            <div class="imgslidethumb">
                                <div class="scrollImg"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ------slide3 ----------- -->
        <div class="slider" style="background:#72c4f5">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-4">
                    <div class="row">
                        <div class="banner-text-content">
                            <h3>Make your memories</h3>
                            <h2>beautiful</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="#">PHOTOGRAPHER LOGIN</a>
                        </div>
                    </div>
                    </div>
                
                    <div class="col-md-8 pull-right imgDiv">
                        <div class="img-holder">
                            <img src="<?php echo base_url(); ?>assets/images/home-img1.png" alt="">
                            <div class="thumbslider"></div>
                            <div class="imgslidethumb">
                                <div class="scrollImg"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
