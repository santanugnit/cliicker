<header>
    <nav class="navbar navbar-default myNavBar navbar-fixed-top">
        <div class="container">
            <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="myMenubar">

                    <div class="logo"><a href="#"><img src="<?php echo base_url(); ?>assets/images/logo.png"></a></div>

                    <div class="col-md-3">
                        <div class="row">
                            <div class="photographer-login-area">
                                <span class="ph-logo"><img src="<?php echo base_url(); ?>assets/images/photo-logo.png"></span>
                                <div class="ph-login-area">
                                    <p>Photographers</p>
                                    <a class="pad-15-rigt divider" href="<?php echo base_url(); ?>auth/signup">Sign up</a>
                                    <a class="pad-15-left" href="<?php echo base_url(); ?>auth">Login</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3 pull-right">
                        <div class="row">
                            <div class="social-icon-area">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                </ul>

                            </div>
                        </div>
                    </div>


                </div>

            </div><!-- /.navbar-collapse -->
        </div>
        </div>
    </nav>
</header>


<!-- start banner area -->
<section class="banner-area">
    <div class="container">
        <div class="banner_txt">
            <h1>Welcome to Clicker </h1>
            <h2>stock your journey</h2>
            <span>lorem ipsum dolor sit</span>
        </div>
    </div>

    <div class="down_arrow"><a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/down_arrow.png"></a></div>

</section>
<!-- end banner area -->