
<?php //echo "<pre>"; print_r($pg_lists); ?>
<header>
    <nav class="navbar navbar-default inner-myNavBar navbar-fixed-top">
        <div class="container">
            <div class="row">

                <div class="myMenubar">
                    <div class="col-md-1">
                        <div class="row">
                            <div class="innerlogo">
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/inner-logo.png"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="row">
                            <div class="seraching-area">
                                <form action="" onsubmit="return false;">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="search" placeholder="Search">
                                    </div>
                                    <input type="submit" class="btn btn-default" value="">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="after-login-option">
                            
                            <?php if( isset($is_pg_login) && $is_pg_login ){ ?>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <span class="userPic">
                                                <img src="<?php echo ( isset($pg_lists['featured_image']) && file_exists($pg_lists['img_url'].$pg_lists['featured_image']) ) ? $pg_lists['img_url'].$pg_lists['featured_image'] : base_url()."assets/images/no-theme-image.jpeg" ; ?>" alt="<?php echo base_url(); ?>assets/images/4.jpg" />
                                            </span>
                                            <dd class="nams"><?php echo $pg_lists['first_name'] .' '. $pg_lists['last_name']; ?></dd>
                                        </a>
                                    </li>

                                    <li><a href="#" class="users"><i class="fa fa-user" aria-hidden="true"></i></a></li>


                                    <li>
                                        <a href="javascript:void(0);" class="notification">
                                            <?php if( isset($pg_nofify) && !empty($pg_nofify) ) { ?><span class="notifi-alert"><?php echo count($pg_nofify); ?></span><?php } ?><i class="fa fa-bell" aria-hidden="true"></i>
                                        </a>
                                    </li>


                                    <li><a href="#" class="askBtn"><i class="fa fa-question-circle" aria-hidden="true"></i><dd><i class="fa fa-caret-down" aria-hidden="true"></i></dd></a></li>
                                </ul>
                            <?php } ?>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        </div>
    </nav>
</header>