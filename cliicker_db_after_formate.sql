-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2018 at 03:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cliicker_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `album_code` varchar(15) DEFAULT NULL COMMENT 'auto generated alpha numeric number like ALB/43/13 -> ALB – prefix of album, 43 – photographer id, 13 – Total album number of this photographer',
  `creation_datetime` datetime DEFAULT NULL,
  `photographer_id` int(11) DEFAULT NULL,
  `occasion_id` int(11) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `short_description` varchar(500) DEFAULT NULL,
  `intro_text` text,
  `preferred_subdomain_name` varchar(255) DEFAULT NULL COMMENT 'suppose abc or xyz',
  `max_storage_allocate` double DEFAULT NULL,
  `space_usage` double DEFAULT NULL,
  `google_drive_link` text,
  `dropbox_link` text,
  `album_price` double DEFAULT NULL,
  `total_pay_amount` double DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `last_payment_txn_id` varchar(255) DEFAULT NULL,
  `is_last_payment_completed` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_project_completed` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `added_date` datetime NOT NULL DEFAULT '0000-00-00',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album_code`, `creation_datetime`, `photographer_id`, `occasion_id`, `album_name`, `short_description`, `intro_text`, `preferred_subdomain_name`, `max_storage_allocate`, `space_usage`, `google_drive_link`, `dropbox_link`, `album_price`, `total_pay_amount`, `last_payment_date`, `last_payment_txn_id`, `is_last_payment_completed`, `is_project_completed`, `is_deleted`, `added_date`, `updated_date`) VALUES
(1, 'ALB/2/1', '2018-03-28 16:39:15', 2, 1, 'SS Wedding ceremony', 'test SS Wedding ceremony', 'SS Wedding ceremony test', 'ss_saha', 1073741824, 6572679, 'https://googledrive.com/hashdhadhaldh/klhsda63387lkhhfd/gdb/iasiyday8008', 'https://dropbox.drive.com/hashdhadhaldh/klhsda63387lkhhfd/gdb/iasiyday8008', 15000, NULL, NULL, NULL, 'N', 'N', 'Y', '2018-03-28 16:39:15', '2018-04-12 13:47:25'),
(2, 'ALB/2/2', '2018-03-28 16:44:50', 2, 1, 'Som Baby shower', 'Test urgent Som Baby shower', 'Som Baby shower urgent', 'ss_baby', 1073741824, 450192100, 'https://googledrive.com/hashdhadhaldh/klhsda63387lkhhfd/gdb/iasiyday8008', 'https://dropbox.drive.com/hashdhadhaldh/klhsda63387lkhhfd/gdb/iasiyday8008', 16000, NULL, NULL, NULL, 'N', 'N', 'Y', '2018-03-28 16:44:50', '2018-03-30 14:56:36'),
(3, 'ALB/1/3', '2018-04-11 19:09:31', 1, 1, 'Bidhan Marriage', 'Bidhan Marriage test', 'test Bidhan Marriage intro', 'bidhan_marriage', 1073741824, 709422601, 'https://googledrive.com/hashdhadhaldh/klhsda63387lkhhfd/bidhan/iasiyday8008', 'https://dropbox.drive.com/hashdhadhaldh/klhsda63387lkhhfd/bidhan/iasiyday8008', 15000, NULL, NULL, NULL, 'N', 'N', 'N', '2018-04-11 19:09:31', '2018-04-19 16:11:07'),
(4, 'ALB/1/4', '2018-04-25 12:53:08', 1, 1, 'Debtanu Marriage', 'Test Album Description ', 'Debtanu Marriage intro text', 'DD_maity', 1073741824, 28624216, '', '', 15000, NULL, NULL, NULL, 'N', 'N', 'N', '2018-04-25 12:53:08', '2018-04-25 12:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `album_selected_themes`
--

CREATE TABLE `album_selected_themes` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL COMMENT 'FK of albums table',
  `album_theme_id` int(11) NOT NULL COMMENT 'FK of "album_themes" table',
  `is_activated` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `album_themes`
--

CREATE TABLE `album_themes` (
  `id` int(11) NOT NULL,
  `occasion_id` int(11) NOT NULL,
  `theme_type` enum('SD','C') DEFAULT 'SD' COMMENT '''SD'': System Defined || ''C'': Customized',
  `theme_name` varchar(255) NOT NULL,
  `short description` varchar(255) DEFAULT NULL,
  `theme_thumbnail_image` varchar(255) NOT NULL,
  `status` enum('Y','N') DEFAULT 'Y',
  `added_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `album_uploaded_files`
--

CREATE TABLE `album_uploaded_files` (
  `id` int(11) NOT NULL,
  `album_id` int(11) DEFAULT NULL COMMENT 'FK of albums table',
  `upload_file_name` varchar(255) DEFAULT NULL,
  `upload_file_size` double DEFAULT NULL,
  `uploaded_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_uploaded_files`
--

INSERT INTO `album_uploaded_files` (`id`, `album_id`, `upload_file_name`, `upload_file_size`, `uploaded_datetime`) VALUES
(2, 1, '1522235328_2357941_o_to_s.zip', 225096050, '2018-03-28 16:38:48'),
(3, 2, '1522235524_6604953_o_to_s.zip', 225096050, '2018-03-28 16:42:04'),
(4, 2, '1522235622_4608387_i_to_n.zip', 470833957, '2018-03-28 16:43:42'),
(9, 2, '1522401967_8476409_o_to_s.zip', 225096050, '2018-03-30 14:56:07'),
(10, 1, '1523520063_3095477_3.zip', 347236, '2018-04-12 13:31:03'),
(11, 1, '1523520797_2794604_3.zip', 347236, '2018-04-12 13:43:17'),
(12, 1, '1523520909_6582372_Sample_Pictures.zip', 6572679, '2018-04-12 13:45:09'),
(13, 1, '1523520910_1209679_Sample_pictures.zip', 6572679, '2018-04-12 13:45:10'),
(14, 1, '1523520952_2871195_Sample_pictures.zip', 6572679, '2018-04-12 13:45:52'),
(46, 3, '1523869523_7580502_Sample_pictures.zip', 6572679, '2018-04-16 14:35:23'),
(47, 3, '1523869524_2672851_3.zip', 347236, '2018-04-16 14:35:24'),
(49, 3, '1523874153_9597208_Sample_pictures.zip', 6572679, '2018-04-16 15:52:33'),
(50, 3, '1523878208_8477935_i_to_n.zip', 470833957, '2018-04-16 17:00:08'),
(51, 3, '1524134464_3587066_o_to_s.zip', 225096050, '2018-04-19 16:11:05'),
(52, 4, '1524641087_8621048_dd_maity.zip', 28624216, '2018-04-25 12:54:47'),
(53, 0, '1524726575_583216_dd_maity.zip', 28624216, '2018-04-26 12:39:35'),
(54, 0, '1524731034_9638097_dd_maity.zip', 28624216, '2018-04-26 13:53:55'),
(55, 0, '1524731489_3351494_dd_maity.zip', 28624216, '2018-04-26 14:01:29'),
(56, 0, '1524731940_7987261_dd_maity.zip', 28624216, '2018-04-26 14:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `header_banner`
--

CREATE TABLE `header_banner` (
  `id` int(11) NOT NULL,
  `controller_name` varchar(100) NOT NULL,
  `page_identifier` varchar(255) NOT NULL,
  `image_path` text NOT NULL,
  `image_text` varchar(255) NOT NULL,
  `default_image_path` text NOT NULL,
  `is_callback_function_exists` enum('N','Y') NOT NULL DEFAULT 'N',
  `is_slider_available` enum('N','Y') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_banner`
--

INSERT INTO `header_banner` (`id`, `controller_name`, `page_identifier`, `image_path`, `image_text`, `default_image_path`, `is_callback_function_exists`, `is_slider_available`) VALUES
(1, 'home', 'home', 'images/banner_images/banner1.jpg', '', 'images/banner_images/banner1.jpg', 'N', 'Y'),
(2, 'page', 'page/mission-and-vision', 'images/banner_images/Mission_Vision.jpg', '', 'images/banner_images/inner-banner.jpg', 'N', 'N'),
(3, 'page', 'page/chairman-message', 'images/banner_images/Chairman_Message.jpg', '', 'images/banner_images/inner-banner.jpg', 'N', 'N'),
(4, 'page', 'page/senior-management-team', 'images/banner_images/Management_Team.jpg', '', 'images/banner_images/inner-banner.jpg', 'N', 'N'),
(5, 'page', 'page/shri-educare-milestone', 'images/banner_images/Milestones.jpg', '', 'images/banner_images/inner-banner.jpg', 'N', 'N'),
(6, 'page', 'page/shri-board-of-directors', 'images/banner_images/Shri_Board_of_Directors.jpg', '', 'images/banner_images/inner-banner.jpg', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `header_banner_slider`
--

CREATE TABLE `header_banner_slider` (
  `id` int(11) NOT NULL,
  `slider_text` varchar(500) NOT NULL,
  `header_banner_id` int(11) NOT NULL,
  `image_path` text NOT NULL,
  `display_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_banner_slider`
--

INSERT INTO `header_banner_slider` (`id`, `slider_text`, `header_banner_id`, `image_path`, `display_order`) VALUES
(1, '<h2>Changing the <br>face of education,</h2>\r\n				<p>The world is growing daily in scale, complexity and severity. The changing  game of education helps the youth to follow its passion and make things happen. We desperately need the next generation to be more adaptable, smarter and prepared than before. </p>', 1, 'images/banner_images/banner1.jpg', 1),
(2, '<h2>Our presence at <br>multiple locations in India</h2>\r\n				<p>Over the years, Shri Educare has garnered its reputation as one of the leading institute in India. We use research based, high quality practices to help the children develop social-emotional and academic skills essential for success in life.</p>', 1, 'images/banner_images/banner2.jpg', 2),
(3, '<h2>Our Services</h2>\r\n				<p>We understand how the education system works. Shri Educare Limited provides efficient and competent solutions and enables the students to achieve its goals</p>', 1, 'images/banner_images/banner3.jpg', 3),
(4, '<h2>Our blog</h2>\r\n				<p>Blogs act as an important source to deepen the connection with people. Our aim is  to act as a center of excellence  and spread the knowledge  and keep the pace  with  the dynamic and rapidly changing global economy.</p>', 1, 'images/banner_images/banner4.jpg', 4);

-- --------------------------------------------------------

--
-- Table structure for table `occasions`
--

CREATE TABLE `occasions` (
  `id` int(11) NOT NULL,
  `occasion_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `invoice_no` varchar(50) DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `pay_amount` double DEFAULT NULL,
  `payment_details` text,
  `transaction_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `is_payment_completed` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photographers`
--

CREATE TABLE `photographers` (
  `id` int(11) NOT NULL,
  `pg_clicker_code` varchar(15) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `address` text,
  `email` varchar(255) NOT NULL,
  `password` varchar(150) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `best_photos` varchar(500) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT 'M' COMMENT '''M'': Male || ''F'': Female',
  `creation_datetime` datetime DEFAULT NULL,
  `last_login_datetime` datetime DEFAULT NULL,
  `reg_number` varchar(50) NOT NULL COMMENT 'Generate random string when photographer registered. ',
  `is_email_verified` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '''Y'': Email verified || ''N'': Email not verified',
  `account_activation_status` enum('NV','V','B','D') NOT NULL DEFAULT 'NV' COMMENT '''NV'': Non Verified || ''V'': Verified || ''B'': Photographer is blocked by site admin || ''D'': Photographer is deleted by both admin or photographer self.',
  `added_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photographers`
--

INSERT INTO `photographers` (`id`, `pg_clicker_code`, `first_name`, `last_name`, `address`, `email`, `password`, `contact_number`, `best_photos`, `featured_image`, `gender`, `creation_datetime`, `last_login_datetime`, `reg_number`, `is_email_verified`, `account_activation_status`, `added_date`, `updated_date`) VALUES
(1, 'CLR2603181', 'Santanu', 'Maity', 'DL 205, Saltlake Sec-2, Kolkata', 'santanu.gnit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', '15220504481.jpg,15220504482.jpg,15220504483.jpg', '4115048_1523967484.jpg', 'M', '2018-03-26 13:17:28', '2018-05-28 12:41:48', '1522050448_978151', 'Y', 'V', '2018-03-26 13:17:28', '2018-05-28 12:41:48'),
(2, 'CLR2603182', 'Tritish', 'Das', 'Test ranku Address', 'ranku_das@gmail.com', '25d55ad283aa400af464c76d713c07ad', '12345678', '15220508871.jpg,15220508872.jpg', '1522050887.jpg', 'M', '2018-03-26 13:24:47', NULL, '1522050887_475454', 'N', 'V', '2018-03-26 13:24:47', '2018-03-28 15:01:06'),
(4, 'CLR1004184', 'Amit', NULL, NULL, 'amit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9874563210', '4238338_1523360257.jpg,5740093_1523360258.jpg,6776765_1523360258.jpg', NULL, 'M', '2018-04-10 17:07:39', NULL, '1523360259_24480', 'N', 'NV', '2018-04-10 17:07:39', '2018-05-28 12:36:36'),
(5, 'CLR1004185', 'Barun', NULL, NULL, 'barun@gmail.com', '202cb962ac59075b964b07152d234b70', '987452136', '8832103_1523360402.jpg,5684246_1523360402.jpg,5533186_1523360403.jpg', NULL, 'M', '2018-04-10 17:10:03', NULL, '1523360403_266185', 'N', 'NV', '2018-04-10 17:10:03', '2018-04-10 17:10:04');

-- --------------------------------------------------------

--
-- Table structure for table `seo_contents`
--

CREATE TABLE `seo_contents` (
  `id` int(11) NOT NULL,
  `page_identifier` varchar(255) NOT NULL,
  `controller_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `controller_rewrite_name` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo_contents`
--

INSERT INTO `seo_contents` (`id`, `page_identifier`, `controller_name`, `controller_rewrite_name`, `item_id`, `item_title`, `slug`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'page', 'home', 'home', 1, 'Home', 'home', 'Home', 'Home', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `website_name` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `wesite_address` varchar(500) NOT NULL,
  `site_contact_email` varchar(255) NOT NULL,
  `site_contact_number` varchar(255) NOT NULL,
  `address_latitude` varchar(150) NOT NULL,
  `address_longitude` varchar(150) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `most_popular_places_ids` varchar(20) NOT NULL COMMENT 'Store State ids with comma separator'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `website_name`, `website_url`, `wesite_address`, `site_contact_email`, `site_contact_number`, `address_latitude`, `address_longitude`, `fb_link`, `twitter_link`, `most_popular_places_ids`) VALUES
(1, 'Codopoliz', 'http://www.codopoliz.com/demo/cliicker', 'Salt Lake, Sector - II, DL - 205, Kolkata - 700091', 'contact@codopoliz.com', '+91-33 4008 0601', '22.580756', '88.427220', 'http://facebook.com/CLIICKER', 'http://facebook.com/CLIICKER', '37,12,16,10');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `page_content` text CHARACTER SET utf8 NOT NULL,
  `is_small_content_available` enum('Y','N') NOT NULL DEFAULT 'Y',
  `small_content` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_active` char(1) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `page_title`, `menu_name`, `page_content`, `is_small_content_available`, `small_content`, `parent_id`, `is_active`, `sort_order`) VALUES
(1, 'Home', 'home', '', 'N', '', 0, 'Y', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `show_email` enum('Y','N') NOT NULL DEFAULT 'N',
  `contact_number` varchar(30) NOT NULL,
  `show_contact` enum('Y','N') NOT NULL DEFAULT 'N',
  `website_url` text,
  `featured_image` varchar(255) NOT NULL,
  `testimonial_text` text,
  `status` enum('Y','N','D') NOT NULL DEFAULT 'Y',
  `created_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `client_name`, `client_email`, `show_email`, `contact_number`, `show_contact`, `website_url`, `featured_image`, `testimonial_text`, `status`, `created_datetime`) VALUES
(1, 'Dhurin Bhattacharya', 'dhurin@gmail.com', 'Y', '9845632170', '', 'http://www.codopoliz.com/', '1522419071154.jpg', 'Test', 'Y', '2018-03-30 17:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_type` enum('SUPERADMINISTRATOR','EDITOR') NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `password` text NOT NULL,
  `password_text` varchar(30) NOT NULL,
  `last_profile_update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_logout_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_status` tinyint(1) NOT NULL,
  `account_activation_status` enum('A','IA','B','D') NOT NULL COMMENT '[A=Active, IA=Inactive, B=Blocked, D=Deleted]'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_type`, `first_name`, `last_name`, `email`, `contact_email`, `address`, `country`, `state`, `city`, `zip`, `phone`, `profile_image`, `creation_date`, `password`, `password_text`, `last_profile_update_datetime`, `last_login_datetime`, `last_logout_datetime`, `login_status`, `account_activation_status`) VALUES
(1, 'SUPERADMINISTRATOR', 'Priyam', 'Ghosh', 'codopoliz.solutions@gmail.com', 'priyam.g.codopoliz@gmail.com', 'Salt Lake, Sector - II, DL - 205, Kolkata - 700091', 'India', '', '', '445555', '+91-33 4008 0601', '', '2015-05-11 00:00:00', 'e91b52bb672057be6040d0e10f997d51', 'nopass123', '2017-09-07 18:03:19', '2018-04-27 12:25:59', '2018-02-07 16:03:02', 1, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `album_code` (`album_code`);

--
-- Indexes for table `album_selected_themes`
--
ALTER TABLE `album_selected_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album_themes`
--
ALTER TABLE `album_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album_uploaded_files`
--
ALTER TABLE `album_uploaded_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `header_banner`
--
ALTER TABLE `header_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header_banner_slider`
--
ALTER TABLE `header_banner_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occasions`
--
ALTER TABLE `occasions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`);

--
-- Indexes for table `photographers`
--
ALTER TABLE `photographers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `seo_contents`
--
ALTER TABLE `seo_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `album_selected_themes`
--
ALTER TABLE `album_selected_themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `album_themes`
--
ALTER TABLE `album_themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `album_uploaded_files`
--
ALTER TABLE `album_uploaded_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `header_banner`
--
ALTER TABLE `header_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `header_banner_slider`
--
ALTER TABLE `header_banner_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `occasions`
--
ALTER TABLE `occasions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `photographers`
--
ALTER TABLE `photographers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `seo_contents`
--
ALTER TABLE `seo_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
