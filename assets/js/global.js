// JavaScript Document

$(document).ready(function(){
	
	stickyHeader();
	//tabber();
	
	//$(".container-body").height(winH);
	
	//clientfeedback();					// client feedback slider
	templeteViewslider();				// verious type of templete slider
	bannerSlider();						// Banner slider
	bannerAutoHeight();
	loginpage();						// Login and Sign up page full width jQuery
	Loginpagetabber();					// Login and sing up tabber
	

});

function Loginpagetabber()
{
	$(".loginBtn-area ul li").find('a').click(function(e) {
        var tab_id = $(this).attr('data-name');
		//alert(tab_id)
		
		if(tab_id == 'signup'){
			$(".login-bg").hide();
			$(".signUp").show();
			$(this).addClass('activeLi2');
			$(".loginBtn-area ul li a").removeClass('activeLi');
			
		}else{
			$(".login-bg").show();
			$(".signUp").hide();
			//$(this).removeClass('activeLi2');
			$(".loginBtn-area ul li a").addClass('activeLi');
			$(".loginBtn-area ul li a.sgn").removeClass('activeLi2');
			$(".loginBtn-area ul li a.sgn").removeClass('activeLi');
			
		}
		
    });
	
}

function loginpage(){
	var winH = $(window).height();
	var winW = $(window).width();
	$("#clientsSlider").height(winH);
	
	var loginBgW = $(".login-back-area").width();
	var loginBgH = $(".login-back-area").height();
	var centerPos = (winW - loginBgW ) / 2;
	var centerPosT = (winH - loginBgH ) / 2;
	
	$(".login-back-area").css({"left": centerPos,"top": centerPosT  + 50});
	
	var ulcontener = $(".loginBtn-area").height();
	var ulH = $(".loginBtn-area ul").height();
	var UlcenterPos = (ulcontener - ulH) / 2;
	
	$(".loginBtn-area ul").css({"top": UlcenterPos});	
	
}

function bannerAutoHeight(){
	
	var winH = window.innerHeight;
	//alert(winH)
	$(".container-body").height(winH);
	$(".slider-contaner").height(winH);
	$(".slider-contaner").find(".slider").height(winH);
	
}

function bannerSlider(){
	
var slideshow = (function () {
        var counter = 0,
            i,
            j,
            slides =  $("#slideshow .slider"),
            slidesLen = slides.length - 1;
        for (i = 0, j = 9999; i < slides.length; i += 1, j -= 1) {
            $(slides[i]).css("z-index", j);
        }
        return {
            startSlideshow: function () {
                window.setInterval(function () {
                    if (counter === 0) {
                        slides.eq(counter).fadeOut();
                        counter += 1;
                    } else if (counter === slidesLen) {
                        counter = 0;
                        slides.eq(counter).fadeIn(function () {
                            slides.fadeIn();
                        });
                    } else {
                        slides.eq(counter).fadeOut();
                        counter += 1;
                    }
                }, 10000);
            }
        };
    }());
    slideshow.startSlideshow();	
	
}

function templeteViewslider(){
	
	var counter = 0;
	var shiftAmount = 530 + 40;
	
	var totalSlide = $(".temple-slide-area ul li").length;
	var slidewidt = $(".temple-slide-area ul li").width();
	var total_slideWidt = totalSlide * slidewidt;
	$(".temple-slide-area ul").css({width:total_slideWidt});
	
	$(".left_arrow").click(function(e){
		e.preventDefault();
		if(counter <totalSlide - 2){
			counter = counter + 1;
			// $('.temple-slide-area ul li:last').after('');
		}
		
		$(".temple-slide-area ul").animate({"left":(counter*shiftAmount*(-1))});
		
	});
	
	
	$(".right_arrow").click(function(e){
		e.preventDefault();							 
		if(counter > 0){
			counter = counter - 1;
		}
		$(".temple-slide-area ul").animate({"left":(counter*shiftAmount*(-1))});			 
	});
	
}

function clientfeedback(){
	
	$('.feedbakslider').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  dots: false,
	  centerMode: true,
	  arrows: false,
	  centerPadding: '150px',
	  focusOnSelect: true
	});
}





function tabber()
{
	$(".loginBtn-area ul li").find('a').click(function(e) {
        var tab_id = $(this).attr('data-name');
		//alert(tab_id)
		
		if(tab_id == 'signup'){
			$(".login-bg").hide();
			$(".signUp").show();
			$(this).addClass('activeLi2');
			$(".loginBtn-area ul li a").removeClass('activeLi');
			
		}else{
			$(".login-bg").show();
			$(".signUp").hide();
			//$(this).removeClass('activeLi2');
			$(".loginBtn-area ul li a").addClass('activeLi');
			$(".loginBtn-area ul li a.sgn").removeClass('activeLi2');
			$(".loginBtn-area ul li a.sgn").removeClass('activeLi');
			
		}
		
    });
	
}

function testomonialslider()
{
	$(".testimon-slider").slick({
		dots: true,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1
	  });	
}

function partnerslider()
{
	$(".partner").slick({
		dots: true,
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1
	  });	
}

function isotopeTabber()
{
	
		var $grid = $('.grid').isotope({
			 filter: '.Men',
			 transitionDuration: 900,
			 animationOptions: {
			 duration: 5000,
			 easing: 'linear',
			 queue: false
		   }
		});
			
		$grid.imagesLoaded().progress( function() {
		  $grid.isotope('layout');
		});	

		// filter functions
		var filterFns = {
		  // show if number is greater than 50
		  numberGreaterThan50: function() {
			var number = $(this).find('.number').text();
			return parseInt( number, 10 ) > 50;
		  },
		  // show if name ends with -ium
		  ium: function() {
			var name = $(this).find('.name').text();
			return name.match( /ium$/ );
		  }
		};
		
		
		// bind filter button click
		$('#filters').on( 'click', 'a', function() {
		    var filters = $(this).data('filter');
        	var parent = $(this).closest('#filter-buttons-holder');
        	$('.product-filter a').removeClass('selected');
        	$(this).addClass('selected');
        	$grid.isotope({ filter: filters });
        	return false;
		});
}


function stickyHeader(){
	
	$(window).scroll(function() {
	if ($(this).scrollTop() > 150){  
		$('.myNavBar').addClass("sticky");
	  }
	  else{
		$('.myNavBar').removeClass("sticky");
	  }
	});	
	
}

function fixcolorbomb(){
	
	$(window).scroll(function() {
		console.log($(this).scrollTop());
	  
        if ($(this).scrollTop() > 1152) {
            $('.colorbomb').addClass("fix");
            if ($(this).scrollTop() > 1507) {
                $('.colorbomb').addClass("hides");
            } else {
               $('.colorbomb').removeClass("hides");
            }
        } else {
            $('.colorbomb').removeClass("fix");
		}
		
		if ($(this).scrollTop() > 1511) {
            $('.colorbomb4').addClass("fix2");
            if ($(this).scrollTop() > 2280) {
                $('.colorbomb4').addClass("hides");
            } else {
               $('.colorbomb4').removeClass("hides");
            }
        } else {
            $('.colorbomb4').removeClass("fix2");
		}
		
		
		if ($(this).scrollTop() > 1380) {
            $('.colorbomb5').addClass("fix3");
            if ($(this).scrollTop() > 1970) {
                $('.colorbomb5').addClass("hidess");
            } else {
               $('.colorbomb5').removeClass("hidess");
            }
        } else {
            $('.colorbomb5').removeClass("fix3");
		}
	 
		
	});	
	
}


