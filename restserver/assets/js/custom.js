/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*------Sorting of grid page--------------*/

$( document ).ready(function() {
	
	
    $( "th" ).each(function() {      
        if( $( this ).attr('class')=="sorting")
        {  
            var current_url=document.URL;
            var splits_url = current_url.split("/", 50);
            for(i=0; i<=(splits_url.length)-1; i++){
             /*--- If found in URL ---*/
                if(splits_url[i]=="order")
                {   
                    if($( this ).attr('data')==splits_url[i+1]){
                        $( this ).removeClass('sorting');
                            if(splits_url[i+2]=="asc")
                                $( this ).addClass('sorting_asc');
                            else
                                $( this ).addClass('sorting_desc');
                    }     
                }
            }
           
        }
    });
	
	$(window).scroll(function() {
		
		//var headH = $("#header").height();
		var bodyH = $(window).height() - 2;
		//var MyScrollH = bodyH - headH
		
		//alert(headH)
		
		if($(this).scrollTop() > 48){
			$('#sidebar').addClass("sticky");
			$('.main-menu').addClass('inner-content-div');
		}else{
			$('#sidebar').removeClass("sticky");
			$('.main-menu').removeClass('inner-content-div');
		}
		
		$('.inner-content-div').slimScroll({
			height: bodyH
		});
	});
	
	
	

});

function set_pagination_sort(col,current_url){
	alert(current_url); alert(col);
    var flag=0; /*-Flag for found or not found -*/
    var splits = current_url.split("/", 50);
    for(i=0; i<=(splits.length)-1; i++){
     /*--- If found in URL ---*/
     if(splits[i]=="order")
     {
     splits[i+1]=col;
	 
          if(splits[i+2]=="desc")
              splits[i+2]="asc";
          else
              splits[i+2]="desc";

     flag=1;
     }     

    }

    /*--- If Not found in URL(For First Time) ---*/
    if(flag==0){
        url=current_url+"/order/"+col+"/desc";
        var frm = document.getElementById("frmlist");
        frm.action=url;
        frm.submit();          
    }else{
        url=splits.join("/");
        var frm = document.getElementById("frmlist");
        frm.action=url;
        frm.submit();
    }
      
    
}


/* 
 * Function use for submitting a page through form
/*------Page submit through url--------------*/
function list_page_redirect(url, is_confirm)
{   
    var frm = document.getElementById("frmlist");
    if(is_confirm!="" && is_confirm==1)
    {    
        
		jConfirm('Are you want to do the changes?', 'Confirmation', function(r){
			if(r) 
			{
				frm.action=url;
				frm.submit();
			}
			else
			{
				return false;
			}
		});
	}
    else
    {
        frm.action=url;
        frm.submit();
    }    
}

/* 
 * Function use for submitting a page through form for multiple action
/*------Page submit through url for multiple action--------------*/
function action_on_selected_items(url)
{       
    var flg=0;
    $(".btn-checkbox").find("input").each(function(){        
    if ($(this).prop('checked')==true){ 
      flg=1;      
    }    
    });
    
    if(flg==1)
    {    
        var frm = document.getElementById("frmlist");
		jConfirm('Are you sure?', 'Confirmation', function(r){
			if(r) 
			{
				frm.action=url;
				frm.submit();
			}
			else
			{
				return false;
			}
		});
    }
    else
    {
        jAlert("No item selected. Please select atleast one!","Message");
        return false;
    }    
    
}


/*-------Check BOX click to active------------*/
jQuery( document ).ready(function() {
   jQuery('.top-checkbox').click(function(){  
       
        if(jQuery('.top-checkbox').hasClass("active")){    
            //alert('has'); return false;
            jQuery('.btn-checkbox').removeClass('active');             
            jQuery('.table input:checkbox').attr('checked',false); 
        }
        else{
            //alert('hasn'); return false;
            jQuery('.table input:checkbox').attr('checked',true); 
            jQuery('.btn-checkbox').addClass('active');   
        }
       
   });  
});

/*-----------Alert Auto Close ---------------------*/
jQuery(document).ready(function() {
  //Top message auto hide------------------------------------ 
  if($('div').hasClass('header_notification_message')==true){
    setTimeout( function(){         
        $(".header_notification_message").hide();
    }
    , 5000 );
  }
    
});



/*----------- SHOW GLOBAL TOASTR-------------------*/
function show_toastr(msg_type,msg_title,msg_text){    
 if(msg_type==1) toastr.info(msg_text,msg_title);   
 if(msg_type==2) toastr.warning(msg_text,msg_title); 
 if(msg_type==3) toastr.error(msg_text,msg_title); 
 if(msg_type==4) toastr.success(msg_text,msg_title); 
}