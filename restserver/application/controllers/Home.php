<?php
require(APPPATH.'/libraries/REST_Controller.php');
class Home extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model');
        $this->api_access_key = get_api_access_key();
    }

    public function index()
    {
        if($this->api_access_key == md5($this->post('apitoken')."#SHA@")) //checking for valid apitoken
        {


        }
    }

    ##---------------------------------------------------------------##
    ##---------- Get portfolio list respect to category ID ----------##
    ##---------------------------------------------------------------##
    function save_order_data_post()
    {
        if($this->post())
        {
            if($this->api_access_key == md5($this->post('apitoken')."#SHA@")) //checking for valid apitoken
            {
                $this->load->model('Orders_model');
                $this->load->library('common_functions');
                $save_mode = 1;
                ##-------------- Upload attachment file (If any) -------------##
                $attached_file = ""; //Initialize variable
                if($_FILES['attached_file']['name']!="") //checking for upload feature image
                {
                    $image_data_config = array(
                        'field_name'=>'attached_file', // this is the field name
                        'upload_path'=>'assets/uploaded_files/order_attachment_files/', // this is the original image path
                        'image_resize' => false, //To resize image with medium, set it true
                        'upload_medium_path'=> '', //if image_resize=true, set the path of the medium image.
                        'image_valid_dimensions'=>'', // this data has been set in config file
                        'image_types'=>"*", // this defined variable is defined in constance file
                    );
                    $return_arr = $this->common_functions->image_upload($image_data_config);
                    if($return_arr['message']=="success") //file uploaded successfully
                    {
                        $attached_file = $return_arr['image_info'];
                    }
                    else
                    {
                        $error_arr = array(1, $return_arr['image_info']);
                        $save_mode = 0;
                        //echo "<pre>"; print_r($error_msg); die();
                    }
                }

                if($save_mode==1)
                {
                    $postdata = array(
                        'name'=> $this->post('name'),
                        'email'=> $this->post('email'),
                        'contact_no'=> $this->post('contact_no'),
                        'services'=> $this->post('services'),
                        'requirement'=> $this->post('requirement'),
                        'attached_file' => $attached_file,
                        'contact_datetime' => date('Y-m-d H:i:s')
                    );
                    $this->Orders_model->save_order_data($postdata);

                    $data = array(
                        'api_syntax_success' => 1,
                        'api_action_success' => 1,
                        'api_action_message' => 'success',
                        'validation_error' => array()
                    );
                }
                else
                {
                    $data = array(
                        'api_syntax_success' => 1,
                        'api_action_success' => 0,
                        'api_action_message' => '',
                        'validation_error' => $error_arr
                    );
                }

            } else {
                $data = array(
                    'api_syntax_success' => 1,
                    'api_action_success' => 0,
                    'api_action_message' => 'Invalid access token',
                    'validation_error' => array()
                );
            }
        }
        else
        {
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 0,
                'api_action_message' => 'Insufficient Data...',
                'validation_error' => array()
            );
        }

        $this->response($data, 200);
    }
    ##---------------------------------------------------------------##
    ##---------------------------------------------------------------##


}
