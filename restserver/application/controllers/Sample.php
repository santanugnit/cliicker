<?php
require(APPPATH.'/libraries/REST_Controller.php');
class Sample extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model');
        $this->api_access_key = get_api_access_key();
    }

    function say_hello_world_get()
    {
        if($this->api_access_key == md5($this->get('apitoken')."#SHA@")) //checking for valid apitoken
        {
            /*$this->load->model('speciality_model');
            $record = $this->speciality_model->get_information_speciality(); */
            $record = "Hello Restapi";
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 1,
                'api_action_message' => 'success',
                'result' => $record
            );
        } else {
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 0,
                'api_action_message' => 'Invalid access token',
                'result' => array()
            );
        }

        $this->response($data, 200);
    }



}
?>
