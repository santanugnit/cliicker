<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Template extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('Template_model');
        $this->load->library('common_functions');
        //Assign the model object into a variable for lts re-usability
        $this->temdl_obj    = $this->Template_model;
        $this->ctrlr_name   = $this->uri->segment(1);
        $this->api_access_key = get_api_access_key();
    }    


    function get_templates_lists_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $postData       = array();
        $rs     = $this->temdl_obj->api_get_template_lists($postData);

        $cat_rs = $this->temdl_obj->api_get_template_category_lists();

        $this->load->model('Photographer_model');
        $this->pmdl_obj    = $this->Photographer_model;
        $pg_rs = $this->pmdl_obj->api_get_photographer_lists();


        //$this->response($rs, 200);
        $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rs, 'cat_row' => $cat_rs, 'pg_lists' => $pg_rs );
        $this->response($data, 200);
    }


    function api_templates_filter_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $postData                   = array();
        $postData["s_name"]         = $this->post('s_name');
        $postData["community"]      = $this->post('community');

        $rs = $this->temdl_obj->api_get_template_filter_lists( $postData );

        $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rs );
        $this->response($data, 200);
    }

    function api_send_photographer_template_suggest_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $postData                   = array();
        $postData['temptale_id']    = $this->post('temptale_id');
        $postData['name']           = $this->post('c_name');
        $postData['email']          = $this->post('c_email');
        $postData['contact_no']     = $this->post('c_phone_no');
        $postData['desc']           = $this->post('c_massage');
        /*$pg_ids                     = $this->post('reg_photographer');*/
        $postData['pg_ids']        = $this->post('reg_photographer');

        /*$this->response($postData, 200);
        $postData['pg_ids']         = array();
        if( is_array($pg_ids) && !empty($pg_ids) )
        {
            $postData['pg_ids']     = implode(",", $pg_ids);
        } else {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Atleast one photographer has to be selected.');
            $this->response($data, 200);
        } */       
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('temptale_id', 'template', 'required|trim|xss_clean|is_natural_no_zero');
        $this->form_validation->set_rules('c_email', 'email', 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('c_name', 'name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('c_phone_no', 'phone no', 'required|trim|xss_clean');
        $this->form_validation->set_rules('reg_photographer', 'photographer', 'required|trim|xss_clean|callback_count_selected_photographer_number');

        if ( $this->form_validation->run() == FALSE )
        {
            $return['result']   = validation_errors();
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'message' => $return['result']);
            $this->response($data,200);
        }

        $insID = $this->temdl_obj->api_insert_photographer_template_suggest($postData);
        if ( (int)$insID < 1 ) {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'message' => 'Some error to insert photographer template sggestion.');
            $this->response($data, 200);
        }

        $data['entity_type_id']     = 1;
        $data['entity_id']          = $insID;
        $data['actor_id']           = 0;
        $data['notifier_id']        = $postData['pg_ids'];


        if( $this->temdl_obj->api_do_create_notification( $data ) )
        {
            $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'message' => 'Your suggestion send successfully.' );
            $this->response($data, 200);
        } else {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'message' => 'There is some problem, please contact with admin or try again after minites.');
            $this->response($data,200);
        }

    }


    function count_selected_photographer_number( $str )
    {
        if( preg_match('/\d/', $str) ) 
        {
            $ids  = explode(',', $str);
            if( is_array($ids) && count( $ids ) === count( array_filter($ids, 'is_numeric')) ) {
                return true;
            }
            else
            {
                $this->form_validation->set_message('count_selected_photographer_number', 'The {field} field can only numeric id(s).');
                return FALSE;
            }
        } 
        else 
        {
            $this->form_validation->set_message('count_selected_photographer_number', 'The {field} field can not be id(s)');
            return FALSE;
        }
    }



}

?>
