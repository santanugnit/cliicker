<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Photographer extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('Photographer_model');
        $this->load->library('common_functions');
        //Assign the model object into a variable for lts re-usability
        $this->pmdl_obj = $this->Photographer_model;
        $this->ctrlr_name = $this->uri->segment(1);
        $this->api_access_key = get_api_access_key();
        $this->load->model('Notification_model', 'notify_model');
    }

    /**
     *
     */
    public function photographer_login_post()
    {
        if ($this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $postData       = array();

        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
        if ( $this->form_validation->run() == TRUE )
        {
            $postData["email"]      = $this->post('email');
            $postData["password"]   = $this->post('password');

            $result = $this->pmdl_obj->check_photographer_login($postData);

            if ( !empty($result) && count($result) > 0 )
            {
                $errorArr = $this->common_functions->get_user_status_arr();
                if ($result["is_email_verified"] == "N")
                {
                    $error_arr = array(1, '<li>Your email is no verified.</li>');
                    $error_msg = $this->common_functions->view_message($error_arr);

                    $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                    $this->response($data, 200);
                }
                elseif (isset($result["account_activation_status"]) && array_key_exists($result["account_activation_status"], $errorArr))
                {
                    $error_arr = array(1, '<li>' . $errorArr[$result["account_activation_status"]] . '</li>');
                    $error_msg = $this->common_functions->view_message($error_arr);

                    $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                    $this->response($data, 200);
                } else {
                    $this->pmdl_obj->update_photographer_last_login($result["id"]);
                    $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $result);
                    $this->response($data, 200);
                }

            } else {
                $error_arr = array(1, "Record not found.");
                $error_msg = $this->common_functions->view_message($error_arr);
                //$data['error_message'] = $error_msg;
                $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data, 200);
            }

        } else {
            $error_arr = array(1, validation_errors());
            $error_msg = $this->common_functions->view_message($error_arr);
            //$data['error_message'] = $error_msg;
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data, 200);
        }
    }

    /**
     *
     */
    public function photographer_signup_post()
    {
        #/*$this->response($_FILES, 200); */ $this->response( $this->post('featured_image'), 200);
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $postData   = array();
        $this->form_validation->set_rules('name', 'name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|is_unique[photographers.email]');
        $this->form_validation->set_rules('contact_number', 'contact number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('gender', 'gender', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
        $this->form_validation->set_rules('conf_pass', 'confirm password', 'required|trim|xss_clean|matches[password]');

        if ($this->form_validation->run() == TRUE)
        {
            $postData["first_name"] = $this->post('name');
            $postData["email"]      = $this->post('email');
            $postData["contact_number"] = $this->post('contact_number');
            $postData["gender"]     = $this->post('gender');
            $postData["password"]   = md5($this->post('password'));

            //$uploadImg      = $this->post('featured_image');
            $best_photosArr = array();
            if ( $this->post('featured_image') != "" )
            {
                $uploadImgArr["featured_image"]    = json_decode($this->post('featured_image'), true);
                $cntUploadImg           = count($uploadImgArr["featured_image"]["name"]);

                for ( $i = 0; $i < $cntUploadImg; $i++ )
                {
                    $featured_image     = $uploadImgArr["featured_image"]["name"][$i];
                    #********* Rename the upload file ***********#
                    $filename_arr       = explode(".", $featured_image);
                    $file_extension     = end($filename_arr);
                    $featured_new_image = rand(99, 9999999).'_'. time() . "." . $file_extension;
                    #********************************************#
                    $img_upload_path = 'assets/uploaded_files/photographer_profile_image/original/'.$featured_new_image;
                    if( copy($uploadImgArr["featured_image"]["tmp_name"][$i], $img_upload_path) )
                    {
                        #$postData['featured_image'] = $featured_new_image;
                        array_push($best_photosArr, $featured_new_image);
                        ####################################################################
                        ######### FUNCTIONALITY OF IMAGE RESIZE FOR CLINIC PROFILE #########
                        #******************************************************************#
                        ####################### MEDIUM LOGO CREATION #######################
                        $config['image_library']    = 'gd2';
                        $config['source_image']     = 'assets/uploaded_files/photographer_profile_image/original/'.$featured_new_image;
                        $config['new_image']        = 'assets/uploaded_files/photographer_profile_image/medium/'.$featured_new_image;
                        $config['allowed_types']    = "jpg|jpeg|png";
                        $config['create_thumb']     = FALSE;
                        $config['maintain_ratio']   = TRUE;
                        $config['master_dim']       = 'auto';
                        ########### CHECK UPLOAD DIMENSION ##########
                        $imgarr         = $this->config->item('image_valid_dimensions');
                        $imgarr_data    = $imgarr["photographer_image_medium"];
                        $data_explode   = explode("|", $imgarr_data);
                        $define_image_width     = $data_explode[0];
                        $define_image_height    = $data_explode[1];
                        ########### CHECK UPLOAD IMAGE DIMENSION ##########

                        $getlogoinfo    = getimagesize($uploadImgArr["featured_image"]["tmp_name"][$i]);
                        $iw = $getlogoinfo[0]; //uploaded image width
                        $ih = $getlogoinfo[1]; //uploaded image height

                        $mw = $define_image_width; //Recommended image width
                        $mh = $define_image_height; //Recomended image height

                        if( $iw <= $mw && $ih <= $mh )
                        {
                            $ow    = $iw;
                            $oh    = $ih;
                        }
                        if( $iw <= $mw && $ih > $mh )
                        {
                            $oh    = $mh;
                            $ow    = ($oh*$iw)/$ih;
                        }
                        if( $iw > $mw && $ih <= $mh )
                        {
                            $ow    = $mw;
                            $oh    = ($ow*$ih)/$iw;
                        }
                        if( $iw > $mw && $ih > $mh && $iw > $ih )
                        {
                            $ow    = $mw;
                            $oh    = ($ow*$ih)/$iw;
                        }
                        if( $iw > $mw && $ih > $mh && $iw <= $ih )
                        {
                            $oh    = $mh;
                            $ow    = ($oh*$iw)/$ih ;
                        }

                        $config['width']    = $ow;
                        $config['height']   = $oh;

                        $this->load->library('image_lib', $config);
                        //$this->image_lib->resize();
                        if(!$this->image_lib->resize())
                        {
                            $postData["error"] = $this->image_lib->display_errors();
                        }

                        #################### MEDIUM IMAGE CREATION #######################
                        #****************************************************************#
                        ########## FUNCTIONALITY OF IMAGE RESIZE FOR CLINIC LOGO #########
                        ##################################################################
                    } else {
                        $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => "Image copy error.");
                        $this->response($data, 200);
                    }
                }
                $postData["best_photos"]    = (count($best_photosArr))? implode(',', $best_photosArr) : "";
            }
            else
            {
                $postData["best_photos"] = "";
            }

            $postData["creation_datetime"]  = date("Y-m-d H:i:s");
            $postData["reg_number"]         = time() .'_'. rand(99, 999999);
            $postData["added_date"]         = date("Y-m-d H:i:s");
            //$this->response($this->pmdl_obj->api_add_photographer($postData), 200);
            if ( $this->pmdl_obj->api_add_photographer($postData) ) {
                $success_arr    = array(2, '<li>Registration success.</li>');
                $success_msg    = $this->common_functions->view_message($success_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $success_msg);
                $this->response($data, 200);
            } else {
                $error_arr      = array(1, validation_errors());
                $error_msg      = $this->common_functions->view_message($error_arr);
                $data           = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data, 200);
            }
        }
        else
        {
            $error_arr      = array(1, validation_errors());
            $error_msg      = $this->common_functions->view_message($error_arr);
            $data           = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data, 200);
        }
    }

    /**
     *
     */
    function get_pg_details_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }       

        $rsPG   = $rsNotify = array();

        $postData               = array();
        $postData["pgID"]           = $this->post('pgID');
        $postData["search_pgID"]    = $this->post('search_pgID');
        if ( $this->post('is_pg_login') ) 
        {
            /*$this->form_validation->set_rules('pgID', 'photographer id', 'trim|required|xss_clean');
            if ( $this->form_validation->run() == FALSE )
            {
                $error_arr      = array(1, validation_errors());
                $error_msg      = $this->common_functions->view_message($error_arr);
                $data           = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data, 200);
            }*/            

            /* Get login photographer details */
            $rsPG       = $this->pmdl_obj->api_get_photographer_details_id($postData);
            if( isset($rsPG["no_followers"]) && (int)$rsPG["no_followers"] > 0 ) 
            {
                $rsPG["followers_arr"] = array();
                $followersArr   = $this->pmdl_obj->api_get_multiple_photographer_details(array("pg_ids" => $rsPG["follower_ids"]));
                if( !empty($followersArr) ) {
                    $rsPG["followers_arr"]  = $followersArr;
                }
            }
            if( isset($rsPG["no_following"]) && (int)$rsPG["no_following"] > 0 ) 
            {
                $rsPG["following_arr"] = array();
                $followingArr   = $this->pmdl_obj->api_get_multiple_photographer_details(array("pg_ids" => $rsPG["following_ids"]));
                if( !empty($followingArr) ) {
                    $rsPG["following_arr"]  = $followingArr;
                }
            }


            /* Fetch login user notification */
            $rsNotify   = $this->pmdl_obj->api_get_photographer_notification($postData);
        }

        /* Fetch search photographer details */
        $rsSearchPG = array();
        if( (int)$postData["search_pgID"] !== (int)$postData["pgID"] ) {
            $rsSearchPG = $this->pmdl_obj->api_get_photographer_details_id( array('pgID' => $postData["search_pgID"]) );
        }


        $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rsPG, 'pg_notify' => $rsNotify, 'search_pg' => $rsSearchPG );
        $this->response($data, 200);
    }

    function api_pg_edit_profile_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        /*$data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $this->post());
        $this->response($data, 200);*/

        $postData   = array();

        $this->form_validation->set_rules('first_name', 'first name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('last_name', 'last name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contact_number', 'contact number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('gender', 'gender', 'required|trim|xss_clean');
        $this->form_validation->set_rules('pgID', 'photographer id', 'required|trim|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            $postData["first_name"]     = $this->post('first_name');
            $postData["last_name"]      = $this->post('last_name');
            $postData["contact_number"] = $this->post('contact_number');
            $postData["gender"]         = $this->post('gender');
            $postData["address"]        = $this->post('address');

            $pgID = $this->post('pgID');

            //$uploadImg      = $this->post('featured_image');
            $best_photosArr = array();
            if ( $this->post('featured_image') != "" )
            {
                $uploadImgArr["featured_image"]    = json_decode($this->post('featured_image'), true);
                //$cntUploadImg           = count($uploadImgArr["featured_image"]["name"]);
                $featured_image     = $uploadImgArr["featured_image"]["name"];
                #********* Rename the upload file ***********#
                $filename_arr       = explode(".", $featured_image);
                $file_extension     = end($filename_arr);
                $featured_new_image = rand(99, 9999999).'_'. time() . "." . $file_extension;
                #********************************************#
                $img_upload_path = 'assets/uploaded_files/photographer_profile_image/original/'.$featured_new_image;
                if( copy($uploadImgArr["featured_image"]["tmp_name"], $img_upload_path) )
                {
                    $postData['featured_image'] = $featured_new_image;
                    //array_push($best_photosArr, $featured_new_image);
                    ####################################################################
                    ######### FUNCTIONALITY OF IMAGE RESIZE FOR CLINIC PROFILE #########
                    #******************************************************************#
                    ####################### MEDIUM LOGO CREATION #######################
                    $config['image_library']    = 'gd2';
                    $config['source_image']     = 'assets/uploaded_files/photographer_profile_image/original/'.$featured_new_image;
                    $config['new_image']        = 'assets/uploaded_files/photographer_profile_image/medium/'.$featured_new_image;
                    $config['allowed_types']    = "jpg|jpeg|png";
                    $config['create_thumb']     = FALSE;
                    $config['maintain_ratio']   = TRUE;
                    $config['master_dim']       = 'auto';
                    ########### CHECK UPLOAD DIMENSION ##########
                    $imgarr         = $this->config->item('image_valid_dimensions');
                    $imgarr_data    = $imgarr["photographer_image_medium"];
                    $data_explode   = explode("|", $imgarr_data);
                    $define_image_width     = $data_explode[0];
                    $define_image_height    = $data_explode[1];
                    ########### CHECK UPLOAD IMAGE DIMENSION ##########

                    $getlogoinfo    = getimagesize($uploadImgArr["featured_image"]["tmp_name"]);
                    $iw = $getlogoinfo[0]; //uploaded image width
                    $ih = $getlogoinfo[1]; //uploaded image height

                    $mw = $define_image_width; //Recommended image width
                    $mh = $define_image_height; //Recomended image height

                    if( $iw <= $mw && $ih <= $mh )
                    {
                        $ow    = $iw;
                        $oh    = $ih;
                    }
                    if( $iw <= $mw && $ih > $mh )
                    {
                        $oh    = $mh;
                        $ow    = ($oh*$iw)/$ih;
                    }
                    if( $iw > $mw && $ih <= $mh )
                    {
                        $ow    = $mw;
                        $oh    = ($ow*$ih)/$iw;
                    }
                    if( $iw > $mw && $ih > $mh && $iw > $ih )
                    {
                        $ow    = $mw;
                        $oh    = ($ow*$ih)/$iw;
                    }
                    if( $iw > $mw && $ih > $mh && $iw <= $ih )
                    {
                        $oh    = $mh;
                        $ow    = ($oh*$iw)/$ih ;
                    }

                    $config['width']    = $ow;
                    $config['height']   = $oh;

                    $this->load->library('image_lib', $config);
                    //$this->image_lib->resize();
                    if(!$this->image_lib->resize())
                    {
                        $postData["error"] = $this->image_lib->display_errors();
                    }
                    #################### MEDIUM IMAGE CREATION #######################
                    #****************************************************************#
                    ########## FUNCTIONALITY OF IMAGE RESIZE FOR CLINIC LOGO #########
                    ##################################################################
                } else {
                    $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => "Image copy error.");
                    $this->response($data, 200);
                }
                //$postData["featured_image"]    = (count($best_photosArr))? implode(',', $best_photosArr) : "";
            }


            if ( $this->pmdl_obj->api_edit_photographer_profile($postData, $pgID) )
            {
                $success_arr    = array(2, '<li>Registration success.</li>');
                $success_msg    = $this->common_functions->view_message($success_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $success_msg);
                $this->response($data, 200);
            } else {
                $error_arr      = array(1, '<li>Registration failed.</li>');
                $error_msg      = $this->common_functions->view_message($error_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data, 200);
            }
        }
        else
        {
            $error_arr      = array(1, validation_errors());
            $error_msg      = $this->common_functions->view_message($error_arr);
            $data           = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data, 200);
        }
    }

    function api_pg_change_password_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
       /* $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $this->post());
        $this->response($data, 200);*/

        $postData = array();
        $this->form_validation->set_rules('pgID', 'photographer id', 'trim|required');
        $this->form_validation->set_rules('old_pwd', 'current password', 'trim|required|callback_current_password_check['.$this->post('pgID').']');
        $this->form_validation->set_rules('new_pwd', 'new password', 'trim|required');
        $this->form_validation->set_rules('new_conf_pwd', 'confirm password', 'trim|required|matches[new_pwd]');

        if ( $this->form_validation->run() == TRUE )
        {
            $postData["password"]   = md5($this->post('new_pwd'));
            $pgID                   = $this->post('pgID');

            if ( $this->pmdl_obj->api_change_photographer_password($postData, $pgID) )
            {
                $success_arr    = array(2, '<li>Password changed successfully.</li>');
                $success_msg    = $this->common_functions->view_message($success_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $success_msg);
                $this->response($data, 200);
            } else {
                $error_arr      = array(1, '<li>Password failed to change.</li>');
                $error_msg      = $this->common_functions->view_message($error_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data, 200);
            }
        }
        else
        {
            $error_arr      = array(1, validation_errors());
            $error_msg      = $this->common_functions->view_message($error_arr);
            $data           = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data, 200);
        }
    }

    public function current_password_check( $old_pwd, $pgID = 0)
    {
        /*$data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $old_pwd.' ** '.$pgID);
        $this->response($data, 200);
        return FALSE;*/
        if ( (int)$pgID > 0 && trim($old_pwd) != '' )
        {
            $is_correct = $this->pmdl_obj->is_photographer_current_password_correct( $old_pwd, $pgID );

            if ( $is_correct ) {
                return TRUE;
            } else {
                $this->form_validation->set_message('current_password_check', 'Please enter correct current password.');
                return FALSE;
            }
        }
        else
        {
            $this->form_validation->set_message('current_password_check', 'Please enter correct current password.');
            return FALSE;
        }
    }

    /*
    *   
    */
    public function ajax_photographer_send_invitation_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $postData               = array();
        $postData['visitor']    = $this->post('visitor');
        $postData['pg_email']   = $this->post('pg_email');

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('visitor', 'email/name/phone no.', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('pg_email', 'photographer email', 'trim|required|valid_email|callback_check_photographer_already_registered['.$postData['pg_email'].']');

        if ( $this->form_validation->run() == FALSE )
        {
            $return['result']   = validation_errors();
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $return);
            $this->response($data,200);
        } 
        else 
        {
            if ( $this->pmdl_obj->api_add_photographer_invitation($postData) )
            {
                $return['result'] = 'Photographer invitation send successfully.';
                $return['status'] = true;
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $return );
                $this->response($data, 200);
            }
            else
            {
                $return['result']   = 'System error.';
                $return['status']   = false;
                $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $return);
                $this->response($data,200);
            }            
        }
    }


    function check_photographer_already_registered( $pg_email )
    {
        
        $is_exists = $this->pmdl_obj->api_get_photographer_by_emailID($pg_email);

        if ( $is_exists ) {
            $this->form_validation->set_message('check_photographer_already_registered', '"'.$pg_email.'" This %s already exists. Please type another name.');
            return false;
        } else {
            return true;
        }
        
    }


    function get_photographer_lists_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $postData       = array();
        $rs = $this->pmdl_obj->api_get_photographer_lists($postData);
        //$this->response($rs, 200);
        $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rs );
        $this->response($data, 200);
    }


    function api_photographer_filter_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $postData                   = array();
        $postData["pg_name"]        = $this->post('pg_name');
        $postData["pg_budget"]      = $this->post('pg_budget');
        $postData["ratings"]        = $this->post('ratings');
        $postData["pg_location"]    = $this->post('pg_location');
        $postData["pg_lat"]         = $this->post('pg_lat');
        $postData["pg_long"]        = $this->post('pg_long');

        $rs = $this->pmdl_obj->api_get_photographer_lists( $postData );

        $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rs );
        $this->response($data, 200);
    }


    function api_ajax_pg_follow_status_changed_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $postData                   = array();
        $postData["pgID"]           = $this->post('pgID');        
        $postData["followed_id"]    = $this->post('followed_id');
        $curr_status                = $this->post('curr_status');

        if( (int)$curr_status == 0 )
        {
            $rs = $this->pmdl_obj->api_pg_add_new_follower( $postData );
            /* $this->response($rs, 200);*/
            if( (int)$rs > 0 ) 
            {
                // Notification data
                $notifyData["entity_type_id"]   = 2;    
                $notifyData["entity_id"]        = $rs;
                $notifyData["actor_id"]         = $postData["pgID"];
                $notifyData["notifier_id"]      = $postData["followed_id"];
                // Create notification
                $this->notify_model->api_do_create_notification($notifyData);

                $return['result'] = 'Now you can followed.';
                $return['status'] = TRUE;
                $return['curr_follow_status'] = ( (int)$curr_status == 0 )? 1 : 0;
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $return );
                $this->response($data, 200);
            } else {
                $return['result'] = 'You already followed. ';
                $return['status'] = FALSE;
                $return['curr_follow_status'] = ( (int)$curr_status == 0 )? 1 : 0;
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'failed', 'rs_row' => $return);
                $this->response($data, 200);
            }
            
        } else {
            $rs = $this->pmdl_obj->api_pg_remove_follower( $postData );

            if( $rs == "Success" ) 
            {
                $return['result'] = 'Unfollow success fully.';
                $return['status'] = TRUE;
                $return['curr_follow_status'] = ( (int)$curr_status == 0 )? 1 : 0;

                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $return );
                $this->response($data, 200);
            } else {
                $return['result'] = 'There are some error to unfollow. ' ;
                $return['status'] = FALSE;
                $return['curr_follow_status'] = (int)$curr_status;

                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'failed', 'rs_row' => $return );
                $this->response($data, 200);
            }
        }

    }


}

?>
