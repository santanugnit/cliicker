<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Album extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('Album_model');
        $this->load->library('common_functions');
        //Assign the model object into a variable for lts re-usability
        $this->albmdl_obj = $this->Album_model;
        $this->ctrlr_name = $this->uri->segment(1);
        $this->api_access_key = get_api_access_key();
    }


    /**
     *
     */
    function ajax_check_unique_subdomain_name_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;

        $preferred_subdomain_name   = $this->post('preferred_subdomain_name');
        $album_id           = $this->post('album_id');
        $operation_mode     = $this->post('operation_mode');

        $this->form_validation->set_error_delimiters('', '');
        if ( $operation_mode == "edit" && (int)$album_id > 0 )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique['.$album_id.']');
            $this->form_validation->set_rules('album_id', 'album id not found', 'trim|required');
        }
        else
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]',
                array('is_unique' => '"' . $preferred_subdomain_name . '" This %s already exists. Please type another name.')
            );
        }

        if ( $this->form_validation->run() == FALSE )
        {
            $return['result']   = validation_errors();
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $return);
            $this->response($data,200);
        } else {
            $return['result'] = '"' . $preferred_subdomain_name . '" this name can be use as album subdomain name.';
            $return['status'] = true;
            $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $return );
            $this->response($data, 200);
        }
    }

    /**
     * @param $subdomainName
     * @param int $album_id
     * @return bool
     */
    function subdomain_name_check_unique( $subdomainName, $album_id = 0)
    {
        if ( (int)$album_id > 0 && trim($subdomainName) != '' )
        {
            $is_exists = $this->common_functions->check_unique_slug_exists($subdomainName, 'preferred_subdomain_name', 'albums', array('id' => $album_id ) );
            if ( $is_exists ) {
                $this->form_validation->set_message('subdomain_name_check_unique', '"'.$subdomainName.'" This %s already exists. Please type another name.');
                return false;
            } else {
                return true;
            }
        }
        else
        {
            return TRUE;
        }
    }

    /**
     *
     */
    function api_album_create_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $data               = array();
        $data['msg_data']   = "";

        //validate form data
        $this->form_validation->set_rules('album_name', 'album name', 'trim|required');
        $this->form_validation->set_rules('photographer_id', 'photographer id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('occasion_id', 'occasion', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]');
        $this->form_validation->set_rules('album_price', 'album price', 'trim|required');

        if ( $this->form_validation->run() == FALSE )
        {
            $error_arr = array(1, validation_errors());
            $error_msg = $this->common_functions->view_message($error_arr);
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data,200);
        }
        else
        {
            $postdata = array(
                'creation_datetime'     => $this->post("creation_datetime"),
                'photographer_id' 		=> $this->post('photographer_id'),
                'occasion_id' 			=> $this->post('occasion_id'),
                'album_name' 			=> $this->post('album_name'),
                'short_description' 	=> nl2br($this->post('short_description')),
                'intro_text' 			=> $this->post('intro_text'),
                'preferred_subdomain_name'  => $this->post('preferred_subdomain_name'),
                'max_storage_allocate'  => 1073741824,
                'album_price'           => $this->post('album_price'),
                'google_drive_link' 	=> $this->post('google_drive_link'),
                'dropbox_link' 	        => $this->post('dropbox_link'),
                'added_date' 		    => date('Y-m-d H:i:s'),
                'updated_date'  	    => date('Y-m-d H:i:s')
            );
            //save data
            $last_id = $this->albmdl_obj->add($postdata);
            if ( $last_id )
            {
                $postArr                = array();
                $postArr["album_code"]  = 'ALB/'. $this->post('photographer_id') .'/'.$last_id;
                $this->albmdl_obj->update_album_data($postArr, $last_id);

                $msg_arr    = array(2, '<li>Record has been saved successfully.</li>');
                $msg_arr    = $this->common_functions->view_message($msg_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => array(
                    "message" => $msg_arr, "album_id" => $last_id, "album_code" => $postArr["album_code"]
                ) );
                $this->response($data, 200);

            } else {

                $error_arr = array(1, '<li>There is some error occured. Please try again.</li>');
                $error_msg = $this->common_functions->view_message($error_arr);
                $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
                $this->response($data,200);
            }
        }

    }

    /**
     * @return mixed
     */
    public function api_album_authenticate_check_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@")) //checking for valid apitoken
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }

        $this->form_validation->set_rules('album_id', 'album id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('photographer_id', 'photographer id', 'trim|required|is_natural_no_zero');
        if ( $this->form_validation->run() == FALSE )
        {
            $error_arr = array(1, validation_errors());
            $error_msg = $this->common_functions->view_message($error_arr);
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data,200);

        }
        else
        {
            $postdata = array( 'id' => $this->post("album_id"), 'photographer_id' => $this->post('photographer_id') );
            $rs = $this->albmdl_obj->get_album_details_by_albumID_pgID($postdata);
            $data = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $rs );
            $this->response($data, 200);
        }
    }

    /**
     * @return mixed
     */
    public function api_get_pg_album_lists_post()
    {
        if ( $this->api_access_key != md5($this->post('apitoken') . "#SHA@"))
        {
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'Invalid access token');
            $this->response($data, 200);
        }
        $this->form_validation->set_rules('pgID', 'photographer id', 'trim|required|is_natural_no_zero');
        if ( $this->form_validation->run() == FALSE )
        {
            $error_arr = array(1, validation_errors());
            $error_msg = $this->common_functions->view_message($error_arr);
            $data = array('api_syntax_success' => 1, 'api_action_success' => 0, 'api_action_message' => 'failed', 'rs_row' => $error_msg);
            $this->response($data,200);
        }
        else
        {
            $pgID       = $this->post('pgID');
            $rs         = $this->albmdl_obj->get_pg_album_lists_by_pgID($pgID);
            $result = array();
            if( !empty($rs) )
            {
                foreach ($rs as $key => $val )
                {
                    $result[$val["id"]]["id"]   = $val["id"];
                    $result[$val["id"]]["album_code"]   = $val["album_code"];
                    $result[$val["id"]]["creation_datetime"]   = $val["creation_datetime"];
                    $result[$val["id"]]["photographer_id"]   = $val["photographer_id"];
                    $result[$val["id"]]["occasion_id"]   = $val["occasion_id"];
                    $result[$val["id"]]["album_name"]   = $val["album_name"];
                    $result[$val["id"]]["short_description"]   = $val["short_description"];
                    $result[$val["id"]]["intro_text"]   = $val["intro_text"];
                    $result[$val["id"]]["preferred_subdomain_name"]   = $val["preferred_subdomain_name"];
                    $result[$val["id"]]["max_storage_allocate"]   = $val["max_storage_allocate"];
                    $result[$val["id"]]["space_usage"]   = $val["space_usage"];
                    $result[$val["id"]]["google_drive_link"]   = $val["google_drive_link"];
                    $result[$val["id"]]["dropbox_link"]   = $val["dropbox_link"];
                    $result[$val["id"]]["album_price"]   = $val["album_price"];
                    $result[$val["id"]]["total_pay_amount"]   = $val["total_pay_amount"];
                    $result[$val["id"]]["last_payment_date"]   = $val["last_payment_date"];
                    $result[$val["id"]]["last_payment_txn_id"]   = $val["last_payment_txn_id"];
                    $result[$val["id"]]["is_last_payment_completed"]   = $val["is_last_payment_completed"];
                    $result[$val["id"]]["is_project_completed"]   = $val["is_project_completed"];
                    $result[$val["id"]]["is_deleted"]   = $val["is_deleted"];
                    $result[$val["id"]]["added_date"]   = $val["added_date"];
                    $result[$val["id"]]["updated_date"]   = $val["updated_date"];
                    $result[$val["id"]]["album_file_detls"][$val["upload_file_id"]]["upload_file_id"]   = $val["upload_file_id"];
                    $result[$val["id"]]["album_file_detls"][$val["upload_file_id"]]["album_id"]   = $val["album_id"];
                    $result[$val["id"]]["album_file_detls"][$val["upload_file_id"]]["upload_file_name"]   = $val["upload_file_name"];
                    $result[$val["id"]]["album_file_detls"][$val["upload_file_id"]]["upload_file_size"]   = $val["upload_file_size"];
                    $result[$val["id"]]["album_file_detls"][$val["upload_file_id"]]["uploaded_datetime"]   = $val["uploaded_datetime"];
                }

            }

            $data   = array( 'api_syntax_success' => 1, 'api_action_success' => 1, 'api_action_message' => 'success', 'rs_row' => $result );
            $this->response($data, 200);
        }
    }


}

?>
