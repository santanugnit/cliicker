<?php
class Home extends CI_Controller
{
    var $data;
    
    function  __construct()
    {
        parent::__construct();

        $this->load->library('admin_init_elements');
        //is Admin logged in?
        $this->admin_init_elements->is_admin_logged_in();
        $this->admin_init_elements->init_elements();
        $this->admin_folder_name = $this->config->item('admin_folder_name'); //Define admin folder name
    }

    function index()
    {
        redirect($this->admin_folder_name.'/home/dashboard');
    }

    
    function dashboard()
    {
		
        $data = array();
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/home_view',$data,TRUE);
        //render full layout, specific to this function
        
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    }
    
    
    
   

}
?>
