<?php
/**
 * Class Photographer
 */

class Photographer extends CI_Controller
{  
    
    function __construct()
    {  
        parent::__construct();
        
        $this->load->library('common_functions');
		$this->load->library('admin_init_elements');
        //is Admin logged in?
        $this->admin_init_elements->is_admin_logged_in();
		$this->admin_init_elements->check_user_access_right(); //Check user type
        $this->admin_init_elements->init_elements();
		$this->search_params = $this->admin_init_elements->get_url_params($_GET);
		$this->load->model('photographer_model');
        $this->mdl_obj = $this->photographer_model;
        $this->admin_folder_name = $this->config->item('admin_folder_name'); //get admin folder name
        $this->ctrlr_name = $this->uri->segment(2);
    }
    
    /**
     * index function of controller
     */
    public function index()
    {
        redirect($this->admin_folder_name.'/photographer/photographer_list');
    }

    /**
     *
     */
    function photographer_list()
    {
        $data = array();
        //################################################################//
        //###################### PAGINATION CODE #########################//
        $cnt = $this->photographer_model->get_row_count();
        $count = $cnt;
        $this->load->library('pagination');

        $config['base_url'] = base_url().$this->admin_folder_name.'/'.$this->ctrlr_name.'/photographer_list/';
        $config['total_rows'] = $count;
        $config['per_page'] = '15';
        $config['uri_segment'] = 4;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<ul class="dataTables_paginate">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '<li>Previous</li>';
        $config['next_link'] = '<li>Next</li>';
        $config['first_link'] = '<li>First</li>';
        $config['last_link'] = '<li>Last</li>';
        $config['cur_tag_open'] = '<li class="curr_active_class">';
        $config['cur_tag_close'] = '</li>';
        $config['anchor_class'] = 'class=""';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $offset = $this->uri->segment(4);
        $offset=empty($offset)?0:($offset-1)*$config['per_page']; // if $config['use_page_numbers'] = FALSE;

        $data['pagination_link'] = $this->pagination->create_links();
        $data['records_list'] = $this->photographer_model->get_photographer_rows_pagination($config['per_page'], $offset);

        ##--------- Total entry showing per page msg ------------##
        $counter_start = ($offset==0) ? 1 : $offset+1;
        $showing_end_counter = ($config['per_page'] > $count) ?  $count : $offset + $config['per_page'];
        $showing_end_counter = ($showing_end_counter > $count) ? $counter_start + ($count - $counter_start) : $showing_end_counter;

        if($counter_start == $showing_end_counter)
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$showing_end_counter.' of '.$count.' entries</div>';
        else
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$counter_start.' to '.$showing_end_counter.' of '.$count.' entries</div>';

        $data['pagination_count_msg'] = $pagination_msg_str;
        ##--------- Total entry showing per page msg ------------##

        //###################### PAGINATION CODE #########################//
        //################################################################//
        //echo "<pre>"; print_r($data['record_list']); die();
        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/photographer_list_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);

    }

    /**
     *   photographer >> add
     */
    function add()
    { 
        $data               = array();
		$data['msg_data']   = "";
		//submitted form data	
		if ( $this->input->post('SaveExit') || $this->input->post('SaveReview') )
		{	
			//validate form data
			if ( $this->photographer_model->validate_form_data() == TRUE )
			{
				//echo "<pre>"; print_r($_POST); die();		
				//save data
				$this->photographer_model->add();
				$msg_arr = array(2, '<li>Record has been added successfully.</li>');
				$msg_arr = $this->common_functions->view_message($msg_arr);
				$this->session->set_flashdata('msg_data', $msg_arr);
				redirect($this->admin_folder_name.'/photographer/add');
            }
			else
			{	
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['msg_data'] = $error_msg;
			}
		}
        
		//populate view, specific to this function
		$this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/photographer_add_view', $data, true);
		
		//render full layout, specific to this function
		$this->load->view($this->admin_folder_name.'/layout_home', $this->data);
        
    }

	#------------------------------------------##
    #---------------- photographer >> edit ------------##
    ##-----------------------------------------##
    function edit()
    {
		$data               = array();
		$data['msg_data']   = "";
		$pval_id            = $this->uri->segment(4);
		
		//submitted form data	
		if ( $this->input->post('SaveExit') || $this->input->post('SaveReview') )
		{	
			//validate form data
			if ( $this->photographer_model->validate_form_data() == TRUE )
			{
				//save data
				$return = $this->photographer_model->edit($pval_id);
				if ( $return == "edit_success" )
				{
					$msg_arr = array(2, '<li>Record has been updated successfully.</li>');
					$msg_arr = $this->common_functions->view_message($msg_arr);
					$this->session->set_flashdata('msg_data', $msg_arr);
					
					redirect($this->admin_folder_name.'/photographer/edit/'.$pval_id);
				}
            }
			else
			{	
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['msg_data'] = $error_msg;
			}
		}
		
		$data['photographer_data'] = $this->photographer_model->get_photographer_details($pval_id);
        //echo "<pre>"; print_r($data['photographer_data']); echo "</pre>"; exit;
		//populate view, specific to this function
		$this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/photographer_edit_view', $data, true);
		
		//render full layout, specific to this function
		$this->load->view($this->admin_folder_name.'/layout_home', $this->data);
		
	}

    ##----------------------------------------------------------------##
    ##------------- Portfolio : Delete image through ajax ------------##
    ##----------------------------------------------------------------##
    function ajax_delete_featured_image()
    {
        $rowId = $this->input->post('rowId');
        $fname =  $this->input->post('fileName');
        $this->photographer_model->delete_featured_image($rowId,$fname);

        $return['result'] = "success";
        echo json_encode($return);
    }

    ##-------------------------------------------------##
    ##--------------- Services >> Delete --------------##
    ##-------------------------------------------------##
    function delete()
    {
        $arr_val        = array();
        $action_mode    = $this->uri->segment(4);

        if ( $action_mode == "multiple" ) {
            $arr_val    = $this->input->get('chk');
        } else {
            array_push($arr_val, $this->uri->segment(5));
        }

        $return = $this->mdl_obj->delete($arr_val);

        if( $return == "success" ) //Some data are not deleted
        {
            $return_msg = "The photographer(s) are not deleted. This photographer(s) are assigned image also deleted";
            $msg_arr = array(1, '<li>'.$return_msg.'</li>');
        }
        else
        {
            $return_msg = "Data has been deleted successfully";
            $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        }

        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/photographer_list');
    }

    ##--------------------------------------------------------------##
    ##------------------ Services >> active/inactive  --------------##
    ##--------------------------------------------------------------##
    function change_status()
    {
        $arr_val        = array();
        $action_mode    = $this->uri->segment(4);
        $status_val     = $this->uri->segment(5);

        if ( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(6));
        }

        $return_val = $this->mdl_obj->change_status_value($arr_val, $status_val);

        $return_msg = "Status has been changed successfully.";
        $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/photographer_list');
    }

} //end class
