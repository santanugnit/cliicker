<?php
class Login extends CI_Controller
{
    var $data;

    function  __construct() 
	{
        parent::__construct();

        $this->load->library('common_functions');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
		$this->load->model('Login_model');
		
		$this->admin_folder_name = $this->config->item('admin_folder_name');
    }

    function index()
	{
        $this->load->helper('cookie');
        $data['error_message'] = '';
        
		if ( $this->input->post('loginSubmit') )
		{
			if ( $this->Login_model->validate_form_data() == TRUE )
			{
				$result 	= $this->Login_model->check_login();
				if ( $result == "login_success" )
				{
					##------------------- cookie create/ delete --------------------##
					$cookie_prefix = $this->config->item('cookie_prefix');
					$cookie_domain = $this->config->item('cookie_domain');
					$cookie_path = $this->config->item('cookie_path');

					if ( isset($_POST['remember']) ) //create cookie
					{
						$cookie = array (
									'name'   => 'adminAuth',
									'value'  => $this->input->post('admin_uname'),
									'expire' => time() + 86500,
									'domain' => $cookie_domain,
									'path'   => $cookie_path,
									'prefix' => $cookie_prefix,
								);
						set_cookie($cookie);
					}
					else
					{
                        // delete cookie
						$cookie = array(
										'name'   => 'adminAuth',
										'value'  => "",
										'expire' => 0,
										'domain' => $cookie_domain,
										'path'   => $cookie_path,
										'prefix' => $cookie_prefix,
									);

						delete_cookie($cookie);
					}
					##-------------------------------------------------------------##
					redirect($this->admin_folder_name.'/home');
				}
				else
				{ 
					if ( $result=="inactive" )
						$error_msg_txt 	= "The account has been inactive.";
					else if ( $result == "blocked" )
						$error_msg_txt = "The account has been blocked.";
					else if ( $result == "deleted" )
						$error_msg_txt = "The account has been deleted.";
					else
						$error_msg_txt = "Please enter valid username or password.";
					
					$error_arr 	= array(1, '<li>'.$error_msg_txt.'</li>');
					$error_msg 	= $this->common_functions->view_message($error_arr);
					$data['error_message']	= $error_msg;
				}
			}
			else
			{
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['error_message'] = $error_msg;
			}
		}
		
        // GET COOKIE
        $cookie_prefix = $this->config->item('cookie_prefix');
        $cookie_name = $cookie_prefix . "adminAuth";
        $adminAuth = get_cookie($cookie_name);
        $data["admin_username"] = $adminAuth;
		
        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/login_view', $data, true);
        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_login', $this->data);
    }

    /**
     *
     */
    function logout()
    {
        $is_admin_logged_in = $this->session->userdata('is_admin_logged_in');
		
        if(isset($is_admin_logged_in) || $is_admin_logged_in == TRUE)
        {
                
				//########################################################//
				//######## UPDATE LOGIN STATUS AND LOGOUT TIMA ###########//
				
				$this->Login_model->set_logout_fields();
				
				//######## UPDATE LOGIN STATUS AND LOGOUT TIMA ###########//
				//########################################################//
				
				
				//$this->session->sess_destroy();
				$this->session->unset_userdata('userid');
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('name');
				$this->session->unset_userdata('is_admin_logged_in');
				
				
                redirect($this->admin_folder_name.'/login');
        }
    }

    /**
     *
     */
    function forgot_password()
    {
        $data['error_message'] 	= '';
        if ( $this->input->post('loginSubmit') )
        {
            if ( $this->Login_model->validate_form_forgot_password_data() == TRUE )
            {
                $user_id 	= 1;
                $result		= $this->Login_model->regenerate_password($user_id);
                if ( $result == "success" )
                {
                    $this->session->set_flashdata('msg_data', 'We sent a auto generated password to your given email address. Please check your mail.');
                    redirect($this->admin_folder_name.'/login/forgot_password');
                }
                else
                {
                    $error_arr 	= array(1, '<li>Please enter valid email.</li>');
                    $error_msg 	= $this->common_functions->view_message($error_arr);
                    $data['error_message']	= $error_msg;
                }
            }
            else
            {
                $error_arr 	= array(1, validation_errors());
                $error_msg 	= $this->common_functions->view_message($error_arr);
                $data['error_message'] 	= $error_msg;
            }

        }

        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/forgot_password_view', $data, true);
        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_login', $this->data);
    }
    
    
}
?>
