<?php
class Seo_content extends CI_Controller
{
    var $data;
		
    //################################################################//
    //###################### LOAD CONSTRUCTOR ########################//
    //################################################################//
	
	function  __construct() 
	{   
		parent::__construct();
		$this->load->library('common_functions');
		$this->load->library('admin_init_elements');
		//is Admin logged in?
		$this->admin_init_elements->is_admin_logged_in();
		$this->admin_init_elements->init_elements();
		$this->admin_folder_name = $this->config->item('admin_folder_name'); //get admin folder name
		
		$this->load->model('Seo_model'); //Load Item master model
        $this->mdl_obj = $this->Seo_model; //Assign the model object into a variable for lts re-usability
		$this->ctrlr_name = $this->uri->segment(2); //Assign the controller name into a variable for lts
				
    }
	
    //################################################################//
    //###################### LOAD CONSTRUCTOR ########################//
    //################################################################//
	
	function index()
	{
		die('No function defined');
		//redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/display');	
	}
   
	
	##----------------------------------------------------##
    ##---------------- Change the slug value -------------##
    ##----------------------------------------------------##
	function change_slug_value()
	{
		$title = $this->input->post('titleVal');
		$itmId = $this->input->post('c_id');
		$chk_identifier = $this->input->post('chkIdentifier');
		$ignore_fields = array();
		if($itmId!="")
			$ignore_fields = array('item_id'=>$itmId);	
		
		if($chk_identifier!="") //Concat identifier for specific the row along with item_id
			$where='page_identifier="'.$chk_identifier.'"';
	
		$slug_val = $this->common_functions->get_unique_slug($title,'item_title','seo_contents',$ignore_fields, $where=NULL);
		
		$return['result'] = $slug_val;
		echo json_encode($return);
	}
	
	

}
?>
