<?php
/**
 * Class Album
 */

class Testimonial extends CI_Controller
{
    /**
     * Define constructor of the class
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('common_functions');
        $this->load->library('admin_init_elements');
        //is Admin logged in?
        $this->admin_init_elements->is_admin_logged_in();
        $this->admin_init_elements->init_elements();
        $this->admin_folder_name = $this->config->item('admin_folder_name'); //get admin folder name

        $this->search_params = $this->admin_init_elements->get_url_params($_GET);
        $this->load->model('Testimonial_model'); //Load Item master model
        $this->mdl_obj = $this->Testimonial_model; //Assign the model object into a variable for lts re-usability
        $this->ctrlr_name = $this->uri->segment(2); //Assign the controller name into a variable for lts re-usability
    }

    /**
     * index function of this class
     */
    public function index()
    {
        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list');
    }

    ##-----------------------------------------------##
    ##--------------- Portfolio >> list -------------##
    ##-----------------------------------------------##
    function testimonial_list()
    {
        $data = array();
        //################################################################//
        //###################### PAGINATION CODE #########################//
        $cnt = $this->mdl_obj->get_row_count();
        $count = $cnt;
        $this->load->library('pagination');

        $config['base_url']     = base_url().$this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list/';
        $config['total_rows']   = $count;
        $config['per_page']     = '15';
        $config['uri_segment']  = 4;
        $config['num_links']    = 5;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="dataTables_paginate">';
        $config['full_tag_close']   = '</ul>';
        $config['prev_link']        = '<li>Previous</li>';
        $config['next_link']        = '<li>Next</li>';
        $config['first_link']       = '<li>First</li>';
        $config['last_link']        = '<li>Last</li>';
        $config['cur_tag_open']     = '<li class="curr_active_class">';
        $config['cur_tag_close']    = '</li>';
        $config['anchor_class']     = 'class=""';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';

        $this->pagination->initialize($config);

        $offset     = $this->uri->segment(4);
        $offset     = empty($offset)? 0 : ($offset-1)*$config['per_page']; // if $config['use_page_numbers'] = FALSE;

        $data['pagination_link']    = $this->pagination->create_links();
        $data['records_list']       = $this->mdl_obj->get_rows_pagination($config['per_page'],$offset);
        //echo "<pre>"; print_r($data['records_list']); echo "</pre>"; exit;

        ##--------- Total entry showing per page msg ------------##
        $counter_start          = ($offset == 0) ? 1 : $offset+1;
        $showing_end_counter    = ($config['per_page'] > $count) ?  $count : $offset + $config['per_page'];
        $showing_end_counter    = ($showing_end_counter > $count) ? $counter_start + ($count - $counter_start) : $showing_end_counter;

        if ( $counter_start == $showing_end_counter )
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$showing_end_counter.' of '.$count.' entries</div>';
        else
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$counter_start.' to '.$showing_end_counter.' of '.$count.' entries</div>';

        $data['pagination_count_msg'] = $pagination_msg_str;
        ##--------- Total entry showing per page msg ------------##
        //###################### PAGINATION CODE #########################//
        //################################################################//
        /*$this->load->model('Portfolio_categories_model');
        $xx = $this->Portfolio_categories_model->get_record_list();
        echo "<pre>"; print_r($xx); die('123');*/

        $this->load->model('photographer_model');
        $data['photographers_list'] = $this->photographer_model->get_all_photographers();

        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/testimonial_list_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    }

    ##----------------------------------------------------##
    ##------------------ Portfolio >> Add ----------------##
    ##----------------------------------------------------##
    function add()
    {
        $data               = array();
        $data['msg_data']   = "";

        //submitted form data
        if ( $this->input->post('SaveExit') || $this->input->post('SaveReview') )
        {
            $this->form_validation->set_rules('client_name', 'client name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('client_email', 'client email', 'required|trim|xss_clean|valid_email');
            $this->form_validation->set_rules('contact_number', 'client contact number', 'required|trim|xss_clean|is_natural|max_length[12]|min_length[10]');
            $this->form_validation->set_rules('website_url', 'Web-site URL', 'required|trim|xss_clean|valid_url');

            if ( $this->form_validation->run() == TRUE )
            {
                $save_mode = 1;
                ##-----------------------------------------------------------------------------------------##
                ##----------------- This section is for managing Featured Image :: Start ------------------##

                $featured_image = ""; //Initialize variable
                if($_FILES['featured_image']['name']!="") //checking for upload feature image
                {
                    $image_data_config = array(
                        'field_name'        =>'featured_image', // this is the field name
                        'upload_path'       =>'assets/uploaded_files/testimonial/original/', // this is the original image path
                        'image_resize'      => true, //To resize image with medium, set it true
                        'upload_medium_path'        =>'assets/uploaded_files/testimonial/medium/', //if image_resize=true, set the path of the medium image.
                        'image_valid_dimensions'    =>  'photographer_image_medium', // this data has been set in config file
                        'image_types'       => "jpg|jpeg|png", // this defined variable is defined in constance file

                    );
                    $profile_image_arr = $this->common_functions->image_upload($image_data_config);
                    if ( $profile_image_arr['message'] == "success" ) //Image upload successfully
                    {
                        $featured_image = $profile_image_arr['image_info'];
                    }
                    else
                    {
                        $error_arr  = array(1, $profile_image_arr['image_info']);
                        $error_msg  = $this->common_functions->view_message($error_arr);
                        $data['msg_data']   = $error_msg;
                        $save_mode  = 0;
                    }
                }
                ##------------------- This section is for managing Featured Image :: End ------------------##
                ##-----------------------------------------------------------------------------------------##
                if ( $save_mode == 1 ) //All fields validations are OK. ready to save data
                {
                    $postdata = array(
                        'created_datetime' => date('Y-m-d H:i:s'),
                        'client_name' => $this->input->post('client_name'),
                        'client_email' => $this->input->post('client_email'),
                        'show_email' => $this->input->post('show_email'),
                        'contact_number' => $this->input->post('contact_number'),
                        'show_contact' => $this->input->post('show_contact'),
                        'website_url' => $this->input->post('website_url'),
                        'featured_image' => $featured_image,
                        'testimonial_text' => nl2br($this->input->post('testimonial_text')),
                        'status' => "N"
                    );
                    //save data
                    $last_id = $this->mdl_obj->add($postdata);
                    if ($last_id != "")
                    {
                        $msg_arr = array(2, '<li>Record has been saved successfully.</li>');
                        $msg_arr = $this->common_functions->view_message($msg_arr);
                        $this->session->set_flashdata('msg_data', $msg_arr);

                        if ($this->input->post('SaveExit'))
                            redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/add');
                        else
                            redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/edit/' . $last_id);
                    }
                }
            }
            else
            {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['msg_data'] = $error_msg;
            }
        }

        $recommended_image_siz_arr = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text"] = "System Recommended Width & Height ". $recommended_image_siz_arr['photographer_image_medium'] . " (In pixcel)";

        /* echo "<pre>". print_r($data['photographers_list'], true)."</pre>"; exit(0); */

        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/testimonial_add_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    }

    ##---------------------------------------------------##
    ##------------------ Portfolio >> Edit --------------##
    ##---------------------------------------------------##
    function edit()
    {
        $data               = array();
        $data['msg_data']   = "";
        $row_id             = $this->uri->segment(4);

        //submitted form data
        if ( $this->input->post('SaveExit') || $this->input->post('SaveStay') )
        {
            //validate form data
            $this->form_validation->set_rules('client_name', 'client name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('client_email', 'client email', 'required|trim|xss_clean|valid_email');
            $this->form_validation->set_rules('contact_number', 'client contact number', 'required|trim|xss_clean|is_natural|max_length[12]|min_length[10]');
            $this->form_validation->set_rules('website_url', 'Web-site URL', 'required|trim|xss_clean|valid_url');

            if ( $this->form_validation->run() == TRUE )
            {
                $save_mode = 1;
                ##-----------------------------------------------------------------------------------------##
                ##----------------- This section is for managing Featured Image :: Start ------------------##

                $featured_image = ""; //Initialize variable
                if($_FILES['featured_image']['name']!="") //checking for upload feature image
                {
                    $image_data_config = array(
                        'field_name'        =>'featured_image', // this is the field name
                        'upload_path'       =>'assets/uploaded_files/other_image/original/', // this is the original image path
                        'image_resize'      => true, //To resize image with medium, set it true
                        'upload_medium_path'    =>'assets/uploaded_files/testimonial/medium/', //if image_resize=true, set the path of the medium image.
                        'upload_medium_path'    =>'assets/uploaded_files/testimonial/medium/', // this is the medium image path
                        'image_valid_dimensions'    =>'photographer_image_medium', // this data has been set in config file
                        'image_types'       => "jpg|jpeg|png", // this defined variable is defined in constance file
                    );
                    $profile_image_arr = $this->common_functions->image_upload($image_data_config);
                    if ( $profile_image_arr['message'] == "success" ) //Image upload successfully
                    {
                        $featured_image = $profile_image_arr['image_info'];
                        ##--------- Delete existing image file from its location if any ----------##
                        if($this->input->post('existing_featured_image')!="")
                        {
                            unlink('assets/uploaded_files/testimonial/medium/'.$this->input->post('existing_featured_image'));
                            unlink('assets/uploaded_files/testimonial/original/'.$this->input->post('existing_featured_image'));
                        }
                        ##------------------------------------------------------------------------##
                    }
                    else
                    {
                        $error_arr = array(1, $profile_image_arr['image_info']);
                        $error_msg = $this->common_functions->view_message($error_arr);
                        $data['msg_data'] = $error_msg;
                        $save_mode = 0;
                    }
                }
                else
                {
                    $featured_image = $this->input->post('existing_featured_image');
                }

                ##------------------- This section is for managing Featured Image :: End ------------------##
                ##-----------------------------------------------------------------------------------------##
                $postdata = array(
                    'client_name' => $this->input->post('client_name'),
                    'client_email' => $this->input->post('client_email'),
                    'show_email' => $this->input->post('show_email'),
                    'contact_number' => $this->input->post('contact_number'),
                    'show_contact' => $this->input->post('show_contact'),
                    'website_url' => $this->input->post('website_url'),
                    'featured_image' => $featured_image,
                    'testimonial_text' => nl2br($this->input->post('testimonial_text'))
                );

                //save data
                $return = $this->mdl_obj->edit($row_id, $postdata);
                if ( $return );
                {
                    $msg_arr = array(2, '<li>Record has been updated successfully.</li>');
                    $msg_arr = $this->common_functions->view_message($msg_arr);
                    $this->session->set_flashdata('msg_data', $msg_arr);

                    if ($this->input->post('SaveStay'))
                        redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/edit/' . $row_id);
                    else
                        redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/testimonial_list/');
                }
            }
            else
            {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['msg_data'] = $error_msg;
            }

        }

        $data['record_dtls'] = $this->mdl_obj->get_details($row_id);

        $recommended_image_siz_arr = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text"] = "System Recommended Width & Height ".$recommended_image_siz_arr['photographer_image_medium']." (In pixcel)";

        //echo "<pre>"; print_r($data['record_dtls']); print_r($data['photographers_list']); echo "</pre>"; exit;
        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/testimonial_edit_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);

    }

    ##--------------------------------------------------------------##
    ##----------------- Portfolio >> active/inactive  --------------##
    ##--------------------------------------------------------------##
    function change_status()
    {
        $arr_val    = array();
        $action_mode    = $this->uri->segment(4);
        $status_val     = $this->uri->segment(5);

        if ( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(6));
        }

        $return_val = $this->mdl_obj->change_status_value($arr_val, $status_val);

        $return_msg = "Status has been changed successfully.";
        $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list');
    }

    ##--------------------------------------------------------------##
    ##----------------- Portfolio >> active/inactive  --------------##
    ##--------------------------------------------------------------##
    function change_field_dispaly_status_value()
    {
        $arr_val    = array();
        $action_mode    = $this->uri->segment(4);
        $status_val     = $this->uri->segment(5);
        echo $field_name     = $this->uri->segment(7);

        if ( trim($field_name) == '' ) {
            $return_msg = "There is some error occure.";
            $msg_arr = array(1, '<li>'.$return_msg.'</li>');
            $msg_arr = $this->common_functions->view_message($msg_arr);
            $this->session->set_flashdata('msg_data', $msg_arr);
            redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list');
        }


        if ( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(6));
        }

        $return_val = $this->mdl_obj->change_fields_display_status($arr_val, $status_val, $field_name);

        if( $return_val )
        {
            $return_msg = "Status has been changed successfully.";
            $msg_arr = array(2, '<li>' . $return_msg . '</li>');
            $msg_arr = $this->common_functions->view_message($msg_arr);
            $this->session->set_flashdata('msg_data', $msg_arr);
        } else {
            $return_msg = "Status change failed to update. Please try again.";
            $msg_arr = array(1, '<li>'.$return_msg.'</li>');
            $msg_arr = $this->common_functions->view_message($msg_arr);
            $this->session->set_flashdata('msg_data', $msg_arr);
        }

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list');
    }

    ##-------------------------------------------------##
    ##--------------- Portfolio >> Delete -------------##
    ##-------------------------------------------------##
    function delete()
    {
        $arr_val    = array();
        $action_mode    = $this->uri->segment(4);

        if( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(5));
        }

        $return = $this->mdl_obj->delete($arr_val);
        var_dump($return); exit;
        if( $return['errorSt'] == 1 ) //Some data are not deleted
        {
            $return_msg = " The service(s) named ".$return['msg_str']." are not deleted. This service(s) are assigned with a category.";
            $msg_arr = array(1, '<li>'.$return_msg.'</li>');
        }
        else
        {
            $return_msg = "Data has been deleted successfully";
            $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        }

        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/testimonial_list');
    }

    /**
     *
     */
    function ajax_check_unique_subdomain_name()
    {
        $return = array();
        $return['result'] = "failed";
        $return['status'] = false;
        $preferred_subdomain_name   = $this->input->post('preferred_subdomain_name');
        $album_id            = $this->input->post('alb_id');
        $operation_mode             = $this->input->post('operation_mode');
        if ( trim($preferred_subdomain_name) == '' )
            return false;

        $this->form_validation->set_error_delimiters('', '');

        if ( $operation_mode == "edit" && (int)$album_id > 0 )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique['.$album_id.']');
        } else {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]',
                array('is_unique' => '"' . $preferred_subdomain_name . '" This %s already exists. Please type another name.')
            );
        }

        if ($this->form_validation->run() == FALSE)
        {
            $return['result']   = validation_errors();
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $return['result'] = '"'.$preferred_subdomain_name.'" this name cab be use as album subdomain name.';
            $return['status'] = true;
            echo json_encode($return);
        }

    }



    /**
     *
     */
    function ajax_delete_featured_image()
    {
        $rowId = $this->input->post('rowId');
        $fname =  $this->input->post('fileName');
        $this->mdl_obj->delete_featured_image($rowId,$fname);

        $return['result'] = "success";
        echo json_encode($return);
    }


} //end class
