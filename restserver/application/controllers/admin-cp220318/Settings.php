<?php
class Settings extends CI_Controller
{
    var $data;

    function  __construct()
    {
        parent::__construct();

        $this->load->library('admin_init_elements');
        //is Admin logged in?
        $this->admin_init_elements->is_admin_logged_in();
		$this->admin_init_elements->check_user_access_right(); //Check user type
        $this->admin_init_elements->init_elements();
        $this->load->library('common_functions');
        $this->load->model('Settings_model');
        
    }
    
    
    #--------------------------------------------------------##
    #------------ Settings >> Default function --------------##
    ##-------------------------------------------------------##
    
    function index()
    {
        redirect($this->config->item('admin_folder_name').'/settings/change_password');
    }
    
    
    #--------------------------------------------------------##
    #------------ Settings >> Change password ---------------##
    ##-------------------------------------------------------##
    
    function change_password()
    {
        $data = array();
        $data['msg_data']="";
        
        //submitted form data	
		if($this->input->post('Submit'))
		{	
			//validate form data
			if($this->Settings_model->validate_form_data() == TRUE)
			{
				//reset password
				$this->Settings_model->reset_password();
                $msg_arr = array(2, '<li>Password has been reset successfully.</li>');
                $msg_arr = $this->common_functions->view_message($msg_arr);
                $this->session->set_flashdata('msg_data', $msg_arr);

                redirect($this->config->item('admin_folder_name').'/settings/change_password');
				
			 }
			else
			{	
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['msg_data'] = $error_msg;
			}
			
		}
                
        $this->data['maincontent'] = $this->load->view($this->config->item('admin_folder_name').'/maincontents/change_password_view',$data,TRUE);
        //render full layout, specific to this function
        $this->load->view($this->config->item('admin_folder_name').'/layout_home', $this->data);
    }
    
    #-------------------------------------------------------------------##
    #------- Settings >> Generate a random password through ajax -------##
    ##------------------------------------------------------------------##
    
    function ajax_generate_random_password()
    {    
        
        $arr_vals = (isset($_POST['arrOpt'])) ? $_POST['arrOpt'] : array();
       
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = "";
        $length =9;
        
        if(count($arr_vals)>0)
        {    
            foreach($arr_vals as $val)
            {    
                if($val=="alpha")
                    $chars .=$alpha;
                elseif($val=="alpha_upper")
                    $chars .=$alpha_upper;
                elseif($val=="numeric")
                    $chars .=$numeric;
                elseif($val=="special")
                    $chars .=$special;
                else    
                    $chars .=$alpha.$alpha_upper.$numeric.$special;
            }
        }
        else
        {    
            $chars .=$alpha.$alpha_upper.$numeric.$special;
        }
        
        $len = strlen($chars);
        $pw = '';

        for ($i=0;$i<$length;$i++)
                $pw .= substr($chars, rand(0, $len-1), 1);

        
        $pw = str_shuffle($pw);
        
        echo $pw;
        
    }
    
    
    #-------------------------------------------------------------------##
    #----------------- Settings >> Manage User Profile -----------------##
    ##------------------------------------------------------------------##
    
    function user_profile()
    {
        $data = array();
        $data['msg_data']="";
        $userid=1;
            
        //submitted form data	
		if($this->input->post('Submit'))
		{	
			//validate form data
			if($this->Settings_model->validate_user_form_data() == TRUE)
			{
				//reset password
				$this->Settings_model->update_user_profile($userid);
                $msg_arr = array(2, '<li>Profile has been updated successfully.</li>');
                $msg_arr = $this->common_functions->view_message($msg_arr);
                $this->session->set_flashdata('msg_data', $msg_arr);

                redirect($this->config->item('admin_folder_name').'/settings/user_profile');
            }
			else
			{	
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['msg_data'] = $error_msg;
			}
		}
        
        $data['user_profile_data'] = $this->Settings_model->get_user_details($userid);
                
        $this->data['maincontent'] = $this->load->view($this->config->item('admin_folder_name').'/maincontents/user_profile_view',$data,TRUE);
        //render full layout, specific to this function
        $this->load->view($this->config->item('admin_folder_name').'/layout_home', $this->data);
    }
    
    
    #--------------------------------------------------##
    #------- Delete profile image through ajax --------##
    ##-------------------------------------------------##
    
    function ajax_delete_profile_image()
    {    
         $uId = $this->input->post('uId');
         $fname =  $this->input->post('fileName');
         $this->Settings_model->delete_profile_image($uId,$fname);
        
         $return['result'] = "success";
		 echo json_encode($return);
    }
	
	
	#--------------------------------------------------##
    #----------------- Website Settings ---------------##
    ##-------------------------------------------------##
	function website_settings()
	{
		$data = array();
        $data['msg_data']="";
        $userid=1;
            
        //submitted form data	
		if($this->input->post('Submit'))
		{	
			//validate form data
			if($this->Settings_model->validate_site_settings_form_data() == TRUE)
			{
				//reset password
				$this->Settings_model->update_website_settings();
                $msg_arr = array(2, '<li>Site settings has been updated successfully.</li>');
                $msg_arr = $this->common_functions->view_message($msg_arr);
                $this->session->set_flashdata('msg_data', $msg_arr);

                redirect($this->config->item('admin_folder_name').'/settings/website_settings');
				
			 }
			else
			{	
				$error_arr = array(1, validation_errors());
				$error_msg = $this->common_functions->view_message($error_arr);
				$data['msg_data'] = $error_msg;
			}
			
		}
        
        $data['web_settings_data'] = $this->Settings_model->get_website_settings();
                
        $this->data['maincontent'] = $this->load->view($this->config->item('admin_folder_name').'/maincontents/website_settings_view',$data,TRUE);
        //render full layout, specific to this function
        $this->load->view($this->config->item('admin_folder_name').'/layout_home', $this->data);
	}
   

}
?>
