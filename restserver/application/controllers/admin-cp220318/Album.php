<?php
/**
 * Class Album
 */

class Album extends CI_Controller
{
    /**
     * Define constructor of the class
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('common_functions');
        $this->load->library('admin_init_elements');
        //is Admin logged in?
        $this->admin_init_elements->is_admin_logged_in();
        $this->admin_init_elements->init_elements();
        $this->admin_folder_name = $this->config->item('admin_folder_name'); //get admin folder name

        $this->search_params = $this->admin_init_elements->get_url_params($_GET);
        $this->load->model('Album_model'); //Load Item master model
        $this->mdl_obj = $this->Album_model; //Assign the model object into a variable for lts re-usability
        $this->ctrlr_name = $this->uri->segment(2); //Assign the controller name into a variable for lts re-usability
        $this->DS       = DIRECTORY_SEPARATOR;
    }

    /**
     * index function of this class
     */
    public function index()
    {
        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/album_list');
    }

    ##-----------------------------------------------##
    ##--------------- Portfolio >> list -------------##
    ##-----------------------------------------------##
    function album_list()
    {
        $data = array();
        //################################################################//
        //###################### PAGINATION CODE #########################//
        $cnt = $this->mdl_obj->get_row_count();
        $count = $cnt;
        $this->load->library('pagination');

        $config['base_url']     = base_url().$this->admin_folder_name.'/'.$this->ctrlr_name.'/album_list/';
        $config['total_rows']   = $count;
        $config['per_page']     = '15';
        $config['uri_segment']  = 4;
        $config['num_links']    = 5;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="dataTables_paginate">';
        $config['full_tag_close']   = '</ul>';
        $config['prev_link']        = '<li>Previous</li>';
        $config['next_link']        = '<li>Next</li>';
        $config['first_link']       = '<li>First</li>';
        $config['last_link']        = '<li>Last</li>';
        $config['cur_tag_open']     = '<li class="curr_active_class">';
        $config['cur_tag_close']    = '</li>';
        $config['anchor_class']     = 'class=""';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';

        $this->pagination->initialize($config);

        $offset     = $this->uri->segment(4);
        $offset     = empty($offset)? 0 : ($offset-1)*$config['per_page']; // if $config['use_page_numbers'] = FALSE;

        $data['pagination_link']    = $this->pagination->create_links();
        $data['records_list']       = $this->mdl_obj->get_rows_pagination($config['per_page'],$offset);
        //echo "<pre>"; print_r($data['records_list']); echo "</pre>"; exit;




        ##--------- Total entry showing per page msg ------------##
        $counter_start          = ($offset == 0) ? 1 : $offset+1;
        $showing_end_counter    = ($config['per_page'] > $count) ?  $count : $offset + $config['per_page'];
        $showing_end_counter    = ($showing_end_counter > $count) ? $counter_start + ($count - $counter_start) : $showing_end_counter;

        if ( $counter_start == $showing_end_counter )
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$showing_end_counter.' of '.$count.' entries</div>';
        else
            $pagination_msg_str = '<div class="dataTables_info" id="datatable1_info">Showing '.$counter_start.' to '.$showing_end_counter.' of '.$count.' entries</div>';

        $data['pagination_count_msg'] = $pagination_msg_str;
        ##--------- Total entry showing per page msg ------------##
        //###################### PAGINATION CODE #########################//
        //################################################################//
        /*$this->load->model('Portfolio_categories_model');
        $xx = $this->Portfolio_categories_model->get_record_list();
        echo "<pre>"; print_r($xx); die('123');*/

        $this->load->model('photographer_model');
        $data['photographers_list'] = $this->photographer_model->get_all_photographers();

        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/album_list_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);

    }

    ##----------------------------------------------------##
    ##------------------ Portfolio >> Add ----------------##
    ##----------------------------------------------------##
    function add()
    {
        $data               = array();
        $data['msg_data']   = "";

        //submitted form data
        if ( $this->input->post('SaveExit') || $this->input->post('SaveReview') )
        {
            //validate form data
            if ( $this->mdl_obj->validate_form_data() == TRUE )
            {
                $postdata = array(
                    'creation_datetime'     => date('Y-m-d H:i:s'),
                    'photographer_id' 		=> $this->input->post('photographer_id'),
                    'occasion_id' 			=> 1,
                    'album_name' 			=> $this->input->post('album_name'),
                    'short_description' 	=> nl2br($this->input->post('short_description')),
                    'intro_text' 			=> $this->input->post('intro_text'),
                    'preferred_subdomain_name'  => $this->input->post('preferred_subdomain_name'),
                    'max_storage_allocate'  => $this->input->post('max_storage_allocate'),
                    'album_price'           => $this->input->post('album_price'),
                    'google_drive_link' 	=> $this->input->post('google_drive_link'),
                    'dropbox_link' 	        => $this->input->post('dropbox_link'),
                    'added_date' 		    => date('Y-m-d H:i:s'),
                    'updated_date'  	    => date('Y-m-d H:i:s')
                );

                //save data
                $last_id = $this->mdl_obj->add($postdata);
                if ( $last_id != "" )
                {
                    $postArr = array();
                    $postArr["upload_ids"]  = $this->input->post('upload_files_ids');
                    $postArr["album_code"]  = 'ALB/'. $this->input->post('photographer_id') .'/'.$last_id;

                    $this->mdl_obj->update_album_data($postArr, $last_id);

                    $msg_arr    = array(2, '<li>Record has been saved successfully.</li>');
                    $msg_arr    = $this->common_functions->view_message($msg_arr);
                    $this->session->set_flashdata('msg_data', $msg_arr);

                    if ( $this->input->post('SaveExit') )
                        redirect($this->admin_folder_name .'/'. $this->ctrlr_name.'/add');
                    else
                        redirect($this->admin_folder_name .'/'. $this->ctrlr_name.'/edit/'.$last_id);
                }
            }
            else
            {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['msg_data'] = $error_msg;
            }
        }

        $recommended_image_siz_arr = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text"] = "System Recommended Width & Height ". $recommended_image_siz_arr['portfolio_image_medium'] . " (In pixcel)";

        $recommended_image_siz_arr_2 = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text_2"] = "System Recommended Width & Height ".$recommended_image_siz_arr['portfolio_image_medium_2']." (In pixcel)";

        $this->load->model('photographer_model');
        $data['photographers_list'] = $this->photographer_model->get_all_photographers();
        /* echo "<pre>". print_r($data['photographers_list'], true)."</pre>"; exit(0); */

        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/album_add_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    }

    ##---------------------------------------------------##
    ##------------------ Portfolio >> Edit --------------##
    ##---------------------------------------------------##
    function edit()
    {
        $data               = array();
        $data['msg_data']   = "";
        $row_id             = $this->uri->segment(4);

        //submitted form data
        if ( $this->input->post('SaveExit') || $this->input->post('SaveStay') )
        {
            //validate form data
            $this->form_validation->set_rules('album_name', 'album name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique['. $row_id  .']');
            $this->form_validation->set_rules('album_price', 'album price', 'required|trim|xss_clean');

            if ( $this->form_validation->run() == TRUE )
            {
                $postdata = array(
                    'album_name' 			=> $this->input->post('album_name'),
                    'short_description' 	=> nl2br($this->input->post('short_description')),
                    'intro_text' 			=> $this->input->post('intro_text'),
                    'preferred_subdomain_name'  => $this->input->post('preferred_subdomain_name'),
                    'max_storage_allocate'  => $this->input->post('max_storage_allocate'),
                    'album_price'           => $this->input->post('album_price'),
                    'google_drive_link' 	=> $this->input->post('google_drive_link'),
                    'dropbox_link' 	        => $this->input->post('dropbox_link'),
                    'updated_date'  	    => date('Y-m-d H:i:s')
                );

                //save data
                $return = $this->mdl_obj->edit($row_id, $postdata);
                if ( $return );
                {
                    $postArr    = array();
                    $postArr["upload_ids"]  = $this->input->post('upload_files_ids');
                    $prev_space_usage       = $this->common_functions->get_album_total_space_usage($row_id);

                    $uploadFlag = $this->mdl_obj->update_album_file_upload_data_on_edit_operation( $postArr, $row_id, $prev_space_usage );

                    if ($uploadFlag)
                    {
                        $msg_arr = array(2, '<li>Record has been updated successfully.</li>');
                        $msg_arr = $this->common_functions->view_message($msg_arr);
                        $this->session->set_flashdata('msg_data', $msg_arr);
                    } else {
                        $error_arr = array(1, '<li>Album files failed to upload.</li>' );
                        $error_msg = $this->common_functions->view_message($error_arr);
                        $data['msg_data'] = $error_msg;
                    }
                    if ($this->input->post('SaveStay'))
                        redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/edit/' . $row_id);
                    else
                        redirect($this->admin_folder_name . '/' . $this->ctrlr_name . '/album_list/');
                }
            }
            else
            {
                $error_arr = array(1, validation_errors());
                $error_msg = $this->common_functions->view_message($error_arr);
                $data['msg_data'] = $error_msg;
            }

        }

        $data['record_dtls'] = $this->mdl_obj->get_details($row_id);

        $recommended_image_siz_arr = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text"] = "System Recommended Width & Height ".$recommended_image_siz_arr['portfolio_image_medium']." (In pixcel)";

        $recommended_image_siz_arr_2 = str_replace("|"," X ", $this->config->item('image_valid_dimensions'));
        $data["image_recommended_text_2"] = "System Recommended Width & Height ".$recommended_image_siz_arr['portfolio_image_medium_2']." (In pixcel)";

        $this->load->model('photographer_model');
        $data['photographers_list'] = $this->photographer_model->get_all_photographers();
        //echo "<pre>"; print_r($data['record_dtls']); print_r($data['photographers_list']); echo "</pre>"; exit;
        //populate view, specific to this function
        $this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/album_edit_view', $data, true);

        //render full layout, specific to this function
        $this->load->view($this->admin_folder_name.'/layout_home', $this->data);

    }

    ##--------------------------------------------------------------##
    ##----------------- Portfolio >> active/inactive  --------------##
    ##--------------------------------------------------------------##
    function change_status()
    {
        $arr_val    = array();
        $action_mode    = $this->uri->segment(4);
        $status_val     = $this->uri->segment(5);

        if ( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(6));
        }

        $return_val = $this->mdl_obj->change_status_value($arr_val, $status_val);

        $return_msg = "Status has been changed successfully.";
        $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/album_list');

    }

    ##-------------------------------------------------##
    ##--------------- Portfolio >> Delete -------------##
    ##-------------------------------------------------##
    function delete()
    {
        $arr_val    = array();
        $action_mode    = $this->uri->segment(4);

        if( $action_mode == "multiple" )
        {
            $arr_val    = $this->input->get('chk');
        }
        else
        {
            array_push($arr_val, $this->uri->segment(5));
        }

        $return = $this->mdl_obj->delete($arr_val);

        if($return['errorSt']==1) //Some data are not deleted
        {
            $return_msg = " The service(s) named ".$return['msg_str']." are not deleted. This service(s) are assigned with a category.";
            $msg_arr = array(1, '<li>'.$return_msg.'</li>');
        }
        else
        {
            $return_msg = "Data has been deleted successfully";
            $msg_arr = array(2, '<li>'.$return_msg.'</li>');
        }

        $msg_arr = $this->common_functions->view_message($msg_arr);
        $this->session->set_flashdata('msg_data', $msg_arr);

        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/list_all');

    }

    /**
     *
     */
    function ajax_plupload()
    {
        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Settings
        //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
        if( isset($_POST["pg_code"]) && trim($_POST["pg_code"])!= '' ) {
            $targetDir = FCPATH . "assets". $this->DS . "uploaded_files" . $this->DS . "album_files" . $this->DS . $_POST["pg_code"];
        } else {
            $targetDir = FCPATH . "assets". $this->DS . "uploaded_files" . $this->DS . "album_files";
        }

        //$targetDir = 'uploads';
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        // Get a file name
        if (isset($_REQUEST["name"])) {
            $fileName = time() .'_'.rand(999,9999999).'_'. $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = time() .'_'.rand(999,9999999).'_'. $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Chunking might be enabled
        $chunk  = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $uploadedFileName   = array();

        // Remove old temp files
        if ($cleanupTargetDir)
        {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false)
            {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }
        //echo "<pre>"; print_r($_FILES);

        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if ( !empty($_FILES) )
        {

            if ( $_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"]) ) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 123456, "message": "Force exit if."}, "id" : "123"}');

            }
            else
            {
                $postdata  = array(
                                'album_id' => '',
                                'upload_file_name' => $fileName,
                                'upload_file_size' => $_FILES["file"]['size'],
                                'uploaded_datetime' => date('Y-m-d H:i:s')
                            );
                $album_files_id = $this->mdl_obj->album_file_upload($postdata);
                if ($album_files_id)
                {
                    $uploadedFileName[$album_files_id] = $fileName;
                }
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);


        //$uploads_dir = '/upload_image';
        /*$uploads_dir = FCPATH . "assets". DIRECTORY_SEPARATOR . "uploaded_files" . DIRECTORY_SEPARATOR . "album_files". DIRECTORY_SEPARATOR;
        move_uploaded_file($_FILES["file"]["tmp_name"], $uploads_dir . $_FILES["file"]["name"]);*/

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }

        $result["status"]   = true;
        $result["message"]  = 'Success';
        $result["file_name"]    = $uploadedFileName;

        echo json_encode($result);
        exit;

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "filename" : '.$uploadedFileName.'}');
    }

    /**
     *
     */
    function ajax_check_unique_subdomain_name()
    {
        $return             = array();
        $return['result']   = "failed";
        $return['status']   = false;
        $preferred_subdomain_name   = $this->input->post('preferred_subdomain_name');
        $album_id            = $this->input->post('alb_id');
        $operation_mode             = $this->input->post('operation_mode');
        if ( trim($preferred_subdomain_name) == '' )
            return false;

        $this->form_validation->set_error_delimiters('', '');

        if ( $operation_mode == "edit" && (int)$album_id > 0 )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique['.$album_id.']');
        } else {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]',
                array('is_unique' => '"' . $preferred_subdomain_name . '" This %s already exists. Please type another name.')
            );
        }

        if ($this->form_validation->run() == FALSE)
        {
            $return['result']   = validation_errors();
            echo json_encode($return);
            exit(0);
        }
        else
        {
            $return['result'] = '"'.$preferred_subdomain_name.'" this name cab be use as album subdomain name.';
            $return['status'] = true;
            echo json_encode($return);
        }

    }

    /**
     * @param $subdomainName
     * @param int $album_id
     * @return bool
     */
    function subdomain_name_check_unique( $subdomainName, $album_id = 0)
    {

        if ( (int)$album_id > 0 && trim($subdomainName) != '' )
        {
            $is_exists = $this->common_functions->check_unique_slug_exists($subdomainName, 'preferred_subdomain_name', 'albums', array('id' => $album_id ) );
            if ( $is_exists ) {
                $this->form_validation->set_message('subdomain_name_check_unique', '"'.$subdomainName.'" This %s already exists. Please type another name.');
                return false;
            } else {
                return true;
            }
        }
        else
        {
            return TRUE;
        }
    }


} //end class
