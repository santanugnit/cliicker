<?php
class Static_pages extends CI_Controller
{
    var $data;
	var $pcategory; //global variable declearation in order to create category list at the backend.
	
    //################################################################//
    //###################### LOAD CONSTRUCTOR ########################//
    //################################################################//
	
	function  __construct() 
	{   
		parent::__construct();
		$this->load->library('common_functions');
		$this->load->library('admin_init_elements');
		//is Admin logged in?
		$this->admin_init_elements->is_admin_logged_in();
		$this->admin_init_elements->init_elements();
		$this->admin_folder_name = $this->config->item('admin_folder_name'); //get admin folder name
		
		$this->load->model('Static_pages_model'); //Load Item master model
        $this->mdl_obj = $this->Static_pages_model; //Assign the model object into a variable for lts re-usability
		$this->ctrlr_name = $this->uri->segment(2); //Assign the controller name into a variable for lts
				
    }
	
    //################################################################//
    //###################### LOAD CONSTRUCTOR ########################//
    //################################################################//
	
	function index()
	{
		redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/display');	
	}
	
	
    ##----------------------------------------------------------------##
    ##--------------------- Static Pages > List ----------------------##
	##----------------------------------------------------------------##
    function display()
	{	
		global $pcategory; //define a global variable for display the menu list in tree view format
		
		##----------------- category listing -----------------------##
        $treeArray= $this->mdl_obj->get_nlevel_array();
		
        $data['category_tree_view']="";        
        if(count($treeArray) >0)
        {  
            $this->create_tree($treeArray, 0 , 0, -1);
            $data['category_tree_view'] = $pcategory;
        }
        ##-----------------------------------------------------------##
		//echo "<pre>"; print_r($data['category_tree_view']); die();	
		//$data['rows'] = $this->mdl_obj->get_rows(); //get all rows
					
		//populate view, specific to this function
		$this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/static_pages_view', $data, true);
		//render full layout, specific to this function
		$this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    
	}
	
	##------------------------------------------------------------------------##
    ##--------------- Product >> Preview category treeview -------------------##
    ##------------------------------------------------------------------------##
    function create_tree($array, $currentParent, $currLevel = 0, $prevLevel = -1) 
    {      
        global $pcategory; //define a global variable for display the menu list in tree view format
        $i=0;

        foreach ($array as $itemId => $dtls) 
        {

            if($currentParent == $dtls['parent_id']) 
            {
                if ($currLevel > $prevLevel) $pcategory .= '<ol class="">';
                if ($currLevel == $prevLevel) $pcategory .='</li>';

                $category_edit_link='<a href="'.base_url().$this->admin_folder_name.'/'.$this->ctrlr_name.'/modify/'.$itemId.'" class="cat_edit"><i class="fa fa-fw fa-edit tree_action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit the page content"></i></a>';

                $pcategory .='<li class="" data-id="'.$itemId.'"><span>'.ucwords($dtls['category_name']).$category_edit_link.'</span>';


                if($currLevel > $prevLevel)
                { 
                        $prevLevel = $currLevel;
                }

                $currLevel++; 
                $this->create_tree($array, $itemId, $currLevel, $prevLevel);
                $currLevel--;     

            }

            $i++;

        }

        if ($currLevel == $prevLevel) $pcategory .="</li></ol>";

    }
		
    
	
	//###########################################################//
	//##################### ADD PAGE ############################//
	//###########################################################//

    function add()
	{		
				
		$data['error_message'] = "";
		//is form submitted?
		if($this->input->post('save'))
		{
			
			//validate form data
			if($this->mdl_obj->validate_form_data() == TRUE)
			{
				//save data
				$result = $this->mdl_obj->add();
				
				if($result == "add_success")
				{
					redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/display/add_success');
				}
				else
				{
					$data['error_message'] = $result;
				}
			 }
			else
			{
				$data['error_message'] = validation_errors();
			}
			
		}
		elseif($this->input->post('cancelbtn'))
		{
			redirect($this->admin_folder_name.'/'.$this->ctrlr_name);
		}
		
		$data['sort_orderno']=$this->mdl_obj->get_sort_order_no();
						
		$this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/static_pages_add_view', $data, true);
		//render full layout, specific to this function
		$this->load->view($this->admin_folder_name.'/layout_home', $this->data);
    
	}
	
	//############################################################//
	//##################### ADD PAGE ############################//
	//###########################################################//
	
	
	//#############################################################//
	//##################### MODIFY PAGE #########################//
	//#############################################################//
	
	function modify()
	{
		$pageid = $this->uri->segment(4);	
		//is form submitted?
		if ($this->input->post('save'))
		{
			//validate form data
			if ($this->mdl_obj->validate_form_data() == TRUE)
			{
				$small_content = (isset($_POST["small_content"])) ? trim($this->input->post('small_content')) : "";
				//save data
				$postdata['main'] = array(
											'page_title'     =>   trim($this->input->post('page_title')),
											'small_content'  =>   $small_content,
											'page_content'   =>   $this->input->post('page_content'),
										);
				$postdata['seo_data'] = array(
											'meta_title' => $this->input->post('meta_title'),
											'meta_keywords' => $this->input->post('meta_keywords'),
											'meta_description' => $this->input->post('meta_description')
										);						
							 
				$result = $this->mdl_obj->modify($pageid, $postdata);
				if($result == "update_success")
				{
					$msg_arr = array(2, '<li>Record has been updated succesfully.</li>');
					$msg_arr = $this->common_functions->view_message($msg_arr);
					$this->session->set_flashdata('msg_data', $msg_arr);
					redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/modify/'.$pageid);
				}
				else
				{
					//$data['error_message'] = $result;
					
					$error_arr = array(1, validation_errors());
					$error_msg = $this->common_functions->view_message($error_arr);
					$data['msg_data'] = $error_msg;
				}
						
			}
			else
			{
						$data['error_message'] = validation_errors();
			}
			
		}
				
		$data['rows'] = $this->mdl_obj->get_rows($pageid); //get current row
		//echo "<pre>"; print_r($data['rows']); exit;
		$this->data['maincontent'] = $this->load->view($this->admin_folder_name.'/maincontents/static_pages_modify_view', $data, true);
		
		//render full layout, specific to this function
		$this->load->view($this->admin_folder_name.'/layout_home', $this->data);

	}
	
	//############################################################//
	//##################### MODIFY PAGE #########################//
	//###########################################################//
	
	
    //################################################################//
    //################### CHANGE STATUS FUNCTION  #####################//
    //###############################################################`#//
	
	
	function change_status()
	{
		
		$pgid=$this->input->get("pgId");
		$stval=$this->input->get("stVal");
		
		$return= $this->mdl_obj->change_status($pgid,$stval);
		
		if($return=="st_change")
		{
			echo "st_change";
		}
			
	}	
	
    //################################################################//
    //################### CHANGE STATUS FUNCTION  #####################//
    //###############################################################`#//
	
	
	//############################################################//
	//##################### DELETE FUNCTION #####################//
	//###########################################################//

    function delete()
	{
        $this->mdl_obj->delete();
        redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/display/dtl_success');
    }

	//############################################################//
	//##################### DELETE FUNCTION ######################//
	//###########################################################//
	
	
	//######################################################//
	//##################### SORT DATA ######################//
	//######################################################//
	
	function sort_data()
	{	
		$return= $this->static_pages_model->change_sort_order();
		
		$msg_arr = array(2, '<li>Record has been sorted succesfully.</li>');
		$msg_arr = $this->common_functions->view_message($msg_arr);
		$this->session->set_flashdata('msg_data', $msg_arr);
		
		redirect($this->admin_folder_name.'/'.$this->ctrlr_name.'/display');
		
	}
	
	//######################################################//
	//##################### SORT DATA ######################//
	//######################################################//
	

}
?>
