<?php
require(APPPATH.'/libraries/REST_Controller.php');
class Portfolio extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Portfolio_model');
        $this->api_access_key = get_api_access_key();
    }

    ##---------------------------------------------------------------##
    ##------- Get portfolio category list along with services -------##
    ##---------------------------------------------------------------##
    function get_category_list_get()
    {
        if($this->api_access_key == md5($this->get('apitoken')."#SHA@")) //checking for valid apitoken
        {
            $this->load->model('Portfolio_categories_model');
            $record = $this->Portfolio_categories_model->get_record_list();
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 1,
                'api_action_message' => 'success',
                'result' => $record
            );
        } else {
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 0,
                'api_action_message' => 'Invalid access token',
                'result' => array()
            );
        }

        $this->response($data, 200);
    }
    ##---------------------------------------------------------------##
    ##---------------------------------------------------------------##

    ##---------------------------------------------------------------##
    ##---------- Get portfolio list respect to category ID ----------##
    ##---------------------------------------------------------------##
    function get_portfolio_list_respect_to_category_get()
    {
        if($this->api_access_key == md5($this->get('apitoken')."#SHA@")) //checking for valid apitoken
        {
            if($this->get('catID')) //Checking for category id
            {
                $catid = $this->get('catID');
                $record = $this->Portfolio_model->get_record_list($catid);
                $data = array(
                    'api_syntax_success' => 1,
                    'api_action_success' => 1,
                    'api_action_message' => 'success',
                    'result' => $record
                );
            }
            else {
                $data = array(
                    'api_syntax_success' => 1,
                    'api_action_success' => 0,
                    'api_action_message' => 'Provide a category id to get results',
                    'result' => array()
                );
            }

        } else {
            $data = array(
                'api_syntax_success' => 1,
                'api_action_success' => 0,
                'api_action_message' => 'Invalid access token',
                'result' => array()
            );
        }

        $this->response($data, 200);
    }
    ##---------------------------------------------------------------##



}
?>
