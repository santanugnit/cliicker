<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_functions
{

	var $CI;
	var $category_tree_dropdown = "";
	var $tree_ul_li = "";

    function  __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }
	
	/**
	 * Dynamic function return a meassge div identifying its type
	 * @param array $val array field
	 * return a div with message identifying its type
	 */
	function view_message($val)
	{   
		$message_val = $val;
		$output_message="";
		$msg_type="";
		$msg_heading="";
				
		if(is_array($message_val))
		{
			//******** GET MESSAGE TYPE ********//
			if($message_val[0]==1)
			{    
				$msg_type = "danger";
				$msg_heading="Error!";
			}
			elseif($message_val[0]==2)
			{    
				$msg_type = "success";
				$msg_heading="Success!";
			}
			elseif($message_val[0]==3)
			{
				$msg_type = "warning";
				$msg_heading="Warning!";
			}
			elseif($message_val[0]==4)
			{
				$msg_type = "info";
				$msg_heading="Info!";
			}
			else
			{    
				$msg_type="";
				$msg_heading="";
			}
			
			$output_message .='<div class="col-md-12 header_notification_message">';
			$output_message .='<div class="alert alert-'.$msg_type.'">';
			$output_message .='<a href="#" data-dismiss="alert" onclick="close_alert();" class="close alert-close">x</a>';
			$output_message .='<h4 class="alert-heading">'.$msg_heading.'</h4>';
			$output_message .='<p>';
			
			$output_message .="<ul>";
			$output_message .= $message_val[1];
			$output_message .="</ul>";
			
			$output_message .='</p>';
			$output_message .='</div>';
			$output_message .='</div>';
			
		}
		
		return $output_message;
		
	}
	
	
	/**
     * Dynamic function to get a value respect to a particular filed of a 
     * table with passing where clause as parameter 
     * @param string $field Field name
     * @param string $tablename The table name
     * @param string $where Where clause condition
     * @return mixed Return a value. Also return NULL if record is not exists. 
     */
    function get_value($field,$tablename,$where='')
    {
        $fieldval="";
        $sql="SELECT ".$field." FROM ".$tablename;
		if($where!="")
		{
			$sql = $sql." WHERE ".$where;
		}

        $query = $this->CI->db->query($sql);
        $result = $query->row_array();
        if(count($result))
        {
            $fieldval= $result[$field];
        }
		
		return $fieldval;
		
    }
	
	
	/**
     * Dynamic function to get tree view structure of categories within select box
     * @param array $data_array category array id=>name structure
     * @param array $selected_ids passing ids displaying as a selected status within select box
     * @return string with html format 
     */
	function get_category_tree($data_array, $selected_ids=array(), $ignore_ids=array(), $params=array())
	{	
		global $category_tree_dropdown;
        
        $selectbox_name = "";
        $selectbox_class = "";
        $is_multiple = 0; //0 means single selected
        $default_selected = "";
        
        if(count($params))
        {  
            $selectbox_name = $params['selectbox_name'];
            $selectbox_class = $params['class'];
            $is_multiple =  ($params['multiple']==1) ? 'multiple="multiple"' : '';
            $selectbox_class = $params['class'];
            $default_select_value = $params['default_select_value'];
            $default_select_text = ($params['default_select_text']!="") ? $params['default_select_text'] : "Select";
        }
         
        if(count($selected_ids)<=0)
        {    
            $default_selected = 'selected="selected"'; 
        }
        
		$category_tree_dropdown .= '<select name="'.$selectbox_name.'" class="'.$selectbox_class.'" '.$is_multiple.'>';
		$category_tree_dropdown .= '<option value="'.$default_select_value.'" '.$default_selected.'>********* '.$default_select_text.' *********</option>';
		
		if(count($data_array))
		{
			$category_tree = $this->build_category_tree($data_array);
			$category_tree_dropdown .= $this->category_tree_dropdown($category_tree,$selected_ids,$ignore_ids);
		}
		
		$category_tree_dropdown .= '</select>'; 
		
		return $category_tree_dropdown;
		
		
	}
	
	function build_category_tree($data , $parent=0)
	{
		$tree = array();
		foreach($data as $d) 
		{
			if($d['parent_id'] == $parent) 
			{
				$children = $this->build_category_tree($data, $d['id']);
				
				// set a trivial key
				if (!empty($children))
				{
					$d['_children'] = $children;
				}
				$tree[] = $d;
			}
		}
		
		return $tree;
	}
	
	function category_tree_dropdown($tree, $selected_ids=array(),$ignore_ids=array(), $r = 0, $p = null)
	{
		global $category_tree_dropdown;
		
		foreach($tree as $i => $t) 
		{
			$dash = ($t['parent_id'] == 0) ? '' : str_repeat('--- ---', $r).' ';
			
            if(count($ignore_ids)!=0 && !in_array($t['id'],$ignore_ids))
            {
                $category_tree_dropdown .= '<option value="'.$t['id'].'" ';
                if(count($selected_ids) && in_array($t['id'],$selected_ids))
                {  
                    $category_tree_dropdown .='selected="selected"';
                }    
                $category_tree_dropdown .= ' >'.$dash.$t["name"].'</option>';
            }
            else
            {    
                $category_tree_dropdown .= '<option value="'.$t['id'].'" ';
                if(count($selected_ids) && in_array($t['id'],$selected_ids))
                {  
                    $category_tree_dropdown .='selected="selected"';
                }    
                $category_tree_dropdown .= ' >'.$dash.$t["name"].'</option>';
            }
			
			if(isset($t['_children']))
			{
				$this->category_tree_dropdown($t['_children'], $selected_ids, $ignore_ids, $r+1, $t['parent_id']);
			}
		}
	}
	
	#---------------------------------------------------------------------------------##
    #---------------- Build n-level tree structure into dropdown menu ----------------##
    ##--------------------------------------------------------------------------------##
	
	
	##-------------------------------------------------------------------------------------##
    ##------------ Get geometric location (latitude, longitude) of an address -------------##
    ##-------------------------------------------------------------------------------------##
    
    function get_long_lat($params)
    {   
        
		$city = str_replace(" ", "+", $params['city']);
        $state ="";
		if(array_key_exists('state',$params))
        {
		    $state = str_replace(" ", "+", $params['state']);
        }
		$country = str_replace(" ", "+", $params['country']);
		$address = str_replace(" ", "+", $params['address']);

        $zip ="";
        if(array_key_exists('zipcode',$params)) {
            $zip = str_replace(" ", "+", $params['zipcode']);
        }
        
        /*$street_name = "82 Fensome Dr";
        $address = "Houghton Regis";
        $city = "Dunstable";
        $country = "";
        $postcode = "LU5 5SH"; */
        if($address!=""){ $main_address = $address; }
        if($state!=""){ $main_address .= ",".$state; }
        if($city!=""){ $main_address .= ",".$city; }
        if($zip!=""){ $main_address .= ",".$zip; }

		/*if($state!="" && $city!="" && $address!="" && $zip!="")
			$main_address = $address.",".$state.",".$city.",".$zip;
		else if($address!="" && $state!="" && $city=="" && $zip=="")
			$main_address = $address.",".$state;
		else if($address!="" && $state!="" && $city!="" && $zip=="")
			$main_address = $address.",".$state.",".$city;
		else
			$main_address =""; */

		//echo $main_address; die('123');
        $url = "http://maps.google.com/maps/api/geocode/json?address=".$main_address."&sensor=false&region=".$country;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        
        $lat="";
        $long="";
        
        if(count($response_a->results))
        {  
            $lat = $response_a->results[0]->geometry->location->lat;
            $long = $response_a->results[0]->geometry->location->lng;
        }
        
        $return=array('latitude'=>$lat,'longitude'=>$long);
        //echo "<pre>"; print_r($return); die();
        return $return;
    }
	
	
	##----------------------------------------------------------------------##
    ##------------ Get Unique slug value respect to post title -------------##
    ##----------------------------------------------------------------------##
	//@ $post_title   : Slug Value that you want to create
	//@ $field_name : Comparing with the uniqueness of the slug value 
	//@ $table_name  : Tablename that contents the slug value
	//@ $ignore_fields  : 
	
	function get_unique_slug($post_title, $field_name, $table_name, $ignore_fields=NULL)
	{
		
		$sql = "SELECT count(*) row_count FROM ".$table_name." WHERE ".$field_name." = '".$post_title."'";
		if(count($ignore_fields))
		{
			$str = "";
			foreach($ignore_fields as $text=>$val)
			{
				$str .= " AND ".$text."!= '".$val."'";
			}
			$sql .= $str;
		}
		//echo $sql; die();
		$query = $this->CI->db->query($sql);
		$result = $query->row_array();
		
		$slug_name = url_title($post_title);
		##-------- MAX LENGTH SHOULD BE 254 ---------##
		if(strlen($slug_name) >= '254'){
			$slug_name=substr($slug_name,0,252);
		}
		##-------- MAX LENGTH SHOULD BE 254 ---------##
		
		if( $result['row_count'] > 0 )  //Duplicate value exists
		{
			$counter = ($result['row_count'] + 1);
			$slug_name = $slug_name."-".$counter; 
		}
		
		
		return strtolower($slug_name);
		
	}

	function remove_all_spacial_character( $string = null, $ignore_fields = array() )
    {
        if( trim($string) == '' || $string == null ){
            return '';
        }

        $filterStr = str_replace(array(' '), "", $string);
        $finalStr = preg_replace('/[^A-Za-z0-9\-]/', '', $filterStr);

        return $finalStr;
    }

    function check_unique_slug_exists($post_title, $field_name, $table_name, $ignore_fields = array())
    {

        $sql = "SELECT count(*) row_count FROM ".$table_name." WHERE ".$field_name." = '".$post_title."'";
        if ( count($ignore_fields) )
        {
            $str = "";
            foreach($ignore_fields as $text => $val)
            {
                $str .= " AND ".$text."!= '".$val."'";
            }
            $sql .= $str;
        }
        //echo $sql; die();
        $query = $this->CI->db->query($sql);
        $result = $query->row_array();

        if( $result['row_count'] > 0 )  //Duplicate value exists
        {
            return true;
        } else {
            return false;
        }
    }
	
		

	
	
    ##----------------------------------------------------------------##
    ##-------------------- Image Resize Function  --------------------##
    ##----------------------------------------------------------------##
    function image_upload($image_data = null)
    {
        if(empty($image_data))
        {
            return false;
        }

        $this->CI->load->library('upload','');
        $this->CI->load->library('image_lib','');

        /********* Rename the upload file ***********/
        $filename_arr = explode(".", $_FILES[$image_data['field_name']]['name']);
        $file_extension = $filename_arr[count($filename_arr) - 1];
        $image_file_name = time().rand(10, 1000).".".$file_extension;
        /********************************************/

        // set upload configuration
        $config['upload_path'] = $image_data['upload_path'];
        $config['allowed_types'] = $image_data['image_types'];
        $config['max_size'] = '1000000'; //KB
        $config['file_name'] = $image_file_name;

        $this->CI->upload->initialize($config);

        if (!$this->CI->upload->do_upload($image_data['field_name']))
        {
            return array('message'=>'failed', 'image_info'=>$this->CI->upload->display_errors());
            //die('Image upload error');
        }
        else
        {

            $data = array('upload_data' => $this->CI->upload->data());
            $feature_image = $data['upload_data']['file_name'];

            ##-------------------------------------------------------##
            ##-------------------- RESIZE IMAGE ---------------------##
            ##-------------------------------------------------------##
            if(array_key_exists('image_resize',$image_data))
            {
                if($image_data['image_resize']==true)
                {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $image_data['upload_path'].$data['upload_data']['file_name'];
                    $config['new_image'] = $image_data['upload_medium_path'].$data['upload_data']['file_name'];
                    $config['allowed_types'] = $image_data['image_types'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['master_dim']='auto';

                    if(@$image_data['quality']>0)
                    {
                        $config['quality'] = @$image_data['quality'];
                    }
                    //###################################################//
                    //########### CHECK UPLOAD IMAGE DIMENSION ##########//

                    $imgarr= $this->CI->config->item('image_valid_dimensions');
                    $imgarr_data=$imgarr[$image_data['image_valid_dimensions']];
                    $data_explode=explode("|",$imgarr_data);

                    $define_image_width=$data_explode[0];
                    $define_image_height=$data_explode[1];

                    //########### CHECK UPLOAD IMAGE DIMENSION ##########//
                    //###################################################//

                    $iw= $data['upload_data']['image_width']; //uploaded image width
                    $ih= $data['upload_data']['image_height']; //uploaded image height

                    $mw = $define_image_width; //Recommended image width
                    $mh = $define_image_height; //Recomended image height

                    if($iw<=$mw && $ih <= $mh)
                    {
                        $ow    = $iw;
                        $oh    = $ih;
                    }
                    if($iw<=$mw && $ih > $mh)
                    {
                        $oh    = $mh;
                        $ow    =($oh*$iw)/$ih;
                    }
                    if($iw>$mw && $ih <= $mh)
                    {
                        $ow    = $mw;
                        $oh    = ($ow*$ih)/$iw;
                    }
                    if($iw>$mw && $ih > $mh && $iw>$ih )
                    {
                        $ow    = $mw;
                        $oh    = ($ow*$ih)/$iw;
                    }
                    if($iw>$mw && $ih > $mh && $iw<=$ih)
                    {
                        $oh    = $mh;
                        $ow    = ($oh*$iw)/$ih ;
                    }

                    $config['width'] = $ow;
                    $config['height'] = $oh;

                    $this->CI->image_lib->initialize($config);
                    $this->CI->image_lib->resize();

                }
            }
            ##-------------------------------------------------------##
            ##-------------------- RESIZE IMAGE ---------------------##
            ##-------------------------------------------------------##

            return array('message'=>'success', 'image_info'=>$image_file_name);
        }
    }


    /**
     * @param int $album_id
     * @return mixed
     */
    function get_album_total_space_usage( $album_id = 0 )
    {
        $sql = 'SELECT SUM(upload_file_size) as total_size FROM album_uploaded_files WHERE id = "'.$album_id.'"';

        $query = $this->CI->db->query($sql);
        $result = $query->row_array();

        return $result['total_size'];
    }

    /**
     * @return array
     */
    public function get_user_status_arr()
    {
        return array(
            'NV' => 'You are verified. Please contact with site admin.',
            'B' => 'You are blocked by site admin',
            'D' => 'Your account has been deleted. Please contact with admin.'
        );
    }


}

?>