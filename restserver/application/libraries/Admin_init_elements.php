<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_init_elements
{
    var $CI;

    function  __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
	    $this->admin_folder_name = $this->CI->config->item('admin_folder_name');
    }

    /**
     *
     */
    function init_head()
    { 
        $this->CI->data = array();
        //populate default meta values for any controller
        $data['title'] = "Codopoliz";
        $data['meta_keyword'] = "Codopoliz";
        $data['meta_description'] = "Codopoliz";
        
        $this->CI->load->model('settings_model');
        $user_arr = $this->CI->session->all_userdata();
        if(array_key_exists('userid',$user_arr))
	{
	    $userid = $user_arr['userid'];
            $data['user_dtls'] = $this->CI->settings_model->get_user_details($userid);
        }      
        $this->CI->data['head'] = $this->CI->load->view($this->admin_folder_name.'/elements/head', $data, true);
        $this->CI->data['topmenu'] = $this->CI->load->view($this->admin_folder_name.'/elements/topmenu', $data, true);
        $this->CI->data['sidemenu'] = $this->CI->load->view($this->admin_folder_name.'/elements/sidemenu', $data, true);
        $this->CI->data['footer'] = $this->CI->load->view($this->admin_folder_name.'/elements/footer', $data, true);
              
    }


    /**
     *
     */
    function init_elements()
    {
        //index call to populate all the header / footer / side bar elements
        $this->init_head();
        
    }

    /**
     *
     */
    function is_admin_logged_in()
    {
        $is_admin_logged_in = $this->CI->session->userdata('is_admin_logged_in');

        if(!isset($is_admin_logged_in) || $is_admin_logged_in != TRUE)
        {
            redirect($this->admin_folder_name);
        }
    }

    /**
     * @param array $getValue
     * @return string
     */
    function get_url_params( $getValue=array())
    {
        $getUrl ="";
        if( isset($getValue) && count($getValue) )
        {
            $getarr = $getValue;
            foreach($getarr as $key=>$value)
            {
                if ( !is_array($value) )
                    $getUrl .= "&".$key."=".$value;
            }
            $getUrl = "?".substr($getUrl,1,strlen($getUrl));
        }
        return $getUrl;
    }

    function check_user_access_right()
    {
        $usertype = $this->CI->session->userdata('userType');
        if ( $usertype == "EDITOR" )
        {
            redirect($this->admin_folder_name.'/home/dashboard');
        }

    }
    
    
}

?>