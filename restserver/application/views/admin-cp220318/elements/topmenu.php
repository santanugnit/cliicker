<?php
$usertype = $this->session->userdata('userType');
?>

<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="btn btn-transparent btn-equal btn-menu" href="javascript:void(0);"><i class="fa fa-bars fa-lg"></i></a>
        <div class="navbar-brand">
            <a class="main-brand" href="index.html">
                <h3 class="text-light text-white"><span>Codopoliz</span></h3>
            </a>
        </div><!--end .navbar-brand -->
        <a class="btn btn-transparent btn-equal navbar-toggle" data-toggle="collapse" data-target="#header-navbar-collapse"><i class="fa fa-wrench fa-lg"></i></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="header-navbar-collapse">

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="javascript:void(0);" class="navbar-profile dropdown-toggle text-bold" data-toggle="dropdown"><?php echo $user_dtls['first_name']." ".$user_dtls['last_name']; ?><i class="fa fa-fw fa-angle-down"></i>
                <?php
                    if( $user_dtls['profile_image'] != "" ) {
                ?>
                        <img class="img-rounded" src="<?php echo base_url(); ?>assets/uploaded_files/user_profile_image/medium/<?php echo $user_dtls['profile_image']; ?>" alt="" />
                <?php } else { ?>
                        <img class="img-rounded" src="<?php echo base_url(); ?>assets/img/user.jpg" alt="" />
                <?php } ?>
                </a>
                <ul class="dropdown-menu animation-slide">
                    <!--<li class="dropdown-header">Config</li>-->
                <?php
                    if($usertype!="EDITOR"){
                        ?>
                        <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/settings/user_profile" ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;My Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/settings/website_settings" ?>"><i class="fa fa-wrench"></i>&nbsp;&nbsp;Website Settings</a></li>
                        <li class="divider"></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/login/logout"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                </ul><!--end .dropdown-menu -->
            </li><!--end .dropdown -->
        </ul><!--end .nav -->
    </div><!--end #header-navbar-collapse -->
</nav>
