<title><?php echo $title; ?></title>

<!-- BEGIN META -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="<?php echo $meta_keyword; ?>">
<meta name="description" content="<?php echo $meta_description; ?>">
<link href="<?php echo base_url(); ?>assets/images/favIcon.png" rel="shortcut icon" type="image/x-icon" />

<!-- END META -->
<!-- BEGIN STYLESHEETS -->
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/> -->

<link  href="<?php echo base_url(); ?>assets/css/google_font.css" rel='stylesheet' type='text/css'/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp-bootstrap-frame.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp-collaps-box.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp_responsive.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/toastr/zp-toastr.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp-custom.css" />

<!-- DATA TABLE -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/DataTables/jquery.dataTables.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/DataTables/TableTools.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/jquery-ui/jquery-ui.css" />

<!-- LOAD JQUERY VALIDATION ENGINE STYLESHEET -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css" />

<!-- END STYLESHEETS -->

<!-- JQUERY MIN -->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

<!-- LOAD JQUERY VALIDATION ENGINE SCRIPT -->
<script src="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js"></script>

<!-- NESTABLE JS -->
<script src="<?php echo base_url(); ?>assets/js/nestable_js/jquery.nestable.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/nestable_js/tree-menu.css" />

<!-- JQUERY ALERT -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/alert/jquery.alerts.css" />

<!-- CUSTOM JS -->
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


<script src="<?php echo base_url(); ?>assets/js/libs/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/multi-select/jquery.multi-select.js"></script>


<!--<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/bootstrap-timepicker/bootstrap-timepicker.min.css" />-->

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/select2/select2000c.css?1401442116" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/libs/multi-select/multi-select5aa2.css?1401442115" />

<!--<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">-->

<!-- DAD (Drag And Drop) CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dad.css" />
<!-- LOAD JQUERY.DAD JS for drag-n-drop -->
<script src="<?php echo base_url(); ?>assets/js/jquery.dad.js"></script>
<!-- Bootstrap Select Box : CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css">



