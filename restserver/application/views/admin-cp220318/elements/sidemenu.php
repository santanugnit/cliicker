<?php
$usertype = $this->session->userdata('userType');
?>
<div id="sidebar">
    <div class="sidebar-back"></div>
    <div class="sidebar-content">
        <div class="nav-brand">
            <a class="main-brand" href="<?php echo base_url().$this->config->item('admin_folder_name')."/"; ?>home/dashboard/">  <img src="<?php echo base_url(); ?>assets/img/logo.png" style="padding-top:6px;" width="136" height="" alt="Cliicker" /></a>
        </div>

        <!-- BEGIN MAIN MENU -->
        <ul class="main-menu">

            <li>
                <a href="<?php echo base_url().$this->config->item('admin_folder_name')."/"; ?>home/dashboard" class="<?php if($this->uri->segment(3)=="dashboard"){ echo "active"; } ?>" >
                    <i class="fa fa-home fa-fw"></i><span class="title">Dashboard</span>
                </a>
            </li>

        <?php
            $photographers = "";
            if ( ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "photographer_list") ||
                ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "add") ||
                ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "edit") )
            {
                $photographers   = "active";
            }

            $albums = "";
            if ( ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "album_list") ||
                ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "add") ||
                ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "edit") )
            {
                $albums   = "active";
            }

            $testimonial = "";
            if ( ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "testimonial_list") ||
                ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "add") ||
                ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "edit") )
            {
                $testimonial   = "active";
            }

            $pcat_act="";
            if ( ($this->uri->segment(2)=="portfolio_categories" && $this->uri->segment(3)=="list_all") ||
                ($this->uri->segment(2)=="portfolio_categories" && $this->uri->segment(3)=="add") ||
                ($this->uri->segment(2)=="portfolio_categories" && $this->uri->segment(3)=="edit") )
            {
                $pcat_act   = "active";
            }

            $service_act="";
            if ( ($this->uri->segment(2)=="services" && $this->uri->segment(3)=="list_all") ||
                ($this->uri->segment(2)=="services" && $this->uri->segment(3)=="add") ||
                ($this->uri->segment(2)=="services" && $this->uri->segment(3)=="edit") )
            {
                $service_act    = "active";
            }

            $portfolio_act="";
            if ( ($this->uri->segment(2)=="portfolio" && $this->uri->segment(3)=="list_all") ||
                ($this->uri->segment(2)=="portfolio" && $this->uri->segment(3)=="add") ||
                ($this->uri->segment(2)=="portfolio" && $this->uri->segment(3)=="edit") )
            {
                $portfolio_act  = "active";
            }

            $order_act="";
            if( ($this->uri->segment(2)=="orders" && $this->uri->segment(3)=="list_all") ||
                ($this->uri->segment(2)=="orders" && $this->uri->segment(3)=="add") ||
                ($this->uri->segment(2)=="orders" && $this->uri->segment(3)=="edit") )
            {
                $order_act  = "active";
            }
        ?>

        <?php /* ?>
				<li class="<?php echo $user_act; ?>">
                    <a href="<?php echo base_url().$this->config->item('admin_folder_name')."/user"; ?>" class="<?php if($this->uri->segment(3)=="user_list" || ($this->uri->segment(2)=="user" && $this->uri->segment(3)=="add") || ($this->uri->segment(2)=="user" && $this->uri->segment(3)=="edit")){ echo "active"; } ?>">
                    <i class="fa fa-user"></i><span class="title">Users</span>
                    </a>
                </li>
        <?php */ ?>
            <li class="<?php echo $photographers; ?>">
                <a href="<?php echo base_url(). $this->config->item('admin_folder_name') ."/photographer"; ?>" class="<?php if( ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "photographers_list") || ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "add") || ($this->uri->segment(2) == "photographer" && $this->uri->segment(3) == "edit") ){ echo "active"; } ?>">
                    <i class="fa fa-camera"></i><span class="title">Photographer</span>
                </a>
            </li>
            <li class="<?php echo $albums; ?>">
                <a href="<?php echo base_url(). $this->config->item('admin_folder_name') ."/album"; ?>" class="<?php if( ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "album_list") || ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "add") || ($this->uri->segment(2) == "album" && $this->uri->segment(3) == "edit") ){ echo "active"; } ?>">
                    <i class="fa fa-book"></i><span class="title">Album</span>
                </a>
            </li>
            <li class="<?php echo $testimonial; ?>">
                <a href="<?php echo base_url(). $this->config->item('admin_folder_name') ."/testimonial"; ?>" class="<?php if( ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "testimonial_list") || ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "add") || ($this->uri->segment(2) == "testimonial" && $this->uri->segment(3) == "edit") ){ echo "active"; } ?>">
                    <i class="fa fa-comments"></i><span class="title">Testimonial</span>
                </a>
            </li>

            <!--Menu Settings-->
            <?php
            $class_settings_active="";
            if($this->uri->segment(3)=="user_profile" || $this->uri->segment(3)=="change_password" || ($this->uri->segment(2)=="static_pages" && $this->uri->segment(3)=="display") || ($this->uri->segment(2)=="static_pages" && $this->uri->segment(3)=="modify") || $this->uri->segment(3)=="website_settings")
            { $class_settings_active= "active"; }
            ?>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-cogs fa-fw"></i><span class="title">Settings</span> <span class="expand-sign">+</span>
                </a>
                <!--start submenu-->
                <ul>
                    <li><a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/settings/user_profile" class="<?php if($this->uri->segment(3)=="user_profile"){ echo $class_settings_active; } ?>">Admin Profile</a></li>

                    <li><a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/settings/website_settings" class="<?php if($this->uri->segment(3)=="website_settings"){ echo $class_settings_active; } ?>">Website Settings</a></li>

                    <li><a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/settings/change_password" class="<?php if($this->uri->segment(3)=="change_password"){ echo $class_settings_active; } ?>">Change Password</a></li>
                </ul><!--end /submenu-->
            </li><!--end /menu-item-->

        </ul><!--end .main-menu -->
        <!--END MAIN MENU-->
    </div>
</div>
