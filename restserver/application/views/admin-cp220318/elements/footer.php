<!-- BEGIN JAVASCRIPT -->
<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/BootstrapFixed.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/spin.js/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/DemoUIMessages.js"></script>

<!-- DATATABLE JAVASCRIPT
<script src="<?php echo base_url(); ?>assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/DataTables/extras/ColVis/js/ColVis.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/DataTables/extras/TableTools/media/js/TableTools.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/DemoTableDynamic.js"></script>-->

<!-- CHART JAVASCRIPT -->
<script src="<?php echo base_url(); ?>assets/js/libs/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/flot/jquery.flot.time.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/flot/jquery.flot.orderBars.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/jquery-knob/jquery.knob.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/DemoCharts.js"></script>

<!-- JQUERY ALERT -->
<script src="<?php echo base_url(); ?>assets/js/libs/alert/jquery.alerts.js"></script>

<!--
<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/DemoFormComponents.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
-->
<!--<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>-->


<script src="<?php echo base_url(); ?>assets/js/core/App.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/Demo.js"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />

<!-- For Hi chart graph -->
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
<!-- For Hi chart graph -->

<script src="<?php echo base_url(); ?>assets/js/eModal.min.js"></script>

<!-- Bootstrap Select Box : JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>

<script>
$(document).ready(function(){
    
	$('select').selectpicker();	
    /*$('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1,
        pickerPosition: 'bottom-left'
    });
    
    $('.form_time').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    }); */
	
	/*$('.timepicker').timepicker({
		defaultTime: '6:00 PM',
		minuteStep: 15,
		showMeridian: true,
	});*/
	
	/*$('.form_time_only').datetimepicker({
		datepicker:false,
		format:'g:i A',
		formatTime: 'g:i A',
		disabledDates: false,
		step:30
	});*/
		
    
});
</script>

<!-- END JAVASCRIPT -->
