<?php
echo $assets_url = $this->config->item('assets_url');
require('assets/ckeditor/ckeditor.php');
require('assets/ckfinder/ckfinder.php');
$controller_name=$this->uri->segment(2);
?>
<!-- <script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script> -->
	<div id="content">

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Static Pages <small>Edit</small></h3>
        </div>
        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/display"; ?>">Back</a></li> 
            </ul>
        </div>

        <div class="section-body">
            <?php
            echo $this->session->flashdata('msg_data');
            ?>
            <!-- START HORIZONTAL FORM -->
            <div class="row">
                <form name="frm" id="frm" class="form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/modify/".$this->uri->segment(4); ?>" method="post" role="form" novalidate="novalidate">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit <strong>Static Page</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label">Page Title</label>
									<input type="text" name="page_title" class="form-control control-width-large validate[required]" value="<?php echo $rows['page_title']; ?>" placeholder="Enter a page title"> <!-- onblur="script:change_slug_value(this.value)" -->
                                </div>
								
								<div class="form-group">
                                    <label class="control-label seourl-input">SEO URL</label>
									<p class="seo-url-text"><span><?php echo base_url(); ?></span>
                                    <input type="text" name="slug" id="slug" class="form-control control-width-normal validate[required]" value="<?php echo $rows['slug']; ?>" placeholder="" disabled>
									<a href="<?php echo base_url().$rows['slug']; ?>" target="_blank"><i class="fa fa-eye"></i>&nbsp;Page Preview</a>
									</p>
									<small>Slug name is predefined and fixed for all the static pages.</small>
                                </div>
								
								<?php
								if($rows["is_small_content_available"]=="Y")
								{	
								?>	
								<div class="form-group">
                                    <label class="control-label">Short Description</label>
									<textarea name="small_content" cols="30" rows="5" class="form-control"><?php echo $rows['small_content']; ?></textarea>
                                </div>
								<?php 
								} 
								?>
                                <div class="form-group">
                                        <label class="control-label">Page Content</label>
                                        <?php
                                            
                                        $basePathUrl = $assets_url;
                                        $CKEditor = new CKEditor();
                                        $CKEditor->basePath = $assets_url.'/ckeditor/';
                                        $CKEditor->config['height'] = 550;
                                        $CKEditor->config['width'] = '100%';
                                        $CKEditor->config['uiColor'] = "";
										$CKEditor->config['skin'] = "kama";
                                        
                                        //########################################################################//
                                        //################ SET ckfinder PATH FOR IMAGE UPLOAD ####################//
                                        //########################################################################//
                                        
                                        $CKEditor->config['filebrowserBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html";
                                        $CKEditor->config['filebrowserImageBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html?Type=Images";
                                        $CKEditor->config['filebrowserFlashBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html?Type=Flash";
                                        $CKEditor->config['filebrowserUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files";
                                        $CKEditor->config['filebrowserImageUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";
                                        $CKEditor->config['filebrowserFlashUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";
                                        
                                        //########################################################################//
                                        //################ SET ckfinder PATH FOR IMAGE UPLOAD ####################//
                                        //########################################################################//
                                        
                                        $CKEditor->returnOutput = true;
                                        $code = $CKEditor->editor("page_content", $rows['page_content']);
                                        echo $code;
                                        
                                        ?>
                                  
                                </div>
								
								<div class="form-group">
                                    <label class="control-label">Meta Title</label>
									<?php echo form_input(array('name'=>'meta_title', 'value'=>$rows['meta_title'],'class'=>'form-control')); ?>
                                </div>
								
								<div class="form-group">
                                    <label class="control-label">Meta Keywords</label>
									<?php echo form_input(array('name'=>'meta_keywords', 'value'=>$rows['meta_keywords'],'class'=>'form-control')); ?>
                                </div>
								
								<div class="form-group">
                                    <label class="control-label">Meta Description</label>
									<?php echo form_input(array('name'=>'meta_description', 'value'=>$rows['meta_description'],'class'=>'form-control')); ?>
                                </div>
								
								<div class="form-group">
                                    <input type="submit" name="save" value="Save" class="btn save_btn"  />
                                </div>
                                
                            </div>
                            
                        </div>
                    </div><!-- /.col-md-12 -->
                        
                    
                </form>
                </div><!--end .box -->
        </div>
	</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});

</script>
