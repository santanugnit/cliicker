<?php
$controller_name=$this->uri->segment(2);
$usertype = $this->session->userdata('userType');
?>

<div id="content">
        <section>

	<div class="section-header section-6">
		<h3 class="text-standard"><i class="fa fa-fw fa fa-briefcase text-gray-light"></i> Portfolio<small><i class="fa fa-fw fa-angle-right"></i> List</small></h3>
	</div>

	<div class="section-header section-4">
		<ul class="forth-menu">
			<li><a href="<?php echo base_url().$this->config->item('admin_folder_name').'/'.$controller_name.'/add'; ?>"><div class="add-new"></div><i class="fa fa-plus"></i>&nbsp;Add Project</a></li>
		</ul>
		<div class="btn-group">
			<button type="button" class="btn btn-default">Manage Data by CSV</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
			<ul class="dropdown-menu animation-zoom" role="menu" style="text-align: left;">
				<li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/download_csv'; ?>"><i class="fa fa-download"></i>&nbsp; Download to CSV</a></li>
			</ul>
		</div>
	</div>
	
	
<div class="section-body">
	
	<?php
	echo $this->session->flashdata('msg_data');
	?>
			
    <!-- START DATATABLE  -->
    <form name="frmlist" id="frmlist" method="get">
    <div class="row">
        <div class="col-lg-12">
        <div class="box">
                       
        <div class="box-body table-responsive">
            
		<?php
		if(count($records_list))
		{	
			echo $pagination_count_msg;
			echo $pagination_link;
		}
		?>
        <table id="datatable1" class="table table-bordered table-hover">
        <thead>
            <tr>
				<th style="width: 6%">
					<div data-toggle="buttons" class="btn top-checkbox btn-checkbox-gray-inverse">
						<input type="checkbox">
					</div>
					<div class="btn-group" style="text-align:left">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i>
						</button>
						<ul class="dropdown-menu pull-left animation-slide" role="menu">
							<li><a onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name.'/delete/multiple'; ?>')" href="javascript:void(0);">
							<i class="fa fa-times icon-style-danger"></i>&nbsp;Delete</a></li>
							<li><a href="javascript:void(0);" onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/change_status/multiple/Y'; ?>')" ><i class="fa fa-unlock"></i>&nbsp; Active</a></li>
							<li><a href="javascript:void(0);" onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/change_status/multiple/N'; ?>')"><i class="fa fa-lock"></i>&nbsp; Inactive</a></li>
                        </ul>
					</div>
				</th>
				<th style="width: 78%">Project Name</th>
				<th style="width: 5%; text-align: center;">Status</th>
				<th style="width: 7%; text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>             
    <?php     
    if(count($records_list)):
      
        for($i = 0; $i<count($records_list); $i++): 
        ?>  
        <tr class="gradeX">
        <td>
            <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
               <input type="checkbox" name="chk[]" id="chk_<?php echo $i;?>" value="<?php echo $records_list[$i]['id']?>">
            </div>
		</td>
        <td>
		<span style="float:left;width:35%;">
		<?php if($records_list[$i]['featured_image_file']!=""){ ?>
				<div class="list_img_wrapper">
				<?php echo img(array('src'=>'assets/uploaded_files/portfolio_image/medium/'.$records_list[$i]['featured_image_file'])); ?>
				</div>
		<?php }	?>
		<?php if($records_list[$i]['live_url']!=""){ ?>
		<a class="urls" href="<?php echo $records_list[$i]['live_url']; ?>" target="_blank" style="background-color: #737373">Live URL</a><?php } ?>
		<?php if($records_list[$i]['case_study_file']!=""){
            $pdf_link = base_url().'assets/uploaded_files/case_study/'.$records_list[$i]['case_study_file'];
		    ?>
		<a class="urls" href="<?php echo $pdf_link; ?>" target="_blank">Download Case Study</a><?php } ?>
		</span>
		<div style=" width:calc(100% - 40%); float:left;">
			<span style="font-size: 18px;padding-bottom: 5px; border-bottom: 1px solid; margin-bottom: 5px;  color: #ef4e3f; font-weight: 600;"><?php echo $records_list[$i]['project_name']; ?> <span style="color:#212121; font-size:14px;"><?php echo $records_list[$i]['cat_name']; ?></span></span>
			
			<p style="font-size: 14px;padding-top: 0px; font-weight: 400;">
			<?php 
				$desc =strip_tags($records_list[$i]['description']);
				if(strlen($desc) >800)
					echo substr($desc,0,800)."...";
				else
					echo $desc;	
			?>
			</p>
		</div>
		
		</td>
        <td style="text-align:center;">
			<?php
				if($records_list[$i]['status'] == 'Y') 
				{
					$status_mode = '<i class="fa fa-unlock"></i>';
					$actval = 'N';
					$tooltext="Active";
					
				}else{
					$status_mode = '<i class="fa fa-lock"></i>';
					$actval = 'Y';
					$tooltext="Blocked";
				}
			?>
			<a href="javascript:void(0);" onclick="script:list_page_redirect('<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name .'/change_status/single/'.$actval.'/'.$records_list[$i]['id']; ?>', 1);" style="text-decoration: none; cursor: hand;" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $tooltext; ?>">
			<?php echo $status_mode; ?></a>
		
		</td>
		<td style="text-align:center;">
			
			<button onclick="script:list_page_redirect('<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name.'/edit/'.$records_list[$i]['id']; ?>',0)" type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row">
            <i class="fa fa-pencil"></i></button>
            
			<button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row" onclick="script:list_page_redirect('<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name.'/delete/single/'.$records_list[$i]['id']; ?>',1)">
            <i class="fa fa-trash-o"></i></button>
			
        </td>
        </tr>
        <?php 
        endfor;
    else:
    
    ?>
        <tr>
            <td colspan="5" align="center"><h3><small>No records found..</small></h3></td>
        </tr>
    <?php    
    endif;    
    ?>
        </tbody>
	</table>
    		
            <?php
			if(count($records_list))
			{
				echo $pagination_count_msg;
				echo $pagination_link; 
			}
			?>
    
           </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
    </div>
    </form>
    <!-- END DATATABLE 1 -->		
</div>
</section>
</div>
<div class="contactForm_area">
	<a class="icon_area" href="#">Mobile Screen</a>
	<div class="mobile-screen-sec">
		<img src="<?php echo base_url()."assets/images/mobile_screens/portfolio.jpg"; ?>" alt="" width="250" />
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {

    $('.icon_area').click(function(e) {
			e.preventDefault();
			$(this).parent('div').siblings('.contactForm_area').removeClass('tabslideout');
				$( this ).parent('div').toggleClass( "tabslideout" );
				e.stopPropagation();
		});
		
		$(document).click(function(e) {
			if (!$(e.target).is('.contactForm_area, .contactForm_area *')) {
				$('.contactForm_area').removeClass('tabslideout');
			}
		});

	
});
</script>
