<script>
$(function()
{
   jQuery("#form1").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
});
</script>

<style>
    .submit-btn{
        background-color:#316ac5;
    }

    .submit-btn:hover{
       background-color:#006699;
    }
</style>

<div class="box-type-login">
    

        <!--<div class="box-head">
            <h2 class="text-light text-white">Stimulus <strong>Hospitality</strong></h2>
            <h4 class="text-light text-inverse-alt">Password re-generate to your admin panel</h4>
        </div> -->

        <?php
        //echo $error_message;
        ?>

        <div class="box-body">
			<div class="login_logo"><img src="<?php echo base_url().'assets/admin/images/top-logo.png'; ?>" alt=" "/></div>
            <h2 class="text-light">Forgot password?</h2>
            
            <div class="custom-alert row">
                <?php
                    echo $error_message;
                    echo $this->session->flashdata('msg_data');
                ?>
            </div> 
            <?php echo form_open($this->config->item('admin_folder_name').'/login/forgot_password', array('method'=>'post','name'=>'form1', 'id'=>'form1')); ?>
            <div class="form-group text-left">
                    
                <label>
                    Don't worry. Please enter your login email address,
                    we will reset your <br> password and send to your given valid email address.
                </label>
                    
            </div>
            <div class="form-group">
                    <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control validate[required,custom[email]]" name="admin_uname" id="admin_uname"  placeholder="Email">
                    </div>
            </div>
            
            <div class="row">
                   <div class="col-xs-6 text-left">
                            
                            <span>Sign in? <a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/login">Click Here</a></span>
                    </div>
                    <div class="col-xs-6 text-right">
                        <input type="submit" class="btn save_btn" value="Send" name="loginSubmit" id="loginSubmit">
                    </div>
            </div>
            <?php
            echo form_close();
            ?>
            </div><!--end .box-body -->

    
</div>
