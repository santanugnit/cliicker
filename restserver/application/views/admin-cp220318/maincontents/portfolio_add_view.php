<?php
$controller_name=$this->uri->segment(2);
?>
<script>
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function getimage2(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="200">';
                $('#img_prev2').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-briefcase text-gray-light"></i> Portfolio <small><i class="fa fa-fw fa-angle-right"></i> Add</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/list_all"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
			<?php
			echo $msg_data;
			?>
            <!-- START HORIZONTAL FORM -->
			<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/add"; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-head">
                            <header class="header-txt-align">
                                <h4 class="text-light">Add <strong>Project</strong></h4>
                            </header>
                        </div>
                            
                        <div class="box-body">
                            <div class="body-head">
                                <p><span>*</span> fields are mandatory</p>
                            </div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Select a Category <span class="required_class">*</span><small>Select a category</small></label>
								</div>

								<div class="col-md-9">
								   <select name="portfolio_category_id" class="form-control control-width-large validate[required]">
										<option value="">Select a Category</option>
										<?php
										if(count($cat_list))
										{
											foreach($cat_list as $val)
											{
										?>
												<option value="<?php echo $val['id']; ?>" <?php if(isset($_POST['portfolio_category_id']) && ($val['id']==$this->input->post('portfolio_category_id'))){ echo 'selected="selected"'; } ?>><?php echo $val['cat_name']; ?></option>
										<?php		
											}
										}	
										?>
								   </select>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Project Name <span class="required_class">*</span><small>Enter a project name</small></label>
								</div>

								<div class="col-md-9">
								   <input type="text" name="project_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('project_name'); ?>" placeholder="Enter a project name">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Project Description <small>Enter a short description</small></label>
								</div>

								<div class="col-md-9">
								   <textarea name="description" class="form-control control-width-large" placeholder="Enter a short description" rows="15"><?php echo $this->input->post('description'); ?></textarea>
								   
								</div>
							</div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label">Features <small>Enter project Features</small></label>
                                </div>

                                <div class="col-md-9">
                                    <textarea name="features" class="form-control control-width-large" placeholder="Ex: Page & Content Management System, Park Management System, 3rd Party Online Booking Program etc..." rows="5"><?php echo $this->input->post('features'); ?></textarea>
                                    <small>Note: Enter all the features separated with comma (,) separator.<br>Ex: Page & Content Management System, Park Management System, 3rd Party Online Booking Program</small>
                                </div>
                            </div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Live URL <small>Enter a live URL</small></label>
								</div>

								<div class="col-md-9">
								   <input type="text" name="live_url" class="form-control control-width-large" value="<?php echo $this->input->post('live_url'); ?>" placeholder="http://codopoliz.com">
								</div>
							</div>
						
							<div class="form-footer">
								 <div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-9">
										<input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                        <input type="submit" class="btn save_btn" value="Save &amp Review" name="SaveReview">
									</div>
								</div>
							</div>	
                        </div>
						
                    </div>

                </div><!--end .box -->
            
				<div class="col-md-4">
					<!-- ------------------------------------------ -->
					<!-- --------- Upload Image :: Start ---------- -->
					<!-- ------------------------------------------ -->
					<div class="row">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-paperclip"></i> Featured Image</h3>
								<small>Uploaded image will reflected at the potfolio deatils screen</small>
							</div>
							<div class="panel-body">
								<div id="img_prev"></div>
								<input type="file" onchange="getimage(this)" name="featured_image_file" id="featured_image_file" /><br/>
								<small><?php echo $image_recommended_text; ?></small>
							</div>
						</div>	
					</div>
					<!-- ------------------------------------------ -->
					<!-- --------- Upload Image :: End ------------ -->
					<!-- ------------------------------------------ -->

                    <!-- -------------------------------------------- -->
                    <!-- --------- Upload Image 2 :: Start ---------- -->
                    <!-- -------------------------------------------- -->
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-paperclip"></i> Featured Image 2</h3>
                                <small>Uploaded image will reflected at potfolio listing screen</small>
                            </div>
                            <div class="panel-body">
                                <div id="img_prev2"></div>
                                <input type="file" onchange="getimage2(this)" name="featured_image_file_2" id="featured_image_file_2" /><br/>
                                <small><?php echo $image_recommended_text_2; ?></small>
                            </div>
                        </div>
                    </div>
                    <!-- -------------------------------------------- -->
                    <!-- --------- Upload Image 2 :: End ------------ -->
                    <!-- -------------------------------------------- -->

                    <!-- ---------------------------------------------------- -->
                    <!-- --------- Upload Case Study file :: Start ---------- -->
                    <!-- ---------------------------------------------------- -->
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-paperclip"></i> Upload Project Case Study</h3>
                                <small>System supported format: .pdf</small>
                            </div>
                            <div class="panel-body">
                                <input type="file" name="case_study_file" id="case_study_file" /><br/>
                                <small>System supported format: .pdf</small>
                            </div>
                        </div>
                    </div>
                    <!-- ---------------------------------------------------- -->
                    <!-- --------- Upload Case Study file :: End ------------ -->
                    <!-- ---------------------------------------------------- -->

				</div>
			</div><!--end .col-lg-12 -->
			</form>
        </div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






