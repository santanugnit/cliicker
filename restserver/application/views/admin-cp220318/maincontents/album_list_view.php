<?php
$controller_name  = $this->uri->segment(2);
$usertype       = $this->session->userdata('userType');
?>

<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa fa-gear text-gray-light"></i> Album<small><i class="fa fa-fw fa-angle-right"></i> List</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name').'/'.$controller_name.'/add'; ?>"><div class="add-new"></div><i class="fa fa-plus"></i>&nbsp;Add Album</a></li>
            </ul>
            <!--
            <div class="btn-group">
                <button type="button" class="btn btn-default">Manage Data by CSV</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                <ul class="dropdown-menu animation-zoom" role="menu" style="text-align: left;">
                    <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/download_csv'; ?>"><i class="fa fa-download"></i>&nbsp; Download to CSV</a></li>
                </ul>
            </div>
            -->
        </div>


        <div class="section-body">

            <?php echo $this->session->flashdata('msg_data'); ?>

            <!-- START DATATABLE  -->
            <form name="frmlist" id="frmlist" method="get">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">

                            <div class="box-body table-responsive">

                            <?php
                                if(count($records_list))
                                {
                                    echo $pagination_count_msg;
                                    echo $pagination_link;
                                }
                            ?>
                            <table id="datatable1" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 6%">
                                            <div data-toggle="buttons" class="btn top-checkbox btn-checkbox-gray-inverse">
                                                <input type="checkbox">
                                            </div>
                                            <div class="btn-group" style="text-align:left">
                                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-left animation-slide" role="menu">

                                                    <li><a href="javascript:void(0);" onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/change_status/multiple/Y'; ?>')" ><i class="fa fa-times icon-style-danger"></i>&nbsp; Delete</a></li>
                                                    <li><a href="javascript:void(0);" onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/change_status/multiple/N'; ?>')"><i class="fa fa-unlock"></i>&nbsp; Alive</a></li>
                                                </ul>
                                            </div>
                                        </th>
                                        <th style="width: 15%">Name</th>
                                        <th style="width: 30%">Album info</th>
                                        <th style="width: 25%">Payment Info</th>
                                        <th style="width: 10%; text-align: center;">Status</th>
                                        <th style="width: 10%; text-align: center;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                            <?php
                                if(count($records_list))
                                {
                                    for ( $i = 0; $i < count($records_list); $i++ )
                                    {
                            ?>
                                <tr class="gradeX">
                                    <td>
                                        <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
                                            <input type="checkbox" name="chk[]" id="chk_<?php echo $i; ?>" value="<?php echo $records_list[$i]['id'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <span style="float: left;">
                                            <?php echo "#".$records_list[$i]['album_code']; ?>
                                        </span>
                                        <span style="float: left;">
                                            <?php echo $records_list[$i]['album_name']; ?>
                                        </span>
                                        <span style="font-size: 16px; padding-top: 28px;">
                                        <?php echo ( is_array($photographers_list) && array_key_exists($records_list[$i]['photographer_id'], $photographers_list) )? $photographers_list[$records_list[$i]['photographer_id']]['first_name'] .' '.$photographers_list[$records_list[$i]['photographer_id']]['first_name'] :""; ?>
                                        </span>
                                    </td>
                                    <td>
                                        <div class="">
                                            <span><label for="">Domain Name : </label><span><?php echo "http://" . $records_list[$i]['preferred_subdomain_name'] .".cliicker.com"; ?></span></span>
                                            <span><label for="">Intro Text : </label><span><?php echo $records_list[$i]['intro_text']; ?></span></span>
                                            <span><label for="">Created On. :</label><span><?php echo date('d/m/Y', strtotime($records_list[$i]['creation_datetime'])); ?></span></span>
                                            <span><label for="">Album price :</label><span><?php echo $records_list[$i]['album_price']; ?></span></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="">
                                            <span>
                                                <label for="">Max Storage. : </label>
                                                <span><?php echo $records_list[$i]['max_storage_allocate']; ?></span>
                                            </span>
                                            <span>
                                                <label for="">Space Usage : </label>
                                                <span><?php echo $records_list[$i]['space_usage']; ?></span>
                                            </span>
                                            <span>
                                                <label for="">Google drive link : </label>
                                                <span><?php echo ($records_list[$i]['google_drive_link'] != '' )? $records_list[$i]['google_drive_link'] : "N/A"; ?></span>
                                            </span>
                                            <span>
                                                <label for="">Dropbox Link : </label>
                                                <span><?php echo ($records_list[$i]['dropbox_link'] != '' )? $records_list[$i]['dropbox_link'] : "N/A"; ?></span>
                                            </span>
                                        </div>
                                    </td>
                                    <td style="text-align:center;">
                                    <?php
                                        if ( $records_list[$i]['is_deleted'] == 'Y') {
                                            $status_mode = '<i class="fa fa-lock"></i>';
                                            $actval = 'N';
                                            $tooltext = "Deleted";
                                        } else {
                                            $status_mode = '<i class="fa fa-unlock"></i>';
                                            $actval = 'Y';
                                            $tooltext = "Alive";
                                        }
                                    ?>
                                        <a href="javascript:void(0);"
                                           onclick="script:list_page_redirect('<?php echo base_url() . $this->config->item('admin_folder_name') . "/" . $controller_name . '/change_status/single/' . $actval . '/' . $records_list[$i]['id']; ?>', 1);" style="text-decoration: none; cursor: hand;" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $tooltext; ?>"> <?php echo $status_mode; ?></a>
                                    </td>

                                    <td style="text-align:center;">
                                        <button onclick="script:list_page_redirect('<?php echo base_url() . $this->config->item('admin_folder_name') . "/" . $controller_name . '/edit/' . $records_list[$i]['id']; ?>',0)" type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
                                        <button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" onclick="script:list_page_redirect('<?php echo base_url() . $this->config->item('admin_folder_name') . "/" . $controller_name . '/delete/single/' . $records_list[$i]['id']; ?>',1)"><i class="fa fa-trash-o"></i></button>

                                    </td>
                                </tr>
                            <?php
                                    }
                                } else {

                            ?>
                                        <tr>
                                            <td colspan="4" align="center"><h3><small>No records found..</small></h3></td>
                                        </tr>
                            <?php
                            }
                            ?>
                                    </tbody>
                                </table>

                                <?php
                                if(count($records_list))
                                {
                                    echo $pagination_count_msg;
                                    echo $pagination_link;
                                }
                                ?>

                            </div><!--end .box-body -->
                        </div><!--end .box -->
                    </div><!--end .col-lg-12 -->
                </div>
            </form>
            <!-- END DATATABLE 1 -->
        </div>
    </section>
</div>
<div class="contactForm_area">
    <a class="icon_area" href="#">Mobile Screen</a>
    <div class="mobile-screen-sec">
        <img src="<?php echo base_url()."assets/images/mobile_screens/Services.jpg"; ?>" alt="" width="250" />
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {

        $('.icon_area').click(function(e) {
            e.preventDefault();
            $(this).parent('div').siblings('.contactForm_area').removeClass('tabslideout');
            $( this ).parent('div').toggleClass( "tabslideout" );
            e.stopPropagation();
        });

        $(document).click(function(e) {
            if (!$(e.target).is('.contactForm_area, .contactForm_area *')) {
                $('.contactForm_area').removeClass('tabslideout');
            }
        });


    });
</script>
