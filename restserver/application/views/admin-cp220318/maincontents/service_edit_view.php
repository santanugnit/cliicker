<?php
$controller_name=$this->uri->segment(2);
$row_id=$this->uri->segment(4);
?>
<script>
    $(document).ready(function () {

        /*********** Delete the existing profile image ***********/
        $('#delete_img').bind('click',delete_featured_image);

        function delete_featured_image()
        {
            var existingFileName = $('#existing_featured_image').val();

            if(existingFileName!="")
            {
                jConfirm('Are you want to delete the feature image?', 'Confirmation', function(r){
                    if(r)
                    {
                        var targetUrl = '<?php echo base_url().$this->config->item('admin_folder_name').'/'.$controller_name."/ajax_delete_featured_image"; ?>';
                        $.ajax({
                            url: targetUrl,
                            type:"POST",
                            dataType:"json",
                            data:{rowId:<?php echo $row_id; ?>, fileName:existingFileName},
                            success:function(data)
                            {
                                if(data.result=="success")
                                {
                                    $("#delete_profile_img").hide(); //hide the delete button
                                    $('#img_prev').html('');
                                    $('#existing_featured_image').val('');
                                }
                            }
                        });
                    }
                    else
                    {
                        return false;
                    }
                });
            }
            else
            {
                return false;
            }
        }
    });
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="120" height="120">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-gear text-gray-light"></i> Services<small> <i class="fa fa-fw fa-angle-right"></i> Edit</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/list_all"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
			<?php
			echo $this->session->flashdata('msg_data');
			echo $msg_data;
			?>
            <!-- START HORIZONTAL FORM -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">

                        <div class="box-head">
                            <header class="header-txt-align">
                                <h4 class="text-light">Edit <strong>Service</strong></h4>
                            </header>
                        </div>
						
						<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/edit/".$row_id; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
						<input type="hidden" name="existing_featured_image" id="existing_featured_image" value="<?php echo $record_dtls['featured_image_file']; ?>">
                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Service Name <span class="required_class">*</span><small>Enter a service name</small></label>
                                    </div>
    
                                    <div class="col-md-9">
                                       <input type="text" name="service_name" class="form-control control-width-large validate[required]" value="<?php echo $record_dtls['service_name']; ?>" placeholder="Enter a service name">
                                    </div>
                                </div>
								
								<div class="form-group">
									<div class="col-md-3">
										<label class="control-label">Service Icon <small>Select a category icon</small></label>
									</div>

									<div class="col-md-9">
                                        <div id="img_prev">
                                            <?php
                                            if($record_dtls['featured_image_file']!="")
                                            {
                                            ?>
                                                <div class="">
                                                    <?php
                                                    echo img(array('id'=>'cat_img','src'=>'assets/uploaded_files/other_image/medium/'.$record_dtls['featured_image_file']));
                                                    ?><br>
                                                    <span id="delete_profile_img">
													 <button id="delete_img" class="btn btn-rounded btn-default" type="button">Delete Feature Image</button>
													   </span>

												</div>

											<?php
											}
											?>
                                        </div>
										<input type="file" onchange="getimage(this)" name="featured_image_file" id="featured_image_file" />
                                        <small><?php echo $image_recommended_text; ?></small>
									</div>
								</div>
                            
                                <div class="form-footer">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Stay" name="SaveStay">
                                        </div>
                                    </div>
						        </div>
                            				
                        </div>
						
						
						</form>	
                    </div>

                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






