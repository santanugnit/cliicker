<?php
$controller_name=$this->uri->segment(2);
?>
<script>

$(document).ready(function(){
    
    /*********** Delete the existing profile image ***********/
    $('#delete_img').bind('click',delete_profile_image);
    
    function delete_profile_image()
    {  
        var existingFileName = $('#existing_profile_image').val();
        var userId = 1;
        
        if(existingFileName!="")
        {    
            jConfirm('Are you want to delete the profile image?', 'Confirmation', function(r){
                if(r) 
                {
                    var adminurl= $('#siteadminurl').val();
                    var controllername = $('#controller_name').val();

                    $.ajax({

                            url:adminurl+controllername+"/ajax_delete_profile_image",
                            type:"POST",
                            dataType:"json",
                            data:{uId:userId, fileName:existingFileName},
                            success:function(data)
                            {
                                if(data.result=="success")
                                {    
                                    $("#delete_profile_img").hide(); //hide the delete button
                                    //$('#profile_img').attr('src','<?php echo base_url(); ?>assets/admin/images/no-image.jpg');
                                    $('#existing_profile_image').val('');
                                }
                            }
                         });
                }
                else
                {  
                    return false;
                }
            });
        }
        else
        {    
            return false;
        }
    }
    
    /*********************************************************/
});    
    
function getimage(input)
{  
     if (input.files && input.files[0]) 
     {
        var reader = new FileReader();
        reader.onload = function (e) {
          var strHtml = '<img src="'+e.target.result+'" width="100" height="100">'; 
          $('#img_prev').html(strHtml);  

        };

        reader.readAsDataURL(input.files[0]);
     }
}

</script>

<div id="content">
        <section>

	<div class="section-header section-12">
		<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Settings <small><i class="fa fa-fw fa-angle-right"></i> Manage Profile</small></h3>
	</div>
	
<div class="section-body">
	
	<?php
	echo $this->session->flashdata('msg_data');
	?>
    <?php
    echo $msg_data;
    ?>
			
    <!-- START DATATABLE  -->
    <div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-head">
                        <header><h4 class="text-light"> Manage <strong>Profile</strong></h4></header>
					</div>
					<div class="box-body">
                        
					<form name="frm" id="frm" method="post" accept-charset="utf-8" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/user_profile"; ?>" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" name="controller_name" id="controller_name"  value="<?php echo $controller_name; ?>" />
                    <input type="hidden" name="siteadminurl" id="siteadminurl" value="<?php echo base_url().$this->config->item('admin_folder_name')."/"; ?>" />
                    <input type="hidden" name="existing_profile_image" id="existing_profile_image" value="<?php echo $user_profile_data['profile_image']; ?>">
						
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">First Name</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="first_name" class="form-control control-width-large validate[required]" value="<?php echo $user_profile_data['first_name']; ?>" placeholder="Enter the first name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Last Name</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="last_name" class="form-control control-width-large validate[required]" value="<?php echo $user_profile_data['last_name']; ?>" placeholder="Enter the last name">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Login Email</label>
                            </div>

                            <div class="col-md-10">
                               <?php echo $user_profile_data['email']; ?>
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Contact Email</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="contact_email" class="form-control control-width-large validate[required,custom[email]]" value="<?php echo $user_profile_data['contact_email']; ?>" placeholder="Enter the site contact email">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Address</label>
                            </div>

                            <div class="col-md-10">
                                <textarea name="address" class="form-control control-width-large validate[required]" placeholder="Example: Studio 103, The Business Centre, 61 Wellfield Road"><?php echo $user_profile_data['address']; ?></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Contact Number</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="phone" class="form-control control-width-large validate[required]" value="<?php echo $user_profile_data['phone']; ?>" placeholder="Enter the contact number">
                            </div>
                        </div>
                        
                        <?php
                        if($user_profile_data['profile_image']!="")
                        {    
                        ?>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Existing Profile Image</label>
                            </div>

                            <div class="col-md-10">
                               <?php 
                                   echo img(array('id'=>'profile_img','src'=>'assets/uploaded_files/user_profile_image/medium/'.$user_profile_data['profile_image']));
                               ?><br>
                               <span id="delete_profile_img">
                               <button id="delete_img" class="btn btn-rounded btn-default" type="button">Delete Profile Image</button> 
                               </span>    

                            </div>
                        </div>
                        <?php
                        }
                        ?>
                                
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Profile Image</label>
                            </div>

                            <div class="col-md-10">
                               <input type="file" onchange="getimage(this)" name="profile_image_file" class="form-control control-width-normal"><br/>
                               <div id="img_prev"></div>
                            </div>
                        </div>
                        
                        <div class="form-footer">
                             <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="submit" class="btn save_btn" value="Save" name="Submit">
                                </div>
                            </div>
                        </div>
						
						
                    </form>
                    </div>
    		
                </div>
        </div>
    
    
    </div>
    
    
    
</div>
</section>
</div>

<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

