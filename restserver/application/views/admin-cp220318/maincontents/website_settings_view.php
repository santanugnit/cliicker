<?php
$controller_name=$this->uri->segment(2);
?>
<div id="content">
        <section>

	<div class="section-header section-12">
		<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Settings <small><i class="fa fa-fw fa-angle-right"></i> Website Settings</small></h3>
	</div>
	
<div class="section-body">
	
	<?php
	echo $this->session->flashdata('msg_data');
	?>
    <?php
    echo $msg_data;
    ?>
			
    <!-- START DATATABLE  -->
    <div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-head">
                        <header><h4 class="text-light"> Manage <strong>Website</strong></h4></header>
					</div>
					<div class="box-body">
                        
					<form name="frm" id="frm" method="post" accept-charset="utf-8" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/website_settings"; ?>" class="form-horizontal">
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Website Name</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="website_name" class="form-control control-width-large validate[required]" value="<?php echo $web_settings_data['website_name']; ?>" placeholder="Enter the website name">
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Website URL</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="website_url" class="form-control control-width-large" value="<?php echo $web_settings_data['website_url']; ?>" placeholder="Enter the website URL">
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Website Address<small>The site address will show at the contact us page.</small></label>
                            </div>

                            <div class="col-md-10">
                                <textarea name="wesite_address" class="form-control control-width-large validate[required]" placeholder="Example: Studio 103, The Business Centre, 61 Wellfield Road"><?php echo $web_settings_data['wesite_address']; ?></textarea>
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Address Map Details
								<small>In order to show google map at the contact us page, please enter valid latitude and longitude at the company address.</small></label>
                            </div>

                            <div class="col-md-10">
							   <label class="control-label"><small>Latitude</small></label>
                               <input type="text" name="address_latitude" class="form-control control-width-small validate[required]" value="<?php echo $web_settings_data['address_latitude']; ?>" placeholder="Enter the latitude" style="text-align:center;">
							   
							   <label class="control-label"><small>Longitude</small></label>
							   <input type="text" name="address_longitude" class="form-control control-width-small validate[required]" value="<?php echo $web_settings_data['address_longitude']; ?>" placeholder="Enter the longitude" style="text-align:center;">
							   
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Contact Email<small>All the site mails will received through this  email address.So please set a valid email address.</small></label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="site_contact_email" class="form-control control-width-large validate[required,custom[email]]" value="<?php echo $web_settings_data['site_contact_email']; ?>" placeholder="Enter the site contact email">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Site Contact Number</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="site_contact_number" class="form-control control-width-large validate[required]" value="<?php echo $web_settings_data['site_contact_number']; ?>" placeholder="Enter the contact number">
                            </div>
                        </div>
						
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Facebook Link</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="fb_link" class="form-control control-width-large" value="<?php echo $web_settings_data['fb_link']; ?>" placeholder="Enter the facebook link. For Ex, 'https://www.facebook.com/codopoliz'">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Twitter Link</label>
                            </div>

                            <div class="col-md-10">
                               <input type="text" name="twitter_link" class="form-control control-width-large" value="<?php echo $web_settings_data['twitter_link']; ?>" placeholder="Enter the twitter link, For Ex, 'https://twitter.com/Codopoliz_GenX'">
                            </div>
                        </div>
                        
                        <div class="form-footer">
                             <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="submit" class="btn save_btn" value="Save" name="Submit">
                                </div>
                            </div>
                        </div>
						
						
                    </form>
                    </div>
    		
                </div>
        </div>
    
    
    </div>
    
    
    
</div>
</section>
</div>

<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

