<?php
    $controller_name    = $this->uri->segment(2);
    $row_id             = $this->uri->segment(4);
?>
<style>
    .ajax-success-msg{display: block; color: #008000;}
    .ajax-error-msg{display: block; color: #FF1D49;}

    .file_upload_details .dl-horizontal dt{ width: 105px; }
    .file_upload_details .dl-horizontal dd { margin-left: 120px; word-wrap: break-word; }

</style>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-globe text-gray-light"></i> Album <small> <i class="fa fa-fw fa-angle-right"></i> Edit</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/album_list"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">

            <?php echo $this->session->flashdata('msg_data'); echo $msg_data; ?>
            <!-- START HORIZONTAL FORM -->
            <form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/edit/".$row_id; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">

                <div class="row">
                    <div class="col-lg-8">
                        <div class="box">

                            <div class="box-head">
                                <header class="header-txt-align">
                                    <h4 class="text-light">Edit <strong> Album</strong></h4>
                                </header>
                            </div>

                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Photographer Name <small></small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <h4>
                                            <a href="javascript:;"><?php echo (is_array($photographers_list) && array_key_exists($record_dtls['photographer_id'], $photographers_list)) ? ($photographers_list[$record_dtls['photographer_id']]["first_name"].' '.$photographers_list[$record_dtls['photographer_id']]["last_name"] . '<small> &nbsp; (#'.$photographers_list[$record_dtls['photographer_id']]["pg_clicker_code"].')</small>') :"N/A";  ?></a>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album Name <span class="required_class"> *</span><small>Enter a album name</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="album_name" class="form-control control-width-large validate[required]" value="<?php echo $record_dtls['album_name']; ?>" placeholder="Enter a album name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album Description <small>Enter a short description</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="short_description" class="form-control control-width-large" placeholder="Enter a short description" rows="5"><?php echo $record_dtls['short_description']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Introduction text <small>Album short introduction text.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="intro_text" class="form-control control-width-large" value="<?php echo $record_dtls['intro_text']; ?>" placeholder="Album short introduction text..">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Subdomain name <span class="required_class">*</span> <small>Preferred subdomain name</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="preferred_subdomain_name" name="preferred_subdomain_name" class="form-control control-width-medium validate[required]" value="<?php echo $record_dtls['preferred_subdomain_name']; ?>" placeholder="" data-previous_value="<?php echo $record_dtls['preferred_subdomain_name']; ?>" style="display: inline-block;"/> <span class="form-control-static" style="display: inline-block;	color: gray; letter-spacing: 1px;	padding-left: 5px; ">.cliicker.com</span> <span class="text_msg" style="display: none;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album price<span class="required_class">*</span><small>Enter price for album creation</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="album_price" class="form-control control-width-small validate[required]" value="<?php echo $record_dtls['album_price']; ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Max storage<small>Max storage are in settings section.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="max_storage_allocate" class="form-control control-width-small" value="<?php echo $record_dtls['max_storage_allocate']; ?>" placeholder="" /><p class="help-block">Please enter storage space in bytes i.e. (1gb = 1024mb = 1024Kb = 1024byte) = (1073741824 bytes = 1024 X 1024 X 1024)</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Google drive link<small>Google drive link</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="google_drive_link" class="form-control control-width-large" value="<?php echo $record_dtls['google_drive_link']; ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Dropbox link<small>Dropbox link of a files.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="dropbox_link" class="form-control control-width-large" value="<?php echo $record_dtls['dropbox_link']; ?>" placeholder="" />
                                    </div>
                                </div>


                                <div class="form-footer">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp; Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp; Stay" name="SaveStay">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div><!--end .box -->

                    <div class="col-md-4">
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: Start ---------- -->
                        <!-- ------------------------------------------ -->
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paperclip"></i> Upload .zip file</h3>
                                    <small>Uploaded image .zip </small>
                                </div>
                                <div class="panel-body">
                        <?php
                            $uploadFiles = array(); $uploadFileSize = 0; $uploadFileName = array();
                            if ( trim($record_dtls['upload_file_data']) != '' )
                            {
                                $upload_file_data = explode('||', $record_dtls['upload_file_data']);
                                if ( is_array($upload_file_data) && !empty($upload_file_data) )
                                {
                                    foreach ( $upload_file_data as $index => $files_dtls )
                                    {
                                        $files_dtlsArr = explode('@@', $files_dtls);
                                        if ( is_array($files_dtlsArr) && !empty($files_dtlsArr) )
                                        {
                                            $uploadFileSize += $files_dtlsArr[2];
                                            $uploadFileName[] = $files_dtlsArr[1];
                                            array_push($uploadFiles,
                                                array(
                                                    'id' => $files_dtlsArr[0],
                                                    'f_name' => $files_dtlsArr[1],
                                                    'f_size' => $files_dtlsArr[2]
                                                )
                                            );
                                        }
                                    }
                                }
                            }
                        ?>
                                    <div class="file_upload_details">
                                        <dl class="dl-horizontal">
                                            <dt>Upload Files : </dt>
                                            <dd><span class="btn btn-primary btn-xs"><?php echo ( !empty($uploadFileName) )? implode(' </span><span class="btn btn-primary btn-xs"> ', $uploadFileName): ""; ?></span></dd>
                                            <dt>Space usage : </dt>
                                            <dd><span><?php echo round(($uploadFileSize/(1024*1024))); ?>MB</span></dd>
                                            <dt>Remaining space : </dt>
                                            <dd><span><?php echo round( ($record_dtls['max_storage_allocate'] - $uploadFileSize)/(1024*1024) ); ?>MB</span></dd>
                                        </dl>
                                    </div>

                                    <hr>
                                </div>

                                <div class="panel-body">
                                    <div id="img_prev"></div>
                                    <div id="container">
                                        <div class="col-lg-6"><button type="button" class="btn btn-rounded btn-primary btn-support1 btn-block" id="pickfiles">Select file</button></div>
                                        <div class="col-lg-6"><button type="button" class="btn btn-rounded btn-primary btn-support2 btn-block" id="uploadfiles">Upload file</button></div>
                                        <!--<a id="pickfiles" href="javascript:;">[Select files]</a>
                                        <a id="uploadfiles" href="javascript:;">[Upload files]</a>-->
                                    </div>
                                    <div id="log"> </div>
                                    <input type="hidden" id="upload_files_ids" name="upload_files_ids" value="" />


                                </div>
                            </div>
                        </div>
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: End ------------ -->
                        <!-- ------------------------------------------ -->
                    </div>

                </div><!--end .col-lg-12 -->
            </form>
        </div><!--end .row -->
        <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
<script src="<?php echo base_url(); ?>assets/js/plupload/plupload.full.min.js"></script>
<script>
    jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});

    $(document).ready( function() {

        $("#preferred_subdomain_name").on( "focusout", function(e){
            var _currElem = $(this);
            var previous_value  = _currElem.attr('data-previous_value');
            if ( previous_value.trim() != '' && previous_value == _currElem.val() )
            {
                return false;
            }
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-error-msg");
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-success-msg");
            var preferred_subdomain_name = $(this).val();

            if( preferred_subdomain_name.trim() == '' ) {
                _currElem.attr('data-previous_value', "");
                return false;
            }

            $.ajax({
                url		: '<?php echo base_url() . $this->config->item("admin_folder_name") . "/" . $controller_name . "/ajax_check_unique_subdomain_name" ?>',
                type	: 'POST',
                data	: { 'preferred_subdomain_name' : preferred_subdomain_name, 'alb_id': '<?php echo $row_id; ?>', 'operation_mode': "edit"  },
                dataType: 'html',
                complete: function(){

                },
                success	: function(data) {
                    var result = $.parseJSON(data);
                    if(result.status){
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-error-msg").addClass("ajax-success-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    } else {
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-success-msg").addClass("ajax-error-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    }
                },
                error: function(error) {
                    alert(error);
                }
            });
        });


    });


    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('container'), // ... or DOM Element itself
        url : '<?php echo base_url().$this->config->item("admin_folder_name")."/".$controller_name."/ajax_plupload"; ?>',
        flash_swf_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.swf',
        silverlight_xap_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.xap',
        multi_selection: false,
        filters : {
            max_file_size : '<?php echo round( ($record_dtls['max_storage_allocate'] - $uploadFileSize)/(1024*1024) ); ?>mb', //1024mb
            mime_types: [
                /*{title : "Image files", extensions : "jpg,gif,png"},*/
                {title : "Zip files", extensions : "zip"}
            ],
            prevent_duplicates: true
        },

        multipart_params : { "pg_code" : <?php echo (is_array($photographers_list) && array_key_exists($record_dtls['photographer_id'], $photographers_list)) ? ($photographers_list[$record_dtls['photographer_id']]["pg_clicker_code"]) :"N/A";  ?>, },

        init: {
            PostInit: function() {
                document.getElementById('img_prev').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },

            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    document.getElementById('img_prev').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                });
            },

            UploadProgress: function(up, file) {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                var upload_files_ids    = [];
                var upload_old_ids      = $('#upload_files_ids').val();
                if ( $('#upload_files_ids').val().trim() != '' ) {
                    upload_files_ids.push(upload_old_ids);
                }

                var data = $.parseJSON(info.response);
                $.each( data.file_name, function( index, value ){
                    //console.log(index+' -- '+value);
                    upload_files_ids.push(index);
                });
                $("#upload_files_ids").val(upload_files_ids.join(","));
            },
            Error: function(up, err) {
                $('#log').html(document.createTextNode("\nError #" + err.code + ": " + err.message));
                //document.getElementById('log').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
        }
    });

    uploader.init();



</script>

