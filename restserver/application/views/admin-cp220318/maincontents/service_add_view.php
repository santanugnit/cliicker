<?php
$controller_name=$this->uri->segment(2);
?>
<script>
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="120" height="120">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-gear text-gray-light"></i> Services <small><i class="fa fa-fw fa-angle-right"></i> Add</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/list_all"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
			<?php
			echo $msg_data;
			?>
            <!-- START HORIZONTAL FORM -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">

                        <div class="box-head">
                            <header class="header-txt-align">
                                <h4 class="text-light">Add <strong>Service</strong></h4>
                            </header>
                        </div>
						
						<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/add"; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
                            
                        <div class="box-body">
                            <div class="body-head">
                                <p><span>*</span> fields are mandatory</p>
                            </div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Service Name <span class="required_class">*</span><small>Enter name of a service</small></label>
								</div>

								<div class="col-md-9">
								   <input type="text" name="service_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('service_name'); ?>" placeholder="Enter a category name">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label">Service Icon <small>Upload a service icon</small></label>
								</div>

								<div class="col-md-9">
								   <div id="img_prev"></div>
									<input type="file" onchange="getimage(this)" name="featured_image_file" id="featured_image_file" /><br/>
									<small><?php echo $image_recommended_text; ?></small>
								</div>
							</div>
						
							<div class="form-footer">
								 <div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-9">
										<input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                        <input type="submit" class="btn save_btn" value="Save &amp Review" name="SaveReview">
									</div>
								</div>
							</div>	
                        </div>
						
						
						</form>	
                    </div>

                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






