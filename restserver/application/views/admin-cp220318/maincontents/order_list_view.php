<?php
$controller_name=$this->uri->segment(2);
$usertype = $this->session->userdata('userType');
?>

<div id="content">
        <section>

	<div class="section-header section-6">
		<h3 class="text-standard"><i class="fa fa-fw fa fa-phone text-gray-light"></i> Orders<small><i class="fa fa-fw fa-angle-right"></i> List</small></h3>
	</div>

	<div class="section-header section-4">
		<div class="btn-group">
			<button type="button" class="btn btn-default">Manage Data by CSV</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
			<ul class="dropdown-menu animation-zoom" role="menu" style="text-align: left;">
				<li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/". $controller_name.'/download_csv'; ?>"><i class="fa fa-download"></i>&nbsp; Download to CSV</a></li>
			</ul>
		</div>
	</div>
	
	
<div class="section-body">
	
	<?php
	echo $this->session->flashdata('msg_data');
	?>
			
    <!-- START DATATABLE  -->
    <form name="frmlist" id="frmlist" method="get">
    <div class="row">
        <div class="col-lg-12">
        <div class="box">
                       
        <div class="box-body table-responsive">
            
		<?php
		if(count($records_list))
		{	
			echo $pagination_count_msg;
			echo $pagination_link;
		}
		?>
        <table id="datatable1" class="table table-bordered table-hover">
        <thead>
            <tr>
				<th style="width: 6%">
					<div data-toggle="buttons" class="btn top-checkbox btn-checkbox-gray-inverse">
						<input type="checkbox">
					</div>
					<div class="btn-group" style="text-align:left">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i>
						</button>
						<ul class="dropdown-menu pull-left animation-slide" role="menu">
							<li><a onclick="script:action_on_selected_items('<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name.'/delete/multiple'; ?>')" href="javascript:void(0);">
							<i class="fa fa-times icon-style-danger"></i>&nbsp;Delete</a></li>
                        </ul>
					</div>
				</th>
				<th style="width: 20%">Contact Person's Name</th>
				<th style="width: 20%">Services</th>
				<th style="width: 30%">Requirements</th>
				<th style="width: 15%">Post Date</th>
				<th style="width: 5%; text-align: center;">Action</th>
            </tr>
        </thead>
        <tbody>             
    <?php     
    if(count($records_list)):
      
        for($i = 0; $i<count($records_list); $i++): 
        ?>  
        <tr class="gradeX">
        <td>
            <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
               <input type="checkbox" name="chk[]" id="chk_<?php echo $i;?>" value="<?php echo $records_list[$i]['id']?>">
            </div>
		</td>
        <td>
            <p><?php echo '<i class="fa fa-user"></i>&nbsp;&nbsp;'.$records_list[$i]['name']; ?></p>
            <p><?php echo '<i class="fa fa-envelope"></i>&nbsp;&nbsp;'.$records_list[$i]['email']; ?></p>
            <p><?php echo '<i class="fa fa-mobile"></i>&nbsp;&nbsp;'.$records_list[$i]['contact_no']; ?></p>
        </td>
        <td>
                <?php
                    echo '<p>'.$records_list[$i]['services'].'</p>';
                    if($records_list[$i]['attached_file']!="")
                    {
                        $file_link= base_url()."assets/uploaded_files/order_attachment_files/".$records_list[$i]['attached_file'];
                    ?>
                        <p><a href="<?php echo $file_link; ?>" target="_blank"><i class="fa fa fa-paperclip"></i>&nbsp; View Attached file</a></p>
                    <?php
                    }
                ?>
        </td>
        <td><?php echo $records_list[$i]['requirement']; ?></td>
        <td><?php echo $records_list[$i]['formatted_datetime']; ?></td>
		<td style="text-align:center;">
            
			<button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row" onclick="script:list_page_redirect('<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name.'/delete/single/'.$records_list[$i]['id']; ?>',1)">
            <i class="fa fa-trash-o"></i></button>
			
        </td>
        </tr>
        <?php 
        endfor;
    else:
    
    ?>
        <tr>
            <td colspan="4" align="center"><h3><small>No records found..</small></h3></td>
        </tr>
    <?php    
    endif;    
    ?>
        </tbody>
	</table>
    		
            <?php
			if(count($records_list))
			{
				echo $pagination_count_msg;
				echo $pagination_link; 
			}
			?>
    
           </div><!--end .box-body -->
        </div><!--end .box -->
    </div><!--end .col-lg-12 -->
    </div>
    </form>
    <!-- END DATATABLE 1 -->		
</div>
</section>
</div>
<div class="contactForm_area">
	<a class="icon_area" href="#">Mobile Screen</a>
	<div class="mobile-screen-sec">
		<img src="<?php echo base_url()."assets/images/mobile_screens/order now.jpg"; ?>" alt="" width="250" />
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {

    $('.icon_area').click(function(e) {
			e.preventDefault();
			$(this).parent('div').siblings('.contactForm_area').removeClass('tabslideout');
				$( this ).parent('div').toggleClass( "tabslideout" );
				e.stopPropagation();
		});
		
		$(document).click(function(e) {
			if (!$(e.target).is('.contactForm_area, .contactForm_area *')) {
				$('.contactForm_area').removeClass('tabslideout');
			}
		});

	
});
</script>
