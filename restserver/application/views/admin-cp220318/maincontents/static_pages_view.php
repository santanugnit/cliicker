<?php
$controller_name=$this->uri->segment(2);
?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery-1.5.min.js"></script>
    <div id="content">
        <section>

            <div class="section-header section-12">
                <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Static Pages <small><i class="fa fa-fw fa-angle-right"></i> List</small></h3>
            </div>

            <div class="section-body">

			<?php
			echo $this->session->flashdata('msg_data');
			?>
			
			<!-- START DATATABLE  -->
				<div class="row">
					<div class="col-lg-12">
						<div class="box">
							<div class="box-body staticmenu">
							<h4 class="text-light">Static Pages</h4>
								<?php echo $category_tree_view; ?>
							</div>
							<!--end .box-body -->
						</div>
						<!--end .box -->
					</div>
					<!--end .col-lg-12 -->
				</div>
				
					<!-- END DATATABLE 1 -->
            </div>
        </section>
    </div>
