<?php
    $controller_name = $this->uri->segment(2);
?>
<style>
    /*[data-toggle="buttons"] > input[type="checkbox"], [data-toggle="buttons"] > label > input[type="checkbox"] {
        display: none;
    }*/
    .show_contact_span, .show_email_span{ padding-left: 10px; cursor: pointer;}
</style>

<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-briefcase text-gray-light"></i> Testimonial <small><i class="fa fa-fw fa-angle-right"></i> Add</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/testimonial_list"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">

            <?php echo $this->session->flashdata('msg_data'); ?>
            <!-- START HORIZONTAL FORM -->
            <form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/add"; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="box">
                            <div class="box-head">
                                <header class="header-txt-align">
                                    <h4 class="text-light">Add <strong> Testimonial</strong></h4>
                                </header>
                            </div>

                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Client name <span class="required_class"> *</span><small>Enter a client.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="client_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('client_name'); ?>" placeholder="Enter a client name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Client email <span class="required_class"> *</span><small>Enter client email id.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="client_email" class="form-control control-width-normal validate[required]" value="<?php echo $this->input->post('client_email'); ?>" placeholder="Email id" style="display: inline-block;">
                                        <div data-toggle="buttons">
                                            <label for="show_email" class="btn checkbox-inline btn-checkbox-success" style="display: inline-block; padding-left: 5px;" ><input type="checkbox" name="show_email" id="show_email"  value="Y" /><span class="show_email_span">Do you want to display email with testimonial? </span></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Contact Number <span class="required_class"> *</span><small>Enter contact no..</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="contact_number" class="form-control control-width-normal validate[required]" value="<?php echo $this->input->post('contact_number'); ?>" placeholder="Contact number" style="display: inline-block;">
                                        <div data-toggle="buttons">
                                            <label for="show_contact" class="btn checkbox-inline btn-checkbox-success" style="display: inline-block; padding-left: 5px;" ><input type="checkbox" name="show_contact" id="show_contact"  value="Y" /><span class="show_contact_span">Do you want to display contact number with testimonial? </span></label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Web-Site URL <small>Web-site url</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="website_url" class="form-control control-width-large" value="<?php echo $this->input->post('website_url'); ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Testimonial Content<small></small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="testimonial_text" class="form-control control-width-large" placeholder="Enter a short description" rows="5"><?php echo $this->input->post('testimonial_text'); ?></textarea>
                                    </div>
                                </div>

                                <div class="form-footer">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Review" name="SaveReview">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div><!--end .box -->

                    <div class="col-md-4">
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: Start ---------- -->
                        <!-- ------------------------------------------ -->
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paperclip"></i> Featured Image</h3>
                                    <small>Uploaded image will reflected at the testimonial section.</small>
                                </div>
                                <div class="panel-body">
                                    <div id="img_prev"></div>
                                    <input type="file" onchange="getimage(this)" name="featured_image" id="featured_image" /><br/>
                                    <small><?php echo $image_recommended_text; ?></small>
                                </div>
                            </div>
                        </div>
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: End ------------ -->
                        <!-- ------------------------------------------ -->
                    </div>

                </div><!--end .col-lg-12 -->
            </form>
        </div><!--end .row -->
        <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->

<script>
    jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});

    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>








