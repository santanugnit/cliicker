<?php
    $controller_name    = $this->uri->segment(2);
    $row_id             = $this->uri->segment(4);
?>
<script>
    $(document).ready(function () {
        $('#delete_img').bind('click',delete_featured_image); //Delete existing featured image

        //******************************************************************//
        //************ Delete file for uploaded featured image *************//
        function delete_featured_image()
        {
            var existingFileName = $('#existing_featured_image').val();

            if( existingFileName != "" )
            {
                jConfirm('Are you want to delete the feature image?', 'Confirmation', function(r){
                    if(r)
                    {
                        var targetUrl = '<?php echo base_url().$this->config->item('admin_folder_name').'/'.$controller_name."/ajax_delete_featured_image"; ?>';
                        $.ajax({
                            url:    targetUrl,
                            type:   "POST",
                            dataType:   "json",
                            data:   {rowId: <?php echo $row_id; ?>, fileName: existingFileName},
                            success:    function(data)
                            {
                                if ( data.result == "success" )
                                {
                                    $("#delete_profile_img").hide(); //hide the delete button
                                    $('#img_prev').html('');
                                    $('#existing_featured_image').val('');
                                }
                            }
                        });
                    }
                    else
                    {
                        return false;
                    }
                });
            }
            else
            {
                return false;
            }
        }
        //******************************************************************//
        //******************************************************************//

    });

    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-briefcase text-gray-light"></i> Testimonial <small> <i class="fa fa-fw fa-angle-right"></i> Edit</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/testimonial_list"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
			<?php
			    echo $this->session->flashdata('msg_data');
			    echo $msg_data;
			?>
            <!-- START HORIZONTAL FORM -->
			<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/edit/".$row_id; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
			<input type="hidden" name="existing_featured_image" id="existing_featured_image" value="<?php echo $record_dtls['featured_image']; ?>" />

            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-head">
                            <header class="header-txt-align">
                                <h4 class="text-light">Edit <strong> Testimonial</strong></h4>
                            </header>
                        </div>
                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Client name <span class="required_class"> *</span><small>Enter a client.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="client_name" class="form-control control-width-large validate[required]" value="<?php echo $record_dtls['client_name']; ?>" placeholder="Enter a client name" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Client email <span class="required_class"> *</span><small>Enter client email id.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="client_email" class="form-control control-width-normal validate[required]" value="<?php echo $record_dtls['client_email']; ?>" placeholder="Email id" style="display: inline-block;">
                                        <div data-toggle="buttons">
                                            <label for="show_email" class="btn checkbox-inline btn-checkbox-success <?php echo ( $record_dtls['show_email'] == "Y" )? 'active' : ""; ?>" style="display: inline-block; padding-left: 5px;" ><input type="checkbox" name="show_email" id="show_email"  value="Y" <?php echo ( $record_dtls['show_email'] == "Y" )? "checked" : ""; ?> /><span class="show_email_span">Do you want to display email with testimonial? </span></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Contact Number <span class="required_class"> *</span><small>Enter contact no..</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="contact_number" class="form-control control-width-normal validate[required]" value="<?php echo $record_dtls['contact_number']; ?>" placeholder="Contact number" style="display: inline-block;">
                                        <div data-toggle="buttons">
                                            <label for="show_contact" class="btn checkbox-inline btn-checkbox-success <?php echo ( $record_dtls['show_contact'] == "Y" )? 'active' : ""; ?>" style="display: inline-block; padding-left: 5px;" ><input type="checkbox" name="show_contact" id="show_contact"  value="Y" <?php echo ( $record_dtls['show_contact'] == "Y" )? 'checked="checked"' : ""; ?> /><span class="show_contact_span">Do you want to display contact number with testimonial? </span></label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Web-Site URL <small>Web-site url</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="website_url" class="form-control control-width-large" value="<?php echo $record_dtls['website_url']; ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Testimonial Content<small></small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="testimonial_text" class="form-control control-width-large" placeholder="Enter a short description" rows="5"><?php echo $record_dtls['testimonial_text']; ?></textarea>
                                    </div>
                                </div>
                            
                                <div class="form-footer">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Stay" name="SaveStay">
                                        </div>
                                    </div>
						        </div>
                            				
                        </div>
						
                    </div>

                </div><!--end .box -->
            
				<div class="col-md-4">
					<!-- ------------------------------------------ -->
					<!-- --------- Upload Image :: Start ---------- -->
					<!-- ------------------------------------------ -->
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-paperclip"></i> Featured Image</h3>
                                <small>Uploaded image will reflected at the testimonial section.</small>
                            </div>
                            <div class="panel-body">
                                <div id="img_prev">
                            <?php
                                if ( $record_dtls['featured_image'] != "" )
                                {
                            ?>
                                    <div class="">
                                <?php
                                    echo img(array('id'=>'cat_img','src'=>'assets/uploaded_files/testimonial/medium/'.$record_dtls['featured_image'],'width' => 350));
                                ?><br>
                                        <span id="delete_profile_img">
                                             <button id="delete_img" class="btn btn-rounded btn-default" type="button">Delete Feature Image</button>
                                        </span>

                                    </div>
                            <?php
                                }
                            ?>
                                </div>
                                <input type="file" onchange="getimage(this)" name="featured_image" id="featured_image" /><br/>
                                <small><?php echo $image_recommended_text; ?></small>
                            </div>
                        </div>
                    </div>
					<!-- ------------------------------------------ -->
					<!-- --------- Upload Image :: End ------------ -->
					<!-- ------------------------------------------ -->


				</div>
			</div><!--end .col-lg-12 -->
			</form>
		</div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






