<?php
$controller_name=$this->uri->segment(2);
?>
<script>
$(document).ready(function(){
   
    $('#gener_pass').bind('click',generatePassword);
    
    function generatePassword()
    {
         var allVals = [];
         $('[name=chk]:checked').each(function() {
           allVals.push($(this).val());
         });
        console.log(allVals);
        
        var adminurl= $('#siteadminurl').val();
        var controllername = $('#controller_name').val();
        var patientId="";
        $.ajax({

                url:adminurl+controllername+"/ajax_generate_random_password",
                type:"POST",
                dataType:"html",
                data:{arrOpt:allVals},
                success:function(data)
                {
                    var strHtml = '<h3 class="text-center"><span>'+ data +'</span></h3>';
                    $('#print_pass').html(strHtml);
                }
             });
        
    }
    
});
</script>

<div id="content">
        <section>

	<div class="section-header section-12">
		<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Settings <small><i class="fa fa-fw fa-angle-right"></i> Reset Password</small></h3>
	</div>
	
<div class="section-body">
	
	<?php
	echo $this->session->flashdata('msg_data');
	?>
    <?php
    echo $msg_data;
    ?>
			
    <!-- START DATATABLE  -->
    <div class="row">
			<div class="col-lg-8">
				<div class="box">
					<div class="box-head">
                        <header><h4 class="text-light"> Reset <strong>Password</strong></h4></header>
					</div>
					<div class="box-body">
                        
                    <div class="well">
                        <span class="label label-danger">NOTE!</span>
                        <span>
                            <strong>Password</strong> is a level of security for your administrative panel.
                            Enter your password such a way that can be difficult to access from other peoples.
                            Password must be minimum 5 digits of character and maximum 15 digits. You may use the 
                            <strong>Password Generator</strong> to genarate random password.
                        </span>
                    </div>
                        
					<form name="frm" id="frm" method="post" accept-charset="utf-8" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/change_password"; ?>" class="form-horizontal">
					<input type="hidden" name="controller_name" id="controller_name"  value="<?php echo $controller_name; ?>" />
                    <input type="hidden" name="siteadminurl" id="siteadminurl" value="<?php echo base_url().$this->config->item('admin_folder_name')."/"; ?>" />
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Enter a Password</label>
                            </div>

                            <div class="col-md-10">
                                <input type="password" name="password" id="password" class="form-control control-width-large validate[required, minSize[6],maxSize[15]]" value="<?php echo $this->input->post('password'); ?>" placeholder="Enter a Password">
                            </div>
						</div>
                        
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Retype Password</label>
                            </div>

                            <div class="col-md-10">
                                <input type="password" name="conf_password" id="conf_password" class="form-control control-width-large validate[required, equals[password]]" value="<?php echo $this->input->post('conf_password'); ?>" placeholder="Retype Password">
                            </div>
						</div>
                        
                        <div class="form-footer">
                             <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="submit" class="btn save_btn" value="Save" name="Submit">
                                </div>
                            </div>
                        </div>
						
						
                    </form>
                    </div>
    		
                </div>
        </div>
    
    
            <div class="col-lg-4">
				<div class="box">
					<div class="box-head">
						<header><h4 class="text-light">Password <strong>Generator</strong></h4></header>
					</div>
					<div class="box-body">
						<table class="table">
							<thead>
								<tr>
									<th colspan="2">
                                    <label>Select the characters that you want to include into your password.</label>
                                    </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
                                    <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
                                       <input type="checkbox" name="chk" id="" value="alpha">
                                    </div>
                                    </td>
									<td>Lowercase A-Z</td>
								</tr>
								<tr>
									<td>
                                    <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
                                       <input type="checkbox" name="chk" id="" value="alpha_upper">
                                    </div>
                                    </td>
									<td>Uppercase A-Z</td>
								</tr>
								<tr>
									<td>
                                    <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
                                       <input type="checkbox" name="chk" id="" value="numeric">
                                    </div>
                                    </td>
									<td>Numbers (0-9)</td>
								</tr>
                                <tr>
									<td>
                                    <div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse">
                                       <input type="checkbox" name="chk" id="" value="special">
                                    </div>
                                    </td>
									<td>Special Characters (.-+=_,!@$#*%<>[]{})</td>
								</tr>
                                
                                <tr>
									<td colspan="2">
                                        <p><button class="btn btn-rounded btn-default" id="gener_pass" type="button">Generate your password</button></p>
                                    </td>
								</tr>
                                
                                <tr>
									<td colspan="2">
                                    <p id="print_pass"></p>    
                                    </td>
								</tr>
                                
                                
							</tbody>
						</table>
					</div><!--end .box-body -->
				</div><!--end .box -->
			</div><!--end .col-lg-6 -->
    
    
    </div>
    
    
    
</div>
</section>
</div>

<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

