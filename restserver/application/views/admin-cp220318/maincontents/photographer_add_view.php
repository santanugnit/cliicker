<?php $controller_name = $this->uri->segment(2); ?>

<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-briefcase text-gray-light"></i> Photographer <small><i class="fa fa-fw fa-angle-right"></i> regiastration</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/photographer_list"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
        <?php echo $this->session->flashdata('msg_data'); ?>
            <!-- START HORIZONTAL FORM -->
			<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/add"; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="box">
                            <div class="box-head">
                                <header class="header-txt-align">
                                    <h4 class="text-light">Add <strong>Photographer</strong></h4>
                                </header>
                            </div>

                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>

                                <!-- ******************************************************************************  -->
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">First Name <span class="required_class">*</span><small>Enter First Name</small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" name="first_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('first_name'); ?>" placeholder="Enter a first name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Last Name <span class="required_class">*</span><small>Enter Last Name</small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" name="last_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('last_name'); ?>" placeholder="Enter a last name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Email Id <span class="required_class">*</span><small>Enter email id</small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('email'); ?>" placeholder="Enter email id" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label"> Gender <span class="required_class">*</span><small></small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <div data-toggle="buttons">
                                            <label class="btn radio-inline btn-radio-primary active">
                                                <input name="gender" id="option1" value="M" checked="" type="radio" /> Male
                                            </label>
                                            <label class="btn radio-inline btn-radio-primary">
                                                <input name="gender" id="option2" value="F" type="radio" /> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Address <span class="required_class">*</span><small>Enter full address</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="address" class="form-control control-width-large validate[required]" placeholder="Ex: Page & Content Management System, Park Management System, 3rd Party Online Booking Program etc..." rows="5"><?php echo $this->input->post('address'); ?></textarea>
                                        <small>Note: Enter all the features separated with comma (,) separator.</small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Password <span class="required_class">*</span><small>Enter password </small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="password" name="password" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('password'); ?>" placeholder="Enter your full address" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label"> Confirm password <span class="required_class">*</span><small>Enter Confirm password</small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="password" name="conf_password" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('conf_password'); ?>" placeholder="Enter your full address" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label"> Contact Number <span class="required_class">*</span><small></small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="contact_number" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('password'); ?>" placeholder="Enter your full address" />
                                    </div>
                                </div>
                                <!-- ******************************************************************************  -->

                                <div class="form-footer">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Review" name="SaveReview">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div><!--end .box -->

                    <div class="col-md-4">
                        <!-- -------------------------------------------------- -->
                        <!-- --------- Upload profile Image :: Start ---------- -->
                        <!-- -------------------------------------------------- -->
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paperclip"></i> Profile Picture</h3>
                                    <small>Uploaded image will reflected at the potfolio deatils screen</small>
                                </div>
                                <div class="panel-body">
                                    <div id="img_prev"></div>
                                    <input type="file" onchange="getimage(this)" name="featured_image" id="featured_image" />
                                    <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </small>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------- -->
                        <!-- --------- Upload profile Image :: End ------------ -->
                        <!-- -------------------------------------------------- -->

                        <!-- -------------------------------------------- -->
                        <!-- --------- Upload Image 2 :: Start ---------- -->
                        <!-- -------------------------------------------- -->
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paperclip"></i> Photographer best three image</h3>
                                    <small>Uploaded image will reflected at potfolio listing screen</small>
                                </div>
                                <div class="panel-body">
                                    <div id="best_photos_view_1"></div>
                                    <input type="file" onchange="getimage2(this)" name="best_photos[]" id="best_photos_1" data-sequence_id="1" />
                                    <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                    <hr>
                                </div>
                                <div class="panel-body">
                                    <div id="best_photos_view_2"></div>
                                    <input type="file" onchange="getimage2(this)" name="best_photos[]" id="best_photos_2" data-sequence_id="2" />
                                    <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                    <hr>
                                </div>
                                <div class="panel-body">
                                    <div id="best_photos_view_3"></div>
                                    <input type="file" onchange="getimage2(this)" name="best_photos[]" id="best_photos_3" data-sequence_id="3" />
                                    <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------- -->
                        <!-- --------- Upload Image 2 :: End ------------ -->
                        <!-- -------------------------------------------- -->
                    </div>
                </div><!--end .col-lg-12 -->
			</form>
        </div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->

<script>
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="350">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function getimage2(input)
    {
        var seqID = $(input).attr('data-sequence_id');
        if (input.files && input.files[0] && parseInt(seqID) > 0 )
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="200" />';
                $('#best_photos_view_'+seqID).html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






