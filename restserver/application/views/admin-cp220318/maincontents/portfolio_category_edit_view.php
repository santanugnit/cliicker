<?php
$controller_name=$this->uri->segment(2);
$row_id=$this->uri->segment(4);
?>
<script>
    $(document).ready(function () {
		
		/******* Calling Jscroll ***/
		$('.ser_div').slimScroll({
			width: '100%',
			height: '225px',
			color: '#efdbc8',
			alwaysVisible: true,
		});
			
        /*********** Delete the existing profile image ***********/
        $('#delete_img').bind('click',delete_featured_image);

        function delete_featured_image()
        {
            var existingFileName = $('#existing_featured_image').val();

            if(existingFileName!="")
            {
                jConfirm('Are you want to delete the feature image?', 'Confirmation', function(r){
                    if(r)
                    {
                        var targetUrl = '<?php echo base_url().$this->config->item('admin_folder_name').'/'.$controller_name."/ajax_delete_featured_image"; ?>';
                        $.ajax({
                            url: targetUrl,
                            type:"POST",
                            dataType:"json",
                            data:{rowId:<?php echo $row_id; ?>, fileName:existingFileName},
                            success:function(data)
                            {
                                if(data.result=="success")
                                {
                                    $("#delete_profile_img").hide(); //hide the delete button
                                    $('#img_prev').html('');
                                    $('#existing_featured_image').val('');
                                }
                            }
                        });
                    }
                    else
                    {
                        return false;
                    }
                });
            }
            else
            {
                return false;
            }
        }
    });
    function getimage(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                var strHtml = '<img src="'+e.target.result+'" width="120" height="120">';
                $('#img_prev').html(strHtml);

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-globe text-gray-light"></i> Portfolio Categories<small> <i class="fa fa-fw fa-angle-right"></i> Edit</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/list_all"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">
			
			<?php
			echo $this->session->flashdata('msg_data');
			echo $msg_data;
			?>
            <!-- START HORIZONTAL FORM -->
			<form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/edit/".$row_id; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
			<input type="hidden" name="existing_featured_image" id="existing_featured_image" value="<?php echo $record_dtls['featured_image_file']; ?>">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">

                        <div class="box-head">
                            <header class="header-txt-align">
                                <h4 class="text-light">Edit <strong>Category</strong></h4>
                            </header>
                        </div>
						
                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Category Name <span class="required_class">*</span><small>Enter a category name</small></label>
                                    </div>
    
                                    <div class="col-md-9">
                                       <input type="text" name="cat_name" class="form-control control-width-large validate[required]" value="<?php echo $record_dtls['cat_name']; ?>" placeholder="Enter a category name">
                                    </div>
                                </div>
								
								<div class="form-group">
									<div class="col-md-3">
										<label class="control-label">Category Icon <small>Upload a category icon</small></label>
									</div>

									<div class="col-md-9">
                                        <div id="img_prev">
                                            <?php
                                            if($record_dtls['featured_image_file']!="")
                                            {
                                            ?>
                                                <div class="">
                                                    <?php
                                                    echo img(array('id'=>'cat_img','src'=>'assets/uploaded_files/other_image/medium/'.$record_dtls['featured_image_file']));
                                                    ?><br>
                                                    <span id="delete_profile_img">
													 <button id="delete_img" class="btn btn-rounded btn-default" type="button">Delete Feature Image</button>
													   </span>

												</div>

											<?php
											}
											?>
                                        </div>
										<input type="file" onchange="getimage(this)" name="featured_image_file" id="featured_image_file" />
                                        <small><?php echo $image_recommended_text; ?></small>
									</div>
								</div>
                            
                                <div class="form-footer">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Stay" name="SaveStay">
                                        </div>
                                    </div>
						        </div>	
                        </div>
                    </div>

                </div><!--end .box -->
            
				<div class="col-md-4">
					<!-- ------------------------------------------ -->
					<!-- --------- Add Services :: Start ---------- -->
					<!-- ------------------------------------------ -->
					<div class="row">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-paperclip"></i>Associate Services</h3>
								<small>Tag services with this category (If Any)</small>
							</div>
							<div class="panel-body">
								<?php
								if(count($service_list))
								{	
								?>
								<div class="scroll-wrapper ser_div">
								<?php	
									foreach($service_list as $arr)
									{
										$active_st = "";
										$select_st = "";
										if($record_dtls['service_ids']!="")
										{
											$ser_arr = explode(',', $record_dtls['service_ids']);
											if(in_array($arr['id'], $ser_arr))
											{
												$active_st = "active";
												$select_st = 'checked="checked"';
											}	
										}		
											
								?>
										<div class="exp_title">
											<div data-toggle="buttons" class="btn btn-checkbox btn-checkbox-gray-inverse <?php echo $active_st; ?>">
											   <input name="service_arr[]" value="<?php echo $arr['id']; ?>" type="checkbox" <?php echo $select_st; ?>>
											</div>
											<?php echo $arr['service_name']; ?>
										</div>
								<?php	
									}
								?>
								</div>
								<?php	
								}	
								?>
							</div>
						</div>	
					</div>
					<!-- ------------------------------------------ -->
					<!-- --------- Add Services :: Start ---------- -->
					<!-- ------------------------------------------ -->
				</div>
			
			</div><!--end .col-lg-12 -->
			</form>
        </div><!--end .row -->
            <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
        
<script>
jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
</script>

	






