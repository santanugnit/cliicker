<?php
    $controller_name = $this->uri->segment(2);
?>
<style>
    .ajax-success-msg{display: block; color: #008000;}
    .ajax-error-msg{display: block; color: #FF1D49;}
</style>

<div id="content">
    <section>

        <div class="section-header section-6">
            <h3 class="text-standard"><i class="fa fa-fw fa-briefcase text-gray-light"></i> Album <small><i class="fa fa-fw fa-angle-right"></i> Add</small></h3>
        </div>

        <div class="section-header section-4">
            <ul class="forth-menu">
                <li><a href="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/album_list"; ?>"> <i class="fa fa-fw fa-backward"></i> Back</a></li>
            </ul>
        </div>

        <div class="section-body">

            <?php echo $this->session->flashdata('msg_data'); ?>
            <!-- START HORIZONTAL FORM -->
            <form name="frm" id="frm" class="form-horizontal form-banded form-bordered form-validation" action="<?php echo base_url().$this->config->item('admin_folder_name')."/".$controller_name."/add"; ?>" method="post" role="form" novalidate="novalidate" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="box">
                            <div class="box-head">
                                <header class="header-txt-align">
                                    <h4 class="text-light">Add <strong>Album</strong></h4>
                                </header>
                            </div>

                            <div class="box-body">
                                <div class="body-head">
                                    <p><span>*</span> fields are mandatory</p>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Select a photographer <span class="required_class">*</span><small>Select a photographer</small></label>
                                    </div>

                                    <div class="col-md-9">
                                        <select name="photographer_id" id="photographer_id" class="form-control control-width-large validate[required]">
                                            <option value="">Select a photographer</option>
                                        <?php
                                            if(count($photographers_list))
                                            {
                                                foreach($photographers_list as $val)
                                                {
                                        ?>
                                                    <option value="<?php echo $val['id']; ?>" <?php if ( isset($_POST['photographer_id']) && ($val['id'] == $this->input->post('photographer_id')) ) { echo 'selected="selected"'; } ?> data-upg-code="<?php echo $val['pg_clicker_code']; ?>"><?php echo $val['first_name'] .' '. $val['last_name'] ; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album Name <span class="required_class"> *</span><small>Enter a album name</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="album_name" class="form-control control-width-large validate[required]" value="<?php echo $this->input->post('album_name'); ?>" placeholder="Enter a album name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album Description <small>Enter a short description</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="short_description" class="form-control control-width-large" placeholder="Enter a short description" rows="5"><?php echo $this->input->post('short_description'); ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Introduction text <small>Album short introduction text.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="intro_text" class="form-control control-width-large" value="<?php echo $this->input->post('intro_text'); ?>" placeholder="Album short introduction text..">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Subdomain name <span class="required_class">*</span> <small>Preferred subdomain name</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="preferred_subdomain_name" name="preferred_subdomain_name" class="form-control control-width-medium validate[required]" value="<?php echo $this->input->post('preferred_subdomain_name'); ?>" placeholder="" data-previous_value="" style="display: inline-block;"/> <span class="form-control-static" style="display: inline-block;	color: gray; letter-spacing: 1px;	padding-left: 5px; ">.cliicker.com</span> <span class="text_msg" style="display: none;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Album price<span class="required_class">*</span><small>Enter price for album creation</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="album_price" class="form-control control-width-small validate[required]" value="<?php echo $this->input->post('album_price'); ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Max storage<small>Max storage are in settings section.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="max_storage_allocate" class="form-control control-width-small" value="<?php echo $this->input->post('max_storage_allocate'); ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Google drive link<small>Google drive link</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="google_drive_link" class="form-control control-width-large" value="<?php echo $this->input->post('google_drive_link'); ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Dropbox link<small>Dropbox link of a files.</small></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="dropbox_link" class="form-control control-width-large" value="<?php echo $this->input->post('dropbox_link'); ?>" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-footer">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <input type="submit" class="btn save_btn" value="Save &amp Exit" name="SaveExit">
                                            <input type="submit" class="btn save_btn" value="Save &amp Review" name="SaveReview">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div><!--end .box -->

                    <div class="col-md-4">
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: Start ---------- -->
                        <!-- ------------------------------------------ -->
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paperclip"></i> Upload .zip file</h3>
                                    <small>Uploaded image .zip </small>
                                </div>
                                <div class="panel-body">
                                    <div id="img_prev"></div>
                                    <div id="container">
                                        <a id="pickfiles" href="javascript:;">[Select files]</a>
                                        <a id="uploadfiles" href="javascript:;">[Upload files]</a>
                                    </div>
                                    <div id="log"> </div>
                                    <input type="hidden" id="upload_files_ids" name="upload_files_ids" value="" />

                                <?php /* ?>
                                    <input type="file" onchange="getimage(this)" name="featured_image_file" id="featured_image_file" /><br/>
                                    <small><?php //echo $image_recommended_text; ?></small>
                                <?php */ ?>
                                </div>
                            </div>
                        </div>
                        <!-- ------------------------------------------ -->
                        <!-- --------- Upload Image :: End ------------ -->
                        <!-- ------------------------------------------ -->
                    </div>

                </div><!--end .col-lg-12 -->
            </form>
        </div><!--end .row -->
        <!-- END HORIZONTAL FORM -->
    </section>
</div><!--end .section-body -->
<script src="<?php echo base_url(); ?>assets/js/plupload/plupload.full.min.js"></script>
<script>
    jQuery("#frm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});

    $(document).ready( function() {

        $("#preferred_subdomain_name").on("focusout", function(e){
            var _currElem = $(this);
            var previous_value  = _currElem.attr('data-previous_value');
            if ( previous_value.trim() != '' && previous_value == _currElem.val() )
            {
                return false;
            }
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-error-msg");
            _currElem.parent("div").find(".text_msg").html("").removeClass("ajax-success-msg");
            var preferred_subdomain_name = $(this).val();

            if( preferred_subdomain_name.trim() == '' ) {
                _currElem.attr('data-previous_value', "");
                return false;
            }

            $.ajax({
                url		: '<?php echo base_url() . $this->config->item("admin_folder_name") . "/" . $controller_name . "/ajax_check_unique_subdomain_name" ?>',
                type	: 'POST',
                data	: { 'preferred_subdomain_name' : preferred_subdomain_name,  },
                dataType: 'html',
                complete: function(){

                },
                success	: function(data) {
                    var result = $.parseJSON(data);
                    if(result.status){
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-error-msg").addClass("ajax-success-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    } else {
                        _currElem.parent("div").find(".text_msg").html(result.result).removeClass("ajax-success-msg").addClass("ajax-error-msg").fadeIn(600);
                        _currElem.attr('data-previous_value', preferred_subdomain_name);
                    }
                },
                error: function(error) {
                    alert(error);
                }
            });
        });


    });



    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('container'), // ... or DOM Element itself
        url : '<?php echo base_url().$this->config->item("admin_folder_name")."/".$controller_name."/ajax_plupload"; ?>',
        flash_swf_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.swf',
        silverlight_xap_url : '<?php echo base_url(); ?>assets/js/plupload/Moxie.xap',

        filters : {
            max_file_size : '1024mb',
            mime_types: [
                /*{title : "Image files", extensions : "jpg,gif,png"},*/
                {title : "Zip files", extensions : "zip"}
            ]
        },

        multipart_params : { "pg_code" : $("#photographer_id").attr("data-upg-code"), },

        init: {
            PostInit: function() {
                document.getElementById('img_prev').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    var pg_code = $("#photographer_id").find(':selected').attr("data-upg-code");
                    console.log(pg_code+" -- before_if");
                    if( $.trim(pg_code) == '' || pg_code == 'undefined' )
                    {
                        document.getElementById('log').innerHTML = '<div id=""><b>Please select a photographer first.</b></div>';

                    } else {

                        document.getElementById('log').innerHTML = '';
                        uploader.settings.multipart_params["pg_code"] = pg_code;
                        uploader.start();
                        return false;
                    }

                    return false;
                };
            },
            BeforeUpload: function(up, file) {

                // Called right before the upload for a given file starts, can be used to cancel it if required
                //console.log('[BeforeUpload]', 'File: ', file);

            },

            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    document.getElementById('img_prev').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                });
            },

            UploadProgress: function(up, file) {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading

                var upload_files_ids    = [];
                var upload_old_ids      = $('#upload_files_ids').val();
                if ( $('#upload_files_ids').val().trim() != '' ) {
                    upload_files_ids.push(upload_old_ids);
                }

                var data = $.parseJSON(info.response);
                $.each( data.file_name, function( index, value ){
                    //console.log(index+' -- '+value);
                    upload_files_ids.push(index);
                });
                $("#upload_files_ids").val(upload_files_ids.join(","));
            },
            Error: function(up, err) {
                document.getElementById('log').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
        }
    });


    uploader.init();
</script>








