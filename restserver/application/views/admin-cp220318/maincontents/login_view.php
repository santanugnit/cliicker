<script>
    $(function()
    {
        jQuery("#form1").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true});
    });
</script>

<style>
    .submit-btn{  background-color:#316ac5; }
    .submit-btn:hover{ background-color:#006699; }
</style>

<div class="box-type-login">

    <!--<div class="box-head">
        <h2 class="text-light">Stimulus <strong>Hospitality</strong></h2>
        <h4 class="text-light text-inverse-alt">Login to your administrative panel</h4>
    </div> -->

    <?php //echo $error_message;  ?>

    <div class="box-body">
        <div class="login_logo"><img src="<?php echo base_url().'assets/images/top-logo.png'; ?>" alt=" "/></div>
        <h2 class="text-light"><strong style="font-size:25px;"><?php echo "Cliicker Happy <strong>".date("l")."</strong>"; ?></strong></h2>
        <h4 class="text-light text-inverse-alt">Just enter your email address and password, we’ll get you right into your site control panel.</h4>

        <div class="custom-alert row">
            <?php echo $error_message; ?>
        </div>
    <?php
        echo form_open($this->config->item('admin_folder_name').'/login', array('method' => 'post', 'name' => 'form1', 'id' => 'form1'));

        if(isset($_POST['admin_uname']))
            $user_name = $this->input->post('admin_uname');
        else
            $user_name = $admin_username;
    ?>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control validate[required,custom[email]]" name="admin_uname" id="admin_uname" value="<?php echo $user_name; ?>"  placeholder="Email" />
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control validate[required]" name="admin_pass" id="admin_pass" placeholder="Password" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-left">
                <div data-toggle="buttons">
                    <label class="btn checkbox-inline btn-checkbox-primary-inverse <?php if ( $admin_username != "" ){ echo 'active'; } ?>">
                        <input type="checkbox" value="default-inverse1" name="remember" <?php if ( $admin_username != "" ){ echo 'checked="checked"'; } ?> /> Remember me
                    </label>
                </div>
                <span>Forgot your password? <a href="<?php echo base_url().$this->config->item('admin_folder_name'); ?>/login/forgot_password">Click Here</a></span>
            </div>
            <div class="col-xs-6 text-right">
                <input type="submit" class="btn save_btn" value="Submit" name="loginSubmit" id="loginSubmit" />
            </div>
        </div>
        <?php echo form_close(); ?>
    </div><!--end .box-body -->


</div>
