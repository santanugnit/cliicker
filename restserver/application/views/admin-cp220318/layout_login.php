<!DOCTYPE html>
<html lang="en">
<head>
<title>Codopoliz - Administrator Login</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="your description">
    <link href="<?php echo base_url(); ?>assets/images/favIcon.png" rel="shortcut icon" type="image/x-icon" />

    <!-- END META -->
    <!-- BEGIN STYLESHEETS -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp-bootstrap-frame.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp-collaps-box.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/zp_responsive.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
    
        <!-- LOAD JQUERY VALIDATION ENGINE STYLESHEET -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/css/validationEngine.jquery.css" />

        <!-- END STYLESHEETS -->
        <script src="<?php echo base_url(); ?>assets/js/libs/jquery/jquery-1.11.0.min.js"></script>
        <!-- END STYLESHEETS -->
</head>

<body class="body-dark">
<?php echo $maincontent; ?>

<!-- BEGIN JAVASCRIPT -->

<script src="<?php echo base_url(); ?>assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/BootstrapFixed.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/spin.js/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/demo/DemoUIMessages.js"></script>

<!-- LOAD JQUERY VALIDATION ENGINE SCRIPT -->
<script src="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jQuery-Validation-Engine-2.6.4/js/jquery.validationEngine-en.js"></script>

<!-- END JAVASCRIPT -->
</body>
</html>
