<?php
class Notification_model extends CI_Model
{
    ##---------- Load Constructor ------------##
    function __construct()
    {
        parent::__construct();
    }  

    function api_do_create_notification( $data = array() )
    {
        /* CI transaction start with '$this->db->trans_begin()' */
        $this->db->trans_begin();

        $insData = array(
            'entity_type_id'    => $data['entity_type_id'],
            'entity_id'         => $data['entity_id'],
            'created_on'        => date("Y-m-d H:i:s"),
            'status'            => 0
        );
        $this->db->insert('notification_object', $insData);
        $notification_object_id = $this->db->insert_id();

        if( (int)$notification_object_id > 0 ) 
        {
            /* Insert to 'notification_change' table */
            $insData = array(
                'notification_object_id'    => $notification_object_id,
                'actor_id'              => $data['actor_id'],
            );
            $this->db->insert('notification_change', $insData);

            /* Insert to 'notification' table */
            if( isset($data['notifier_id']) && trim($data['notifier_id']) != '' )
            {
                $notifier_ids   = explode(',', $data['notifier_id']); 
                if( is_array($notifier_ids) && !empty($notifier_ids) )
                {
                    foreach( $notifier_ids as $notifier_id )
                    {
                        $insData = array(
                            'notification_object_id'    => $notification_object_id,
                            'notifier_id'           => $notifier_id,
                            'added_date'            => date("Y-m-d H:i:s"),
                            'notification_status'   => 'unread',
                        );
                        $this->db->insert('notification', $insData); 
                    }
                } 
                else 
                {
                    $this->db->trans_rollback();
                    return false;
                }                   
            } 
            else
            {
                $this->db->trans_rollback();
                return false;
            } 
        }

        if ( $this->db->trans_status() === FALSE )
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }






}