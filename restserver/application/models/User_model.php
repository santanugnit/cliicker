<?php

class User_model extends CI_Model
{
    
		##----------------------------------------##
		##---------- Load Constructor ------------##
		function __construct() 
		{        
			parent::__construct();
		}
		##----------------------------------------##
        
		##-----------------------------------------------------##
		##--------- Get total count for pagination ------------##
		##-----------------------------------------------------##
		function get_row_count()
		{	
            $sql = "SELECT * FROM `user` WHERE user_type='EDITOR' AND account_activation_status!='D'";
            $query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				return $query->num_rows();
			}
		}
		
        
        ##--------------------------------------------------------##
        ##--------- GET ALL THE USER LIST AS ARRAY FORMAT --------##
        ##--------------------------------------------------------##
    
        function get_user_rows_pagination($num=NULL,$offset=NULL)
        {	
            $sql = "SELECT * FROM `user`
					WHERE user_type='EDITOR' AND account_activation_status!='D'
                    ORDER by id DESC LIMIT ".$offset.",".$num;
			
            $query = $this->db->query($sql);
			$result = $query->result_array();
			
            return $result;
        }
		
				
		##---------------------------------------------------------------------------##
		##------------------------  VALIDATION FUNCTION  ----------------------------##
		##---------------------------------------------------------------------------##
		function validate_form_data()
		{
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('first_name', 'first name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('last_name', 'last name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('contact_email', 'contact email', 'required|trim|xss_clean');
			$this->form_validation->set_rules('account_activation_status', 'account activation status', 'required|trim|xss_clean');
			
			if($this->uri->segment(3)!='edit')
            {
				$ignore_val_arr = array('account_activation_status'=>'D');	
                $this->form_validation->set_rules('email', 'email', 'required|valid_email|trim|xss_clean|is_unique_value[user.email.'.serialize($ignore_val_arr).']');
                $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[6]|max_length[15]|matches[conf_password]');
                $this->form_validation->set_rules('conf_password', 'confirm password', 'required|trim|xss_clean');
            }
			else
			{
				if($this->input->post("changePass"))
				{
					$this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[6]|max_length[15]|matches[conf_password]');
					$this->form_validation->set_rules('conf_password', 'confirm password', 'required|trim|xss_clean');
				}	
			}
            			
			if ($this->form_validation->run() == TRUE)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
    
        		
		##------------------------------------------------------##
		##-------------------  Add a user  ---------------------##
		##------------------------------------------------------##
		function add()
		{
            
            $profile_image = "";
            if($_FILES['profile_image_file']['name']!="") //If profile image upload
			{
				/********* Rename the upload file ***********/
                $filename_arr = explode(".", $_FILES['profile_image_file']['name']);
                $file_extension = $filename_arr[count($filename_arr) - 1];
                $image_file_name = time().".".$file_extension;    
				/********************************************/
				
				// set upload configuration
				$config['upload_path'] = "assets/uploaded_files/user_profile_image/original/";
				$config['allowed_types'] = "jpg|jpeg|png";
				$config['max_size'] = '1000000'; //KB
				$config['file_name'] = $image_file_name;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('profile_image_file'))
				{
					return $this->upload->display_errors();
				}
				else
				{

					$data = array('upload_data' => $this->upload->data());
					$profile_image = $data['upload_data']['file_name'];

					//################################################################//
					//################# FUNCTIONALITY OF IMAGE RESIZE ################//

					//***************************************************************//
					//################### MEDIUM IMAGE CREATION ######################//

					$config['image_library'] = 'gd2';
					$config['source_image'] = 'assets/uploaded_files/user_profile_image/original/'.$data['upload_data']['file_name'];
					$config['new_image'] = 'assets/uploaded_files/user_profile_image/medium/'.$data['upload_data']['file_name'];
					$config['allowed_types'] = "jpg|jpeg|gif";
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim']='auto';


					//###################################################//
					//########### CHECK UPLOAD IMAGE DIMENSION ##########//

					$imgarr= $this->config->item('image_valid_dimensions');
					$imgarr_data=$imgarr["user_image_medium"];
					$data_explode=explode("|",$imgarr_data);

					$define_image_width=$data_explode[0];
					$define_image_height=$data_explode[1];

					//########### CHECK UPLOAD IMAGE DIMENSION ##########//
					//###################################################//

					$iw= $data['upload_data']['image_width']; //uploaded image width
					$ih= $data['upload_data']['image_height']; //uploaded image height

					$mw = $define_image_width; //Recommended image width
					$mh = $define_image_height; //Recomended image height

					if($iw<=$mw && $ih <= $mh)
					{
						$ow    = $iw;
						$oh    = $ih;
					}
					if($iw<=$mw && $ih > $mh)
					{
						$oh    = $mh;
						$ow    =($oh*$iw)/$ih;
					}
					if($iw>$mw && $ih <= $mh)
					{
						$ow    = $mw;
						$oh    = ($ow*$ih)/$iw;
					}
					if($iw>$mw && $ih > $mh && $iw>$ih )
					{
						$ow    = $mw;
						$oh    = ($ow*$ih)/$iw;
					}
					if($iw>$mw && $ih > $mh && $iw<=$ih)
					{
						$oh    = $mh;
						$ow    = ($oh*$iw)/$ih ;
					}

					$config['width'] = $ow;
					$config['height'] = $oh;

					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					//################### MEDIUM IMAGE CREATION ######################//
					//***************************************************************//

				//################# FUNCTIONALITY OF IMAGE RESIZE ################//
				//################################################################//

				}

			}
						
            //############## Insert post value into user table ##################//
			$postdata = array(
								'user_type' => "EDITOR",
								'first_name' => $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),
								'contact_email' => $this->input->post('contact_email'),
                                'email' => $this->input->post('email'),
                                'address' => $this->input->post('address'),
                                'phone' => $this->input->post('phone'),
                                'profile_image' => $profile_image,
								'creation_date' => date('Y-m-d H:i:s'),
								'password' => md5($this->input->post('password')),
								'password_text' => $this->input->post('password'),
								'account_activation_status' =>  $this->input->post('account_activation_status')
							 );
	
			$this->db->insert('user',$postdata);
							
			return "success";
		}
		
		
		##------------------------------------------------------##
		##--------------------  Edit a User  -------------------##
		##------------------------------------------------------##
		function edit($user_id)
		{
			if($_FILES['profile_image_file']['name']!="") //If profile image upload
			{
				
				/********* Rename the upload file ***********/
				$filename_arr = explode(".", $_FILES['profile_image_file']['name']);
				$file_extension = $filename_arr[count($filename_arr) - 1];
				$image_file_name = time().".".$file_extension;    
				/********************************************/

				// set upload configuration
				$config['upload_path'] = "assets/uploaded_files/user_profile_image/original/";
				$config['allowed_types'] = "jpg|jpeg|png";
				$config['max_size'] = '1000000'; //KB
				$config['file_name'] = $image_file_name;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('profile_image_file'))
				{
					return $this->upload->display_errors();
				}
				else
				{

					$data = array('upload_data' => $this->upload->data());
					$profile_image = $data['upload_data']['file_name'];

					//################################################################//
					//################# FUNCTIONALITY OF IMAGE RESIZE ################//

					//***************************************************************//
					//################### MEDIUM IMAGE CREATION ######################//

					$config['image_library'] = 'gd2';
					$config['source_image'] = 'assets/uploaded_files/user_profile_image/original/'.$data['upload_data']['file_name'];
					$config['new_image'] = 'assets/uploaded_files/user_profile_image/medium/'.$data['upload_data']['file_name'];
					$config['allowed_types'] = "jpg|jpeg|gif";
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim']='auto';


					//###################################################//
					//########### CHECK UPLOAD IMAGE DIMENSION ##########//

					$imgarr= $this->config->item('image_valid_dimensions');
					$imgarr_data=$imgarr["user_image_medium"];
					$data_explode=explode("|",$imgarr_data);

					$define_image_width=$data_explode[0];
					$define_image_height=$data_explode[1];

					//########### CHECK UPLOAD IMAGE DIMENSION ##########//
					//###################################################//

					$iw= $data['upload_data']['image_width']; //uploaded image width
					$ih= $data['upload_data']['image_height']; //uploaded image height

					$mw = $define_image_width; //Recommended image width
					$mh = $define_image_height; //Recomended image height

					if($iw<=$mw && $ih <= $mh)
					{
						$ow    = $iw;
						$oh    = $ih;
					}
					if($iw<=$mw && $ih > $mh)
					{
						$oh    = $mh;
						$ow    =($oh*$iw)/$ih;
					}
					if($iw>$mw && $ih <= $mh)
					{
						$ow    = $mw;
						$oh    = ($ow*$ih)/$iw;
					}
					if($iw>$mw && $ih > $mh && $iw>$ih )
					{
						$ow    = $mw;
						$oh    = ($ow*$ih)/$iw;
					}
					if($iw>$mw && $ih > $mh && $iw<=$ih)
					{
						$oh    = $mh;
						$ow    = ($oh*$iw)/$ih ;
					}

					$config['width'] = $ow;
					$config['height'] = $oh;

					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					//################### MEDIUM IMAGE CREATION ######################//
					//***************************************************************//

			//################# FUNCTIONALITY OF IMAGE RESIZE ################//
			//################################################################//


			//################################################################//        
			//################# DELETE EXISTING IMAGE IF ANY #################//

			if($this->input->post('existing_profile_image')!="")
			{    
				unlink('assets/uploaded_files/user_profile_image/medium/'.$this->input->post('existing_profile_image'));
				unlink('assets/uploaded_files/user_profile_image/original/'.$this->input->post('existing_profile_image'));
			}

			//################################################################//        

			}

			}
			else
			{    
				$profile_image=$this->input->post('existing_profile_image');
			}
			
			
			$postdata = array(
								'first_name' => $this->input->post('first_name'),
								'last_name' => $this->input->post('last_name'),
								'contact_email' => $this->input->post('contact_email'),
								'address' => $this->input->post('address'),
								'profile_image' => $profile_image,
								'phone' => $this->input->post('phone'),
								'account_activation_status' =>  $this->input->post('account_activation_status')
							);
			if($this->input->post('password')!="")
            {    
                $postdata = array(
                                    'password'=>md5($this->input->post('password')),
                                    'password_text'=>$this->input->post('password')
                             );
            }			
						
			
			$this->db->where('id',$user_id);
			$this->db->update('user',$postdata);
					
			
			return "edit_success";
		}
		
		
		##---------------------------------------------------##
		##--------------  GET Expert Details  ---------------##
		##---------------------------------------------------##
		function get_user_details($p_value_id)
		{
			$sql = "SELECT * FROM `user` WHERE id='".$p_value_id."'";
					
			$query = $this->db->query($sql);
			$result = $query->row_array();
			
			return $result;
		}
        
        ##--------------------------------------------------------##
		##-----------------  delete image  -----------------------##
		##--------------------------------------------------------##
        function delete_image($pId,$fname)
        {    
            $this->db->where('id',$pId);
        	$this->db->update('user',array('profile_image'=>''));
             
			 ##--------------------- Delete images from its respective paths -------------------##
			 unlink('assets/uploaded_files/user_profile_image/medium/'.$fname);
			 unlink('assets/uploaded_files/user_profile_image/original/'.$fname);
             ##---------------------------------------------------------------------------------##
        }
    
            
    
        ##---------------------------------------------------##
		##--------- Delete records from user table  ---------##
		##---------------------------------------------------##
		function delete($arr_val)
		{	
			foreach($arr_val as $tid)
			{
				$this->db->where('id',$tid);
				$this->db->update('user',array('account_activation_status'=>'D'));
			}
			
			return "success";
		}
		
		
		##--------------------------------------------------------##
		##---------------- get all values in array ---------------##
		function get_all_users($limit="")
		{
			$sql = "SELECT * FROM `user` ORDER BY id DESC";
					
			if($limit!="")
			{
				$sql .=" limit 0,".$limit;
			}		
			$query = $this->db->query($sql);

            return $query->result_array();
		}
		
        
    

}
