<?php

class Testimonial_model extends CI_Model
{

    ##----------------------------------------##
    ##---------- Load Constructor ------------##
    function __construct()
    {
        parent::__construct();
    }
    ##----------------------------------------##



    ##----------------------------------------------------##
    ##---------- Get total count for pagination ----------##
    ##----------------------------------------------------##
    function get_row_count()
    {
        $this->db->select('*');
        $query = $this->db->get("testimonials");

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }


    ##-----------------------------------------------------------------------##
    ##---------------- Get all the record list with pagination --------------##
    ##-----------------------------------------------------------------------##
    /**
     * @param null $num
     * @param null $offset
     * @return mixed
     */
    function get_rows_pagination($num = NULL, $offset = NULL)
    {
        $sql = 'SELECT * FROM `testimonials` ORDER BY id DESC LIMIT '.$offset.','.$num;
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    /**
     * @param $row_id
     * @return mixed
     */
    function get_details($row_id)
    {
        $sql = 'SELECT *  FROM `testimonials` WHERE id ="'.$row_id.'"  GROUP BY id';
        $query = $this->db->query($sql);

        return $query->row_array();

    }
    ##---------------------------------------------------------------------------##
    ##------------------------  VALIDATION FUNCTION  ----------------------------##
    ##---------------------------------------------------------------------------##
    function validate_form_data()
    {
        $ignore_val_arr = array();
        if( $this->uri->segment(4)!= "" )
        {
            $ignore_val_arr['id'] = $this->uri->segment(4);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('album_name', 'album name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('album_price', 'album price', 'required|trim|xss_clean');
        if( $this->uri->segment(3) == 'edit' )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique_update["'. $this->uri->segment(4) .'"]');
        } else {
            $this->form_validation->set_rules('photographer_id', 'select phptographer', 'required|trim|xss_clean');
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]',
                array('is_unique' => 'This %s already exists. Please type another name.')
            );
        }
        if($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    ##------------------------------------------------------------##
    ##--------------------  Add a record  ------------------------##
    ##------------------------------------------------------------##
    function add($postdata)
    {
        $this->db->insert('testimonials', $postdata);
        return $this->db->insert_id();
    }


    ##--------------------------------------------------##
    ##---------  EDIT record respect to id  ------------##
    ##--------------------------------------------------##
    function edit($row_id, $postdata)
    {
        $this->db->where('id', $row_id);
        $this->db->update('testimonials', $postdata);
        if ( $this->db->affected_rows() )
        {
            return true;
        } else {
            return false;
        }
    }

    ##----------------------------------------------------------------##
    ##------------------  Delete featured Image  ---------------------##
    ##----------------------------------------------------------------##
    function delete_featured_image($rowId, $filename)
    {
        $this->db->where('id', $rowId);
        $this->db->update('testimonials', array('featured_image' => ''));

        ##-------- Delete image from its respective paths ----------##
        unlink('assets/uploaded_files/testimonial/medium/'. $filename);
        unlink('assets/uploaded_files/testimonial/original/'. $filename);
        ##----------------------------------------------------------##
    }

    /**
     * @param $arr_val
     * @param $status_val
     * @return string
     */
    function change_status_value($arr_val, $status_val)
    {
        foreach($arr_val as $id)
        {
            $this->db->where('id', $id);
            $this->db->update('testimonials', array('status' => $status_val));
        }

        return "success";
    }


    /**
     * @param $arr_val
     * @param $status_val
     * @return string
     */
    function change_fields_display_status( $arr_val, $status_val, $fields = null )
    {
        if( $fields == null || trim($fields) == '' ){
            return false;
        }
        foreach($arr_val as $id)
        {
            $this->db->where('id', $id);
            $this->db->update('testimonials', array( $fields => $status_val));
        }
        //return $this->db->last_query();
        if ( $this->db->affected_rows() > 0 )
            return true;
        else
            return false;
    }


    ##---------------------------------------------------##
    ##----------------  Delete Record(s)  ---------------##
    ##---------------------------------------------------##
    /**
     * @param $arr_val
     * @return string
     */
    function delete($arr_val)
    {
        $this->load->library('common_functions');
        foreach($arr_val as $ids)
        {
            $filename = $this->common_functions->get_value('featured_image', 'testimonials','id="'.$ids.'"');
           /* if ( $filename != "" ) // Featured image exists
            {
                ##-------- Delete location image from its respective paths ----------##
                unlink('assets/uploaded_files/testimonial/medium/'.$filename);
                unlink('assets/uploaded_files/testimonial/original/'.$filename);
                ##-------------------------------------------------------------------##
            }*/
            $this->db->where('id', $ids);
            $this->db->update('testimonials', array( 'status' => "D") );
        }
        return $this->db->last_query();
        //return $arr_msg;
    }

    ##----------------------------------------------------------------##
    ##---------------  Download data to CSV  -------------------------##
    ##----------------------------------------------------------------##
    function download_csv()
    {
        $sql = "SELECT * FROM `testimonials`";
        $query = $this->db->query($sql);
        $this->load->helper('csv');
        query_to_csv($query, TRUE, 'testimonials_list.csv');
        return TRUE;
    }



}
