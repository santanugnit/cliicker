<?php

class Settings_model extends CI_Model
{
	
		
	//###############################################################//
	//############### INITIALIZE CONSTRUCTOR CLASS ##################//
	//###############################################################//
	
	function  __construct() 
	{
        parent::__construct();
    }
	
	//###############################################################//
	//############### INITIALIZE CONSTRUCTOR CLASS ##################//
	//###############################################################//
    
    
    ##---------------------------------------------------------------------------##
    ##------------------------  VALIDATION FUNCTION  ----------------------------##
    ##---------------------------------------------------------------------------##
    function validate_form_data()
    {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[6]|max_length[15]|matches[conf_password]');
        $this->form_validation->set_rules('conf_password', 'confirm password', 'required|trim|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    
    ##---------------------------------------------------------------------------##
    ##------------------------  RESET ADMIN PASSWORD  ---------------------------##
    ##---------------------------------------------------------------------------##
    
    function reset_password()
    {    
        $this->db->where('id',1);
        $this->db->update('user',array('password'=>md5($this->input->post('password')),'password_text'=>$this->input->post('password')));
    }
    
    
    function get_user_details($userid)
    {    
        $this->db->select('*');
        $this->db->where('id',$userid);
        $query = $this->db->get('user');
        
        return $query->row_array();
        
    }
    
    function validate_user_form_data()
    {    
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('first_name', 'first name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('last_name', 'last name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('phone', 'contact number', 'required|trim|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    
    }
    
    function update_user_profile($userid)
    {

        if($_FILES['profile_image_file']['name']!="") //If profile image upload
        {
            $image_data_config = array(
                'field_name'=>'profile_image_file', // this is the field name
                'upload_path'=>'assets/uploaded_files/user_profile_image/original/', // this is the original image path
                'upload_medium_path'=>'assets/uploaded_files/user_profile_image/medium/', // this is the medium image path
                'image_valid_dimensions'=>'user_image_medium', // this data has been set in config file
                'image_types'=>"jpg|jpeg|png", // this defined variable is defined in constance file

            );
            $profile_image_arr = $this->common_functions->image_upload($image_data_config);
            $profile_image = $profile_image_arr['image_info'];

            //################################################################//
            //################# DELETE EXISTING IMAGE IF ANY #################//
            if($profile_image!="")
            {
                if($this->input->post('existing_profile_image')!="")
                {
                    unlink('assets/uploaded_files/user_profile_image/medium/'.$this->input->post('existing_profile_image'));
                    unlink('assets/uploaded_files/user_profile_image/original/'.$this->input->post('existing_profile_image'));
                }
            }
            //################################################################//

        }
        else
        {
            $profile_image=$this->input->post('existing_profile_image');
        }
        
        $arr = array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'contact_email' => $this->input->post('contact_email'),
						'address' => $this->input->post('address'),
						'profile_image' => $profile_image,
						'phone' => $this->input->post('phone')
                    );
					
		
        $this->db->where('id',1);
        $this->db->update('user',$arr);
    }
    
    
    function delete_profile_image($uId,$filename)
    {    
        $this->db->where('id',$uId);
        $this->db->update('user',array('profile_image'=>''));

         ##--------------------- Delete profile images from its respective paths -------------------##
         unlink('assets/uploaded_files/user_profile_image/medium/'.$filename);
         unlink('assets/uploaded_files/user_profile_image/original/'.$filename);
         ##-----------------------------------------------------------------------------------------##

    }
	
	
	function get_website_settings()
    {    
        $this->db->select('*');
        $this->db->where('id',1);
        $query = $this->db->get('site_settings');
        
        return $query->row_array();
        
    }
	
	function validate_site_settings_form_data()
    {    
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('website_name', 'website name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('wesite_address', 'website address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('site_contact_email', 'contact email', 'required|trim|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    
    }
	
	function update_website_settings()
	{
		$arr = array(
						'website_name' 			=> $this->input->post('website_name'),
						'website_url' 			=> $this->input->post('website_url'),
						'wesite_address' 		=> $this->input->post('wesite_address'),
						'site_contact_email' 	=> $this->input->post('site_contact_email'),
						'site_contact_number' 	=> $this->input->post('site_contact_number'),
						'address_latitude' 		=> $this->input->post('address_latitude'),
						'address_longitude' 	=> $this->input->post('address_longitude'),
						'fb_link' 				=> $this->input->post('fb_link'),
						'twitter_link' 			=> $this->input->post('twitter_link')
                    );
		
        $this->db->where('id',1);
        $this->db->update('site_settings',$arr);
		
	}
	
	
	function save_popular_places($postdata)
	{
		$state_ids ="";
		if(count($postdata))
		{
			$state_ids = implode(",", $postdata);
		}
		
        $this->db->where('id',1);
        $this->db->update('site_settings',array('most_popular_places_ids'=>$state_ids));
		
	}
    
	
		
}


?>