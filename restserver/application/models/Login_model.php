<?php

class Login_model extends CI_Model
{
    //###############################################################//
    //############### INITIALIZE CONSTRUCTOR CLASS ##################//
    //###############################################################//

    function  __construct()
    {
        parent::__construct();
    }

    //###############################################################//
    //############### INITIALIZE CONSTRUCTOR CLASS ##################//
    //###############################################################//
    /**
     * @return bool
     */
    function validate_form_data()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('admin_uname', 'email', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('admin_pass', 'password', 'required|xss_clean');

        if ( $this->form_validation->run() == TRUE )
            return TRUE;
        else
            return FALSE;
    }

    //##################################################################//
    //######################  VALIDATION FUNCTION  #####################//
    //##################################################################//

    //##################################################################//
    //######################  CHECK LOGIN DATA  ########################//
    //##################################################################//

    function check_login()
    {

        $sql = 'SELECT id, email, user_type, CONCAT(`first_name`," ",`last_name`) name, account_activation_status 
				  FROM `user` 
				  WHERE 
				    `email` = "'. $this->input->post('admin_uname') .'" AND 
				    `password` ="'. md5($this->input->post('admin_pass')) .'"';

        $query  = $this->db->query($sql);
        $result = $query->row_array();

        if ( count($result) )
        {
            if ( $result['account_activation_status'] == "A" )
            {
                /*************  Set User Session ****************/
                $newdata = array(
                    'userid'    		=> $result['id'],
                    'userType'    		=> $result['user_type'],
                    'email'  			=> $result['email'],
                    'name'  			=> $result['name'],
                    'is_admin_logged_in'=> TRUE
                );
                $this->session->set_userdata($newdata);
                /*************  Set User Session ****************/
                //##############################################//
                //########## UPDATE FIELD LAST LOGIN ###########//
                $postdata = array( 'last_login_datetime' 	=> date('Y-m-d H:i:s'), 'login_status' => 1 );

                $this->db->where('id', $this->session->userdata('userid'));
                $this->db->update('user',$postdata);
                //########## UPDATE FIELD LAST LOGIN ###########//
                //##############################################//
                $result = "login_success";
            }
            else
            {
                if ( $result['account_activation_status'] == "IA" )
                    $result = "inactive";
                else if ( $result['account_activation_status'] == "B" )
                    $result = "blocked";
                else
                    $result = "deleted";
            }
        }
        else
        {
            $result = "invalid_login";
        }

        return $result;
    }

    //##################################################################//
    //######################  CHECK LOGIN DATA  ########################//
    //##################################################################//


    //##################################################################//
    //######################  SET LOGOUT FIELDS  #######################//

    function set_logout_fields()
    {

        $postdata = array(
            'last_logout_datetime' => date('Y-m-d H:i:s'),
            'login_status' => 0
        );

        $this->db->where('id',$this->session->userdata('userid'));
        $this->db->update('user',$postdata);

    }

    //######################  SET LOGOUT FIELDS  #######################//
    //##################################################################//

    //##################################################################//
    //######################  VALIDATION FUNCTION  #####################//
    //##################################################################//

    function validate_form_forgot_password_data()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('admin_uname', 'email', 'trim|required|valid_email');
        if ($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    function regenerate_password($user_id)
    {
        $password = "55H@".rand(1000,9999);
        $to_email = $this->input->post('admin_uname');

        $sql = "SELECT * FROM `user` WHERE email='".$to_email."' AND id='".$user_id."' ";
        $query = $this->db->query($sql);
        if($query->num_rows() >0) //valid email address
        {

            $postdata = array('password' => md5($password),'password_text'=>$password);
            $this->db->where('id',$user_id);
            $this->db->update('user',$postdata);

            //Mail sent to the given email address with reset password
            $this->load->library('email');
            $to_mail = $to_email;
            $subject = "Forgot password - 55newhomes Admin Panel";
            $body='<table  width="600" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="100%">
                                    Hi, <br><br>
                                    We have created a auto generated password for your temporary login.<br>
                                    Please enter your admin account with the same email ('.$to_email.') <br> 
                                    and the password given below. Don\'t forget to reset your password <br> 
                                    after successfull login.

                                    <br><br>
                                    <strong>Password:</strong>&nbsp; '.$password.'
                                    <br><br>
                                    Thanks,<br>
                                    55Newhomes.com Support Team
                                </td>
                              </tr>
                            </table>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                      </tr>
                    </table>';

            $this->email->from('support@55newhomes.com', 'Support Team');
            $this->email->to($to_mail);
            $this->email->subject($subject);
            $this->email->message($body);
            $this->email->set_mailtype('html');
            $this->email->send();

            return 'success';

        }
        else
        {
            return 'email_invalid';
        }

    }




}


?>