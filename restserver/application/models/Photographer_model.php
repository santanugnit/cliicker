<?php

class Photographer_model extends CI_Model
{
    
    ##----------------------------------------##
    ##---------- Load Constructor ------------##
    function __construct()
    {
        parent::__construct();
    }
    ##----------------------------------------##
        

    /**
     * Get total count for pagination.
     * @return mixed
     */
    function get_row_count()
    {
        $sql = 'SELECT * FROM photographers'; /* WHERE user_type = 'EDITOR' AND account_activation_status!='D' */
        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }

    /**
     * GET ALL THE USER LIST AS ARRAY FORMAT.
     * @param null $num
     * @param null $offset
     * @return mixed
     */
    function get_photographer_rows_pagination( $num = NULL, $offset = NULL )
    {
        $sql = 'SELECT * FROM photographers ORDER by id DESC LIMIT '. $offset .','. $num;
        /* WHERE user_type='EDITOR' AND account_activation_status!='D' */

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    /**
     * VALIDATION FUNCTION
     * @return bool
     */
    function validate_form_data()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_name', 'first name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('last_name', 'last name', 'required|trim|xss_clean');

        $this->form_validation->set_rules('gender', 'gender', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'address', 'required|trim|xss_clean');


        if ( $this->uri->segment(3) != 'edit' )
        {
            $ignore_val_arr = array('account_activation_status'=>'D');
            $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|is_unique[photographers.email]',
                array(
                    'is_unique'     => '"'. $this->input->post('email') .'" - this %s already exists.'
                )
            );
            $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[6]|max_length[15]');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|trim|xss_clean|matches[password]');
        }
        else
        {
            if ( $this->input->post("changePass") )
            {
                $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[6]|max_length[15]|matches[conf_password]');
                $this->form_validation->set_rules('conf_password', 'confirm password', 'required|trim|xss_clean');
            }
        }

        if ( $this->form_validation->run() == TRUE )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Add a user.
     * @return string
     */
    function add()
    {

        $featured_image = "";
        if ( $_FILES['featured_image']['name'] != "" ) //If profile image upload
        {
            /********* Rename the upload file ***********/
            $filename_arr       = explode(".", $_FILES['featured_image']['name']);
            $file_extension     = $filename_arr[count($filename_arr) - 1];
            $image_file_name    = time().".".$file_extension;
            /********************************************/
            // set upload configuration
            $config['upload_path']      = "assets/uploaded_files/photographer_profile_image/original/";
            $config['allowed_types']    = "jpg|jpeg|png";
            $config['max_size']         = '1000000'; //KB
            $config['file_name']        = $image_file_name;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('featured_image'))
            {
                return $this->upload->display_errors();
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $featured_image = $data['upload_data']['file_name'];
                //################################################################//
                //################# FUNCTIONALITY OF IMAGE RESIZE ################//
                //***************************************************************//
                //################### MEDIUM IMAGE CREATION ######################//
                $config['image_library']    = 'gd2';
                $config['source_image']     = 'assets/uploaded_files/photographer_profile_image/original/'.$data['upload_data']['file_name'];
                $config['new_image']        = 'assets/uploaded_files/photographer_profile_image/medium/'.$data['upload_data']['file_name'];
                $config['allowed_types']    = "jpg|jpeg|gif";
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = TRUE;
                $config['master_dim']       = 'auto';
               
                ########### CHECK UPLOAD IMAGE DIMENSION ##########
                $imgarr         = $this->config->item('image_valid_dimensions');
                $imgarr_data    = $imgarr["photographer_image_medium"];
                $data_explode   = explode("|", $imgarr_data);

                $define_image_width     = $data_explode[0];
                $define_image_height    = $data_explode[1];
                ########### CHECK UPLOAD IMAGE DIMENSION ##########

                $iw = $data['upload_data']['image_width']; //uploaded image width
                $ih = $data['upload_data']['image_height']; //uploaded image height

                $mw = $define_image_width; //Recommended image width
                $mh = $define_image_height; //Recomended image height

                if ( $iw <= $mw && $ih <= $mh)
                {
                    $ow    = $iw;
                    $oh    = $ih;
                }
                if ( $iw <= $mw && $ih > $mh)
                {
                    $oh    = $mh;
                    $ow    = ($oh*$iw)/$ih;
                }
                if ( $iw > $mw && $ih <= $mh)
                {
                    $ow    = $mw;
                    $oh    = ($ow*$ih)/$iw;
                }
                if ( $iw > $mw && $ih > $mh && $iw > $ih )
                {
                    $ow    = $mw;
                    $oh    = ($ow*$ih)/$iw;
                }
                if ( $iw > $mw && $ih > $mh && $iw <= $ih )
                {
                    $oh    = $mh;
                    $ow    = ($oh*$iw)/$ih ;
                }

                $config['width'] = $ow;
                $config['height'] = $oh;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                //################### MEDIUM IMAGE CREATION ######################//
                //***************************************************************//
                //################# FUNCTIONALITY OF IMAGE RESIZE ################//
                //################################################################//
            }
        }
        ##------------- Upload gallery multiple images --------------##

        $best_photos    = array();
        if($_FILES['best_photos']['name'][0] != "" )
        {
            for ( $i = 0; $i < count($_FILES['best_photos']['name']); $i++)
            {
                if( $_FILES['best_photos']['name'][$i] != "" )
                {
                    $_FILES['best_photo']['name'] = $_FILES['best_photos']['name'][$i];
                    $_FILES['best_photo']['type'] = $_FILES['best_photos']['type'][$i];
                    $_FILES['best_photo']['tmp_name'] = $_FILES['best_photos']['tmp_name'][$i];
                    $_FILES['best_photo']['error'] = $_FILES['best_photos']['error'][$i];
                    $_FILES['best_photo']['size'] = $_FILES['best_photos']['size'][$i];

                    $img_name = $_FILES['best_photo']['name'];
                    /********* Rename the upload file ***********/
                    $filename_arr = explode(".", $img_name);
                    $file_extension = $filename_arr[count($filename_arr) - 1];
                    $img_name = time().".".$file_extension;
                    /********************************************/

                    // set upload configuration
                    $config['upload_path'] = "assets/uploaded_files/photographer_profile_image/original/";
                    $config['allowed_types'] = "jpg|jpeg|png";
                    $config['max_size'] = '1000000'; //KB
                    $config['file_name'] = $img_name;

                    $this->upload->initialize($config);

                    if(!$this->upload->do_upload('best_photo'))
                    {
                        return $this->upload->display_errors();
                    }
                    else
                    {

                        $data = array('upload_data' => $this->upload->data());
                        $img_name = $data['upload_data']['file_name'];

                        //***************************************************************//
                        //################### MEDIUM IMAGE CREATION ######################//

                        $config['image_library']    = 'gd2';
                        $config['source_image']     = 'assets/uploaded_files/photographer_profile_image/original/'.$img_name;
                        $config['new_image']        = 'assets/uploaded_files/photographer_profile_image/medium/'.$img_name;
                        $config['allowed_types']    = "jpg|jpeg|png";
                        $config['create_thumb']     = FALSE;
                        $config['maintain_ratio']   = TRUE;
                        $config['master_dim']       = 'auto';
                        //###################################################//
                        //########### CHECK UPLOAD IMAGE DIMENSION ##########//
                        $imgarr         = $this->config->item('image_valid_dimensions');
                        $imgarr_data    = $imgarr["photographer_image_medium"];
                        $data_explode   = explode("|", $imgarr_data);

                        $define_image_width     = $data_explode[0];
                        $define_image_height    = $data_explode[1];
                        //########### CHECK UPLOAD IMAGE DIMENSION ##########//
                        //###################################################//

                        $iw = $data['upload_data']['image_width']; //uploaded image width
                        $ih = $data['upload_data']['image_height']; //uploaded image height

                        $mw = $define_image_width; //Recommended image width
                        $mh = $define_image_height; //Recomended image height

                        if($iw<=$mw && $ih <= $mh)
                        {
                            $ow    = $iw;
                            $oh    = $ih;
                        }
                        if($iw<=$mw && $ih > $mh)
                        {
                            $oh    = $mh;
                            $ow    =($oh*$iw)/$ih;
                        }
                        if($iw>$mw && $ih <= $mh)
                        {
                            $ow    = $mw;
                            $oh    = ($ow*$ih)/$iw;
                        }
                        if($iw>$mw && $ih > $mh && $iw>$ih )
                        {
                            $ow    = $mw;
                            $oh    = ($ow*$ih)/$iw;
                        }
                        if($iw>$mw && $ih > $mh && $iw<=$ih)
                        {
                            $oh    = $mh;
                            $ow    = ($oh*$iw)/$ih ;
                        }

                        $config['width']    = $ow;
                        $config['height']   = $oh;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();

                        //################### MEDIUM IMAGE CREATION ######################//
                        //***************************************************************//

                        $best_photos[]  = $img_name;

                    }
                }
            }
        }
        ##---------------------------------------------------------------##
        $reg_number = time() .'_'. rand(99, 999999);
        //############## Insert post value into user table ##################//
        $postdata = array(
                        'first_name'        => $this->input->post('first_name'),
                        'last_name'         => $this->input->post('last_name'),
                        'address'           => $this->input->post('address'),
                        'email'             => $this->input->post('email'),
                        'contact_number'    => $this->input->post('contact_number'),
                        'featured_image'    => $featured_image,
                        'best_photos'       => (( is_array($best_photos) && count($best_photos) > 0 )? implode(',', $best_photos):""),
                        'password'          => md5($this->input->post('password')),
                        'gender'            => $this->input->post('gender'),
                        'creation_datetime' => date('Y-m-d H:i:s'),
                        'reg_number'        => $reg_number,
                        'added_date'        => date('Y-m-d H:i:s')
                    );
        $this->db->insert('photographers', $postdata);
        $last_id = $this->db->insert_id();

        if( $last_id ){
            $this->db->where('id', $last_id);
            $this->db->update('photographers', array( 'pg_clicker_code' => 'CLR'.date('dmy').$last_id ));
        }

        return "success";
    }

    /**
     * Edit a User
     * @param $photographer_id
     * @return string
     */
    function edit( $photographer_id)
    {
        if( $_FILES['featured_image']['name'] != "" ) //If profile image upload
        {
            /********* Rename the upload file ***********/
            $filename_arr       = explode(".", $_FILES['featured_image']['name']);
            $file_extension     = $filename_arr[count($filename_arr) - 1];
            $image_file_name    = time().".".$file_extension;
            /********************************************/

            // set upload configuration
            $config['upload_path']      = "assets/uploaded_files/photographer_profile_image/original/";
            $config['allowed_types']    = "jpg|jpeg|png";
            $config['max_size']         = '1000000'; //KB
            $config['file_name']        =  $image_file_name;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('featured_image'))
            {
                return $this->upload->display_errors();
            }
            else
            {

                $data           = array('upload_data' => $this->upload->data());
                $profile_image  = $data['upload_data']['file_name'];
                //################################################################//
                //################# FUNCTIONALITY OF IMAGE RESIZE ################//
                //***************************************************************//
                //################### MEDIUM IMAGE CREATION ######################//
                $config['image_library']    = 'gd2';
                $config['source_image']     = 'assets/uploaded_files/photographer_profile_image/original/'.$data['upload_data']['file_name'];
                $config['new_image']        = 'assets/uploaded_files/photographer_profile_image/medium/'.$data['upload_data']['file_name'];
                $config['allowed_types']    = "jpg|jpeg|gif";
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = TRUE;
                $config['master_dim']       = 'auto';

                //###################################################//
                //########### CHECK UPLOAD IMAGE DIMENSION ##########//
                $imgarr         = $this->config->item('image_valid_dimensions');
                $imgarr_data    = $imgarr["photographer_image_medium"];
                $data_explode   = explode("|",$imgarr_data);

                $define_image_width     = $data_explode[0];
                $define_image_height    = $data_explode[1];
                //########### CHECK UPLOAD IMAGE DIMENSION ##########//
                //###################################################//

                $iw= $data['upload_data']['image_width']; //uploaded image width
                $ih= $data['upload_data']['image_height']; //uploaded image height

                $mw = $define_image_width; //Recommended image width
                $mh = $define_image_height; //Recomended image height

                if ($iw <= $mw && $ih <= $mh)
                {
                    $ow    = $iw;
                    $oh    = $ih;
                }
                if ($iw <= $mw && $ih > $mh)
                {
                    $oh    = $mh;
                    $ow    =($oh*$iw)/$ih;
                }
                if ($iw > $mw && $ih <= $mh)
                {
                    $ow    = $mw;
                    $oh    = ($ow*$ih)/$iw;
                }
                if ($iw > $mw && $ih > $mh && $iw > $ih )
                {
                    $ow    = $mw;
                    $oh    = ($ow*$ih)/$iw;
                }
                if ($iw > $mw && $ih > $mh && $iw <= $ih)
                {
                    $oh    = $mh;
                    $ow    = ($oh*$iw)/$ih ;
                }

                $config['width'] = $ow;
                $config['height'] = $oh;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                //################### MEDIUM IMAGE CREATION ######################//
                //***************************************************************//

                //################# FUNCTIONALITY OF IMAGE RESIZE ################//
                //################################################################//


                //################################################################//
                //################# DELETE EXISTING IMAGE IF ANY #################//

                if($this->input->post('existing_featured_image') != "")
                {
                    unlink('assets/uploaded_files/photographer_profile_image/medium/'.$this->input->post('existing_featured_image'));
                    unlink('assets/uploaded_files/photographer_profile_image/original/'.$this->input->post('existing_featured_image'));
                }
            }
        }
        else
        {
            $profile_image = $this->input->post('existing_featured_image');
        }

        $postdata = array(
                        'first_name'        => $this->input->post('first_name'),
                        'last_name'         => $this->input->post('last_name'),
                        'address'           => $this->input->post('address'),
                        'contact_number'    => $this->input->post('contact_number'),
                        'featured_image'    => $profile_image,
                        'gender'            => $this->input->post('gender'),
                    );

        $this->db->where('id', $photographer_id);
        $this->db->update('photographers', $postdata);
        return "edit_success";
    }

    /**
     * GET Expert Details.
     * @param $p_value_id
     * @return mixed
     */
    function get_photographer_details( $p_value_id)
    {
        $sql = 'SELECT p.*, a.id as albumID, a.album_code, a.creation_datetime, a.occasion_id, a.album_name 
                  FROM photographers p
                  LEFT JOIN albums a ON a.photographer_id = p.id
                  WHERE p.id="'. $p_value_id .'"';

        $query = $this->db->query($sql);
        $result = $query->result_array();
        if( !empty($result) )
        {
            $pgRS   = array();
            foreach ($result as $pgArr )
            {
                $pgRS["id"]                 = $pgArr["id"];
                $pgRS["pg_clicker_code"]    = $pgArr["pg_clicker_code"];
                $pgRS["first_name"]         = $pgArr["first_name"];
                $pgRS["last_name"]          = $pgArr["last_name"];
                $pgRS["address"]            = $pgArr["address"];
                $pgRS["email"]              = $pgArr["email"];
                $pgRS["contact_number"]     = $pgArr["contact_number"];
                $pgRS["best_photos"]        = $pgArr["best_photos"];
                $pgRS["featured_image"]     = $pgArr["featured_image"];
                $pgRS["gender"]             = $pgArr["gender"];
                $pgRS["creation_datetime"]  = $pgArr["creation_datetime"];
                $pgRS["last_login_datetime"]   = $pgArr["last_login_datetime"];
                $pgRS["reg_number"]         = $pgArr["reg_number"];
                $pgRS["is_email_verified"]  = $pgArr["is_email_verified"];
                $pgRS["account_activation_status"]   = $pgArr["account_activation_status"];
                $pgRS["added_date"]         = $pgArr["added_date"];
                $pgRS["updated_date"]       = $pgArr["updated_date"];

                if( (int)$pgArr["albumID"] > 0 )
                {
                    $pgRS["album_dtls"][$pgArr["albumID"]]["albumID"]     = $pgArr["albumID"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["album_code"]  = $pgArr["album_code"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["occasion_id"] = $pgArr["occasion_id"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["album_name"]  = $pgArr["album_name"];
                }
            }
            return $pgRS;
        }

        return $result;
    }

    /**
     * delete image
     * @param $pId
     * @param $fname
     */
    function delete_image( $pId, $fname)
    {
        $this->db->where('id',$pId);
        $this->db->update('user',array('profile_image'=>''));

         ##--------------------- Delete images from its respective paths -------------------##
         unlink('assets/uploaded_files/photographer_profile_image/medium/'.$fname);
         unlink('assets/uploaded_files/photographer_profile_image/original/'.$fname);
         ##---------------------------------------------------------------------------------##
    }

    /**
     * @param $arr_val
     * @return string
     */
    function delete($arr_val)
    {
        $this->load->library('common_functions');
        $arr_msg    = array();
        $str_msg    = "";
        foreach($arr_val as $ids)
        {
            $filename = $this->common_functions->get_value('featured_image','photographers','id="'.$ids.'"');
            if ( $filename != "" ) // Featured image exists
            {
                ##-------- Delete location image from its respective paths ----------##
                unlink('assets/uploaded_files/photographer_profile_image/medium/'.$filename);
                unlink('assets/uploaded_files/photographer_profile_image/original/'.$filename);
                ##-------------------------------------------------------------------##
            }
            $this->db->where('id', $ids);
            $this->db->update( 'photographers', array("account_activation_status" => "D") );
            /* $this->db->delete('photographers',array('id' => $ids)); */
        }

        return "success";
    }


    /**
     * get all values in array.
     * @param string $limit
     * @return mixed
     */
    function get_all_photographers( $limit = "")
    {
        $result = array();
        $sql = 'SELECT * FROM photographers WHERE account_activation_status	= "V" ORDER BY id DESC';

        if ( $limit != "" ) {
            $sql .=" limit 0,".$limit;
        }
        $query = $this->db->query($sql);
        if (count($query->result_array()) )
        {
            foreach ( $query->result_array() as $val )
            {
                $result[$val["id"]] = $val;
            }
        }
        //return $query->result_array();
        return $result;
    }

    ##----------------------------------------------------------------##
    ##------------------  Delete featured Image  ---------------------##
    ##----------------------------------------------------------------##
    function delete_featured_image($rowId, $filename)
    {
        $this->db->where('id', $rowId);
        $this->db->update('photographers', array('featured_image' => ''));

        ##-------- Delete image from its respective paths ----------##
        unlink('assets/uploaded_files/photographer_profile_image/medium/'.$filename);
        unlink('assets/uploaded_files/photographer_profile_image/original/'.$filename);
        ##----------------------------------------------------------##
    }


    /**
     * @param $arr_val
     * @param $status_val
     * @return string
     */
    function change_status_value($arr_val, $status_val)
    {
        foreach($arr_val as $id)
        {
            $this->db->where('id', $id);
            $this->db->update('photographers',array('account_activation_status' => $status_val));
        }

        return "success";
    }


    public function check_photographer_login($data = array())
    {
        $result = array();

        if( !isset($data["email"]) || trim($data["email"]) == '' || !isset($data["password"]) || trim($data["password"]) == '' ){
            return $result;
        }

        $sql = 'SELECT *, CONCAT(`first_name`," ",`last_name`) full_name 
				  FROM `photographers` 
				  WHERE 
                      `email` = "'. $data["email"]  .'" AND  
                      `password` ="'. md5($data["password"]) .'"';

        $query  = $this->db->query($sql);
        $result = $query->row_array();
        //return $this->db->last_query();
        return $result;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function update_photographer_last_login( $id = 0)
    {
        if( (int)$id < 1 ) {
            return false;
        }
        $postdata = array( 'last_login_datetime' 	=> date('Y-m-d H:i:s') );
        $this->db->where('id', $id);
        $this->db->update('photographers',$postdata);

        if( $this->db->affected_rows() ){
            return true;
        } else{
            return false;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function api_add_photographer( $data = array())
    {
        if( empty($data) ) {
            return false;
        }

        $this->db->insert('photographers', $data);
        $last_id = $this->db->insert_id();
        if( $last_id ) {
            $this->db->where('id', $last_id);
            $this->db->update('photographers', array( 'pg_clicker_code' => 'CLR'.date('dmy').$last_id ));
        }
        return $last_id;
    }

    /**
     * @param array $data
     * @return array|bool
     */
    public function api_get_photographer_details_id( $data = array())
    {
        $result     = array();
        if( empty($data) || !isset($data["pgID"]) || (int)$data["pgID"] < 1 ) {
            return false;
        }
        $imgURL = base_url().'assets/uploaded_files/photographer_profile_image/medium/';

        $this->db->where("pg.id", $data["pgID"]);
        $this->db->select('pg.*, "'.$imgURL.'" as img_url, R.avg_rating, alb.no_albums, followers.no_followers, followers.follower_ids, following.no_following, following.following_ids');
        $this->db->from('photographers pg');
        $this->db->join('(SELECT ar.photographer_id, AVG(ar.rating) avg_rating FROM photographer_rating AS ar GROUP BY ar.photographer_id) as R ', 'pg.id = R.photographer_id', 'left');
        $this->db->join('(SELECT photographer_id, count(*) as no_albums FROM albums GROUP BY photographer_id) as alb', 'pg.id = alb.photographer_id', 'left');

        $this->db->join('(SELECT followed_id, count(*) as no_followers, GROUP_CONCAT(follower_id) as follower_ids FROM pg_followers GROUP BY followed_id) as followers', 'pg.id = followers.followed_id', 'left');

        $this->db->join('(SELECT follower_id, count(*) as no_following, GROUP_CONCAT(followed_id) as following_ids FROM pg_followers GROUP BY follower_id) as following', 'pg.id = following.follower_id', 'left');

        $query = $this->db->get();

        //$query = $this->db->get_where('photographers', array('id' => $data["pgID"]));
        $result = $query->row_array();
        return $result;
    }

    /**
     * @param array $data
     * @param int $pgID
     * @return bool
     */
    public function api_edit_photographer_profile( $data = array(), $pgID = 0 )
    {
        if( empty($data) || !isset($pgID) || (int)$pgID < 1 ) {
            return false;
        }

        $this->db->where('id', $pgID);
        $this->db->update('photographers', $data);
        if( $this->db->affected_rows() ){
            return true;
        } else{
            return false;
        }
    }

    /**
     * @param null $old_pwd
     * @param int $pgID
     * @return bool
     */
    public function is_photographer_current_password_correct( $old_pwd = null, $pgID = 0)
    {
        if( is_null($old_pwd) || trim($old_pwd) == '' || (int)$pgID < 1 ) {
            return false;
        }

        $sql = 'SELECT id, pg_clicker_code, first_name FROM photographers WHERE id = "'.$pgID.'" AND password = "'. md5($old_pwd).'"';

        $query = $this->db->query($sql);
        //$result = $query->row_array();

        if( $query->num_rows() > 0 )
            return true;
        else
            return false;

    }

    /**
     * @param array $data
     * @param int $pgID
     * @return bool
     */
    public function api_change_photographer_password( $data = array(), $pgID = 0 )
    {
        if( empty($data) || !isset($pgID) || (int)$pgID < 1 ) {
            return false;
        }

        $this->db->where('id', $pgID);
        $this->db->update('photographers', $data);
        if( $this->db->affected_rows() ){
            return true;
        } else {
            return false;
        }
    }

    public function api_get_photographer_by_emailID( $email = '' )
    {
        if( trim($email) == '' ) {
            return true;
        }

        $this->db->select('email');
        $this->db->from('photographers');
        $this->db->where( 'email', $email );
        $num_results = $this->db->count_all_results();

        if( $num_results > 0 ){
            return true;
        } else {
            return false;
        }
        /*$this->db->where('email', $email);
        $this->db->update('photographers', $data);
        if( $this->db->affected_rows() ){
            return true;
        } else {
            return false;
        }*/
    }

    public function api_add_photographer_invitation( $data = array() )
    {
        if( empty($data) || !isset($data['visitor']) || trim($data['visitor']) == '' || !isset($data['pg_email']) || trim($data['pg_email']) == '' ) { 
            return false;
        }

        $ins['sender']          = $data['visitor'];
        $ins['new_pg_email']    = $data['pg_email'];
        $ins['added_datetime']  = date("Y-m-d H:i:s");

        $this->db->insert('photographer_invititation', $ins);
        return $this->db->insert_id();
    }

    public function api_get_photographer_lists( $data = array() )
    {
        $result = array();

        $imgURL = base_url().'assets/uploaded_files/photographer_profile_image/medium/';

        $cond = ' WHERE 1 = 1 ';
        $selStr = '';
        /*return "Test"; */
        if( isset($data['pg_name']) && trim($data['pg_name']) != '' ){
            $cond   .= ' AND pg.first_name LIKE "%'.$data['pg_name'].'%"';
        }
        if( isset($data['pg_budget']) && trim($data['pg_budget']) != '' ){
            $budgetArr = explode(',', $data['pg_budget']);
            $min_budget = ( isset($budgetArr[0]) )? (int)$budgetArr[0] : 0;
            $max_budget = ( isset($budgetArr[1]) )? (int)$budgetArr[1] : 0;
            $cond   .= ' AND (pg.budget  BETWEEN "'.$min_budget.'" AND "'.$max_budget.'") ';
        }
        if( isset($data['ratings']) && trim($data['ratings']) != '' ){            
            $cond   .= ' AND R.avg_rating IN ('.trim($data['ratings']).')';
        }
        
        if( isset($data["pg_lat"]) && trim($data["pg_lat"]) != '' && isset($data["pg_long"]) && trim($data["pg_long"]) != '' ){
            $cond   .= ' AND ((((acos(
                    sin(("'.$data['pg_lat'].'" * pi()/180)) * 
                    sin((pg.pg_lat * pi()/180)) + cos(("'.$data['pg_lat'].'" * pi()/180)) * 
                    cos((pg.pg_lat * pi()/180)) * cos((("'.$data['pg_long'].'" - pg.pg_long) * pi()/180))
                    )) * 180/pi()) * (60 * 1.1515 * 1.609344)
                ) <= 50)';

            $selStr .= ', (((acos(
                sin(("'.$data['pg_lat'].'" * pi()/180)) * 
                sin((pg.pg_lat * pi()/180)) + cos(("'.$data['pg_lat'].'" * pi()/180)) * 
                cos((pg.pg_lat * pi()/180)) * cos((("'.$data['pg_long'].'" - pg.pg_long) * pi()/180))
                    )) * 180/pi()) * (60 * 1.1515 * 1.609344)
                ) as distance';
        }  

        /*$sql = 'SELECT pg.id, pg.pg_clicker_code, pg.first_name, pg.last_name, pg.address, pg.pg_lat, pg.pg_long, pg.email, pg.password, pg.contact_number, pg.best_photos, pg.featured_image, pg.gender, pg.budget, pg.creation_datetime, pg.last_login_datetime, pg.reg_number, pg.is_email_verified, pg.account_activation_status, pg.added_date, pg.updated_date, "'.$imgURL.'" AS img_url, 
            (
                SELECT AVG(ar.rating)
                FROM photographer_rating AS ar
                WHERE ar.photographer_id = pg.id
            ) AS avg_rating,
            (
                SELECT count(*)
                FROM albums AS alb
                WHERE alb.photographer_id = pg.id
            ) AS no_albums
            FROM  
                photographers pg';*/

        $sql = 'SELECT pg.id, pg.pg_clicker_code, pg.first_name, pg.last_name, pg.address, pg.pg_lat, pg.pg_long, pg.email, pg.password, pg.contact_number, pg.best_photos, pg.featured_image, pg.gender, pg.budget, pg.creation_datetime, pg.last_login_datetime, pg.reg_number, pg.is_email_verified, pg.account_activation_status, pg.added_date, pg.updated_date, "'.$imgURL.'" AS img_url, R.avg_rating, alb.no_albums '.$selStr.' 
            FROM  
                photographers pg
            LEFT JOIN (SELECT ar.photographer_id, AVG(ar.rating) avg_rating FROM photographer_rating AS ar GROUP BY ar.photographer_id) as R ON pg.id = R.photographer_id 
            LEFT JOIN (SELECT photographer_id, count(*) as no_albums FROM albums GROUP BY photographer_id) as alb ON pg.id = alb.photographer_id';

        $sql .= $cond;
        //return $sql;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        
        return $result;
    }

    public function api_get_photographer_notification( $data = array() )
    {
        if( empty($data) || !isset($data["pgID"]) || (int)$data["pgID"] < 1 ) {
            return false;
        }

        $sql = 'SELECT NF.*, NF_obj.entity_type_id, NF_obj.entity_id, PG_SUGG.name, PG_SUGG.email, PG_SUGG.contact_no, PG_SUGG.desc, PG_SUGG.added_datetime 
                FROM notification NF
                INNER JOIN notification_object NF_obj ON NF_obj.id = NF.notification_object_id
                INNER JOIN photographer_template_suggest PG_SUGG ON PG_SUGG.id = NF_obj.entity_id
                WHERE NF.notifier_id = '. $data["pgID"] .' AND NF.notification_status = "unread"';

        $query = $this->db->query($sql);
        $result = $query->result_array();
                
        return $result;
    }


    public function api_pg_add_new_follower( $data = array() )
    {
        if( empty($data) || !isset($data["pgID"]) || (int)$data["pgID"] < 1  || !isset($data["followed_id"]) || (int)$data["followed_id"] < 1 ) {
            return false;
        }
        $postdata["follower_id"]    = $data["pgID"];
        $postdata["followed_id"]    = $data["followed_id"];
        $postdata["added_datetime"]    = date("Y-m-d H:i:s");

        $this->db->where('follower_id', $postdata["follower_id"]);
        $this->db->where('followed_id', $postdata["followed_id"]);
        $this->db->where('status', '1');
        $q = $this->db->get('pg_followers');
        if ( $q->num_rows() < 1 ) 
        {
            $this->db->insert('pg_followers', $postdata);
            return $this->db->insert_id();
        } else {
            return 0;
        }        
    }


    public function api_pg_remove_follower( $data = array() )
    {
        if( empty($data) || !isset($data["pgID"]) || (int)$data["pgID"] < 1  || !isset($data["followed_id"]) || (int)$data["followed_id"] < 1 ) {
            return false;
        }

        $postdata["follower_id"]        = $data["pgID"];
        $postdata["followed_id"]        = $data["followed_id"];
        //return $data;
        $this->db->where('follower_id', $postdata["follower_id"]);
        $this->db->where('followed_id', $postdata["followed_id"]);
        $this->db->where('status', '1');
        $this->db->limit(1);
        /*$this->db->delete('pg_followers');*/  
        $this->db->update('pg_followers', array("status" => 0, "unfollow_date" => date("Y-m-d H:i:s")) );
        
        if ( !$this->db->affected_rows() ) {
            $result = 'Error! row where ID ['. $postdata["follower_id"] . ' and  ' . $postdata["followed_id"] .'] not found.';
        } else {
            $result = 'Success';
        }
        return $result;        
    }

    function api_get_multiple_photographer_details( $data = array() )
    {
        $result     = array();
        if( empty($data) || !isset($data["pg_ids"]) || empty( explode(",", $data["pg_ids"]) ) ) {
            return false;
        }
        $imgURL = base_url().'assets/uploaded_files/photographer_profile_image/medium/';

        //$this->db->where("pg.id", $data["pgID"]);
        $this->db->where_in("pg.id", $data["pg_ids"], FALSE);

        $this->db->select('pg.*, "'.$imgURL.'" as img_url, R.avg_rating, alb.no_albums, followers.no_followers, followers.follower_ids, following.no_following, following.following_ids');
        $this->db->from('photographers pg');

        $this->db->join('(SELECT ar.photographer_id, AVG(ar.rating) avg_rating FROM photographer_rating AS ar GROUP BY ar.photographer_id) as R ', 'pg.id = R.photographer_id', 'left');

        $this->db->join('(SELECT photographer_id, count(*) as no_albums FROM albums GROUP BY photographer_id) as alb', 'pg.id = alb.photographer_id', 'left');

        $this->db->join('(SELECT followed_id, count(*) as no_followers, GROUP_CONCAT(follower_id) as follower_ids FROM pg_followers GROUP BY followed_id) as followers', 'pg.id = followers.followed_id', 'left');

        $this->db->join('(SELECT follower_id, count(*) as no_following, GROUP_CONCAT(followed_id) as following_ids FROM pg_followers GROUP BY follower_id) as following', 'pg.id = following.follower_id', 'left');

        $query = $this->db->get();
        //return $this->db->last_query();
        //$query = $this->db->get_where('photographers', array('id' => $data["pgID"]));
        $result = $query->result_array();
        return $result;
    }



}
