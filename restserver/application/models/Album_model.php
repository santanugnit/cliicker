<?php

class Album_model extends CI_Model
{

    ##----------------------------------------##
    ##---------- Load Constructor ------------##
    function __construct()
    {
        parent::__construct();

    }
    ##----------------------------------------##

    /**
     * @param array $data
     * @return mixed
     */
    function album_file_upload( $data = array())
    {
        $this->db->insert('album_uploaded_files', $data);
        return $this->db->insert_id();
    }

    /**
     * @param int $id
     * @return int
     */
    function get_album_uploaded_file_details_by_id( $id = 0 )
    {
        if( $id == null || (int)$id < 1 ) {
            return 0;
        }
        $query = $this->db->get_where('album_uploaded_files', array('id' => $id));
        foreach( $query->result_array() as $row )
        {
            return $row["upload_file_size"];
        }
        return 0;
    }

    /**
     * @param array $data
     * @param int $album_id
     * @return bool
     */
    function update_album_data( $data = array(), $album_id = 0 )
    {
        if( (int)$album_id < 1 || empty($data) || !isset($data["album_code"]) || trim($data["album_code"]) == "" ) {
            return false;
        }
        $fileSize = 0;
        if ( isset($data["upload_ids"]) && trim($data["upload_ids"]) != "" )
        {
            $upload_id_arr  = explode(",", $data["upload_ids"]);
            if( is_array($upload_id_arr) && !empty($upload_id_arr) )
            {
                foreach ( $upload_id_arr as $ids )
                {
                    $this->db->where('id', $ids);
                    $this->db->update('album_uploaded_files', array("album_id" => $album_id));
                    $fileSize += $this->get_album_uploaded_file_details_by_id($ids);
                }
            }
        }
        $updateData = array("album_code" => $data["album_code"], "space_usage" => $fileSize);
        /*$updateData["album_code"]   = $data["album_code"];
        $updateData["space_usage"]   = $fileSize;*/

        $this->db->where('id', $album_id);
        $this->db->update('albums', $updateData);

        if ($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }

    /**
     * @param array $data
     * @param int $album_id
     * @param int $prev_space_usage
     * @return bool
     */
    function update_album_file_upload_data_on_edit_operation( $data = array(), $album_id = 0, $prev_space_usage = 0 )
    {
        if( (int)$album_id < 1 || empty($data) || !isset($data["upload_ids"]) || trim($data["upload_ids"]) == "" ) {
            return false;
        }

        $fileSize = 0;
        $upload_id_arr  = explode(",", $data["upload_ids"]);
        if( is_array($upload_id_arr) && !empty($upload_id_arr) )
        {
            foreach ( $upload_id_arr as $ids )
            {
                $this->db->where('id', $ids);
                $this->db->update('album_uploaded_files', array("album_id" => $album_id));
                $fileSize += $this->get_album_uploaded_file_details_by_id($ids);
            }
            $fileSize += (int)$prev_space_usage;
            $this->db->where('id', $album_id);
            $this->db->update('albums', array("space_usage" => $fileSize) );

            if ($this->db->affected_rows() > 0)
                return true;
            else
                return false;
        }

        return false;

    }

    ##----------------------------------------------------##
    ##---------- Get total count for pagination ----------##
    ##----------------------------------------------------##
    function get_row_count()
    {
        $this->db->select('*');
        $query = $this->db->get("albums");

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }


    ##-----------------------------------------------------------------------##
    ##---------------- Get all the record list with pagination --------------##
    ##-----------------------------------------------------------------------##
    /**
     * @param null $num
     * @param null $offset
     * @return mixed
     */
    function get_rows_pagination($num = NULL, $offset = NULL)
    {
        $sql = 'SELECT P.*, 
                  GROUP_CONCAT(CONCAT(AUF.id, "@@", AUF.upload_file_name, "@@", AUF.upload_file_size ) SEPARATOR "||") as upload_file_data   
                FROM `albums` P INNER JOIN `album_uploaded_files` AUF
                    ON P.id = AUF.album_id
                GROUP BY P.id
                ORDER BY P.id DESC LIMIT '.$offset.','.$num;
        $query = $this->db->query($sql);

        return $query->result_array();
    }



    ##---------------------------------------------------------------------------##
    ##------------------------  VALIDATION FUNCTION  ----------------------------##
    ##---------------------------------------------------------------------------##
    function validate_form_data()
    {
        $ignore_val_arr = array();
        if( $this->uri->segment(4)!= "" )
        {
            $ignore_val_arr['id'] = $this->uri->segment(4);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('album_name', 'album name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('album_price', 'album price', 'required|trim|xss_clean');
        if( $this->uri->segment(3) == 'edit' )
        {
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|callback_subdomain_name_check_unique_update["'. $this->uri->segment(4) .'"]');
        } else {
            $this->form_validation->set_rules('photographer_id', 'select phptographer', 'required|trim|xss_clean');
            $this->form_validation->set_rules('preferred_subdomain_name', 'subdomain name', 'trim|required|max_length[50]|alpha_dash|is_unique[albums.preferred_subdomain_name]',
                array('is_unique' => 'This %s already exists. Please type another name.')
            );
        }
        if($this->form_validation->run() == TRUE)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    ##------------------------------------------------------------##
    ##--------------------  Add a record  ------------------------##
    ##------------------------------------------------------------##
    function add($postdata)
    {
        $this->db->insert('albums',$postdata);
        return $this->db->insert_id();
    }


    ##--------------------------------------------------##
    ##---------  EDIT record respect to id  ------------##
    ##--------------------------------------------------##
    function edit($row_id, $postdata)
    {
        $this->db->where('id', $row_id);
        $this->db->update('albums', $postdata);
        if ( $this->db->affected_rows() )
        {
            return true;
        } else {
            return false;
        }
    }

    ##----------------------------------------------------------------##
    ##------------------  Delete featured Image  ---------------------##
    ##----------------------------------------------------------------##
    function delete_featured_image($rowId,$filename)
    {
        $this->db->where('id',$rowId);
        $this->db->update('albums',array('featured_image_file'=>''));

        ##-------- Delete image from its respective paths ----------##
        unlink('assets/uploaded_files/portfolio_image/medium/'.$filename);
        unlink('assets/uploaded_files/portfolio_image/original/'.$filename);
        ##----------------------------------------------------------##

    }

##----------------------------------------------------------------##
##------------------  Delete featured Image 2  -------------------##
##----------------------------------------------------------------##
    function delete_featured_image_2($rowId,$filename)
    {
        $this->db->where('id',$rowId);
        $this->db->update('albums',array('featured_image_file_2'=>''));

        ##-------- Delete image from its respective paths ----------##
        unlink('assets/uploaded_files/portfolio_image/medium/'.$filename);
        unlink('assets/uploaded_files/portfolio_image/original/'.$filename);
        ##----------------------------------------------------------##

    }

    ##----------------------------------------------------------------##
    ##-----------------  Delete case study image  --------------------##
    ##----------------------------------------------------------------##
    function delete_case_study_file($rowId,$filename)
    {
        $this->db->where('id',$rowId);
        $this->db->update('albums',array('case_study_file'=>''));

        ##-------- Delete file from its respective paths ----------##
        unlink('assets/uploaded_files/case_study/'.$filename);
        ##---------------------------------------------------------##
    }

    ##---------------------------------------------------##
    ##----------- Get details respect to id  ------------##
    ##---------------------------------------------------##
    /**
     * @param $row_id
     * @return mixed
     */
    function get_details($row_id)
    {
        $sql = 'SELECT P.*, 
                  GROUP_CONCAT(CONCAT(AUF.id, "@@", AUF.upload_file_name, "@@", AUF.upload_file_size ) SEPARATOR "||") as upload_file_data   
                FROM `albums` P 
                    INNER JOIN `album_uploaded_files` AUF ON P.id = AUF.album_id
                WHERE P.id ="'.$row_id.'"                 
                    GROUP BY P.id';
        $query = $this->db->query($sql);

        return $query->row_array();

    }

    ##-------------------------------------------##
    ##-------------  Change Status  -------------##
    ##-------------------------------------------##
    /**
     * @param $arr_val
     * @param $status_val
     * @return string
     */
    function change_status_value($arr_val, $status_val)
    {
        foreach($arr_val as $id)
        {
            $this->db->where('id', $id);
            $this->db->update('albums', array('is_deleted' => $status_val));
        }

        return "success";
    }


    ##---------------------------------------------------##
    ##----------------  Delete Record(s)  ---------------##
    ##---------------------------------------------------##
    /**
     * @param $arr_val
     * @return string
     */
    function delete($arr_val)
    {
        $this->load->library('common_functions');
        foreach($arr_val as $ids)
        {
            $filename = $this->common_functions->get_value('featured_image_file','albums','id="'.$ids.'"');
            if($filename!="") // Featured image exists
            {
                ##-------- Delete location image from its respective paths ----------##
                unlink('assets/uploaded_files/portfolio_image/medium/'.$filename);
                unlink('assets/uploaded_files/portfolio_image/original/'.$filename);
                ##-------------------------------------------------------------------##
            }
            //$this->db->delete('albums',array('id'=>$ids));

        }

        //return $arr_msg;
    }

    ##----------------------------------------------------------------##
    ##---------------  Download data to CSV  -------------------------##
    ##----------------------------------------------------------------##
    function download_csv()
    {
        $sql = "SELECT * FROM `albums`";
        $query = $this->db->query($sql);
        $this->load->helper('csv');
        query_to_csv($query, TRUE, 'portfolio_list.csv');
        return TRUE;
    }

    ##------------------------------------------------------------------##
    ##-------  Get all the record list filtered with category id  ------##
    ##------------------------------------------------------------------##
    function get_record_list($catid)
    {
        $sql = "SELECT id, project_name, description, features, 
                if(featured_image_file!='', CONCAT('".base_url()."assets/uploaded_files/portfolio_image/medium/',featured_image_file),'') as featured_image_filepath,
                if(featured_image_file_2!='', CONCAT('".base_url()."assets/uploaded_files/portfolio_image/medium/',featured_image_file_2),'') as featured_image_filepath_2, case_study_file, live_url,
                 if(case_study_file!='', CONCAT('".base_url()."assets/uploaded_files/case_study/',case_study_file),'') as case_study_filepath
                FROM `albums`
                WHERE status='Y' AND portfolio_category_id='".$catid."' ORDER BY id DESC";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        return $result;

    }

    /**
     * @param array $data
     * @return bool
     */
    function get_album_details_by_albumID_pgID( $data = array() )
    {
        if( empty($data) || (int)$data["id"] < 1 || (int)$data["photographer_id"] < 1 ) {
            return false;
        }
        $query = $this->db->get_where('albums', array('id' => $data["id"], 'photographer_id' => $data["photographer_id"]));

        $result = $query->row_array();
        return $result;
    }

    /**
     * @param int $pgID
     * @return array
     */
    function get_pg_album_lists_by_pgID( $pgID = 0 )
    {
        $result = array();
        if( (int)$pgID < 1 ) {
            return $result;
        }
        //$query = $this->db->get_where('albums', array( 'photographer_id' => $pgID, "is_deleted" => "N"));
        $this->db->where('ALB.photographer_id', $pgID);
        $this->db->where('ALB.is_deleted', "N");
        $this->db->select('ALB.*, AUF.id as upload_file_id, AUF.album_id, AUF.upload_file_name, AUF.upload_file_size, AUF.uploaded_datetime');
        $this->db->from('albums ALB');
        $this->db->join('album_uploaded_files AUF', 'AUF.album_id = ALB.id', "left");
        $query = $this->db->get();
        return $query->result_array();
    }


}
