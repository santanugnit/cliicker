<?php

class Template_model extends CI_Model
{
    ##---------- Load Constructor ------------##
    function __construct()
    {
        parent::__construct();
    }  

    /**
     * Get total count for pagination.
     * @return mixed
     */
    function get_template_row_count()
    {
        $sql = 'SELECT * FROM album_themes'; /* WHERE user_type = 'EDITOR' */
        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }

    /**
     * GET ALL THE USER LIST AS ARRAY FORMAT.
     * @param null $num
     * @param null $offset
     * @return mixed
     */
    function get_template_rows_pagination( $num = NULL, $offset = NULL )
    {
        $sql = 'SELECT * FROM album_themes ORDER by id DESC LIMIT '. $offset .','. $num;
        /* WHERE user_type='EDITOR' AND account_activation_status!='D' */

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    

    /**
     * GET Expert Details.
     * @param $p_value_id
     * @return mixed
     */
    function get_template_details( $p_value_id)
    {
        $sql = 'SELECT p.*, a.id as albumID, a.album_code, a.creation_datetime, a.occasion_id, a.album_name 
                  FROM album_themes p
                  LEFT JOIN albums a ON a.photographer_id = p.id
                  WHERE p.id="'. $p_value_id .'"';

        $query = $this->db->query($sql);
        $result = $query->result_array();
        if( !empty($result) )
        {
            $pgRS   = array();
            foreach ($result as $pgArr )
            {
                $pgRS["id"]                 = $pgArr["id"];
                $pgRS["pg_clicker_code"]    = $pgArr["pg_clicker_code"];
                $pgRS["first_name"]         = $pgArr["first_name"];
                $pgRS["last_name"]          = $pgArr["last_name"];
                $pgRS["address"]            = $pgArr["address"];
                $pgRS["email"]              = $pgArr["email"];
                $pgRS["contact_number"]     = $pgArr["contact_number"];
                $pgRS["best_photos"]        = $pgArr["best_photos"];
                $pgRS["featured_image"]     = $pgArr["featured_image"];
                $pgRS["gender"]             = $pgArr["gender"];
                $pgRS["creation_datetime"]  = $pgArr["creation_datetime"];
                $pgRS["last_login_datetime"]   = $pgArr["last_login_datetime"];
                $pgRS["reg_number"]         = $pgArr["reg_number"];
                $pgRS["is_email_verified"]  = $pgArr["is_email_verified"];
                $pgRS["account_activation_status"]   = $pgArr["account_activation_status"];
                $pgRS["added_date"]         = $pgArr["added_date"];
                $pgRS["updated_date"]       = $pgArr["updated_date"];

                if( (int)$pgArr["albumID"] > 0 )
                {
                    $pgRS["album_dtls"][$pgArr["albumID"]]["albumID"]     = $pgArr["albumID"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["album_code"]  = $pgArr["album_code"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["occasion_id"] = $pgArr["occasion_id"];
                    $pgRS["album_dtls"][$pgArr["albumID"]]["album_name"]  = $pgArr["album_name"];
                }
            }
            return $pgRS;
        }

        return $result;
    }


    /**
     * get all values in array.
     * @param string $limit
     * @return mixed
     */
    function get_all_template( $limit = "")
    {
        $result = array();
        $sql = 'SELECT * FROM album_themes WHERE account_activation_status	= "V" ORDER BY id DESC';

        if ( $limit != "" ) {
            $sql .=" limit 0,".$limit;
        }
        $query = $this->db->query($sql);
        if (count($query->result_array()) )
        {
            foreach ( $query->result_array() as $val )
            {
                $result[$val["id"]] = $val;
            }
        }
        //return $query->result_array();
        return $result;
    }


    /**
     * @param array $data
     * @return array|bool
     */
    public function api_get_template_details_id( $data = array())
    {
        $result     = array();
        if( empty($data) || !isset($data["pgID"]) || (int)$data["pgID"] < 1 ) {
            return false;
        }
        $imgURL = base_url().'assets/uploaded_files/photographer_profile_image/medium/';

        $this->db->where("pg.id", $data["pgID"]);
        $this->db->select('pg.*, "'.$imgURL.'" as img_url, R.avg_rating, alb.no_albums');
        $this->db->from('photographers pg');
        $this->db->join('(SELECT ar.photographer_id, AVG(ar.rating) avg_rating FROM photographer_rating AS ar GROUP BY ar.photographer_id) as R ', 'pg.id = R.photographer_id', 'left');
        $this->db->join('(SELECT photographer_id, count(*) as no_albums FROM albums GROUP BY photographer_id) as alb', 'pg.id = alb.photographer_id', 'left');
        $query = $this->db->get();

        //$query = $this->db->get_where('photographers', array('id' => $data["pgID"]));
        $result = $query->row_array();
        return $result;
    }    


    public function api_get_template_lists( $data = array() )
    {
        $result = array();
        $imgURL = FCPATH.'assets/uploaded_files/templates_image/medium/';

        $cond = ' WHERE 1 = 1 ';
        
        $cond   .= ' AND AT.status = "Y"';

        $sql = 'SELECT AT.id, AT.occasion_id, AT.theme_type, AT.theme_name, AT.short_description, AT.theme_thumbnail_image, AT.status, AT.added_date, AT.updated_date, "'.$imgURL.'" AS img_url
            FROM  
                album_themes AT';

        $sql .= $cond;

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        
        return $result;
    }   


    public function api_get_template_filter_lists( $data = array() )
    {
        $result = array();
        $imgURL = FCPATH.'assets/uploaded_files/templates_image/medium/';
        $groupby    = ' GROUP BY ATCA.album_theme_id'; 
        $cond = ' WHERE 1 = 1 ';

        if( isset($data["s_name"]) && trim($data["s_name"]) != '' ){
            $cond   .= ' AND AT.theme_name LIKE "%'.$data["s_name"].'%"';
        }
        if( isset($data["community"]) && trim($data["community"]) != '' ){
            $cond   .= ' AND ATCA.album_theme_cat_id IN('.$data["community"].') ';
        }

        
        $cond   .= ' AND AT.status = "Y"';

        $sql = 'SELECT AT.id, AT.occasion_id, AT.theme_type, AT.theme_name, AT.short_description, AT.theme_thumbnail_image, AT.status, AT.added_date, AT.updated_date, GROUP_CONCAT(ATCA.album_theme_cat_id) as cat_ids,  "'.$imgURL.'" AS img_url
            FROM album_themes AT
            INNER JOIN album_theme_category_assoc ATCA ON ATCA.album_theme_id = AT.id 
            INNER JOIN album_theme_category ATC ON ATC.id = ATCA.album_theme_cat_id AND ATC.is_activated = "Y"';
        $sql .= $cond . $groupby;

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        /*return $this->db->last_query();*/        
        return $result;
    }

    function api_get_template_category_lists()
    {
        $this->db->select('abl_cat.id, abl_cat.cat_name, abl_cat.cat_parent, abl_cat.theme_count, abl_cat.short_description, abl_cat.is_activated, abl_cat.added_date, abl_cat.updated_date');
        $this->db->where('abl_cat.is_activated', 'Y');
        $query = $this->db->get('album_theme_category abl_cat');
        $result = $query->result_array();
        
        return $result;
    }

    function api_insert_photographer_template_suggest( $data = array() )
    {
        $insData = array(
            'name'          => $data['name'],
            'email'         => $data['email'],
            'contact_no'    => $data['contact_no'],
            'desc'          => $data['desc'],
            'pg_ids'            => $data['pg_ids'],
            'added_datetime'    => date("Y-m-d H:i:s"),
        );

        $this->db->insert('photographer_template_suggest', $insData);
        $id = $this->db->insert_id();
        return $id;
    }


    function api_do_create_notification( $data = array() )
    {
        /* CI transaction start with '$this->db->trans_begin()' */
        $this->db->trans_begin();

        $insData = array(
            'entity_type_id'    => $data['entity_type_id'],
            'entity_id'         => $data['entity_id'],
            'created_on'        => date("Y-m-d H:i:s"),
            'status'            => 0
        );
        $this->db->insert('notification_object', $insData);
        $notification_object_id = $this->db->insert_id();

        if( (int)$notification_object_id > 0 ) 
        {
            /* Insert to 'notification_change' table */
            $insData = array(
                'notification_object_id'    => $notification_object_id,
                'actor_id'              => $data['actor_id'],
            );
            $this->db->insert('notification_change', $insData);

            /* Insert to 'notification' table */
            if( isset($data['notifier_id']) && trim($data['notifier_id']) != '' )
            {
                $notifier_ids   = explode(',', $data['notifier_id']); 
                if( is_array($notifier_ids) && !empty($notifier_ids) )
                {
                    foreach( $notifier_ids as $notifier_id )
                    {
                        $insData = array(
                            'notification_object_id'    => $notification_object_id,
                            'notifier_id'           => $notifier_id,
                            'added_date'            => date("Y-m-d H:i:s"),
                            'notification_status'   => 'unread',
                        );
                        $this->db->insert('notification', $insData); 
                    }
                } 
                else 
                {
                    $this->db->trans_rollback();
                    return false;
                }                   
            } 
            else
            {
                $this->db->trans_rollback();
                return false;
            } 
        }

        if ( $this->db->trans_status() === FALSE )
        {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }





}
