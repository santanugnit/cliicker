<?php

class Static_pages_model extends CI_Model
{

    //###############################################################//
    //############### INITIALIZE CONSTRUCTOR CLASS ##################//
    //###############################################################//

    function  __construct()
    {
        parent::__construct();
    }

    //###############################################################//
    //############### INITIALIZE CONSTRUCTOR CLASS ##################//
    //###############################################################//



    //###############################################################//
    //##################### GET SORT ORDER NUMBER ###################//
    //###############################################################//

    function get_sort_order_no()
    {

        $sql="SELECT max(sort_order) max_no FROM `static_pages`";

        $rs=mysql_query($sql);
        if(mysql_num_rows($rs)>0)
        {
            $rec=mysql_fetch_object($rs);
            $orno=($rec->max_no) + 1;
        }
        else
        {
            $orno=1;
        }

        return $orno;

    }

    //###############################################################//
    //##################### GET SORT ORDER NUMBER ###################//
    //###############################################################//


    //###############################################################//
    //################ GET ALL ROW VALUE IN THE TABLE ###############//
    //###############################################################//

    function get_rows($id = 0)
    {
		$result = array();
		if ($id == 0) //all rows requested
		{
			$sql = "SELECT * FROM `static_pages` ORDER BY sort_order ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
		}
		else //single row requested
		{	
			$sql = "SELECT SP.id, SP.page_title, SP.menu_name, SP.page_content, 
					SP.is_small_content_available, SP.small_content,
					SP.parent_id, SP.is_active, SP.sort_order,
					SLG.slug, SLG.meta_title, SLG.meta_description, SLG.meta_keywords 
					FROM `static_pages` as SP LEFT JOIN `seo_contents` as SLG
					ON SP.id=SLG.item_id
					WHERE SLG.page_identifier='page' AND SP.id='".$id."'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			
		}
		//return $this->db->last_query();
		return $result;

    }

    //###############################################################//
    //################ GET ALL ROW VALUE IN THE TABLE ###############//
    //###############################################################//

    //##################################################################//
    //######################  VALIDATION FUNCTION  #####################//
    //##################################################################//

    function validate_form_data()
    {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('page_title', 'page title', 'required|xss_clean');

            if ($this->form_validation->run() == TRUE)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
    }


    //##################################################################//
    //######################  VALIDATION FUNCTION  #####################//
    //##################################################################//

    //#######################################################//
    //################## MODIFY FUNTION  ###################//
    //######################################################//

    function modify($pageid, $postdata)
	{
		//Update data from `static_pages table`
		$this->db->where('id',$pageid);
		$this->db->update('static_pages',$postdata['main']);
		
		//Update data from `seo_content_table` respect to page id
		$this->db->where('page_identifier','page');
		$this->db->where('id',$pageid);
		$this->db->update('seo_contents',$postdata['seo_data']);

		return "update_success";
    }

    //#######################################################//
    //################## MODIFY FUNTION  ###################//
    //######################################################//


    //#######################################################//
    //################ CHANGED SORT ORDER  ##################//
    //######################################################//

    function change_sort_order()
    {

        $rows=$this->get_rows();
        foreach ($rows as $row1)
        {
             $dt=$this->input->post($row1['id']);
             $postdata = array(
                                    'sort_order' => $dt
                                );

              $this->db->where('id',$row1['id']);
              $query = $this->db->update('static_pages',$postdata);

        }

        return "ok";

    }

    //#######################################################//
    //################ CHANGED SORT ORDER  ##################//
    //######################################################//
    
	
	##-------------------------------------------------------##
	##------------ Get page details respect to slug ---------##
	##-------------------------------------------------------##
    function get_row_data($page_slug)
    {	
		$sql="SELECT SP.*, SC.slug, SC.meta_title, SC.meta_keywords, SC.meta_description
			  FROM `static_pages` SP INNER JOIN `seo_contents` as SC
			  ON SP.id=SC.item_id WHERE SC.slug='".$page_slug."' AND page_identifier='page'";
			  
		$query=$this->db->query($sql);
		$result= $query->row_array();
		
		return $result;

    }
	
	
	##-------------------------------------------------------##
	##------------- Get n-level category array --------------##
	function get_nlevel_array()
	{
		$sql="SELECT * FROM `static_pages` WHERE is_active='Y' ORDER BY sort_order";
		$query = $this->db->query($sql);
		$arr = array();
		if($query->num_rows() > 0)
		{    
			foreach($query->result() as $row)
			{ 
				$arr[$row->id] = array(
											'category_name'  => $row->menu_name,
											'parent_id'      => $row->parent_id, 
											'sort_order'	 => $row->sort_order
									   );   
			}
		}

		 return $arr;
	}
	


}

?>
