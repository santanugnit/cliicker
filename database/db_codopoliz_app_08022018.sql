-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2018 at 08:26 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_codopoliz_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `portfolio_category_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `featured_image_file` varchar(255) NOT NULL,
  `case_study_file` varchar(255) NOT NULL,
  `live_url` varchar(500) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `created_datetime` datetime NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `portfolio_category_id`, `project_name`, `description`, `featured_image_file`, `case_study_file`, `live_url`, `status`, `created_datetime`, `updated_datetime`) VALUES
(1, 1, 'Jumparena: Indoor Trampoline Park UK', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br /><br /><br /><br /><br />\r\n<br /><br /><br /><br /><br /><br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1518013618971.png', 'abcdef', 'http://codopoliz.com', 'Y', '2018-02-07 15:08:31', '2018-02-07 15:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_categories`
--

CREATE TABLE `portfolio_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `featured_image_file` varchar(255) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `display_order` smallint(6) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio_categories`
--

INSERT INTO `portfolio_categories` (`id`, `cat_name`, `featured_image_file`, `status`, `display_order`, `created_datetime`, `updated_datetime`) VALUES
(1, 'Website Design & Development', '151798544271.png', 'Y', 1, '0000-00-00 00:00:00', '2018-02-07 11:02:06'),
(2, 'Mobile Design & Development', '1517984492173.png', 'Y', 3, '0000-00-00 00:00:00', '2018-02-07 07:21:32'),
(3, 'Creative & Graphics Artworks', '151798445917.png', 'Y', 2, '0000-00-00 00:00:00', '2018-02-07 12:44:18'),
(4, 'SEO & Digital Marketing', '1517984513868.png', 'Y', 4, '0000-00-00 00:00:00', '2018-02-07 07:21:53');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_categories_services_asso`
--

CREATE TABLE `portfolio_categories_services_asso` (
  `id` int(11) NOT NULL,
  `portfolio_category_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio_categories_services_asso`
--

INSERT INTO `portfolio_categories_services_asso` (`id`, `portfolio_category_id`, `service_id`) VALUES
(5, 1, 8),
(6, 1, 7),
(7, 1, 2),
(8, 1, 1),
(9, 1, 9),
(10, 1, 3),
(11, 1, 4),
(12, 1, 5),
(13, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `featured_image_file` varchar(255) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_datetime` datetime NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_name`, `featured_image_file`, `status`, `created_datetime`, `updated_datetime`) VALUES
(1, 'Custom Application Development', '1517989311915.png', 'Y', '2018-02-07 08:41:51', '0000-00-00 00:00:00'),
(2, 'Content Management System', '1517989718979.png', 'Y', '2018-02-07 08:48:38', '0000-00-00 00:00:00'),
(3, 'Large Scale eCommerce', '1517989801466.png', 'Y', '2018-02-07 08:50:01', '0000-00-00 00:00:00'),
(4, 'Online Bookink Application', '1517989829100.png', 'Y', '2018-02-07 08:50:29', '0000-00-00 00:00:00'),
(5, 'Social Network Development', '1517990152978.png', 'Y', '2018-02-07 08:55:52', '0000-00-00 00:00:00'),
(6, 'Wordpress Plugin Development', '1517990195658.png', 'Y', '2018-02-07 08:56:35', '0000-00-00 00:00:00'),
(7, 'API Development', '1517990216733.png', 'Y', '2018-02-07 08:56:56', '0000-00-00 00:00:00'),
(8, 'Affiliate Based Application', '1517990254197.png', 'Y', '2018-02-07 08:57:34', '0000-00-00 00:00:00'),
(9, 'Custom Relation Management & ERP', '1517990287741.png', 'Y', '2018-02-07 08:58:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `website_name` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `wesite_address` varchar(500) NOT NULL,
  `site_contact_email` varchar(255) NOT NULL,
  `site_contact_number` varchar(255) NOT NULL,
  `address_latitude` varchar(150) NOT NULL,
  `address_longitude` varchar(150) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `most_popular_places_ids` varchar(20) NOT NULL COMMENT 'Store State ids with comma separator'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `website_name`, `website_url`, `wesite_address`, `site_contact_email`, `site_contact_number`, `address_latitude`, `address_longitude`, `fb_link`, `twitter_link`, `most_popular_places_ids`) VALUES
(1, 'Codopoliz', 'http://www.codopoliz.com/demo/codopolizapp', 'Salt Lake, Sector - II, DL - 205, Kolkata - 700091', 'contact@codopoliz.com', '+91-33 4008 0601', '22.580756', '88.427220', 'http://facebook.com/CHAAKNA', 'http://facebook.com/CHAAKNA', '37,12,16,10');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `page_content` text CHARACTER SET utf8 NOT NULL,
  `small_content` text NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_active` char(1) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `page_title`, `menu_name`, `slug`, `page_content`, `small_content`, `meta_title`, `meta_description`, `meta_keywords`, `is_active`, `sort_order`) VALUES
(1, 'Our Services', 'Services', 'services', '<p>\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus pretium quam vel interdum. Duis viverra massa ut ligula cursus, non congue ligula dapibus. Vivamus placerat vitae est a rutrum. Quisque dui ante, sodales eu justo sed, sagittis pellentesque felis. Donec et arcu eu sem venenatis finibus non ut enim. Quisque placerat nibh felis, nec placerat augue feugiat non. Suspendisse accumsan pellentesque purus, eu commodo dui sagittis sit amet. Aenean tristique laoreet augue id venenatis. Proin egestas dictum ex, tempor vestibulum magna finibus eget. Maecenas et purus et metus posuere eleifend ac pretium eros. Integer at pretium nisi, nec rutrum nisl.</p>\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'Services', 'Services', 'Services', 'Y', 2),
(9, 'Welcome to 55newhomes - Find Your Perfect 55+ Active Community', 'Home', 'home', '<p>\n This content is coming from manage page section under \"Home\".</p>\n', '', 'Find Your Perfect 55+ Active Community', 'Find Your Perfect 55+ Active Community', '55+ Active Adult Community, Active Lilfestyle, Senior Lifestyle', 'Y', 1),
(6, 'About Us - 55newhomes.com', 'About Us', 'about-us', '<p>\n Home is where you make memories to cherish for a lifetime. It is where each corner offers a pause and where the walls embrace memories, life becomes a reason to celebrate. 55newhomes doesn’t have any fake listings. Our website offers endless site visits, unbiased reviews and information. Our vision is to deliver a trustworthy experience to active adult communities that they will cherish for a lifetime.</p>\n<p>\n Moving to a new home for senior citizens is a big deal. Retirement times are exciting time where adults have endless possibilities for entertainment, travel and new friendships. Buying a home at this age means you intend to stay for the rest of your life. With such a big investment come lots of issues and commitments. Thus it’s important to weigh all the possibilities first before moving forward. There are many real estate websites that will help you find a home, but few like <b>55newhomes</b> will devote a substantial amount of time to help you find your abode of heaven. After all, home is where the good things start.</p>\n<p>\n At 55newhomes you will get all types of information that include videos, photos, and listings of homes, descriptions and reviews. Our sophisticated but simple property search platform helps the consumers to research the market and find their home by choosing from thousands of property listings with market data. 55newhomes is one of the fastest growing websites with a huge database attracting millions of people. We provide all the information so that you find it easy and convenient to select your next home. At 55newhomes we don’t just help you find a new home, we help you find joy!</p>\n', 'Home is where you make memories to cherish for a lifetime. It is where each corner offers a pause and where the walls embrace memories, life becomes a reason to celebrate.', 'Who we are', 'Who we are', 'Who we are', 'Y', 1),
(5, 'Why Us', 'Why Us', 'why-us', '<p>\r\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'Why Us', 'Why Us', 'Why Us', 'Y', 1),
(10, 'Privacy Policy', 'Privacy Policy', 'privacy-policy', '<p>\n The use of the site 55newhomes.com by a visitor to the site, or of an 55newhomes.com account by an account holder (collectively referred to as “you”, “your”, “yourself”, “I”, or “me”) is subject to the following privacy policy that governs the collection and use of personally identifiable information about you that may be gathered by 55newhomes.com (collectively referred to herein as “we”, “our”, “us” or “55newhomes.com”). 55newhomes.com provides the web based computer application through which the content and functionality available on to you at the site or at your 55newhomes.com account is delivered, and which facilitates your consummation of a real estate transaction on terms acceptable to you. Your privacy on the internet is of the utmost importance to us. We want to make your use of the site, or any 55newhomes.com account, as defined below, both satisfying and safe.</p>\n<p>\n We gather certain types of information about site users, and 55newhomes.com account holders, you should fully understand the terms and conditions surrounding the capture and use of that information. This privacy policy discloses what information we gather and how we use it.</p>\n<h3>\n IMPORTANT INFORMATIONS COLLECTED AND TRACKED</h3>\n<p>\n In general, when you visit the site, you remain anonymous until such time as you decide to become a registered user by creating your 55newhomes.com account.</p>\n<p>\n <strong>* Voluntary Information</strong></p>\n<p>\n We may sometimes ask you to submit information about yourself, such as your name, address, telephone number, e-mail address and property search criteria. for example, we request identifying information about you when you register as a 55newhomes.com account holder, and we may do so when you use or manage your 55newhomes.com account to enable us to better understand your needs, adjust your 55newhomes.com account to your personal preferences, enhance your ability to search properties on your 55newhomes.com account, enable you to participate in any message boards, and send you e-mail and postal mail newsletters and announcements, and for other similar business purposes.</p>\n<p>\n We give you the opportunity, however, to elect not to receive further materials from us. We also will share your personal information and information about your use of your 55newhomes.com account with any real estate agent or broker with whom you choose to collaborate through your 55newhomes.com account. We offer 55newhomes.com account holders an opportunity from time to time to be referred to third parties that provide products and services about which you may be interested. If you affirmatively indicate to us your consent to be referred to such a third party, we will provide that third party with your personal contact information to enable that third party to communicate with you about their products and services in which you may be interested. Any commercial transactions in which you engage with such a third party are at your own risk. 55newhomes.com disclaims any liability to you for the results of such transactions, or for the conduct of such third parties in delivering their products or services to you.</p>\n<p>\n <strong>* Cookies</strong></p>\n<p>\n We, or third party service providers engaged by us, may place text files called “cookies” in the browser files of your computer to track usage of the site, your 55newhomes.com account, or web pages within those sites. The cookies themselves do not contain personal information about you, although they will enable us to relate your use of the site or your 55newhomes.com account to information that you have specifically and knowingly provided to us. The only personal information cookies can contain is information you supply yourself.</p>\n<p>\n Cookies cannot read data on your hard disk or read cookie files created by other sites. We use cookies to track user traffic patterns. We may also use cookies when you register for your 55newhomes.com account. In these situations, a cookie will store useful information that enables the site or your 55newhomes.com account to remember you when you return to the site or to your 55newhomes.com account. You can refuse cookies by turning them off in your browser. If you have set your browser to warn you before accepting cookies, you will receive the warning message with each cookie. Please refer to your browser instructions for information regarding erasing, blocking, and receiving warnings for cookies. You should understand, however, that if you prevent us from placing cookies on your computer, your use of the site and your 55newhomes.com account may be less efficient.</p>\n<p>\n <strong>* Tracking</strong></p>\n<p>\n We collect tracking information and monitor traffic patterns throughout the site; however, we do not correlate this information with data about individual users unless you have registered as a 55newhomes.com account holder. We do break down overall usage statistics according to a user’s domain name, browser type, and internet protocol address by reading this information from the browser string (information contained in every internet user’s browser). This information allows us to better tailor our content to site users’ and to our account holders’ needs and to help our advertisers and sponsors better understand the demographics of our site users and account holders.</p>\n<p>\n <strong>* Other details</strong></p>\n<p>\n We may collect data from or about you in other ways not specifically described in this policy. For example, you might contact us with a question. We maintain such data like other business records. all opinions, ideas, suggestions, and other feedback submitted by you through the site, or through your 55newhomes.com account may be used by us, without any restriction and free of charge, including your name as the source of such content, and nothing contained in this policy shall limit or otherwise affect 55newhomes.com rights in this regard. We may also collect non-personally identifiable information such as the type of internet browser or the type of computer operating system you are using, and the domain name of the website from which you linked into the site, your 55newhomes.com account, or an advertisement appearing on such sites.</p>\n<p>\n Please note that we will definitely cooperate with any court order or other legal process and will provide your personal information to the extent required in any legal proceeding.</p>\n<h3>\n ALL YOUR INFORMATION ARE PROTECTED</h3>\n<p>\n As mentioned above, we gather and use personal information about you when you register as a 55newhomes.com account holder and we track your usage and management of your 55newhomes.com account and use of the site, to better understand your needs, adjust your 55newhomes.com account to your personal preferences, enable you to search properties the site, or in your 55newhomes.com account, enable you to participate in any message boards, and send you e-mail and postal mail newsletters and announcements, and for other similar business purposes.</p>\n<p>\n We also permit any real estate agent or broker with whom you may be collaborating to view this information to enable the agent or broker to better serve your real estate needs and expectations. We do not in the regular course of our business disclose or sell mailing lists containing personally identifiable information about you to any outside parties. We will not share your personal information with any outside parties without your permission, such as when you authorize us to refer you to a third party provider of products and services in which you may be interested in acquiring or using, unless such disclosure is necessary to comply with applicable law or valid legal process or to protect the personal safety or property of our users, employees, independent contractors or the public.</p>\n<p>\n We may use third party contractors who can access our databases for the purpose of conducting repairs or upgrades to our computer system. We may also use third party service providers and give access to your personal information as part of the services they provide to us. We hold all such third parties to the same privacy standards to which we hold ourselves. We require these third parties to use and disclose your personal information only as necessary to perform their services for us.</p>\n<h4>\n ⇒ NOT UNDER THE AGE OF 13</h4>\n<p>\n Consistent with the Federal Children’s Online Privacy Protection Act (COPPA), we will never knowingly request personally identifiable information from anyone under the age of thirteen (13) years. If we discover that a child under the age of thirteen (13) has provided us with any personally identifiable information, we will delete that information from our systems</p>\n<h4>\n ⇒ SECURITY AND PRIVACY POLICIES ARE PERIODICALLY REVIEWED</h4>\n<p>\n We operate secure data networks protected by industry standard security protocols, such as (if and where appropriate) firewalls, encryption, and password protection systems. Our security and privacy policies are periodically reviewed and enhanced as necessary, and only authorized individuals have access to the information provided by our users.</p>\n<h4>\n ⇒ UNSUBSCRIBE OPTION</h4>\n<p>\n You may unsubscribe or opt-out of receiving electronic correspondence from us by either logging into your 55newhomes.com account to modify your preferences, following any instructions in such electronic correspondence on how to unsubscribe or opt-out from any further electronic communications from us, or notifying us at the contact information displayed on the site. The foregoing does not opt you out of receiving e-mail correspondence about your 55newhomes.com account, or prevent us from enabling the real estate agent or broker with whom you may be collaborating through your 55newhomes.com account to view your activity within your account.</p>\n<h4>\n ⇒ OTHER WEBSITES LINKS ARE NOT UNDER OUR CONTROL</h4>\n<p>\n Your 55newhomes.com account may contain links to other websites that are not under our control. We are not responsible for the content or privacy practices of those sites, and we provide the links solely for the convenience and information of our users and account holders.</p>\n<h4>\n ⇒ UPDATES</h4>\n<p>\n By using your 55newhomes.com account or the site, you consent to the collection and use by us of the information about you in the manner described in this privacy policy. If we decide to change our privacy policy, we will post those changes on this page so you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>\n<h4>\n CONTACT US</h4>\n<p>\n If you have any additional questions or concerns about this Privacy Policy, please contact <a href=\"contact-us\"><strong>55newhomes.com</strong></a></p>\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', 'Y', 4),
(12, 'Terms of Use', 'Terms of Use', 'terms-of-use', '<p>\n By accessing the website 55newhomes.com, you agree to be bound by this agreement. you are referred to in this agreement as “you”,” I”, or “me.” subject to your compliance with the terms, conditions and notices contained in this agreement, you are authorized to access the website and the content and functionality accessible to you on the website. The website is an online application that provides you with access to real estate listing information, and other real estate services, content and functionality to assist you in the purchase, lease or sale of residential real estate. By accessing the website, you make the following agreements, representations and warranties:</p>\n<ol>\n <li>\n  I represent and warrant that I am over the age of majority and have a bona fide interest in the purchase, sale or lease of residential real estate of the type displayed on the website or accessible to me. I represent and warrant that I will use the website only for lawful purposes and will not use my website to engage in any conduct that is unlawful or prohibited by this agreement. I understand that 55newhomes.com may discontinue, change, or update the information and content on the website without notice to me</li>\n <li>\n  I acknowledge and agree that the website may contain advertisements and hyperlinks to third party websites that are not under the control of 55newhomes.com, and that 55newhomes.com is not responsible for any content in any advertisement or hyperlink on the website or for any content in any hyperlinked website. I acknowledge that if I access a third party website from the website, I do so at my own risk. I understand that a hyperlink to a third party website does not imply that 55newhomes.com endorses the content on, or the business of, the hyperlinked website unless the webpage containing the hyperlink otherwise provides. I understand that I am solely responsible for determining the integrity and reliability of the information in any advertisement or hyperlink on the website, as well as the information on the hyperlinked website. I understand that my dealings with or participation in promotions of advertisers found on the website, including payment for and delivery of goods and services, and any other terms (such as warranties) are solely between me and such advertisers. I agree that 55newhomes.com shall not be responsible for any loss or damage of any sort relating to my dealings with such third parties.</li>\n <li>\n  I understand and agree that the website may include information, tips, facts, views and opinions that 55newhomes.com deem worthy of publication, including 55newhomes.com newsletters to which I may subscribe by providing 55newhomes.com with my contact information to which the newsletters can be sent. all such material is for information purposes only and is not professional advice upon which I or any third party should rely in making any decision concerning the acquisition or sale of real property, or any personal, legal, business, financial, or other decisions understand that accessing the website does not impose a financial obligation on me or create any representation or fiduciary relationship between me and 55newhomes.com or any real estate agent or broker whose contact information may be displayed on the website. I acknowledge that any real estate brokerage services that may be rendered to me by reason of my use of the functionality available to me at the website are being rendered to me solely by a licensed real estate broker or agent I chose to engage for such purposes, and not by 55newhomes.com, or by any other party, whether or not the names or logos of such parties may appear on the website.</li>\n <li>\n  I understand and agree that my activity on the website may be viewed by 55newhomes.com, as well as by any real estate broker or agent I authorize to act on my behalf, including, without limitation, when I access the website, what I viewed while visiting the website, any information that I post, submit or transmit while visiting the website (collectively “website activity”); provided, however, that if I post any “private notes”, such content shall not be accessible to any broker or agent. If I am collaborating with a real estate broker or agent through the website, that broker or agent my reassign me to another real estate licensee affiliated with that broker without prior notice to me or my consent. The real estate broker or agent may also transmit or share my website activity with other licensed agents affiliated with the broker without prior notice to me or securing my permission. Subject to the privacy policy applicable to this site, the real estate broker with whom I have elected to collaborate or 55newhomes.com may send me electronic correspondence, including, without limitation, marketing information and information relating to my use of the website.</li>\n <li>\n  Subject to the ability to unsubscribe/opt out in accordance with the 55newhomes.com privacy policy, i hereby grant to 55newhomes.com my express written consent to engage in unsolicited communications with me for a period of 12 months from the last day i access the website. such unsolicited communications may take place by email at the email address I provided to you or at any alternative email address that i may provide in the future understand that 55newhomes.com may limit the number of properties displayed to me in response to my search requests based upon a determination of the reasonable number of properties appropriate to be displayed in response to a single search request, given existing market conditions, technology limitations, total number of matching listings in an MLS database compilation, and other \"reasonable use\" restrictions deemed appropriate by 55newhomes.com, which total number may vary from market to market and from time to time. I further understand and agree that 55newhomes.com may limit information about properties displayed to me at the website and that the information about real properties for sale or rent originates from third party licensors or real estate multiple listing services (MLSs).</li>\n <li>\n  I agree that, as between 55newhomes.com and myself, any electronic or printed real estate listing data, or other content derived from the website, including any trademarks or service marks of any real estate broker, agent, 55newhomes.com or third party advertisers or service providers (\"site materials\" or “content”), constitutes the copyrighted and proprietary property of the real estate broker, agent, 55newhomes.com, or third parties from whom the broker, agent or 55newhomes.com has secured permission to display such site materials. I acknowledge and agree that the MLS or other third party licensor that supplies the real estate data and images displayed to me at the website owns all copyrights in and to such data and images, and that I will not contest the validity of such copyright or ownership claims, nor assist others in doing so.</li>\n <li>\n  I acknowledge that 55newhomes.com has granted to me only for as long as this agreement is in effect a limited, non-exclusive license to use the site materials solely for my personal and non-commercial use in conjunction with my bona fide potential purchase or sale of real estate of the type available through the website. I will not copy, re-distribute, re-transmit, commercialize, sell, license, re-license, sub-license, or otherwise make the site materials available to any third party, use such site materials in any manner that competes with the business of 55newhomes.com or any third party that supplies the real estate data and images accessible to me at the website, nor will I assist others in doing so; provided that I may share information about individual properties that I may wish to purchase or rent with my spouse, children, other family members, loan officer, accountant, attorney or tax advisor.</li>\n <li>\n  I will use my best efforts to cooperate with 55newhomes.com or its third party content licensors on reasonable terms in the event such persons or entities deem it necessary to seek to enjoin or otherwise prohibit the unauthorized or infringing use by a third party of the site materials by reason of my use of the website. I hereby grant 55newhomes.com a non-exclusive, royalty-free, perpetual, worldwide license to copy, distribute, re-license, transmit, publicly display, publicly perform, reproduce, edit, translate, sublicense and reformat any materials or content that I submit or contribute through the website, or transmit to a real estate broker, agent or 55newhomes.com through my 55newhomes.com account, including feedback, comments, and suggestions for improvement to the content or functionality of the website (“submissions”), and to use my name in connection with my submissions in their discretion, provided that I may revoke my permission to use my name in connection with my submissions by written notice to 55newhomes.com. 55newhomes.com is not obligated to publish or use any of my submissions, and may remove such submissions from display on the website, or on any other web site at any time without notice to me, and without my consent. I represent and warrant that any submissions made by me do not infringe upon the intellectual property rights of any third party. I understand and agree that 55newhomes.com does not assume any responsibility or liability for any material or content that I submit to or through the website, or directly to real estate agent, broker or 55newhomes.com. I understand that no compensation shall be paid or owed to me with respect to the use of my submissions as licensed above.</li>\n <li>\n  I understand that I may not post to the website or transmit to a real estate broker, agent, or 55newhomes.com any “prohibited submissions”, which shall include, but are not limited to, submissions that are deemed by 55newhomes.com in its sole discretion to be patently offensive, or promote racism, bigotry, hatred or physical harm of any kind against any group, individual, or property; harass or advocate harassment of another person; exploit people in a sexual or violent manner; contain violence or offensive subject matter or contain a link to an adult website; solicit personal information from anyone under the age of eighteen (18); promote or disseminate information that I know is false or misleading or promote illegal activities or conduct that are abusive, threatening, obscene, defamatory or libellous; promote or disseminate an illegal or unauthorized copy of another person’s copyrighted work; involve the transmission of “junk mail,” “chain letters,” or unsolicited mass mailing, instant messaging, “spamming,” or “spamming”; further or promote any criminal activity or enterprise or provide instructional information about illegal activities, violating someone’s privacy, or providing or creating computer viruses; solicit passwords or personal identifying information for commercial or unlawful purposes from other users; or include a photograph of another person that is posted without that person’s consent. 55newhomes.com reserves the right, in its sole discretion, to investigate and take appropriate legal action against anyone who it has reason to believe has violated this provision, including without limitation, removing the offending submissions from the website, or other websites hosted or maintained by 55newhomes.com on which they appear, and terminating the access of such persons to the website.</li>\n <li>\n  55newhomes.com does not assume any responsibility for, and expressly disclaims, any and all liability for any of your submissions. 55newhomes.com does not assume any responsibility for monitoring submissions and, if at any time 55newhomes.com chooses, in its sole discretion, to monitor submissions, 55newhomes.com nonetheless does not assume any responsibility for the submissions, or any obligation to modify or remove any inappropriate, inaccurate or prohibited submission, or any responsibility for your or any other 55newhomes.com website visitor’s conduct in submitting any submission. Furthermore, 55newhomes.com does not make any warranties, express or implied, concerning your ability to rely upon any submissions submitted by other website visitors or others, or any submissions that you transmit to other website visitors or other parties.</li>\n <li>\n  55newhomes.com uses good faith efforts to provide accurate, complete, and current information on the website, but I understand that it is my responsibility to verify any information provided on or through the website. 55newhomes.com does not guaranty or warrant that the content on the website is accurate, complete, timely, or free of technical or typographical errors. I expressly acknowledge and agree that my use of the content displayed on the website is at my sole risk. 55newhomes.com provides the content on an “as is” and “as available” basis. 55newhomes.com expressly disclaims all warranties of any kind, whether express or implied, concerning the website or property, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose, and non-infringement. 55newhomes.com does not make any warranty that the content available to me on the website or any other 55newhomes.com property will meet my requirements, or that access to the website and the content thereon will be uninterrupted, timely, secure, accurate, and virus-free or error free. 55newhomes.com does not make any warranty about the results that may be obtained from the use of information, products or services provided on or through the website or any other 55newhomes.com property, or about the accuracy or reliability of any content on, or accessible through, any websites or properties. 55newhomes.com does not make any warranties regarding any information obtained by you, or anyone acting on your behalf, from any hyperlinked third party site.</li>\n <li>\n  I agree that neither 55newhomes.com, nor any party involved in creating, producing, or displaying the website shall be responsible or liable for any damages whatsoever, including direct, incidental, consequential, or indirect damages, arising out of my access to, use of, or inability to use, the website, any other 55newhomes.com products, services, or content offered or provided on the website, any other hyperlinked website, or any errors or omissions in the content or functionality thereof. 55newhomes.com shall not have any responsibility or liability for any person’s reliance on any information or content provided on or through the website, whether or not the information is correct, current, or complete, or the consequences of any action I or any other person may take or fail to take based on content provided by or as a result of the use of the website. I specifically agree that 55newhomes.com is not liable for any conduct by me associated with the website. 55newhomes.com is not responsible for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, computer equipment, software, or failure of any email due to technical problems or traffic congestion on the internet or on the website, including any injury or damage to my, or any other person’s, computer that relates to or results from use of the website or any other 55newhomes.com products or services. In no event shall 55newhomes.com, its officers, directors, employees, accountants, attorneys, vendors, independent contractors, or agents be liable for any damages whatsoever, including indirect, incidental, special, or consequential damages, resulting from my use or my inability to use the website, or from any information, products or services purchased, obtained, or accessed from the website, or from any messages received or transmitted from or through my 55newhomes.com account or the website, or transactions entered into through the website or resulting from unauthorized access to or alteration of transmissions or data, including but not limited to, damages for loss of profits, use, data, or other intangible property, whether based on contract, tort, strict liability or otherwise, even if 55newhomes.com has been advised of the possibility of such damages. I understand some jurisdictions do not allow the limitation or exclusion of liability for incidental or consequential damages, so some of the above limitations may not apply to me.</li>\n <li>\n  I agree to defend, indemnify, and hold harmless 55newhomes.com and any of its affiliates, officers, subsidiaries, parents, partners, directors, employees, agents or independent contractors from any damages, judgments, costs, and expenses, including attorney’s fees and litigation costs and expenses at trial or on appeal, arising from any claim by a third party based upon my use of the Website or any Submissions I transmit to the Website, or to 55.newhomes.com , or based on any breach by me of my duties, obligations, representations or warranties set forth in this Agreement.</li>\n <li>\n  i agree that my unauthorized use of the site materials will cause injury to 55newhomes.com or one or more of its affiliates or independent contractors, that cannot adequately be remedied by money damages, and that 55newhomes.com, its respective affiliates, or any one of them shall be entitled to preliminary or permanent injunctive relief to enjoin my use of the website, or the site materials, which injunctive relief shall be in addition to any other remedies available to such parties at law or equity. If such party is successful in securing a preliminary injunction order against me, I waive any obligation of such party to post a security bond in conjunction therewith.</li>\n <li>\n  I understand that this agreement does not impose a financial obligation on me. I understand that any licensed real estate broker or agent have not entered into any form of fiduciary or brokerage relationship with me solely by granting me access to, and limited use of, the site materials available on the website. any form of fiduciary or brokerage relationship between me and a real estate broker or agent shall not exist unless and until the broker or agent shows me property available for sale or lease, or I request the agent to assist me in procuring a real estate sales or lease contract on terms acceptable to me.</li>\n <li>\n  I understand and agree that 55newhomes.com, in its sole discretion, may terminate this agreement, and any of my corresponding subscriptions, delete any content stored on the website, direct me to cease accessing the website, and discontinue or restrict my access to the website, all without notice to me and for any reason. I agree that 55newhomes.com shall not be liable to me or to any third party for any modification, suspension, or discontinuance of my access to the website, or any parts thereof. Upon termination of this agreement, 55newhomes.com shall not have any further obligation to me, including any duty to return or destroy any information I may have supplied to 55newhomes.com during the term of this agreement.</li>\n <li>\n  I understand and agree that I 55newhomes.com has entered into referral agreements with third party providers of products and services, including but not limited to, in-home care services, for which 55newhomes.com receives compensation from the third party providers if visitors to the 55newhomes.com website contact such third parties about the possibility of purchasing their products or services. If I complete a form accessible on the website indicating my interest in receiving more information about such third parties’ products and services, I further authorize 55newhomes.com to provide such service providers with my, or my family members’, name, phone number, zip code, email address, and such other information about me or my family members as may be requested by the product or service providers. I certify to 55newhomes.com that such contact information I provide to 55newhomes.com for this purpose is true and correct. I further understand and agree that representatives of such product or service provider(s) can and will contact me or my family member(s) about the specific products or services they offer, and the terms and conditions under which they will provide such products and services to me or my family member(s).</li>\n <li>\n  In the event that any provision of this Agreement is, becomes, or is declared by a court of competent jurisdiction to be illegal, unenforceable, or void, this Agreement shall continue in full force and effect without such provision. The failure of 55newhomes.com at any time or times to require performance of any provision hereof shall in not in any manner affect the right of 55newhomes.com at a later time to enforce the same.</li>\n <li>\n  I acknowledge that 55newhomes.com use of any information I supply through my 55newhomes.com account is subject to the privacy policy located on the website. This agreement may not be assigned by me by contract, operation of law, or otherwise. I understand and agree that 55newhomes.com may assign this agreement, or otherwise transfer their rights and obligations under this agreement at any time without my consent or notice to me.</li>\n <li>\n  I agree that 55newhomes.com may amend or modify this agreement at any time without notice to me or my consent, and make such amended versions of this agreement accessible at the website. My use of the website after 55newhomes.com has amended or modified this agreement shall be deemed my acceptance of such amendments or modifications. If I do not want to be bound by an amendment or modification of this agreement, my sole remedy is to terminate this agreement by written or email notice to 55newhomes.com and to refrain immediately from accessing the website upon transmittal of such notice.</li>\n</ol>\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'Terms of Use', 'Terms of Use', 'Terms of Use', 'Y', 1),
(14, 'FAQ', 'FAQ', 'faq', '<p>\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus pretium quam vel interdum. Duis viverra massa ut ligula cursus, non congue ligula dapibus. Vivamus placerat vitae est a rutrum. Quisque dui ante, sodales eu justo sed, sagittis pellentesque felis. Donec et arcu eu sem venenatis finibus non ut enim. Quisque placerat nibh felis, nec placerat augue feugiat non. Suspendisse accumsan pellentesque purus, eu commodo dui sagittis sit amet. Aenean tristique laoreet augue id venenatis. Proin egestas dictum ex, tempor vestibulum magna finibus eget. Maecenas et purus et metus posuere eleifend ac pretium eros. Integer at pretium nisi, nec rutrum nisl.</p>\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'FAQ', 'FAQ', 'FAQ', 'Y', 1),
(15, 'Contact Us', 'Contact Us', 'contact-us', '<p>\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus pretium quam vel interdum. Duis viverra massa ut ligula cursus, non congue ligula dapibus. Vivamus placerat vitae est a rutrum. Quisque dui ante, sodales eu justo sed, sagittis pellentesque felis. Donec et arcu eu sem venenatis finibus non ut enim. Quisque placerat nibh felis, nec placerat augue feugiat non. Suspendisse accumsan pellentesque purus, eu commodo dui sagittis sit amet. Aenean tristique laoreet augue id venenatis. Proin egestas dictum ex, tempor vestibulum magna finibus eget. Maecenas et purus et metus posuere eleifend ac pretium eros. Integer at pretium nisi, nec rutrum nisl.</p>\n', 'Whether you\'re looking to sell, or let your home or want to buy or rent a home, we really are the people for you to come to.', 'Contact Us', 'Contact Us', 'Contact Us', 'Y', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_type` enum('SUPERADMINISTRATOR','EDITOR') NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `password` text NOT NULL,
  `password_text` varchar(30) NOT NULL,
  `last_profile_update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_datetime` datetime NOT NULL,
  `last_logout_datetime` datetime NOT NULL,
  `login_status` tinyint(1) NOT NULL,
  `account_activation_status` enum('A','IA','B','D') NOT NULL COMMENT '[A=Active, IA=Inactive, B=Blocked, D=Deleted]'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_type`, `first_name`, `last_name`, `email`, `contact_email`, `address`, `country`, `state`, `city`, `zip`, `phone`, `profile_image`, `creation_date`, `password`, `password_text`, `last_profile_update_datetime`, `last_login_datetime`, `last_logout_datetime`, `login_status`, `account_activation_status`) VALUES
(1, 'SUPERADMINISTRATOR', 'Priyam', 'Ghosh', 'codopoliz.solutions@gmail.com', 'priyam.g.codopoliz@gmail.com', 'Salt Lake, Sector - II, DL - 205, Kolkata - 700091', 'India', '', '', '445555', '+91-33 4008 0601', '1516361330295.jpg', '2015-05-11 00:00:00', 'aaaf6fea4354900ac7ec4c8caa0235b5', 'nopass123', '2017-09-07 18:03:19', '2018-02-08 06:37:43', '2018-02-07 16:03:02', 1, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_categories_services_asso`
--
ALTER TABLE `portfolio_categories_services_asso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `portfolio_categories_services_asso`
--
ALTER TABLE `portfolio_categories_services_asso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
